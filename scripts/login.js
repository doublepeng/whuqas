$(document).ready
(
	function()
	{
		var code; //在全局定义验证码   
		createCode();
		
		$("#password").focus
		(
		 	function() 
			{
				$("#label_p").hide();
			}
		);
		
		$("#password").blur
		(
		 	function() 
			{
				if($("#password").val() == "")
					$("#label_p").show();
				if($("#username").val() == "" && $("#password").val() == "")
					$("#confirm").text("");
			}
		);
		
		$("#username").focus
		(
		 	function() 
			{
				$("#label_u").hide();
			}
		);
		
		$("#username").blur
		(
		 	function() 
			{
				if($("#username").val() == "")
					$("#label_u").show();
				if($("#username").val() == "" && $("#password").val() == "")
					$("#confirm").text("");
			}
		);
		
		$("#check").focus
		(
		 	function() 
			{
				$("#label_c").hide();
			}
		);
		
		$("#check").blur
		(
		 	function() 
			{
				if($("#check").val() == "")
					$("#label_c").show();
			}
		);

		$("#loginform").submit
		( 
			function()
			{
				login();
				return false;
			}
		);
	
	}
);
 
function createCode()   
{    
	code = "";   
	var codeLength = 4;//验证码的长度   
	var checkCode = document.getElementById("checkCode");  //$("#checkCode");   
	var selectChar = new Array(0,1,2,3,4,5,6,7,8,9,'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
           
	for(var i=0;i<codeLength;i++)   
	{             
		var charIndex = Math.floor(Math.random()*36);   
		code +=selectChar[charIndex];            
	}     
	if(checkCode)   
	{   
		checkCode.className="txt";   
        checkCode.value = code;  
	}           
}   
        
function validate()   
{   
	var inputCode = $("#check").val();  
	var temp= inputCode.toUpperCase();
	if(temp.length <=0)   
	{   
		$("#confirm").text("请输入验证码");
		$("#check").focus();
		return false; 
	}   
	else if(temp != code )   
	{   
		$("#confirm").text("验证码输入错误");
		createCode();//刷新验证码  
		$("#check").val("");
		$("#check").focus();
		return false;
	}   
	else   
	{
		//alert('ok');
		return true;   
	}   
}   
 
function login()
{
	var user = $("#username").val();
	var pass = $("#password").val();
	var type = $("input[name='choose']:checked").val();  /*student teacher manager*/

	if (user == "")
	{
		$("#confirm").text("请输入登录账号");
		$("#username").focus();
		return false;
	}

	if(pass == "") 
	{
		$("#confirm").text("请输入登录密码");
		$("#password").focus();
		return false;
	}

	//2011-10-18 modified by minzhenyu 
	if(!validate())
	{
		return false;
	}
	
	$("#confirm").val("");
	 
	 var loginPara = {"sid": user,"pwd": $.md5(pass),"type": type};

	$.getJSON(
		"./testdrive/index.php?r=site/login", //../frame/student/interface/loginPara.php
		{"loginPara": $.toJSON(loginPara)},	
		function(ret){
			createCode();//刷新验证码  
			if(ret.success == true)
			{
				var sidInfo = Array(ret.results.name, user);
				$.cookie('csidInfo', sidInfo, { expires: 7, path: '/' });	
				if(type == "student")
				{
					if(ret.results.registered == false)
					{
						window.location.href = "frame/s_register.html";
						return true;
					}
					else
					{
						//用来给学生添加grade记录的
						$.ajax({
							url : "../testdrive/index.php?r=zing/addStuGrade",
							async : false,
							type : "POST",
							success : function (result){
								var para = $.evalJSON(result);
								if(para.success == false)
								{
									alert('服务器返回错误信息=>"'+para.message+'"');
								}
							}
						});
					}
				}
				
				window.location.href = "frame/main.html";
				
				return true;
			}
			else
			{
				alert('服务器端返回错误信息=>"'+ret.message+'"');
				return false;
			}
		}
	);
}
