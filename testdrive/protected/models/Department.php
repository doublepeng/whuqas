<?php

/**
 * This is the model class for table "department".
 *
 * The followings are the available columns in table 'department':
 * @property integer $departmentid
 * @property string $departmentname
 * @property string $remark1
 * @property string $remark2
 * @property double $F1
 * @property double $F2
 * @property double $F3
 */
class Department extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Department the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'department';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('F1, F2, F3', 'numerical'),
			array('departmentname', 'length', 'max'=>60),
			array('remark1, remark2', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('departmentid, departmentname, remark1, remark2, F1, F2, F3', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'departmentid' => 'Departmentid',
			'departmentname' => 'Departmentname',
			'remark1' => 'Remark1',
			'remark2' => 'Remark2',
			'F1' => 'F1',
			'F2' => 'F2',
			'F3' => 'F3',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('departmentid',$this->departmentid);
		$criteria->compare('departmentname',$this->departmentname,true);
		$criteria->compare('remark1',$this->remark1,true);
		$criteria->compare('remark2',$this->remark2,true);
		$criteria->compare('F1',$this->F1);
		$criteria->compare('F2',$this->F2);
		$criteria->compare('F3',$this->F3);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}