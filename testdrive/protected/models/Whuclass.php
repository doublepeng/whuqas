<?php

/**
 * This is the model class for table "whuclass".
 *
 * The followings are the available columns in table 'whuclass':
 * @property integer $classid
 * @property string $classname
 * @property string $grade
 * @property integer $majorid
 * @property integer $t_classid
 * @property integer $gradecheck
 */
class Whuclass extends CActiveRecord
{
	public static $err = "输入的班级名称已经存在，请重新输入！";
	/**
	 * Returns the static model of the specified AR class.
	 * @return Whuclass the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'whuclass';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('classname, grade, majorid', 'required'),
			array('majorid, t_classid, gradecheck', 'numerical', 'integerOnly'=>true),
			array('classname', 'length', 'max'=>50),
			array('grade', 'length', 'max'=>5),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('classid, classname, grade, majorid, t_classid, gradecheck', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'classid' => 'Classid',
			'classname' => 'Classname',
			'grade' => 'Grade',
			'majorid' => 'Majorid',
			't_classid' => 'T Classid',
			'gradecheck' => 'Gradecheck',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('classid',$this->classid);
		$criteria->compare('classname',$this->classname,true);
		$criteria->compare('grade',$this->grade,true);
		$criteria->compare('majorid',$this->majorid);
		$criteria->compare('t_classid',$this->t_classid);
		$criteria->compare('gradecheck',$this->gradecheck);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function exist($name, $grade, $majorid)
	{
		$results = Whuclass::model()->findAll("classname='$name' and grade='$grade' and majorid='$majorid'");
		if(empty($results))
			return false;
		else return true;
	}
}
