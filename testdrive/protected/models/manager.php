<?php

/**
 * This is the model class for table "manager".
 *
 * The followings are the available columns in table 'manager':
 * @property integer $managerid
 * @property string $managername
 * @property string $departmentname
 * @property string $password
 * @property string $remark1
 * @property string $remark2
 */
class manager extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return manager the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'manager';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('managername, departmentname, password', 'required'),
			array('managername, password', 'length', 'max'=>20),
			array('departmentname', 'length', 'max'=>60),
			array('remark1, remark2', 'length', 'max'=>25),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('managerid, managername, departmentname, password, remark1, remark2', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'checklists' => array(self::HAS_MANY, 'Checklist', 'managerid'),
			'departmentname0' => array(self::BELONGS_TO, 'Department', 'departmentname'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'managerid' => 'Managerid',
			'managername' => 'Managername',
			'departmentname' => 'Departmentname',
			'password' => 'Password',
			'remark1' => 'Remark1',
			'remark2' => 'Remark2',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('managerid',$this->managerid);

		$criteria->compare('managername',$this->managername,true);

		$criteria->compare('departmentname',$this->departmentname,true);

		$criteria->compare('password',$this->password,true);

		$criteria->compare('remark1',$this->remark1,true);

		$criteria->compare('remark2',$this->remark2,true);

		return new CActiveDataProvider('manager', array(
			'criteria'=>$criteria,
		));
	}
	public function  validatePassword($password)
	{
		return $this->password == $password;
	}
}