<?php

/**
 * This is the model class for table "t_class".
 *
 * The followings are the available columns in table 't_class':
 * @property integer $t_classid
 * @property string $name
 * @property string $teacherid
 * @property string $year
 * @property string $F1
 * @property string $F2
 * @property string $F3
 */
class TClass extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return TClass the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 't_class';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'length', 'max'=>40),
			array('teacherid', 'length', 'max'=>20),
			array('year, F1, F2, F3', 'length', 'max'=>5),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('t_classid, name, teacherid, year, F1, F2, F3', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			't_classid' => 'T Classid',
			'name' => 'Name',
			'teacherid' => 'Teacherid',
			'year' => 'Year',
			'F1' => 'F1',
			'F2' => 'F2',
			'F3' => 'F3',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('t_classid',$this->t_classid);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('teacherid',$this->teacherid,true);
		$criteria->compare('year',$this->year,true);
		$criteria->compare('F1',$this->F1,true);
		$criteria->compare('F2',$this->F2,true);
		$criteria->compare('F3',$this->F3,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}