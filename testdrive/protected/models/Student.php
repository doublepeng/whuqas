<?php

/**
 * This is the model class for table "student".
 *
 * The followings are the available columns in table 'student':
 * @property string $studentid
 * @property string $studentname
 * @property string $sex
 * @property string $birthDate
 * @property string $grade
 * @property integer $classid
 * @property string $password
 * @property string $IDtype
 * @property string $IDnum
 * @property string $nation
 * @property string $province
 * @property string $political
 * @property string $bankid
 * @property string $ispoor
 * @property string $cadre
 * @property string $phone
 * @property integer $stuType
 * @property string $authority
 *
 * The followings are the available model relations:
 * @property Score[] $scores
 */
class Student extends CActiveRecord
{
	//public static $err = "学号为$studentid的学生已经存在，请重新输入！";
	/**
	 * Returns the static model of the specified AR class.
	 * @return Student the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'student';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('studentid', 'required'),
			array('classid, stuType', 'numerical', 'integerOnly'=>true),
			array('studentid, cadre, phone', 'length', 'max'=>20),
			array('studentname, political', 'length', 'max'=>40),
			array('sex', 'length', 'max'=>4),
			array('grade', 'length', 'max'=>5),
			array('password, province', 'length', 'max'=>50),
			array('IDtype', 'length', 'max'=>15),
			array('IDnum', 'length', 'max'=>30),
			array('nation', 'length', 'max'=>14),
			array('bankid', 'length', 'max'=>25),
			array('ispoor', 'length', 'max'=>10),
			array('authority', 'length', 'max'=>2),
			array('birthDate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('studentid, studentname, sex, birthDate, grade, classid, password, IDtype, IDnum, nation, province, political, bankid, ispoor, cadre, phone, stuType, authority', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'scores' => array(self::HAS_MANY, 'Score', 'studentid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'studentid' => 'Studentid',
			'studentname' => 'Studentname',
			'sex' => 'Sex',
			'birthDate' => 'Birth Date',
			'grade' => 'Grade',
			'classid' => 'Classid',
			'password' => 'Password',
			'IDtype' => 'Idtype',
			'IDnum' => 'Idnum',
			'nation' => 'Nation',
			'province' => 'Province',
			'political' => 'Political',
			'bankid' => 'Bankid',
			'ispoor' => 'Ispoor',
			'cadre' => 'Cadre',
			'phone' => 'Phone',
			'stuType' => 'Stu Type',
			'authority' => 'Authority',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('studentid',$this->studentid,true);
		$criteria->compare('studentname',$this->studentname,true);
		$criteria->compare('sex',$this->sex,true);
		$criteria->compare('birthDate',$this->birthDate,true);
		$criteria->compare('grade',$this->grade,true);
		$criteria->compare('classid',$this->classid);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('IDtype',$this->IDtype,true);
		$criteria->compare('IDnum',$this->IDnum,true);
		$criteria->compare('nation',$this->nation,true);
		$criteria->compare('province',$this->province,true);
		$criteria->compare('political',$this->political,true);
		$criteria->compare('bankid',$this->bankid,true);
		$criteria->compare('ispoor',$this->ispoor,true);
		$criteria->compare('cadre',$this->cadre,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('stuType',$this->stuType);
		$criteria->compare('authority',$this->authority,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function exist($id)
	{
		$results = Student::model()->findAll("studentid='$id'");
		if(empty($results))
			return false;
		else return true;
	}
}
