<?php

/**
 * This is the model class for table "excestuschoarshiptype".
 *
 * The followings are the available columns in table 'excestuschoarshiptype':
 * @property integer $id
 * @property integer $departmentid
 * @property integer $year
 * @property integer $Alevelnums
 * @property integer $Blevelnums
 * @property integer $Clevelnums
 * @property integer $totalnums
 */
class Excestuschoarshiptype extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Excestuschoarshiptype the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'excestuschoarshiptype';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('departmentid, year, Alevelnums, Blevelnums, Clevelnums, totalnums', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, departmentid, year, Alevelnums, Blevelnums, Clevelnums, totalnums', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'departmentid' => 'Departmentid',
			'year' => 'Year',
			'Alevelnums' => 'Alevelnums',
			'Blevelnums' => 'Blevelnums',
			'Clevelnums' => 'Clevelnums',
			'totalnums' => 'Totalnums',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('departmentid',$this->departmentid);
		$criteria->compare('year',$this->year);
		$criteria->compare('Alevelnums',$this->Alevelnums);
		$criteria->compare('Blevelnums',$this->Blevelnums);
		$criteria->compare('Clevelnums',$this->Clevelnums);
		$criteria->compare('totalnums',$this->totalnums);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}