<?php

/**
 * This is the model class for table "teacher".
 *
 * The followings are the available columns in table 'teacher':
 * @property string $teacherid
 * @property integer $departmentid
 * @property string $teachername
 * @property string $password
 * @property string $remark1
 * @property string $remark2
 */
class Teacher extends CActiveRecord
{
	public static $err = "输入的教工号已经存在，请重新输入！";
	/**
	 * Returns the static model of the specified AR class.
	 * @return Teacher the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'teacher';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('teacherid', 'required'),
			array('departmentid', 'numerical', 'integerOnly'=>true),
			array('teacherid', 'length', 'max'=>20),
			array('teachername', 'length', 'max'=>30),
			array('password', 'length', 'max'=>50),
			array('remark1, remark2', 'length', 'max'=>25),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('teacherid, departmentid, teachername, password, remark1, remark2', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'teacherid' => 'Teacherid',
			'departmentid' => 'Departmentid',
			'teachername' => 'Teachername',
			'password' => 'Password',
			'remark1' => 'Remark1',
			'remark2' => 'Remark2',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('teacherid',$this->teacherid,true);
		$criteria->compare('departmentid',$this->departmentid);
		$criteria->compare('teachername',$this->teachername,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('remark1',$this->remark1,true);
		$criteria->compare('remark2',$this->remark2,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function exist($id)
	{
		$results = Teacher::model()->findAll("teacherid='$id'");
		if(empty($results))
			return false;
		else return true;
	}
}
