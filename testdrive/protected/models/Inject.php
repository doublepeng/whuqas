<?php
  class Inject {
    public function injectCheck($str){
      $tmp=preg_match('/select|insert|update|delete|\'|\/\*|\*|\.\.\/|\.\/|union|into|load_file|outfile/', $str); // 进行过滤
      if($tmp)
      {
        echo json_encode(array("success"=>false, "message"=>"该用户尝试sql注入或输入非法字符，不允许操作！", "results"=>array()));	
        exit;
      }
      else
      {
        return $str;
      }
    }  
  }
?>
