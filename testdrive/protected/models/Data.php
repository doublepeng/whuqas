<?php
	class Data
	{	public function addStudent($filename)//向数据库student表添加学生，包括学生学号，姓名，班级，密码
		{
			require_once('C:/WEB/Apache2.2/htdocs/frame/lib/Excel/Reader/ImportGPA.php');
			try 
			{
				$ret = array();
				$gpa = new ImportGPA; 
				$import = $gpa->import($filename);//加载学生成绩表
				$students= $gpa->_getStudents();//获得多个学生
				$password = md5('123456');
				foreach($students as $student)//对每一条学生记录
				{
					//中文处理
					$studentname  = iconv("gb2312","UTF-8",$student[1]);//学生姓名
					//新建学生对象
					//需要加入异常处理，重复添加！！！！！！！！
					$obstudent = new student;
					$obstudent->studentid = $student[0];
					$classname = iconv("gb2312","UTF-8","计算机应用1班");
					$obstudent->classname = $classname;
					$obstudent->studentname = $studentname;
					$obstudent->password = $password;//设定密码，应该用md5加密后存入进去
					$obstudent->save();
				}
							
			} 
			catch (Exception $e) 
			{
				echo $e;
			}
			
		}
		public function addCourse($filename)//添加学院课程表
		{
			require_once('C:/WEB/Apache2.2/htdocs/frame/lib/Excel/Reader/ImportGPA.php');	
			try 
			{
				$ret = array();
				$gpa = new ImportGPA; 
				$import = $gpa->import($filename);
				//$scoreRecords = $gpa->_getRecords();
				$courses = $gpa->_getCourses();
				foreach ($courses as $course)
				{
					//数据库解决中文存储
					$course[0] = iconv("gb2312","UTF-8",$course[0]); 
					$course[1] = iconv("gb2312","UTF-8",$course[1]);
					//先判断课程表中是否有该课程
					$obcourse = course::model()->find('coursename=? and 
					coursetype=? and coursepoint=?',
					array($course[0],$course[1],$course[2]));
					//print_r($obcourse);
					if($obcourse==null){//如果不存在该课程则添加
					$obcourse = new course;
					$obcourse->coursename = $course[0];//课程名
					$obcourse->coursetype = $course[1];//课程类型
					$obcourse->coursepoint = $course[2];//课程学分
					$obcourse->save();
					}					
				}
				$ret['success'] = 'success';
				echo json_encode($ret);
			}
			catch (Exception $e)
			{
				echo $e;
				$ret['success'] = 'failed';
				echo json_encode($ret);
			}
		}
		public function saveScore($filename)//添加学生成绩
		{
		   require_once('C:/WEB/Apache2.2/htdocs/frame/lib/Excel/Reader/ImportGPA.php');
           try
           {
           		$ret = array();
           		$gpa = new ImportGPA;
				$import = $gpa->import($filename);//加载学生成绩表
				$records = $gpa->_getRecords();
				$sql1 = "insert into score(studentid,courseid,year,coursescore) values";
				foreach ($records as $record)//某个学生成绩表
				{
					$studentid = $record[0];
					$studentname = iconv("gb2312","UTF-8",$record[1]);;
					$ascore = unserialize($record[2]);				
					foreach($ascore as $score)//某个学生的某项成绩
					{
						//中文处理课程名
						$coursename = iconv("gb2312","UTF-8",$score[0]);						
						//中文处理课程类型
						$coursetype = iconv("gb2312","UTF-8",$score[1]);
						$coursepoint = $score[2];
						//根据课程名称，类型和学分判断该课程是否存在于课程表数据库中
						$db = Yii::app()->db;
						$sql = "SELECT courseid FROM course where coursename = '$coursename' and coursetype = '$coursetype' and coursepoint = $coursepoint ";
						//应该返回唯一的选项
						$result = $db->createCommand($sql)->queryRow();
						//返回一个 CDbDataReader实例
						$courseid = $result['courseid'];
						$year = date("Y");
						if (isset($courseid))//如果存在该课程,则将成绩存入到score表中
						{
							/*$obscore = new score;
							$obscore->studentid=$studentid;
							$obscore->courseid = $courseid;
							$obscore->year = $year;
							$obscore->coursescore = $score[3];
							$obscore->save();*/
							$sql1 = $sql1."($studentid,$courseid,$year,$score[3]),";
							
							/*$sql = "insert into score(studentid,courseid,year,coursescore) 
							values($studentid,$courseid,$year,$score[3])";
							$db->createCommand($sql)->execute();	*/										
						}
						else 
						{
							echo "课程".$coursename."不存在";
						}
						
					}
		    	}
		    		$sql1 = substr($sql1, 0, -1);
					//echo $sql1;
					$db->createCommand($sql1)->execute();
           }
           catch(Exception $e)
           {
           		echo $e;
           }
         }
         
         public function saveSelfScore($stuselfeva)//添加学生自我评价
         {
            try
            {
					session_start();
					$studentid = $_SESSION['ID'];
					//$student = student::model()->find('LOWER(studentid)=?',array($studentid));
					//$studentid = '2';// 个人自我评价内容
					$selfEva = $stuselfeva['selfEva'];
					//全部删除该学生的自我评价记录
					$db = Yii::app()->db;
					$sql ="delete from selfevascore where studentid = '$studentid'";
					$db->createCommand($sql)->execute();
					
					foreach($selfEva as $records)//某一项记录
					{
						$evaluateid = $records['choose'];
						foreach ($records['contents'] as $record)//某一项单独得分
						{
							$sevs = new selfevascore;//新建学生自我评价选项得分对象
							$sevs->studentid = $studentid;
							$sevs->year = date("Y");
							$sevs->evaluateid = $evaluateid;
							$sevs->content = $record['content'];
							$sevs->evascore = $record['score'];
							$sevs->save();
						}
					}
					
            }
            catch (Exception $e)
            {
            	echo "Sorry,storage failure.Please try again";
            }    
         }
	}
?>