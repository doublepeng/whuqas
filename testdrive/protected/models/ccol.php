<?php
	class ccol{
		public function online($username)
		{
      		$maxAlive = 1000;//最大访问人数
			$duration = 1800;//在30分钟内范围内
			$db = Yii::app()->db;
			//删除超过半小时的记录
			$desql = "DELETE FROM ccol WHERE (UNIX_TIMESTAMP(NOW())-UNIX_TIMESTAMP(dtstamp))>$duration";
			$db->createCommand($desql)->execute();
			//判断当前的帐号是否存在该表中
     		$sesql = "SELECT count(id) as num FROM ccol WHERE studentid='$username'";		
     		$result = $db->createCommand($sesql)->queryRow();
     		if($result['num'] != 0)//如果不为0，即存在这条记录
			{
				//允许继续访问
				return true;
			}
			else //不存在这条记录
			{
				//首先判断在线人数是否超过所设定的值
				$onlinesql = "select count(id) as cnum from ccol";
				$numre = $db->createCommand($onlinesql)->queryRow();
				$cnum = $numre['cnum'];//得到在线人数
				if($cnum < $maxAlive)//允许访问
				{
					$insql = "insert into ccol(studentid,dtstamp) values('$username',now())";
					$db->createCommand($insql)->execute();//插入记录集，允许访问
					return true;
				}
				else 
					return false;
			}
		}
	} 
?>
