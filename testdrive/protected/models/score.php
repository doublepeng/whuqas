<?php

/**
 * This is the model class for table "score".
 *
 * The followings are the available columns in table 'score':
 * @property string $studentid
 * @property integer $courseid
 * @property string $year
 * @property double $coursescore
 */
class score extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return score the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'score';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('courseid', 'required'),
			array('courseid', 'numerical', 'integerOnly'=>true),
			array('coursescore', 'numerical'),
			array('studentid', 'length', 'max'=>20),
			array('year', 'length', 'max'=>10),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('studentid, courseid, year, coursescore', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'course' => array(self::BELONGS_TO, 'Course', 'courseid'),
			'student' => array(self::BELONGS_TO, 'Student', 'studentid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'studentid' => 'Studentid',
			'courseid' => 'Courseid',
			'year' => 'Year',
			'coursescore' => 'Coursescore',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('studentid',$this->studentid,true);

		$criteria->compare('courseid',$this->courseid);

		$criteria->compare('year',$this->year,true);

		$criteria->compare('coursescore',$this->coursescore);

		return new CActiveDataProvider('score', array(
			'criteria'=>$criteria,
		));
	}
}