<?php

/**
 * This is the model class for table "scholarshiptype".
 *
 * The followings are the available columns in table 'scholarshiptype':
 * @property string $scholoarshipid
 * @property string $name
 * @property string $level
 * @property integer $amount
 * @property integer $departmentid
 */
class Scholarshiptype extends CActiveRecord
{
	public static $err = '该专项奖学金信息已存在！';
	/**
	 * Returns the static model of the specified AR class.
	 * @return Scholarshiptype the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'scholarshiptype';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('amount, departmentid', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>50),
			array('level', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('scholoarshipid, name, level, amount, departmentid', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'scholoarshipid' => 'Scholoarshipid',
			'name' => 'Name',
			'level' => 'Level',
			'amount' => 'Amount',
			'departmentid' => 'Departmentid',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('scholoarshipid',$this->scholoarshipid,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('level',$this->level,true);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('departmentid',$this->departmentid);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function exist($name, $amount, $did)
	{
		$results = Scholarshiptype::model()->findAll("name='$name' and departmentid='$did' and amount='$amount'");
		if(empty($results))
			return false;
		else return true;
	}
}
