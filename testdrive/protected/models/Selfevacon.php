<?php

/**
 * This is the model class for table "selfevacon".
 *
 * The followings are the available columns in table 'selfevacon':
 * @property integer $evaluateid
 * @property string $evaluatename
 * @property integer $departmentid
 * @property string $maxscore
 * @property string $year
 */
class Selfevacon extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Selfevacon the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'selfevacon';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('departmentid', 'required'),
			array('departmentid', 'numerical', 'integerOnly'=>true),
			array('evaluatename', 'length', 'max'=>60),
			array('maxscore, year', 'length', 'max'=>5),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('evaluateid, evaluatename, departmentid, maxscore, year', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'evaluateid' => 'Evaluateid',
			'evaluatename' => 'Evaluatename',
			'departmentid' => 'Departmentid',
			'maxscore' => 'Maxscore',
			'year' => 'Year',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('evaluateid',$this->evaluateid);
		$criteria->compare('evaluatename',$this->evaluatename,true);
		$criteria->compare('departmentid',$this->departmentid);
		$criteria->compare('maxscore',$this->maxscore,true);
		$criteria->compare('year',$this->year,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function existEvaCon($evacon, $did, $year)
	{
		$results = Selfevacon::model()->findAll("evaluatename='$evacon' and departmentid='$did' and year='$year'");
		if(empty($results))
			return false;
		else return true;
	}
}
