<?php

/**
 * This is the model class for table "selfevascore".
 *
 * The followings are the available columns in table 'selfevascore':
 * @property string $studentid
 * @property string $evaluateid
 * @property string $year
 * @property string $content
 * @property string $evascore
 */
class selfevascore extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return selfevascore the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'selfevascore';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('evaluateid, year', 'required'),
			array('studentid, year', 'length', 'max'=>50),
			array('evaluateid', 'length', 'max'=>2),
			array('content', 'length', 'max'=>100),
			array('evascore', 'length', 'max'=>3),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('studentid, evaluateid, year, content, evascore', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'evaluate' => array(self::BELONGS_TO, 'Selfevacon', 'evaluateid'),
			'student' => array(self::BELONGS_TO, 'Student', 'studentid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'studentid' => 'Studentid',
			'evaluateid' => 'Evaluateid',
			'year' => 'Year',
			'content' => 'Content',
			'evascore' => 'Evascore',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('studentid',$this->studentid,true);

		$criteria->compare('evaluateid',$this->evaluateid,true);

		$criteria->compare('year',$this->year,true);

		$criteria->compare('content',$this->content,true);

		$criteria->compare('evascore',$this->evascore,true);

		return new CActiveDataProvider('selfevascore', array(
			'criteria'=>$criteria,
		));
	}
}