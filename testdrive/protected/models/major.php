<?php

/**
 * This is the model class for table "major".
 *
 * The followings are the available columns in table 'major':
 * @property string $majorname
 * @property string $departmentname
 */
class major extends CActiveRecord
{
	public static $err = '该专业信息已存在！';
	/**
	 * Returns the static model of the specified AR class.
	 * @return major the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'major';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('majorname, departmentname', 'required'),
			array('majorname', 'length', 'max'=>50),
			array('departmentname', 'length', 'max'=>60),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('majorname, departmentname', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'classes' => array(self::HAS_MANY, 'Whuclass', 'majorname'),
			'departmentname' => array(self::BELONGS_TO, 'Department', 'departmentname'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'majorname' => 'Majorname',
			'departmentname' => 'Departmentname',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('majorname',$this->majorname,true);

		$criteria->compare('departmentname',$this->departmentname,true);

		return new CActiveDataProvider('major', array(
			'criteria'=>$criteria,
		));
	}

	public static function exist($name, $did)
	{
		$results = major::model()->findAll("majorname='$name' and departmentid='$did'");
		if(empty($results))
			return false;
		else return true;
	}
}
