<?php

class ChechlistController extends Controller
{

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated users to access all actions
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionStartTest()   //开始学年测评
	{
		if (!isset($_SESSION))
		{
			session_start();
		}

		if(!isset($_SESSION['ID']))
		{
			echo json_encode(array(
				"success"=>false, 
				"message"=>"您登录的时间超时，请返回首页重新登录！", 
				"results"=>array('timeout'=>true)
			));
			exit();
		}

		if (isset($_SESSION["ID"]))
		{
			$supermanagerid = $_SESSION["ID"];
			$startgradeEvalu = isset($_POST["startgradeEvalu"])?$_POST["startgradeEvalu"]:$_GET["startgradeEvalu"];
			$startgradeEvalu = json_decode($startgradeEvalu,true);
			$buttonName = $startgradeEvalu["buttonName"];

			$button = "开始学年测评";

			if ($buttonName == $button)
			{
				$time = date("Y-m-d");
				$currentyear = date("Y");   //得到当前年份

				try
				{
					$db = Yii::app()->db;
					
          //$currentyear = '2014';
          //首先判断学年测评是否开始
          $sql = "select * from checklist where starttime like '$currentyear-%'";
          $query = $db->createCommand($sql)->query();
					$res = $query->readAll();
          if(!empty($res))   //如果已经开始了
          {
            echo json_encode(array("success"=>false,"message"=>$currentyear."度学年测评已经开始，您无需重复此操作！","results"=>array()));
            exit;
          }
        
          //如果还没有开始
          $insertmanager = "insert into checklist(managerid,starttime) values('$supermanagerid','$time')";
					$db->createCommand($insertmanager)->execute();

					$querysdepartment = "select departmentid from department";
					$departmentrets = $db->createCommand($querysdepartment)->queryAll();
					foreach ($departmentrets as $datarow)
					{
						$lastyear = $currentyear - 1;
						$departmentid = $datarow["departmentid"];

						$queryselfevacon_c = "select evaluatename,maxscore from selfevacon where departmentid='$departmentid' and year='$currentyear'";
						$currentrets = $db->createCommand($queryselfevacon_c)->queryAll();	
						if (empty($currentrets))
						{
							$queryselfevacon_l = "select evaluatename,maxscore from selfevacon where departmentid='$departmentid' and year='$lastyear'";
							$lastrets = $db->createCommand($queryselfevacon_l)->queryAll();	
							if (!empty($lastrets))
							{
								foreach ($lastrets as $dataRow)
								{
									$evaluatename = $dataRow["evaluatename"];
									$maxscore = $dataRow["maxscore"];
											
									$insertselfevacon = "insert into selfevacon(evaluatename,departmentid,maxscore,year) values('$evaluatename','$departmentid','$maxscore','$currentyear')";
									$db->createCommand($insertselfevacon)->execute();
								}
							}
						}
          }
					$rets = array("success"=>true,"message"=>"开始学年测评操作成功！","results"=>array());
					echo json_encode($rets);
				}
				catch (Exception $e)
				{
					$rets = array("success"=>false,"message"=>$e->getMessage(),"results"=>array());
					echo json_encode($rets);

				}
			}
		}
	}


	public function actionEndTest()   //结束学年测评
	{
		if (!isset($_SESSION))
		{
			session_start();
		}

		if(!isset($_SESSION['ID']))
		{
			echo json_encode(array(
				"success"=>false, 
				"message"=>"您登录的时间超时，请返回首页重新登录！", 
				"results"=>array('timeout'=>true)
			));
			exit();
		}

		if (isset($_SESSION["ID"]))
		{
			$supermanagerid = $_SESSION["ID"];
			//$supermanagerid = 1;
			$endgradeEvalu = isset($_POST["endgradeEvalu"])?$_POST["endgradeEvalu"]:$_GET["endgradeEvalu"];
			$endgradeEvalu = json_decode($endgradeEvalu,true);
			$buttonName = $endgradeEvalu["buttonName"];

			$button = "结束学年测评";

			if ($buttonName == $button)
			{
				$time = date("Y-m-d");
				$currentyear = date("Y");
				
				try
				{
					$db = Yii::app()->db;

          //首先判断学年测评是否开始
          $sql = "select * from checklist where starttime like '$currentyear-%'";
          $query = $db->createCommand($sql)->query();
					$res = $query->readAll();
          if(empty($res))   //如果没开始
          {
            echo json_encode(array("success"=>false,"message"=>$currentyear."度学年测评尚未开始，您需要先“开始学年测评”！","results"=>array()));
            exit;
          }
          
          //学年测评已经开始，那么需要判断学年测评是否已经结束
          if(!empty($res[0]["endtime"]))   //表示学年测评已经结束
          {
            echo json_encode(array("success"=>false,"message"=>$currentyear."度学年测评已经结束，您无需重复此操作！","results"=>array()));
            exit;
          }

          //尚未结束学年测评
          $updateendtime = "update checklist set endtime='$time' where managerid='$supermanagerid' and starttime like '$currentyear-%'";
					$db->createCommand($updateendtime)->execute();

					$updatewhuclass = "update whuclass set t_classid=NULL,gradecheck=0";
					$db->createCommand($updatewhuclass)->execute();

					$updatestudent = "update student set authority=0";
					$db->createCommand($updatestudent)->execute();

					$name = "测评小组";
					$delteacher = "delete from teacher where teachername='$name'";
					$db->createCommand($delteacher)->execute();

					$rets = array("success"=>true,"message"=>"结束学年测评操作成功！","results"=>array());
					echo json_encode($rets);
				}
				catch (Exception $e)
				{
					$rets = array("success"=>false,"message"=>$e->getMessage(),"results"=>array());
					echo json_encode($rets);
				}
			}
		}
	}
	


}
