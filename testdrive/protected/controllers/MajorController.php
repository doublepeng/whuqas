<?php

class MajorController extends Controller
{
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated users to access all actions
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionDisplayMajorByGrade()	//显示对应学院对应年级的专业信息，接口1.1
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}
		if(!isset($_SESSION['ID']))	//Èç¹û²»ŽæÔÚ±äÁ¿£¬ÕŠÉàËµÃ÷µÇÂŒ³¬Ê±£¬ÇëÖØÐÂµÇÂŒ
		{
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","ÄúµÇÂŒµÄÊ±Œä³¬Ê±£¬Çë·µ»ØÊ×Ò³ÖØÐÂµÇÂŒ£¡"), "results"=>array("timeout"=>true)));
			exit();
		}
		if (isset($_SESSION["ID"]))
		{
			$departmentid = $_SESSION["departmentid"];	//通过session获取学院id

			try 
			{
				$db = Yii::app()->db;	//连接数据库

				$sqlquery = "SELECT majorid,majorname FROM major WHERE departmentid='$departmentid'";
				$queryinfo = $db->createCommand($sqlquery)->query();
				$results = $queryinfo->readAll();				//查询对应学院对应年级的所有专业

				$rets = array("success"=>"true","message"=>"","results"=>$results);
				echo json_encode($rets);
			}
			catch (Exception $e)
			{
				$rets = array("success"=>"false","message"=>"数据库查询异常...".$e->getMessage(),"results"=>array());
				echo json_encode($rets);
			}
		}

	}


	public function actionSaveMajor()//保存专业信息：接口2.1
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}
		if(!isset($_SESSION['ID']))	//Èç¹û²»ŽæÔÚ±äÁ¿£¬ÕŠÉàËµÃ÷µÇÂŒ³¬Ê±£¬ÇëÖØÐÂµÇÂŒ
		{
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","ÄúµÇÂŒµÄÊ±Œä³¬Ê±£¬Çë·µ»ØÊ×Ò³ÖØÐÂµÇÂŒ£¡"), "results"=>array("timeout"=>true)));
			exit();
		}
		if (isset($_SESSION["ID"]))
		{
			$departmentid = $_SESSION["departmentid"];//通过session获取学院id
			
			$saveMajor = isset($_POST["saveMajor"])?$_POST["saveMajor"]:$_GET["saveMajor"];
			$saveMajor = json_decode($saveMajor,true);
			
			try
			{
				$db = Yii::app()->db;//建立mysql数据库连接

				foreach ($saveMajor as $value)
				{
					//往专业表major中插入新增的学院专业及专业所对应的学院id
					$majorname = $value["majorname"];
					
          //防止插入重复
          $sql = "select * from major where departmentid = '$departmentid' and majorname = '$majorname'"; 
          $query = $db->createCommand($sql)->query();
          $res = $query->readAll();
          if(!empty($res))   //如果已经存在了
          {
            echo json_encode(array("success"=>false,"message"=>'专业名为“'.$majorname."”的专业已经存在，请修改后再提交！","results"=>array()));
            exit;
          }

					$insertmajor = "INSERT INTO major(majorname,departmentid) VALUES('$majorname','$departmentid')";
					$db->createCommand($insertmajor)->execute();
				}

				$rets = array("success"=>"true","message"=>"","results"=>array());
				echo json_encode($rets);
			}
			catch (Exception $e)
			{
				$rets = array("success"=>"false","message"=>$e->getMessage(),"results"=>array());
				echo json_encode($rets);
			}
		}
	}

	public function actionDisplayMajor()//显示专业信息，接口2.2
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}
		if(!isset($_SESSION['ID']))	//Èç¹û²»ŽæÔÚ±äÁ¿£¬ÕŠÉàËµÃ÷µÇÂŒ³¬Ê±£¬ÇëÖØÐÂµÇÂŒ
		{
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","ÄúµÇÂŒµÄÊ±Œä³¬Ê±£¬Çë·µ»ØÊ×Ò³ÖØÐÂµÇÂŒ£¡"), "results"=>array("timeout"=>true)));
			exit();
		}
		if (isset($_SESSION["ID"]))
		{
			$departmentid = $_SESSION["departmentid"];//通过session获取学院id

			try
			{
				$db = Yii::app()->db;//连接mysql数据库

				//查询学院的所有专业id号及名称
				$querymajor = "SELECT majorid,majorname FROM major WHERE departmentid='$departmentid'";
				$queryrets = $db->createCommand($querymajor)->query();
				$results = $queryrets->readAll();

				$rets = array("success"=>"false","message"=>"","results"=>$results);
				echo json_encode($rets);
			}
			catch (Exception $e)
			{
				$rets = array("success"=>"false","message"=>$e->getMessage(),"results"=>array());
				echo json_encode($rets);
			}
		}
	}

	public function actionSaveEditMajor()//保存编辑后的专业信息，接口2.3
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}
		if(!isset($_SESSION['ID']))	//Èç¹û²»ŽæÔÚ±äÁ¿£¬ÕŠÉàËµÃ÷µÇÂŒ³¬Ê±£¬ÇëÖØÐÂµÇÂŒ
		{
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","ÄúµÇÂŒµÄÊ±Œä³¬Ê±£¬Çë·µ»ØÊ×Ò³ÖØÐÂµÇÂŒ£¡"), "results"=>array("timeout"=>true)));
			exit();
		}
		if (isset($_SESSION["ID"]))
		{
			$departmentid = $_SESSION["departmentid"];//获取学院id号
			
			
			$saveEditMajor = isset($_POST["saveEditMajor"])?$_POST["saveEditMajor"]:$_GET["saveEditMajor"];
			$saveEditMajor = json_decode($saveEditMajor,true);
			$updatemajor = $saveEditMajor["update"];
			$delmajor = $saveEditMajor["delete1"];
		
			try
			{
				$db = Yii::app()->db;//连接mysql数据库
				
				//要更新的专业不为空时，则进行更新
				if (!empty($updatemajor))
				{
					//逐行进行专业更行
					foreach($updatemajor as $updatecolum)
					{
						$majorid = $updatecolum["majorid"];
						$majorname = $updatecolum["majorname"];
						//$majorname = iconv("gb2312","utf-8",$majorname);
						$sqlupdate = "UPDATE major SET majorname='$majorname' WHERE majorid='$majorid' and departmentid='$departmentid'";
						$db->createCommand($sqlupdate)->execute();
					}
				}
				
				//传入的删除专业不为空时，则进行删除
				if (!empty($delmajor))
				{
					//逐行删除
					foreach($delmajor as $delcolum)
					{
						$majorid = $delcolum["majorid"];
						$sqldel = "DELETE FROM major WHERE majorid='$majorid'";
						$db->createCommand($sqldel)->execute();
					}
				}

				$rets = array("success"=>"true","message"=>"","results"=>array());
				echo json_encode($rets);
			}
			catch(Exception $e)
			{
				$rets = array("success"=>"false","message"=>$e->getMessage(),"results"=>array());
				echo json_encode($rets);
			}
		}
	}

}
