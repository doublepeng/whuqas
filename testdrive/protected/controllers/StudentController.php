<?php

class StudentController extends Controller
{
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated users to access all actions
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionSaveStudentInfo()//保存新增加的学生基本信息,接口9.1
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}

		if(!isset($_SESSION['ID']))
		{
			echo json_encode(array(
				"success"=>false, 
				"message"=>"您登录的时间超时，请返回首页重新登录！", 
				"results"=>array('timeout'=>true)
			));
			exit();
		}

		if (isset($_SESSION["ID"]))//判定用户已登录
		{
			$departmentid = $_SESSION["departmentid"];

			$saveStudentInfo = isset($_POST["saveStudentInfo"]) ? $_POST["saveStudentInfo"] :$_GET["saveStudentInfo"];
			$saveStudentInfo = json_decode($saveStudentInfo,true);

			try
			{
				$db = Yii::app()->db;
        $success = true;
        $msg = "学号为";
				foreach($saveStudentInfo as $value)
				{
					$studentid = $value["studentid"];//学号
					$studentname = $value["studentname"];//姓名
					$grade = $value["grade"];//年级
					$classid  = $value["class"];//班级号
					
					//查询要插入的学生是否已经存在，如果存在则有对应学号的学生名；否则，没有相应学生姓名
					//$querystudent = "select studentname from student where studentid='$studentid' and grade='$grade' and classid='$classid'";
					//2012-01-31 minzhenyu 学号是唯一的，所以直接查学号对应的学生姓名即可。
					$querystudent = "select studentname from student where studentid='$studentid'";
					$studentinfo = $db->createCommand($querystudent)->queryRow();
					$name = $studentinfo["studentname"];
					
					if (empty($name))//学生信息不存在student表中时，则直接插入相关信息
					{
            $password = md5("123456");
						$sqlinsert = "insert into student(studentid,studentname,password,grade,classid) values('$studentid','$studentname','$password','$grade','$classid')";
						$db->createCommand($sqlinsert)->execute();
						//$rets = array("success"=>true,"message"=>"","results"=>"");
						//echo json_encode($rets);
					}
					else //学生信息存在student表中时，报错，不允许直接插入相关信息
					{
						$msg = $msg.$studentid." ";
						//$msg = iconv("gb2312","utf-8",$msg);
						//$rets = array("success"=>false,"message"=>$msg,"results"=>"");
						//echo json_encode($rets);
						$success = false;
					}
				}
				if($success == true)
				{
          $rets = array("success"=>true,"message"=>"","results"=>array());
					echo json_encode($rets);
				}
				else
				{
          $rets = array("success"=>false,"message"=>$msg."的学生信息已经存在！","results"=>array());
					echo json_encode($rets);
				}
			}
			catch (Exception $e)
			{
				$rets = array("success"=>false,"message"=>$e->getMessage(),"results"=>array());
				echo json_encode($rets);
			}
		}
	}

	public function actionDisplayClassByGrade()//显示对应学院对应年级的全部班级,接口9.2
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}
		if(!isset($_SESSION['ID']))
		{
			echo json_encode(array(
				"success"=>false, 
				"message"=>"您登录的时间超时，请返回首页重新登录！", 
				"results"=>array('timeout'=>true)
			));
			exit();
		}
		if (isset($_SESSION["ID"]))//判定用户已登录
		{
			//$departmentid = $_SESSION["departmentid"];

			$displayClassByYearByMajor = isset($_POST["displayClassByYearByMajor"]) ? $_POST["displayClassByYearByMajor"] :$_GET["displayClassByYearByMajor"];
			$displayClassByYearByMajor = json_decode($displayClassByYearByMajor,true);	
			$grade = $displayClassByYearByMajor["grade"];
			$majorid = $displayClassByYearByMajor["majorid"];

			try
			{
				$db = Yii::app()->db;

				$sqlquery = "select classid,classname from whuclass where grade='$grade' and majorid='$majorid'";
				$queryinfo = $db->createCommand($sqlquery)->query();
				$results = $queryinfo->readAll();

				$rets = array("success"=>true,"message"=>"","results"=>$results);
				echo json_encode($rets);
			}
			catch (Exception $e)
			{
				$rets = array("success"=>false,"message"=>$e->getMessage(),"results"=>array());
				echo json_encode($rets);
			}
		}
	}

	public function actionDisplayStudentInfoByClass()//显示对应班级的全部学生的基本信息,接口9.3
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}
		if(!isset($_SESSION['ID']))
		{
			echo json_encode(array(
				"success"=>false, 
				"message"=>"您登录的时间超时，请返回首页重新登录！", 
				"results"=>array('timeout'=>true)
			));
			exit();
		}
		if (isset($_SESSION["ID"]))//判定用户已登录
		{
			$displayStudentInfoByClass = isset($_POST["displayStudentInfoByClass"]) ? $_POST["displayStudentInfoByClass"] :$_GET["displayStudentInfoByClass"];
			$displayStudentInfoByClass = json_decode($displayStudentInfoByClass,true);	
			$grade = $displayStudentInfoByClass["grade"];
			$classid = $displayStudentInfoByClass["classid"];
			$majorid = $displayStudentInfoByClass["majorid"];

			try
			{
				$db = Yii::app()->db;

				$sqlquery = "select studentid,studentname,nation as people,province as nativeplace,political as politicaloutlook,ispoor as										poorstudent,cadre as posting,stuType as studentsort,phone as phonenumber from whuclass,student where majorid='$majorid' and 
								whuclass.grade='$grade' and whuclass.classid=student.classid and whuclass.classid='$classid' and student.grade='$grade' and student.classid='$classid'";
				$queryinfo = $db->createCommand($sqlquery)->query();
				$results = $queryinfo->readAll();

				$rets = array("success"=>true,"message"=>"","results"=>$results);
				echo json_encode($rets);

			}
			catch (Exception $e)
			{
				$rets = array("success"=>false,"message"=>$e->getMessage(),"results"=>array());
				echo json_encode($rets);
			}
		}
	}
}
