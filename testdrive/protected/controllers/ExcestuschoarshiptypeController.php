<?php

class ExcestuschoarshiptypeController extends Controller
{
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated users to access all actions
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionStatisticsExcellNums()
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}
		if(!isset($_SESSION['ID']))	//如果不存在变量，咋舌说明登录超时，请重新登录
		{
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array("timeout"=>true)));
			exit();
		}
		if (isset($_SESSION["ID"]))
		{
			//通过session获取学院id
			$departmentid = $_SESSION["departmentid"];
			
			//通过获取当前系统的年份来设置默认查询年份
			$year = date("Y");

			$scholarname = "优秀学生奖学金";
			$scholarname = iconv("gb2312","utf-8",$scholarname);
			
			/*
			初始化优秀学生奖学金甲乙丙等三类的统计值为0，其中，
			$Alevelnums,$Blevelnums,$Clevelnums依次为甲乙丙；
			*/
			$Alevelnums = 0;
			$Alevel = "甲等";
			$Alevel = iconv("gb2312","utf-8",$Alevel);
			$Blevelnums = 0;
			$Blevel = "乙等";
			$Blevel = iconv("gb2312","utf-8",$Blevel);
			$Clevelnums = 0;
			$Clevel = "丙等";
			$Clevel = iconv("gb2312","utf-8",$Clevel);
			
			try
			{
				$db = Yii::app()->db;

				//查询优秀学生奖学金的等级（甲乙丙）及其id号
				$queryscholarshiptype = " select scholarshipid,level from scholarshiptype where name='$scholarname'";
				$scholarshiprets = $db->createCommand($queryscholarshiptype)->queryAll();
				//print_r($scholarshiprets);

				foreach ($scholarshiprets as $value)
				{
					$scholarid = $value["scholarshipid"];
					$scholarlevel = $value["level"];
					
					//查询学院获得优秀学生奖学金的id号
					$sqlclassid = "select classid from whuclass where majorid in (select majorid from major where departmentid='$departmentid')";
					$sqlstudentid = "select studentid from student where classid in (";
					$sqlquery = "select scholarshipid from scholarship where year='$year' and studentid in (".$sqlstudentid.$sqlclassid."))";
					$scholarshipidarray = $db->createCommand($sqlquery)->queryAll();

					foreach ($scholarshipidarray as $val)
					{
						$scholarshipid = $val["scholarshipid"];
							
						if ($scholarlevel==$Alevel)//统计优秀学生奖学金甲等人数
						{
							if ($scholarshipid==$scholarid)
								$Alevelnums++;
						}

						if ($scholarlevel==$Blevel)//统计优秀学生奖学金乙等人数
						{
							if ($scholarshipid==$scholarid)
								$Blevelnums++;
						}
		
						if ($scholarlevel==$Clevel)//统计优秀学生奖学金丙等人数
						{
							if ($scholarshipid==$scholarid)
								$Clevelnums++;
						}
					}
				}
				
				//计算学院优秀学生奖学金获得总数
				$totalnums = 0;
				$totalnums += ($Alevelnums+$Blevelnums+$Clevelnums);
			}
			catch(Exception $e)
			{
				$rets = array("success"=>"false", "message"=>$e->getMessage(), "results"=>array());
				echo json_encode($rets);
			}
			
			try
			{
				//将统计优秀学生奖学金甲乙丙等人数以及总数结果插入到优秀学生奖学金表（excestuschoarshiptype）中
				$insertexcestuschoarshiptype = "insert into excestuschoarshiptype(departmentid,year,Alevelnums,Blevelnums,Clevelnums,totalnums) values(
							'$departmentid','$year','$Alevelnums','$Blevelnums','$Clevelnums','$totalnums')";
				$results = $db->createCommand($insertexcestuschoarshiptype)->execute();

				$rets = array("success"=>"true", "message"=>"", "results"=>$results);
				echo json_encode($rets);
			}
			catch (Exception $e)
			{
				$rets = array("success"=>"false", "message"=>$e->getMessage(), "results"=>array());
				echo json_encode($rets);
			}
		}
	}
}
