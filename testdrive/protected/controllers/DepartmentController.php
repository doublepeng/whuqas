<?php

class DepartmentController extends Controller
{
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated users to access all actions
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionSetFactor()//显示整个学院的测评系数，接口7.1
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}

		if(!isset($_SESSION['ID']))	//如果不存在变量，咋舌说明登录超时，请重新登录
		{
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array("timeout"=>true)));
			exit();
		}
		
		if (isset($_SESSION["ID"]))//判定用户已登录
		{
			$departmentid = $_SESSION["departmentid"];//通过session获取学院id
			
			try
			{
				$db = Yii::app()->db;//连接mysql数据库

				//查询学院测评系数F1,F2,F3
				$sqlquery = "select F1,F2,F3 from department where departmentid='$departmentid'";
				$queryinfo = $db->createCommand($sqlquery)->query();
				$results = $queryinfo->readAll();

				$rets = array("success"=>true,"message"=>"","results"=>$results);
				echo json_encode($rets);
			}
			catch (Exception $e)
			{
				$rets = array("success"=>false,"message"=>$e,"results"=>array());
				echo json_encode($rets);
			}
		}


	}

	public function actionDisplayTClass()//显示测评班级信息到下拉列表框中,接口7.2
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}
		if(!isset($_SESSION['ID']))	//如果不存在变量，咋舌说明登录超时，请重新登录
		{
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array("timeout"=>true)));
			exit();
		}

		if (isset($_SESSION["ID"]))//判定用户已登录
		{
			$departmentid = $_SESSION["departmentid"];	

			$displayT_class = isset($_POST["displayT_class"]) ? $_POST["displayT_class"] :$_GET["displayT_class"];
			$displayT_class = json_decode($displayT_class,true);
			$grade = $displayT_class["grade"];

			//modified by minzhenyu
			//只取当前年份的测评班级
			$year = date("Y");
			try
			{
				$db = Yii::app()->db;

				$sqlquery = "select DISTINCT t_class.t_classid,t_name as t_classname from major,whuclass,t_class where major.departmentid='$departmentid' and 
								major.majorid=whuclass.majorid and whuclass.grade='$grade' and whuclass.t_classid=t_class.t_classid and year = '$year'";
				$queryinfo = $db->createCommand($sqlquery)->query();
				$results = $queryinfo->readAll();

				$rets = array("success"=>true,"message"=>"","results"=>$results);
				echo json_encode($rets);
			}
			catch (Exception $e)
			{
				$rets = array("success"=>false,"message"=>$e,"results"=>array());
				echo json_encode($rets);
			}
		}
	}

	public function actionDisplayFactorByTClass()//对应各个测评班级显示测评系数，接口7.3
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}
		if(!isset($_SESSION['ID']))	//如果不存在变量，咋舌说明登录超时，请重新登录
		{
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array("timeout"=>true)));
			exit();
		}
		if (isset($_SESSION["ID"]))//判定用户已登录
		{
			$departmentid = $_SESSION["departmentid"];	
	
			$displayFactorByT_class = isset($_POST["displayFactorByT_class"]) ? $_POST["displayFactorByT_class"] :$_GET["displayFactorByT_class"];
			$displayFactorByT_class = json_decode($displayFactorByT_class,true);
			$grade = $displayFactorByT_class["grade"];
			$t_classid = $displayFactorByT_class["t_classid"];
			//modified by minzhenyu
			//只取当前年份的测评班级
			$year = date("Y");
			
			try
			{
				$db = Yii::app()->db;

				$sqlquery = "select F1 as factor1,F2 as factor2,F3 as factor3 from major,whuclass,t_class where major.departmentid='$departmentid' and major.majorid=whuclass.majorid and whuclass.t_classid=t_class.t_classid and whuclass.grade='$grade' and t_class.t_classid='$t_classid' and year = '$year'";
				$queryinfo = $db->createCommand($sqlquery)->query();
				$results = $queryinfo->readAll();
				$results = array_unique($results);

				$rets = array("success"=>true,"message"=>"","results"=>$results);
				echo json_encode($rets);
			}
			catch (Exception $e)
			{
				$rets = array("success"=>false,"message"=>$e->getMessage(),"results"=>array());    //$e不能直接返回，$e是一个错误数组，我定义的需要一个错误字符串。
				echo json_encode($rets);
			}
		}
	}

	public function actionSaveEditFactor()//接口7.5
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}
		if(!isset($_SESSION['ID']))	//如果不存在变量，咋舌说明登录超时，请重新登录
		{
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array("timeout"=>true)));
			exit();
		}
		if (isset($_SESSION["ID"]))//判定用户已登录
		{
			$departmentid = $_SESSION["departmentid"];	

			$saveEditFactor = isset($_POST["saveEditFactor"]) ? $_POST["saveEditFactor"] :$_GET["saveEditFactor"];
			$saveEditFactor = json_decode($saveEditFactor,true);
			$F1 = $saveEditFactor["factor1"];
			$F2 = $saveEditFactor["factor2"];
			$F3 = $saveEditFactor["factor3"];

			try
			{
				$db = Yii::app()->db;

				$updatedepartment = "update department set F1='$F1',F2='$F2',F3='$F3' where departmentid='$departmentid'";
				$db->createCommand($updatedepartment)->execute();

				$rets = array("success"=>true,"message"=>"","results"=>array());
				echo json_encode($rets);
			}
			catch (Exception $e)
			{
				$rets = array("success"=>false,"message"=>$e->getMessage(),"results"=>array());
				echo json_encode($rets);
			}
		}
	}


	public function actionSaveEditTclassFactor()//接口7.4
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}
		if(!isset($_SESSION['ID']))	//如果不存在变量，咋舌说明登录超时，请重新登录
		{
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array("timeout"=>true)));
			exit();
		}
		if (isset($_SESSION["ID"]))//判定用户已登录
		{
			$saveEditTclassFactor = isset($_POST["saveEditTclassFactor"]) ? $_POST["saveEditTclassFactor"] :$_GET["saveEditTclassFactor"];
			$saveEditTclassFactor = json_decode($saveEditTclassFactor,true);
			$F1 = $saveEditTclassFactor["factor1"];
			$F2 = $saveEditTclassFactor["factor2"];
			$F3 = $saveEditTclassFactor["factor3"];
			$t_classid = $saveEditTclassFactor["t_classid"];
			$grade = $saveEditTclassFactor["grade"];

			try
			{
				$db = Yii::app()->db;

				$updatet_class = "update t_class,whuclass set F1='$F1',F2='$F2',F3='$F3' where grade='$grade' and whuclass.t_classid=t_class.t_classid and									t_class.t_classid='$t_classid'";
				$db->createCommand($updatet_class)->execute();

				$rets = array("success"=>true,"message"=>"","results"=>array());
				echo json_encode($rets);
			}
			catch (Exception $e)
			{
				$rets = array("success"=>false,"message"=>$e->getMessage(),"results"=>array());
				echo json_encode($rets);
			}
		}
	}

}
