<?php

class ScoreController extends Controller
{
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated users to access all actions
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionDisplayScore()//接口1.3，显示对应学院对应年份对应年级对应班级的全部学生的成绩列表
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}
		if(!isset($_SESSION['ID']))	//如果不存在变量，咋舌说明登录超时，请重新登录
		{
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array("timeout"=>true)));
			exit();
		}
		if (isset($_SESSION["ID"]))
		{
			$departmentid = $_SESSION["departmentid"];//通过session获取学院id

			$displayScore = isset($_POST["displayScore"]) ? $_POST["displayScore"] :$_GET["displayScore"];
			$displayScore = json_decode($displayScore,true);

			$grade = $displayScore["grade"];	//获取前台传送的年级数据
			$year = $displayScore["year"];//获取年份	
			$classid = $displayScore["classid"];//获取班级id

			try
			{
				$db = Yii::app()->db;
				
				/*
				*查询学院中某一年级中某一班级全部学生的总评成绩,按学生学号进行排序
				*总评成绩包括学生基本素质得分qualityscore，学生课程成绩得分gpascore，创新实践得分selfscore，以及总得分total
				*/
				$querygrade = "select grade.studentid,student.studentname,qualityscore as score1,gpascore as score2,selfscore as score3,total as score4 from grade,student where year='$year' and									grade.studentid=student.studentid and student.grade='$grade' and student.classid in (select classid from whuclass where grade='$grade'						and majorid in (select majorid from	major where departmentid='$departmentid')) and student.classid='$classid' order by									grade.studentid";
				$queryrets = $db->createCommand($querygrade)->query();
				$results = $queryrets->readAll();

				$rets = array("success"=>true,"message"=>"","results"=>$results);
				echo json_encode($rets);
			}
			catch (Exception $e)
			{
				$rets = array("success"=>false,"message"=>$e->getMessage(),"results"=>array());
				echo json_encode($rets);		
			}
		}
	}

}
