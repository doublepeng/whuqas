<?php

class ScholarshiptypeController extends Controller
{
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated users to access all actions
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionDisplayMajorByGrade()//显示对应学院对应年级的全部专业信息到列表框中,接口6.1
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}
		if(!isset($_SESSION['ID']))	//如果不存在变量，咋舌说明登录超时，请重新登录
		{
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array("timeout"=>true)));
			exit();
		}
		if (isset($_SESSION["ID"]))//判定用户已登录
		{
			$departmentid = $_SESSION["departmentid"];//通过session获取学院id

			try
			{
				$db = Yii::app()->db;//连接mysql数据库
				
				//查询学院所有专业信息，包括专业号及专业名
				$sqlquery = "select majorid,majorname from major where departmentid='$departmentid'";
				$queryinfo = $db->createCommand($sqlquery)->query();
				$results = $queryinfo->readAll();

				$rets = array("success"=>true,"message"=>"","results"=>$results);
				echo json_encode($rets);
			}
			catch (Exception $e)
			{
				$rets = array("success"=>false,"message"=>$e->getMessage(),"results"=>array());
				echo json_encode($rets);
			}
		}
	}

	public function actionDisplayClassByGradeByMajor()//显示班级列表信息，接口6.2
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}
		if(!isset($_SESSION['ID']))	//如果不存在变量，咋舌说明登录超时，请重新登录
		{
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array("timeout"=>true)));
			exit();
		}
		if (isset($_SESSION["ID"]))//判定用户已登录
		{
			$departmentid = $_SESSION["departmentid"];//通过session获取学院id

			$displayClassByGradeByMajor = isset($_POST["displayClassByGradeByMajor"]) ? $_POST["displayClassByGradeByMajor"] :$_GET["displayClassByGradeByMajor"];
			$displayClassByGradeByMajor = json_decode($displayClassByGradeByMajor,true);
			$grade = $displayClassByGradeByMajor["grade"];
			$majorid = $displayClassByGradeByMajor["majorid"];
			$year = $displayClassByGradeByMajor["year"];

			try
			{
				$db = Yii::app()->db;//连接mysql数据库

				if ((!empty($grade))&&(!empty($majorid))&&(!empty($year)))//如果年级、专业号、年份均不为空时，则查询学院对应专业对应年级对应年份的测评班级信息
				{
					$queryt_class = "select distinct t_class.t_classid,t_name as t_classname,teachername,gcheck from t_class,whuclass,teacher where grade='$grade' and majorid='$majorid' and whuclass.t_classid=t_class.t_classid and year='$year' and t_class.teacherid=teacher.teacherid and departmentid='$departmentid'";
					$queryinfo = $db->createCommand($queryt_class)->query();
					$results = $queryinfo->readAll();
					//$results = array_unique($results);//踢出重复计算的测评班级

					$rets = array("success"=>true,"message"=>"","results"=>$results);
					echo json_encode($rets);
				}
				else//如果年级、专业号、年份中某一为空时，则取出学院全部测评班级信息
				{
					$queryt_class = "select distinct t_class.t_classid,t_name as t_classname,teachername,gcheck from t_class,whuclass,teacher,major where major.departmentid='$departmentid' and major.majorid=whuclass.majorid and whuclass.t_classid=t_class.t_classid and t_class.teacherid=teacher.teacherid and teacher.departmentid='$departmentid'";
					//$year = '';
					if(!empty($grade))
						$queryt_class = $queryt_class . " and grade = '$grade'";
					if(!empty($year))
						$queryt_class = $queryt_class . " and year = '$year'";
					if(!empty($majorid))
						$queryt_class = $queryt_class . " and majorid = '$majorid'";

					//$queryt_class = "select distinct t_class.t_classid,t_name as t_classname,teachername,gcheck from t_class,whuclass,teacher,major where //major.departmentid='$departmentid' and major.majorid=whuclass.majorid and whuclass.t_classid=t_class.t_classid and //t_class.teacherid=teacher.teacherid and teacher.departmentid='$departmentid' and t_class.year = '$year'";
					$queryinfo = $db->createCommand($queryt_class)->query();
					$results = $queryinfo->readAll();
					//$results = array_unique($results);//踢出重复计算的测评班级

					$rets = array("success"=>true,"message"=>"","results"=>$results);
					echo json_encode($rets);
				}
			}
			catch (Exception $e)
			{
				$rets = array("success"=>false,"message"=>$e->getMessage(),"results"=>array());
				echo json_encode($rets);
			}
		}
	}


	public function actionDisplayScholarByClass()//显示对应学院对应年级对应专业对应班级的全部学生的得奖情况以及基本成绩情况,接口6.3
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}
		if(!isset($_SESSION['ID']))	//如果不存在变量，咋舌说明登录超时，请重新登录
		{
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array("timeout"=>true)));
			exit();
		}
		if (isset($_SESSION["ID"]))//判定用户已登录
		{
			$departmentid = $_SESSION["departmentid"];			//通过session获取学院id

			$displayScholarByClass = isset($_POST["displayScholarByClass"]) ? $_POST["displayScholarByClass"] :$_GET["displayScholarByClass"];
			$displayScholarByClass = json_decode($displayScholarByClass,true);
			$t_classid = $displayScholarByClass["t_classid"];
			$year = $displayScholarByClass["year"];

			try
			{
				$db = Yii::app()->db;//连接mysql数据库
				
				//查询测评班级所有学生的学生id
				$querystudentid = "select distinct studentid from scholarship where studentid in (select studentid from student where classid in (select classid from whuclass,t_class where whuclass.t_classid=t_class.t_classid and whuclass.t_classid='$t_classid' and year='$year'))";
				$queryrets = $db->createCommand($querystudentid)->query();
				$studentidarray = $queryrets->readAll();
				//$studentidarray	= array_unique($studentidarray);//踢出查询中存在的重复学号

				$results = array();
				//$i = 0;
				foreach ($studentidarray as $datarow)//按照学号逐条查询学生的学号，姓名，所在班级，基本素质成绩，课程成绩，创新实践成绩，总成绩以及获得的奖学金名
				{
					$studentid = $datarow["studentid"];
					
					//查询对应学号的学生的学号，姓名，所在班级，基本素质成绩，课程成绩，创新实践成绩，总成绩
					$querybase = "select student.studentid as sid,studentname as sname,classname as cname,qualityscore as F1,gpascore as F2,selfscore as F3,total from student,grade,whuclass where student.studentid='$studentid' and grade.studentid='$studentid' and student.classid=whuclass.classid and t_classid='$t_classid' and grade.year = '$year'";
					$queryrets = $db->createCommand($querybase)->query();
					$baseinfo = $queryrets->readAll();
					
          $excellent = '优秀学生奖学金'; $excellent = iconv("gb2312", "utf-8", $excellent);
					//查询对应学号的学生的奖学金名，学生有可能会同时获得多个奖学金
					$queryscholar = "select if(type = '$excellent', concat(name,type), name) as name from scholarship,scholarshiptype where studentid='$studentid' and scholarship.scholarshipid=scholarshiptype.scholarshipid and year='$year'";
					$scholarrets = $db->createCommand($queryscholar)->query();
					$scholarinfo = $scholarrets->readAll();
					
					//将对应学号学生的奖学金名拼接为一个名称，例如，获得国家奖学金同时又获得优秀学生奖学金，那么奖学金就为"国家奖学金优秀学生奖学金"
					$scholarname = "";
					foreach ($scholarinfo as $name)
					{
						$scholarname =$scholarname.$name["name"].', ';
					}
          if($scholarname != "")  //去掉后面的逗号
            $scholarname = substr($scholarname, 0, -2);
					$scholar["scholar"] = $scholarname;

					$studentinfo = array_merge($baseinfo[0],$scholar);//将对应学号学生的所有信息汇总并存放到同一数组中
					array_push($results, $studentinfo);
					//$i++;
				}

				$rets = array("success"=>true,"message"=>"","results"=>$results);
				echo json_encode($rets);
			}
			catch (Exception $e)
			{
				$rets = array("success"=>false,"message"=>$e->getMessage(),"results"=>array());
				echo json_encode($rets);
			}
		}
	}


	public function actionDisplayScholarNumber()//接口6.4
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}
		if(!isset($_SESSION['ID']))	//如果不存在变量，咋舌说明登录超时，请重新登录
		{
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array("timeout"=>true)));
			exit();
		}
		if (isset($_SESSION["ID"]))//判定用户已登录
		{
			$departmentid = $_SESSION["departmentid"];			//通过session获取学院id

			$displayScholarNumber = isset($_POST["displayScholarNumber"]) ? $_POST["displayScholarNumber"] :$_GET["displayScholarNumber"];
			$displayScholarNumber = json_decode($displayScholarNumber,true);
			$t_classid = $displayScholarNumber["t_classid"];//测评班级号
			$year = $displayScholarNumber["year"];//获得奖学金的年份

			try
			{
				$db = Yii::app()->db;//连接mysql数据库

				//查询测评班级所有学生的学生id
				$querystudentid = "select distinct studentid from scholarship where studentid in (select studentid from student where classid in (select classid from whuclass,t_class where whuclass.t_classid=t_class.t_classid and whuclass.t_classid='$t_classid' and year='$year'))";
				$queryrets = $db->createCommand($querystudentid)->query();
				$studentidarray = $queryrets->readAll();
				//$studentidarray	= array_unique($studentidarray);//踢出查询中存在的重复学号
				//print_r($studentidarray);
				
				$countryScholarNum = 0;
				$lizhiScholarNum = 0;

				$ScholarA = 0;
				$ScholarB = 0;
				$ScholarC = 0;

				$nationscholar = "国家奖学金";
				$nationscholar = iconv("gb2312","utf-8",$nationscholar);
				$mnationscholar = "国家励志奖学金";
				$mnationscholar = iconv("gb2312","utf-8",$mnationscholar);
				$excellstudscholar = "优秀学生奖学金";
				$excellstudscholar = iconv("gb2312","utf-8",$excellstudscholar);
				$specialscholar = "专项奖学金";
				$specialscholar = iconv("gb2312","utf-8",$specialscholar);

				$Astudscholar = "甲等";
				$Astudscholar = iconv("gb2312","utf-8",$Astudscholar);
				$Bstudscholar = "乙等";
				$Bstudscholar = iconv("gb2312","utf-8",$Bstudscholar);
				$Cstudscholar = "丙等";
				$Cstudscholar = iconv("gb2312","utf-8",$Cstudscholar);

				//查询所有的专项奖学金名
				$queryspecial = "select name from scholarshiptype where type='$specialscholar' and departmentid='$departmentid'";
				$specialrets = $db->createCommand($queryspecial)->queryAll();
				$special = array();
				$i = 0;
				foreach ($specialrets as $data)
				{
					$special[$i]["schname"] = $data["name"];
					$special[$i]["num"] = 0;
					$i++;
				}

				foreach ($studentidarray as $datarow)
				{
					//查询对应学号学生获得奖学金名及类型
					$studentid = $datarow["studentid"];
					$queryscholar = "select name,type from scholarship,scholarshiptype where studentid='$studentid' and year='$year' and scholarship.scholarshipid=scholarshiptype.scholarshipid and (scholarshiptype.departmentid='$departmentid' or scholarshiptype.departmentid='1')";
					$scholarinfo = $db->createCommand($queryscholar)->queryAll();

					foreach ($scholarinfo as $value)
					{
						$scholartype = $value["type"];
						$scholarname = $value["name"];

						if ($scholartype==$nationscholar)//统计国家奖学金获得人数
						{
							$countryScholarNum++;
						}

						if ($scholartype==$mnationscholar)//统计国家励志奖学金获得人数
						{
							$lizhiScholarNum++;
						}

						if ($scholartype==$specialscholar)//统计专项奖学金得人数
						{
							for($j=0; $j<$i; $j++)
							{
								$specialname = $special[$j]["schname"];
								$num = $special[$j]["num"];
								if ($specialname==$scholarname)
								{
									$num = $num + 1;
									$special[$j]["num"] = $num;
								}
							}
						}

						if ($scholartype==$excellstudscholar)//统计优秀学生奖学金人数
						{
							if ($scholarname==$Astudscholar)//统计优秀学生奖学金甲等人数
								$ScholarA++;

							if ($scholarname==$Bstudscholar)//统计优秀学生奖学金乙等人数
								$ScholarB++;

							if ($scholarname==$Cstudscholar)//统计优秀学生奖学金丙等人数
								$ScholarC++;
						}
					}
				}
				$results["countryScholarNum"] = $countryScholarNum;
				$results["lizhiScholarNum"] = $lizhiScholarNum;
				$results["ScholarA"] = $ScholarA;
				$results["ScholarB"] = $ScholarB;
				$results["ScholarC"] = $ScholarC;
				$results["special"] = $special;
				
				$rets = array("success"=>true,"message"=>"","results"=>$results);
				echo json_encode($rets);
			}
			catch (Exception $e)
			{
				$rets = array("success"=>false,"message"=>$e->getMessage(),"results"=>array());
				echo json_encode($rets);
			}
		}
	}
	

	public function actionPassScholar()//接口6.5
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}
		if(!isset($_SESSION['ID']))	//如果不存在变量，咋舌说明登录超时，请重新登录
		{
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array("timeout"=>true)));
			exit();
		}
		if (isset($_SESSION["ID"]))//判定用户已登录
		{
			$passScholar = isset($_POST["passScholar"]) ? $_POST["passScholar"] :$_GET["passScholar"];
			$passScholar = json_decode($passScholar,true);
			
			try
			{
				$db = Yii::app()->db;

				foreach ($passScholar as $t_classid)
				{
					$updatewhuclass = "update whuclass set gradecheck=1 where t_classid='$t_classid'";//gradecheck为1时表示此班级已经通过了奖学金审核
					$db->createCommand($updatewhuclass)->execute();
          
          $updatewhuclass = "update t_class set gcheck=1 where t_classid='$t_classid'";//gcheck=1时表示此班级已经通过了奖学金审核
					$db->createCommand($updatewhuclass)->execute();
				}

				$rets = array("success"=>true,"message"=>"","results"=>array());
				echo json_encode($rets);
			}
			catch (Exception $e)
			{
				$rets = array("success"=>false,"message"=>$e->getMessage(),"results"=>array());
				echo json_encode($rets);
			}
		}
	}


	public function actionSaveScholarType()//接口12.1
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}
		if(!isset($_SESSION['ID']))	//如果不存在变量，咋舌说明登录超时，请重新登录
		{
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array("timeout"=>true)));
			exit();
		}
		if (isset($_SESSION["ID"]))//判定用户已登录
		{
			$departmentid = $_SESSION["departmentid"];//通过session获取学院id

			$saveScholarType = isset($_POST["saveScholarType"]) ? $_POST["saveScholarType"] :$_GET["saveScholarType"];
			$saveScholarType = json_decode($saveScholarType,true);

			try
			{
				$db = Yii::app()->db;

				foreach ($saveScholarType as $value)
				{
					$scholarname = $value["scholarname"];//要添加的奖学金名
					$scholartype = $value["scholartype"];//要添加的奖学金类型
					$remark = $value["remark"];//奖学金备注
					$money = $value["money"];//奖学金奖励金额

					if(Scholarshiptype::exist($scholarname, $money, $departmentid))//exists
					{
						echo json_encode(array('success'=>false, 'message'=>Scholarshiptype::$err));
						exit;
					}
					
					//添加奖学金
					$insertscholar = "insert into scholarshiptype(name,type,amount,departmentid,remark)																				values('$scholarname','$scholartype','$money','$departmentid','$remark')";
					$db->createCommand($insertscholar)->execute();
				}
	
				$rets = array("success"=>true,"message"=>"","results"=>array());
				echo json_encode($rets);
			}
			catch (Exception $e)
			{
				$rets = array("success"=>false,"message"=>$e->getMessage(),"results"=>array());
				echo json_encode($rets);
			}
		}
	}

	public function actionShowScholar()//接口12.2
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}
		if(!isset($_SESSION['ID']))	//如果不存在变量，咋舌说明登录超时，请重新登录
		{
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array("timeout"=>true)));
			exit();
		}
		if (isset($_SESSION["ID"]))//判定用户已登录
		{
			$departmentid = $_SESSION["departmentid"];//通过session获取学院id
			$scholarname = "专项奖学金";
			$scholarname = iconv("gb2312","utf-8",$scholarname);

			try
			{
				$db = Yii::app()->db;
				
				//查询学院奖学金id及名称
				$queryscholar = "select scholarshipid as scholarid,name as scholarname,amount as money,remark from scholarshiptype where											departmentid='$departmentid' and type='$scholarname'";
				$queryrets = $db->createCommand($queryscholar)->query();
				$results = $queryrets->readAll();

				$rets = array("success"=>true,"message"=>"","results"=>$results);
				echo json_encode($rets);
			}
			catch (Exception $e)
			{
				$rets = array("success"=>false,"message"=>$e->getMessage(),"results"=>array());
				echo json_encode($rets);
			}
		}
	}


	public function actionSaveEditScholarType()//接口12.3
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}
		if(!isset($_SESSION['ID']))	//如果不存在变量，咋舌说明登录超时，请重新登录
		{
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array("timeout"=>true)));
			exit();
		}
		if (isset($_SESSION["ID"]))//判定用户已登录
		{
			$departmentid = $_SESSION["departmentid"];//通过session获取学院id

			$saveEditScholarType = isset($_POST["saveEditScholarType"]) ? $_POST["saveEditScholarType"] :$_GET["saveEditScholarType"];
			$saveEditScholarType = json_decode($saveEditScholarType,true);
			$updatescholar = $saveEditScholarType["update"];//要更新的奖学金信息
			$deletescholar = $saveEditScholarType["delete1"];//要删除的奖学金信息

			try
			{
				$db = Yii::app()->db;

				if (!empty($updatescholar))
				{
					foreach ($updatescholar as $datarow)
					{
						$scholarid = $datarow["scholarid"];//奖学金id
						$scholarname = $datarow["scholarname"];//更新后的奖学金名
						$amount = $datarow["money"];//更新后的奖学金名
						$remark = $datarow["remark"];//更新后的奖学金名
	
						//更新对应奖学金id号的奖学金名
						$updatescholarshiptype = "update scholarshiptype set name='$scholarname',amount='$amount',remark='$remark' where scholarshipid='$scholarid'								and departmentid='$departmentid'";
						$db->createCommand($updatescholarshiptype)->execute();
					}
				}

				if (!empty($deletescholar))
				{
					foreach ($deletescholar as $row)
					{
						$scholarid = $row["scholarid"];//奖学金id
						
						//删除对应奖学金id号的奖学金记录
						$delscholarshiptype = "delete from scholarshiptype where scholarshipid='$scholarid' and departmentid='$departmentid'";
						$db->createCommand($delscholarshiptype)->execute();
					}
				}

				$rets = array("success"=>true,"message"=>"","results"=>array());
				echo json_encode($rets);
			}
			catch (Exception $e)
			{
				$rets = array("success"=>false,"message"=>$e->getMessage(),"results"=>array());
				echo json_encode($rets);
			}
		}
	}


}



