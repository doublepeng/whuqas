<?php
  	//header('Content-type: text/html;charset=GB2312'); 
	/** Error reporting */
	error_reporting(E_ALL);
	//设置时区为中国上海
	date_default_timezone_set('Asia/Shanghai');
	/** PHPExcel_IOFactory */
	require_once '../Classes/PHPExcel/IOFactory.php';
	/** PHPExcel */
	require_once '../Classes/PHPExcel.php';

	$downloadtable = isset($_POST["downloadtable"]) ? $_POST["downloadtable"] :$_GET["downloadtable"];
	$downloadtable = json_decode($downloadtable,true);
	$currentyear = $downloadtable["year"];

	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}
		if (isset($_SESSION["ID"]))//判定用户已登录
		{
			//$start = microtime(true);

			$encode = "set names 'utf8'";
			$con = @mysql_connect("localhost","root","root") or die("连接数据库失败！");
			@mysql_select_db("whuqas2011212", $con) or die("选择数据库失败！");
			mysql_query($encode);

						//echo 'after db '.(microtime(true)-$start).'<br/>';
			//加载模板
			$objPHPExcel = new PHPExcel();
			$objReader = PHPExcel_IOFactory::createReader('Excel5');
			$objPHPExcel = $objReader->load("templates/武汉大学所有奖学金基本情况汇总表.xls");

			$rowline = 2;
			/*$scholarships = array();
			for($i = 0; $i < 20000; $i++)
			  array_push($scholarships, array(
					'dname'=>'计算机学院',
					'sid'=>'2011282110218',
					'sname'=>'minzhenyu',
					'bankid'=>'6222023202003381127',
					'sex'=>'男',
					'name'=>'优秀学生奖学金',					
					'level'=>'甲等',		
					'amount'=>'4000'
				));*/

			
			//首先查询等级奖
			$sql = "select c.departmentname, a.studentid, b.bankid, b.studentname, b.sex, d.type as name, d.name as level, d.amount from scholarship a 
				left join scholarshiptype d on d.scholarshipid = a.scholarshipid 
				left join student b on a.studentid = b.studentid 
				left join (select departmentid, departmentname from department where departmentid != '1')c on c.departmentid = a.departmentid 
				where a.year='$currentyear' and a.scholarshipid in (1,2,3) order by a.scholarshipid, a.departmentid, a.studentid";
			$rets = @mysql_query($sql,$con) or die("查询全校等级奖基本情况失败！");
			//echo $sql;
			while ($row=mysql_fetch_array($rets))
			{
				/*array_push($scholarships, array(
					'dname'=>$row['departmentname'],
					'sid'=>$row['studentid'],
					'sname'=>$row['studentname'],
					'bankid'=>$row['bankid'],
					'sex'=>$row['sex'],
					'name'=>$row['name'],					
					'level'=>$row['level'],		
					'amount'=>$row['amount']
				));*/

								//填充学院名称
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowline, $row['departmentname']);
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowline, "'".$row['studentid']);
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowline, "'".$row['bankid']);
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowline, $row['studentname']);
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowline, $row['sex']);
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$rowline, $row['name']);
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$rowline, $row['level']);
				$objPHPExcel->getActiveSheet()->setCellValue('H'.$rowline, $row['amount']);

				$rowline++;
			}
			

			//接着查询其他奖项
			$sql = "select tmp.studentid, tmp.name, tmp.type, tmp.amount, if(d.name is null, '', d.name) as level, 
				e.departmentname, f.sex, f.bankid, f.studentname from 
				(select a.studentid, a.scholarshipid, a.departmentid, b.name, b.type, b.amount from scholarship a 
				left join scholarshiptype b on b.scholarshipid = a.scholarshipid 
				where a.year='$currentyear' and a.scholarshipid not in(1,2,3)) tmp 
				left join (select * from scholarship where year='$currentyear' and scholarshipid in(1,2,3)) c on c.studentid = tmp.studentid 
				left join (select scholarshipid, name from scholarshiptype) d on d.scholarshipid = c.scholarshipid 
				left join (select departmentid, departmentname from department)e on e.departmentid = tmp.departmentid
				left join student f on f.studentid = tmp.studentid order by tmp.type, d.scholarshipid, tmp.departmentid, tmp.studentid";
			$rets = @mysql_query($sql,$con) or die("查询全校专项、国家、励志奖学金基本情况失败！");
			//echo "<br/>".$sql;
			while ($row=mysql_fetch_array($rets))
			{
				/*array_push($scholarships, array(
					'dname'=>$row['departmentname'],
					'sid'=>$row['studentid'],
					'bankid'=>$row['bankid'],
					'sname'=>$row['studentname'],
					'sex'=>$row['sex'],
					'name'=>$row['name'],
					'level'=>$row['level'],
					'amount'=>$row['amount']
				));*/
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowline, $row['departmentname']);
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowline, "'".$row['studentid']);
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowline, "'".$row['bankid']);
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowline, $row['studentname']);
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowline, $row['sex']);
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$rowline, $row['name']);
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$rowline, $row['level']);
				$objPHPExcel->getActiveSheet()->setCellValue('H'.$rowline, $row['amount']);

				$rowline++;
			}
			//print_r($scholarships);
			@mysql_close($con) or die('关闭数据库连接失败！');

			/*foreach ($scholarships as $id => $dataRow)
			{
				//填充学院名称
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowline, $dataRow['dname']);
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowline, $dataRow['sid']);
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowline, $dataRow['bankid']);
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowline, $dataRow['sname']);
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowline, $dataRow['sex']);
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$rowline, $dataRow['name']);
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$rowline, $dataRow['level']);
				$objPHPExcel->getActiveSheet()->setCellValue('H'.$rowline, $dataRow['amount']);

				$rowline++;
			}*/
			
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save(str_replace('.php', '.xls', __FILE__));

			ob_end_clean();
			ob_start();
			
			// Redirect output to a client’s web browser (Excel5)
			header('Content-Type: application/vnd.ms-excel');
      			$filename = "武汉大学".$currentyear."年优秀学生奖学金获得者基本情况汇总表.xls";

			$encoded_filename = urlencode($filename);
			$encoded_filename = str_replace("+", "%20", $encoded_filename);
			$ua = $_SERVER["HTTP_USER_AGENT"];
			header('Content-Type: application/octet-stream');
			if (preg_match("/MSIE/", $ua)) {
			    header('Content-Disposition: attachment; filename="' . $encoded_filename . '"');
			} else if (preg_match("/Firefox/", $ua)) {
			    header('Content-Disposition: attachment; filename*="utf8\'\'' . $filename . '"');
			} else {
			    header('Content-Disposition: attachment; filename="' . $filename . '"');
			}


			//header('Content-Disposition: attachment;filename='.$filename);
			header('Cache-Control: max-age=0');
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
			exit;

		}
		else {echo '您还没有登录，请先登录系统！'; exit;}
	}

?>
