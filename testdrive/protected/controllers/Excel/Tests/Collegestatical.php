<?php
	header('Content-type: text/html;charset=GB2312'); 
	/** Error reporting */
	error_reporting(E_ALL);
	//设置时区为中国上海
	date_default_timezone_set('Asia/Shanghai');
	/** PHPExcel_IOFactory */
	require_once '../Classes/PHPExcel/IOFactory.php';
	/** PHPExcel */
	require_once '../Classes/PHPExcel.php';


	$downloadtable6 = isset($_POST["downloadtable6"]) ? $_POST["downloadtable6"] :$_GET["downloadtable6"];
	$downloadtable6 = json_decode($downloadtable6,true);
	$currentyear = $downloadtable6["year"];
	$tableid = $downloadtable6["tableid"];

	$tablename = "fTable";

	if ($tableid == $tablename)
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}

		if (isset($_SESSION["ID"]))//判定用户已登录
		{

			$encode = "set names 'utf8'";
			$con = @mysql_connect("localhost","root","root") or die("连接数据库失败！");
			@mysql_select_db("whuqas2011212", $con) or die("选择数据库失败！");
			mysql_query($encode);

			//得到全部学院的id和name列表
			$departments = array();
			$sql = "select departmentid, departmentname from department where departmentid != '1' ORDER BY departmentid";
			$rets = @mysql_query($sql,$con) or die("查询学院信息失败！");
			while ($row=mysql_fetch_array($rets))
			{
				$departments[$row["departmentid"]] = array(
					'name'=>$row["departmentname"], 
					'Alevel'=>0,'Blevel'=>0,'Clevel'=>0,'ABCSum'=>0,'ABCMoney'=>0,
					'GJAlevel'=>0, 'GJBlevel'=>0, 'GJClevel'=>0, 'GJSum'=>0, 'GJMoney'=>0,
					'LZAlevel'=>0, 'LZBlevel'=>0, 'LZClevel'=>0, 'LZSum'=>0, 'LZMoney'=>0,
					'ZXAlevel'=>0, 'ZXBlevel'=>0, 'ZXClevel'=>0, 'ZXSum'=>0, 'ZXMoney'=>0);
			}
			//var_dump($departments);

			//得到各个学院的奖学金的类型情况,得到id即可
			$youxiu = array(); $guojia = array(); $lizhi = array(); $zhuangxiang = array();
			$sql = 'select * from scholarshiptype where departmentid = 1 order by scholarshipid';
			$rets = @mysql_query($sql,$con) or die("查询优秀学生奖学金类型表格失败！");
			while ($row=mysql_fetch_array($rets))
			{
				if(isset($row['type']) && $row['type'] == '优秀学生奖学金')
					array_push($youxiu, $row['scholarshipid']);
				if(isset($row['type']) && $row['type'] == '国家奖学金')
					array_push($guojia, $row['scholarshipid']);
				if(isset($row['type']) && $row['type'] == '国家励志奖学金')
					array_push($lizhi, $row['scholarshipid']);
			}
			$youxiu = implode(',', $youxiu); if(empty($youxiu)) $youxiu = 'NULL';
			$guojia = implode(',', $guojia); if(empty($guojia)) $guojia = 'NULL';
			$lizhi = implode(',', $lizhi); if(empty($lizhi)) $lizhi = 'NULL';

			//针对各个学院开始计算奖学金的实际冲抵获得情况
			foreach($departments as $id=>$arr)
			{
				//查询等级奖的获得情况
				$sql = "select  count(b.amount) as amount, b.name as name from scholarship as a left join scholarshiptype b on a.scholarshipid = b.scholarshipid where a.year='$currentyear' and a.departmentid = '$id' and a.scholarshipid in ($youxiu) GROUP BY a.scholarshipid order by a.scholarshipid";
				$rets = @mysql_query($sql,$con) or die("计算".$arr['name']."等级奖学金实际获得情况失败！");
				while ($row=mysql_fetch_array($rets))
				{
					if(isset($row['name']) && $row['name'] == '甲等')
						$departments[$id]['Alevel'] = $row['amount'];
					if(isset($row['name']) && $row['name'] == '乙等')
						$departments[$id]['Blevel'] = $row['amount'];
					if(isset($row['name']) && $row['name'] == '丙等')
						$departments[$id]['Clevel'] = $row['amount'];
				}
				$departments[$id]['ABCSum'] = $departments[$id]['Alevel'] + $departments[$id]['Blevel'] +$departments[$id]['Clevel'];
				//查询等级奖的总金额
				$sql = "select  sum(b.amount) as amount from scholarship as a left join scholarshiptype b on a.scholarshipid = b.scholarshipid where a.year='$currentyear' and a.departmentid = '$id' and a.scholarshipid in ($youxiu) order by a.scholarshipid";
				$rets = @mysql_query($sql,$con) or die("计算".$arr['name']."等级奖学金总金额情况失败！");
				while ($row=mysql_fetch_array($rets))
				{
					if(isset($row['amount']))
						$departments[$id]['ABCMoney'] = $row['amount'];
				}

				//计算专项奖学金的冲抵情况
				$zhuanxiang = array();
				$sql = "select * from scholarshiptype where departmentid = '$id' and type='专项奖学金' order by scholarshipid";
				$rets = @mysql_query($sql,$con) or die("查询专项奖学金类型表格失败！");
				while ($row=mysql_fetch_array($rets))
				{
					if(isset($row['type']) && $row['type'] == '专项奖学金')
						array_push($zhuanxiang, $row['scholarshipid']);
				}
				$zhuanxiang = implode(',', $zhuanxiang); if(empty($zhuanxiang)) $zhuanxiang = 'NULL';

				$sql = "select count(*) as amount, c.name as name from scholarship a left join scholarship b on a.studentid = b.studentid left join scholarshiptype c on c.scholarshipid = b.scholarshipid
where a.year = '$currentyear' and a.departmentid = '$id' and a.scholarshipid in ($zhuanxiang) and a.scholarshipid != b.scholarshipid GROUP BY c.scholarshipid ORDER BY c.scholarshipid"; 
				$rets = @mysql_query($sql,$con) or die("查询专项奖学金冲抵情况失败！");
				while ($row=mysql_fetch_array($rets))
				{
					if(isset($row['name']) && $row['name'] == '甲等')
						$departments[$id]['ZXAlevel'] = $row['amount'];
					if(isset($row['name']) && $row['name'] == '乙等')
						$departments[$id]['ZXBlevel'] = $row['amount'];
					if(isset($row['name']) && $row['name'] == '丙等')
						$departments[$id]['ZXClevel'] = $row['amount'];
				}
				
				//计算专项冲抵的总钱
				$sql = "select sum(c.amount) as amount from scholarship a left join scholarship b on a.studentid = b.studentid left join scholarshiptype c on c.scholarshipid = b.scholarshipid where a.year = '$currentyear' and a.departmentid = '$id' and a.scholarshipid in ($zhuanxiang) and a.scholarshipid != b.scholarshipid";
				$rets = @mysql_query($sql,$con) or die("查询专项奖学金冲抵总额情况失败！");
				while ($row=mysql_fetch_array($rets))
				{
					if(isset($row['amount']))
						$departments[$id]['ZXMoney'] = $row['amount'];
				}

				//计算国家奖学金冲抵情况
				$sql = "select count(*) as amount, c.name as name from scholarship a left join scholarship b on a.studentid = b.studentid left join scholarshiptype c on c.scholarshipid = b.scholarshipid
where a.year = '$currentyear' and a.departmentid = '$id' and a.scholarshipid in ($guojia) and a.scholarshipid != b.scholarshipid GROUP BY c.scholarshipid ORDER BY c.scholarshipid"; 
				$rets = @mysql_query($sql,$con) or die("查询国家奖学金冲抵情况失败！");
				while ($row=mysql_fetch_array($rets))
				{
					if(isset($row['name']) && $row['name'] == '甲等')
						$departments[$id]['GJAlevel'] = $row['amount'];
					if(isset($row['name']) && $row['name'] == '乙等')
						$departments[$id]['GJBlevel'] = $row['amount'];
					if(isset($row['name']) && $row['name'] == '丙等')
						$departments[$id]['GJClevel'] = $row['amount'];
				}
				$departments[$id]['GJSum'] = $departments[$id]['GJAlevel'] + $departments[$id]['GJBlevel'] +$departments[$id]['GJClevel'];
				//计算国家冲抵的总钱
				$sql = "select sum(c.amount) as amount from scholarship a left join scholarship b on a.studentid = b.studentid left join scholarshiptype c on c.scholarshipid = b.scholarshipid where a.year = '$currentyear' and a.departmentid = '$id' and a.scholarshipid in ($guojia) and a.scholarshipid != b.scholarshipid";
				$rets = @mysql_query($sql,$con) or die("查询国家奖学金冲抵总额情况失败！");
				while ($row=mysql_fetch_array($rets))
				{
					if(isset($row['amount']))
						$departments[$id]['GJMoney'] = $row['amount'];
				}

				//计算国家励志奖学金冲抵情况
				$sql = "select count(*) as amount, c.name as name from scholarship a left join scholarship b on a.studentid = b.studentid left join scholarshiptype c on c.scholarshipid = b.scholarshipid
where a.year = '$currentyear' and a.departmentid = '$id' and a.scholarshipid in ($lizhi) and a.scholarshipid != b.scholarshipid GROUP BY c.scholarshipid ORDER BY c.scholarshipid"; 
				$rets = @mysql_query($sql,$con) or die("查询国家励志奖学金冲抵情况失败！");
				while ($row=mysql_fetch_array($rets))
				{
					if(isset($row['name']) && $row['name'] == '甲等')
						$departments[$id]['LZAlevel'] = $row['amount'];
					if(isset($row['name']) && $row['name'] == '乙等')
						$departments[$id]['LZBlevel'] = $row['amount'];
					if(isset($row['name']) && $row['name'] == '丙等')
						$departments[$id]['LZClevel'] = $row['amount'];
				}
				$departments[$id]['LZSum'] = $departments[$id]['LZAlevel'] + $departments[$id]['LZBlevel'] +$departments[$id]['LZClevel'];
				//计算国家励志冲抵的总钱
				$sql = "select sum(c.amount) as amount from scholarship a left join scholarship b on a.studentid = b.studentid left join scholarshiptype c on c.scholarshipid = b.scholarshipid where a.year = '$currentyear' and a.departmentid = '$id' and a.scholarshipid in ($lizhi) and a.scholarshipid != b.scholarshipid";
				$rets = @mysql_query($sql,$con) or die("查询国家励志奖学金冲抵总额情况失败！");
				while ($row=mysql_fetch_array($rets))
				{
					if(isset($row['amount']))
						$departments[$id]['LZMoney'] = $row['amount'];
				}
			}
			@mysql_close($con) or die('关闭数据库连接失败！');

			//print_r($departments);
			
			//echo 'after db '.(microtime(true)-$start).'<br/>';
			//加载模板
			$objPHPExcel = new PHPExcel();
			$objReader = PHPExcel_IOFactory::createReader('Excel5');
			$objPHPExcel = $objReader->load("templates/奖学金实发人数.xls");

			$baseRow = 2; $currentRow = 0; 
			//定义合计总数
			$Alevel = 0; $Blevel = 0; $Clevel = 0; $ABCSum = 0; $ABCMoney = 0;
			foreach ($departments as $id => $dataRow)
			{
				$rowline = $baseRow + $currentRow;

				//填充学院名称
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowline, $dataRow['name']);

				$iNum1 = $dataRow['Alevel'] - $dataRow['ZXAlevel'] - $dataRow['GJAlevel'] - $dataRow['LZAlevel'];
				//实际获得甲等优秀学生奖学金人数
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowline, $iNum1);

				$iNum2 = $dataRow['Blevel'] - $dataRow['ZXBlevel'] - $dataRow['GJBlevel'] - $dataRow['LZBlevel'];
				//实际获得乙等优秀学生奖学金人数
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowline, $iNum2);

				$iNum3 = $dataRow['Clevel'] - $dataRow['ZXClevel'] - $dataRow['GJClevel'] - $dataRow['LZClevel'];
				//实际获得丙等优秀学生奖学金人数
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowline, $iNum3);

				//实际获得优秀学生奖学金总人数
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowline, $iNum1+$iNum2+$iNum3);

				//实际获得优秀学生奖学金总金额
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$rowline, $dataRow['ABCMoney']-$dataRow['ZXMoney']-$dataRow['GJMoney']-$dataRow['LZMoney']);
				
				$Alevel += $iNum1; $Blevel += $iNum2; $Clevel += $iNum3; 
				$ABCSum += ($iNum1+$iNum2+$iNum3); 
				$ABCMoney += ($dataRow['ABCMoney']-$dataRow['ZXMoney']-$dataRow['GJMoney']-$dataRow['LZMoney']);
				$currentRow++;
			}

			$rowline = $rowline + 1;
			//填充合计
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowline, "合计");

			//实际获得甲等优秀学生奖学金总人数
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowline, $Alevel);

			//实际获得乙等优秀学生奖学金总人数
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowline, $Blevel);

			//实际获得丙等优秀学生奖学金总人数
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowline, $Clevel);

			//实际获得优秀学生奖学金总人数
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowline, $ABCSum);

			//实际获得优秀学生奖学金总金额
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$rowline, $ABCMoney);

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save(str_replace('.php', '.xls', __FILE__));

			ob_end_clean();
			ob_start();
			
			// Redirect output to a client’s web browser (Excel5)
			header('Content-Type: application/vnd.ms-excel');
      			$filename = "武汉大学".$currentyear."年度奖学金实发人数.xls";

			$encoded_filename = urlencode($filename);
			$encoded_filename = str_replace("+", "%20", $encoded_filename);
			$ua = $_SERVER["HTTP_USER_AGENT"];
			header('Content-Type: application/octet-stream');
			if (preg_match("/MSIE/", $ua)) {
			    header('Content-Disposition: attachment; filename="' . $encoded_filename . '"');
			} else if (preg_match("/Firefox/", $ua)) {
			    header('Content-Disposition: attachment; filename*="utf8\'\'' . $filename . '"');
			} else {
			    header('Content-Disposition: attachment; filename="' . $filename . '"');
			}


			//header('Content-Disposition: attachment;filename='.$filename);
			header('Cache-Control: max-age=0');
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
			exit;
	  	}
	}
?>
