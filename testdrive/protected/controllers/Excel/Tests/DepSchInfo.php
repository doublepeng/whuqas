<?php
	header('Content-type: text/html;charset=GB2312'); 
	/** PHPExcel_IOFactory */
	require_once '../Classes/PHPExcel/IOFactory.php';
	/** PHPExcel */
	require_once '../Classes/PHPExcel.php';
	require_once 'Tjyouxiuscholarship.php';
	
	$hostname = 'localhost';
	$user = 'root';
	$pwd = 'root';
	$encode = "set names 'utf8'";
	$dbname = 'whuqas2011212';
	
	$downloadtable = isset($_POST["downloadtable"]) ? $_POST["downloadtable"] :$_GET["downloadtable"];
	$downloadtable = json_decode($downloadtable,true);
	$year = $downloadtable["year"];//所需要下载年份的表格
	
	
	$currentyear = $year;
	$lastyear = $currentyear-1;
	//测试用例
	//$year = 2012;
	//测试结束

	if (!isset($_SESSION))session_start();//开启session
	$departmentid = $_SESSION["departmentid"];//学院id
	//测试用例
	//$departmentid = 2;

	//获取数据库连接
	$connect = mysql_connect($hostname, $user, $pwd); 
    mysql_query($encode);
    mysql_select_db($dbname); 
    
	//生成学生获奖情况汇总表
	$objPHPExcel = new PHPExcel();
	$objReader = PHPExcel_IOFactory::createReader('Excel5');
	//加载模板
	$objPHPExcel = $objReader->load("templates/武汉大学院系学生奖学金汇总表.xls");

	//查询学院名称
	$dnameSql = "select departmentname from department where departmentid='$departmentid' limit 1";
	$queryDname =  @mysql_query($dnameSql, $connect) or die("数据库未找到本学院的名称，请联系学工部修改！");;
	$rowDname = mysql_fetch_row($queryDname);
	if($rowDname[0] == false)//表示没有查询到该学院名称
	{
		echo json_encode(array('success'=>true,'message'=>"",'results'=>array()));
        exit;
	}
	$departmentname = $rowDname[0];
	//获得学院名称，用于生成表格的名字
	//生成优秀学生获奖情况汇总表
	//规定甲乙丙，国家励志，国家奖学金的id在1-5范围内
	//这里是1，2，3表示优秀学生奖学金
	$stuSql = "select student.studentid,studentname,sex,bankid,political,phone,scholarshiptype.name,scholarshiptype.type,scholarshiptype.amount
	from student,scholarship,scholarshiptype where 
	student.studentid in (select studentid from student,whuclass where student.classid = whuclass.classid
	and whuclass.majorid in (select majorid from major where departmentid = '$departmentid' ) )
	and student.studentid = scholarship.studentid and scholarship.year = '$year' and 
	scholarship.scholarshipid = scholarshiptype.scholarshipid and (scholarshiptype.scholarshipid < 4 and scholarshiptype.scholarshipid > 0)
	order by scholarshiptype.scholarshipid,student.studentid ";

	$queryStu=  @mysql_query($stuSql, $connect) or die("生成优秀学生获奖情况表失败") ;//学院相关信息
	
	//如果没有出现错误
	$baseRow = 2;
	$r = 0;
	//获取选择当前活动sheet
	$objPHPExcel->setActiveSheetIndex(0); 
	$objActSheet = $objPHPExcel->getActiveSheet(); 
	while($row = mysql_fetch_array($queryStu))
	{
		$rowline = $baseRow + $r;
		
		$studentid = $row['studentid'];
		$studentname = $row['studentname'];
		$sex = $row['sex'];
		$bankid = $row['bankid'];
		$type = $row['type'];
		$name = $row['name'];
		$amount = $row['amount'];
		$objActSheet->setCellValue('A'.$rowline, $departmentname);
		$objActSheet->setCellValue('B'.$rowline, "'".$studentid);
		$objActSheet->setCellValue('C'.$rowline, $bankid);
		$objActSheet->setCellValue('D'.$rowline, $studentname);
		$objActSheet->setCellValue('E'.$rowline, $sex);
		$objActSheet->setCellValue('F'.$rowline, $type);
		$objActSheet->setCellValue('G'.$rowline, $name);
		$objActSheet->setCellValue('H'.$rowline, $amount);
		
		$r++;

	}
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save(str_replace('.php', '.xls', __FILE__));

	
	//国家励志奖学金
	$stuSql = "select student.studentid,studentname,sex,bankid,political,phone,scholarshiptype.name,scholarshiptype.type,scholarshiptype.amount
	from student,scholarship,scholarshiptype where 
	student.studentid in (select studentid from student,whuclass where student.classid = whuclass.classid
	and whuclass.majorid in (select majorid from major where departmentid = '$departmentid' ) )
	and student.studentid = scholarship.studentid and scholarship.year = '$year' and 
	scholarship.scholarshipid = scholarshiptype.scholarshipid and (scholarshiptype.scholarshipid = 4  )
	order by scholarshiptype.scholarshipid,student.studentid ";

	$queryStu=  @mysql_query($stuSql, $connect) or die("生成优秀学生获奖情况表失败") ;//学院相关信息
	
	//如果没有出现错误
	$baseRow = 2;
	$r = 0;
	//获取选择当前活动sheet
	$objPHPExcel->setActiveSheetIndex(1); 
	$objActSheet = $objPHPExcel->getActiveSheet(); 
	while($row = mysql_fetch_array($queryStu))
	{
		$rowline = $baseRow + $r;
		
		$studentid = $row['studentid'];
		$studentname = $row['studentname'];
		$sex = $row['sex'];
		$bankid = $row['bankid'];
		$type = $row['type'];
		$name = $row['name'];
		$amount = $row['amount'];
		$objActSheet->setCellValue('A'.$rowline, $departmentname);
		$objActSheet->setCellValue('B'.$rowline, "'".$studentid);
		$objActSheet->setCellValue('C'.$rowline, $bankid);
		$objActSheet->setCellValue('D'.$rowline, $studentname);
		$objActSheet->setCellValue('E'.$rowline, $sex);
		$objActSheet->setCellValue('F'.$rowline, $type);
		$objActSheet->setCellValue('G'.$rowline, $name);
		$objActSheet->setCellValue('H'.$rowline, $amount);
		
		$r++;

	}
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save(str_replace('.php', '.xls', __FILE__));
	
	
	//国家奖学金
	$stuSql = "select student.studentid,studentname,sex,bankid,political,phone,scholarshiptype.name,scholarshiptype.type,scholarshiptype.amount
	from student,scholarship,scholarshiptype where 
	student.studentid in (select studentid from student,whuclass where student.classid = whuclass.classid
	and whuclass.majorid in (select majorid from major where departmentid = '$departmentid' ) )
	and student.studentid = scholarship.studentid and scholarship.year = '$year' and 
	scholarship.scholarshipid = scholarshiptype.scholarshipid and (scholarshiptype.scholarshipid = 5)
	order by scholarshiptype.scholarshipid,student.studentid ";

	$queryStu=  @mysql_query($stuSql, $connect) or die("生成优秀学生获奖情况表失败") ;//学院相关信息
	
	//如果没有出现错误
	$baseRow = 2;
	$r = 0;
	//获取选择当前活动sheet
	$objPHPExcel->setActiveSheetIndex(2); 
	$objActSheet = $objPHPExcel->getActiveSheet(); 
	while($row = mysql_fetch_array($queryStu))
	{
		$rowline = $baseRow + $r;
		
		$studentid = $row['studentid'];
		$studentname = $row['studentname'];
		$sex = $row['sex'];
		$bankid = $row['bankid'];
		$type = $row['type'];
		$name = $row['name'];
		$amount = $row['amount'];
		$objActSheet->setCellValue('A'.$rowline, $departmentname);
		$objActSheet->setCellValue('B'.$rowline, "'".$studentid);
		$objActSheet->setCellValue('C'.$rowline, $bankid);
		$objActSheet->setCellValue('D'.$rowline, $studentname);
		$objActSheet->setCellValue('E'.$rowline, $sex);
		$objActSheet->setCellValue('F'.$rowline, $type);
		$objActSheet->setCellValue('G'.$rowline, $name);
		$objActSheet->setCellValue('H'.$rowline, $amount);
		
		$r++;

	}
	
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save(str_replace('.php', '.xls', __FILE__));
	
	//专项国奖励志学生冲抵金额汇总表
	 $scholarname = "优秀学生奖学金";
     $Alevel = "甲等";
     $Blevel = "乙等";
     $Clevel = "丙等";
     //初始化国家、国家励志和专项奖学金人数为0
	 $nationtotal = 0;
	 $mnationtotal = 0;
	 $specialtotal = 0;
	
	 //初始化国家、国家励志和专项奖学金冲抵总金额为0
	 $nationmoney = 0;
	 $mnationmoney = 0;
	 $specialmoney = 0;
	 
	 $Aamount = 0;
	 $Bamount = 0;
	 $Camount = 0;
	 $level = "";
	 $amount = 0;
	 
	 $queryamount = "select name,amount from scholarshiptype where type='$scholarname' and departmentid= 1";//在这里查询属于学校的优秀奖学金以及金额
	 $amountrets = @mysql_query($queryamount,$connect) or die("查询学校优秀奖学金失败");
	 //统计金额
	 while ($rows=mysql_fetch_array($amountrets))
	 {
	 	$level = $rows["name"];
	 	$amount = $rows["amount"];
	 	if ($level==$Alevel)
	 	{
	 		$Aamount=$amount;//优秀学生奖学金甲等金额
	 	}
	 	if ($level==$Blevel)
	 	{
	 		$Bamount=$amount;//优秀学生奖学金乙等金额
	 	}
		if ($level==$Clevel)
		{
			$Camount=$amount;//优秀学生奖学金丙等金额
		}
	 }
	 
	mysql_close($connect);

	  //首先查询是否有该年度的奖学金成绩，没有的话那么就重新计算
	  StatisticsNums($departmentid,$year);
	 //
	// mysql_close($connect);
	 $connect = mysql_connect($hostname, $user, $pwd); 
     mysql_query($encode);
     mysql_select_db($dbname); 
     
     
	 $queryscholarship = "select * from scholarshipoffset where departmentid='$departmentid' and year='$year'";
	 $results = @mysql_query($queryscholarship,$connect)or die("查询学院冲抵表格失败！");

	 $data = array();
	 while ($rows=mysql_fetch_array($results))
	 {
		$data = array(																								
					array("0"=>$rows["Alevelspecialscholarnums"],"1"=>$rows["Alevelnationnalscholarnums"],"2"=>$rows["Alevelnationalmotivationalscholarnums"]),
					array("0"=>$rows["Blevelspecialscholarnums"],"1"=>$rows["Blevelnationnalscholarnums"],"2"=>$rows["Blevelnationalmotivationalscholarnums"]),
					array("0"=>$rows["Clevelspecialscholarnums"],"1"=>$rows["Clevelnationnalscholarnums"],"2"=>$rows["Clevelnationalmotivationalscholarnums"])
					);
		
		$nationtotal =					
			($rows["Alevelnationnalscholarnums"]+$rows["Blevelnationnalscholarnums"]+$rows["Clevelnationnalscholarnums"]);
		$mnationtotal =		
			($rows["Alevelnationalmotivationalscholarnums"]+$rows["Blevelnationalmotivationalscholarnums"]+$rows["Clevelnationalmotivationalscholarnums"]);
		$specialtotal =					
			($rows["Alevelspecialscholarnums"]+$rows["Blevelspecialscholarnums"]+$rows["Clevelspecialscholarnums"]);		
		$nationmoney =
			($rows["Alevelnationnalscholarnums"]*$Aamount+$rows["Blevelnationnalscholarnums"]*$Bamount+$rows["Clevelnationnalscholarnums"]*$Camount);
		$mnationmoney =
			($rows["Alevelnationalmotivationalscholarnums"]*$Aamount+$rows["Blevelnationalmotivationalscholarnums"]*$Bamount+
			$rows["Clevelnationalmotivationalscholarnums"]*$Camount);
		$specialmoney =
			($rows["Alevelspecialscholarnums"]*$Aamount+$rows["Blevelspecialscholarnums"]*$Bamount+
				$rows["Clevelspecialscholarnums"]*$Camount);
	}
	$name_1 = "(系)";
	$name_2 = "学年度专项、国家、励志学生奖学金冲抵优秀奖金额汇总表";
	$name_3 = "--";
	$titlename = $departmentname.$name_1.$lastyear.$name_3.$currentyear.$name_2;
	$rowline = 1;
	
	//获取选择当前活动sheet
	$objPHPExcel->setActiveSheetIndex(3); 
	$objActSheet = $objPHPExcel->getActiveSheet(); 
	$objActSheet->setCellValue('A'.$rowline, $titlename);
	$baseRow = 3;
	
	foreach($data as $r => $dataRow) 
	{
		$row = $baseRow + $r;
		$objActSheet->setCellValue('B'.$row, $dataRow["0"]);
		$objActSheet->setCellValue('C'.$row, $dataRow["1"]);
		$objActSheet->setCellValue('D'.$row, $dataRow["2"]);
	}
	
	//填充冲抵的专项、国家和国家励志奖学金的冲抵总人数
	$row = 6;
	$objActSheet->setCellValue('B'.$row, $specialtotal);
	$objActSheet->setCellValue('C'.$row, $nationtotal);
	$objActSheet->setCellValue('D'.$row, $mnationtotal);

	//填充冲抵的专项、国家和国家励志奖学金的冲抵总金额
	$row = 7;
	$objActSheet->setCellValue('B'.$row, $specialmoney);

	$objActSheet->setCellValue('C'.$row, $nationmoney);
	$objActSheet->setCellValue('D'.$row, $mnationmoney);

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save(str_replace('.php', '.xls', __FILE__));
	
	
	
	//年度专项奖学金或得这学生基本情况汇总表
	//专项奖学金汇总
	$specialname = "专项奖学金";
	$stuSql = "select student.studentid,studentname,sex,birthDate,bankid,political,phone,scholarshiptype.name,scholarshiptype.type,scholarshiptype.amount,
	whuclass.grade from student,scholarship,scholarshiptype,whuclass where 
	student.studentid in (select studentid from student,whuclass where student.classid = whuclass.classid
	and whuclass.majorid in (select majorid from major where departmentid = '$departmentid' ) )
	and student.studentid = scholarship.studentid and scholarship.year = '$year' and 
	scholarship.scholarshipid = scholarshiptype.scholarshipid and scholarshiptype.type = '$specialname'
	and student.classid = whuclass.classid
	order by scholarshiptype.scholarshipid,student.studentid ";

	$queryStu=  @mysql_query($stuSql, $connect) or die("生成年度专项奖学金学生基本情况汇总表");//学院相关信息
	
	//如果没有出现错误
	$baseRow = 2;
	$r = 0;
	//获取选择当前活动sheet
	$objPHPExcel->setActiveSheetIndex(4); 
	$objActSheet = $objPHPExcel->getActiveSheet(); 
	while($row = mysql_fetch_array($queryStu))
	{
		$rowline = $baseRow + $r;
		
		$studentid = $row['studentid'];
		$studentname = $row['studentname'];
		$sex = $row['sex'];
		$birthDate = $row['birthDate'];
		$bankid = $row['bankid'];
		$grade = $row['grade'];
		$type = $row['type'];
		$name = $row['name'];
		$amount = $row['amount'];
		$political = $row['political'];
		//根据学号和年份查询学生的专业排名
		$rankSql = "select RankMajor('$studentid','$year')as rk,total from grade where studentid = '$studentid'
		and year = '$year'";
		$RankRe=  @mysql_query($rankSql, $connect) or die("查询学号为".$studentid."的学生专业成绩及排名失败");//学院相关信息
		$rkRow = mysql_fetch_array($RankRe);
		
		$rk = $rkRow['rk'];
		$total = $rkRow['total'];
		$phone = $row['phone'];
		
		$objActSheet->setCellValue('A'.$rowline, $departmentname);
		$objActSheet->setCellValue('B'.$rowline, "'".$studentid);
		$objActSheet->setCellValue('C'.$rowline, $bankid);
		$objActSheet->setCellValue('D'.$rowline, $studentname);
		$objActSheet->setCellValue('E'.$rowline, $sex);
		$objActSheet->setCellValue('F'.$rowline, $birthDate);
		$objActSheet->setCellValue('G'.$rowline, $name);
		$objActSheet->setCellValue('H'.$rowline, $amount);
		$objActSheet->setCellValue('I'.$rowline, $grade);
		$objActSheet->setCellValue('J'.$rowline, $political);
		$objActSheet->setCellValue('K'.$rowline, $total);
		$objActSheet->setCellValue('L'.$rowline, $rk);
		$objActSheet->setCellValue('M'.$rowline, $phone);
		
		
		$r++;

	}
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save(str_replace('.php', '.xls', __FILE__));
	$objPHPExcel->setActiveSheetIndex(0); 
	mysql_close($connect);
	
	 
     
	
	
	ob_end_clean();
	ob_start();
	
	

	
	// Redirect output to a client’s web browser (Excel5)
	header('Content-Type: application/vnd.ms-excel');
    //$filename = iconv("gb2312", "utf-8", "武汉大学").$departmentname.$currentyear.iconv("gb2312", "utf-8", "年度优秀学生奖学金获得者基本情况汇总表.xls");
	$filename = "武汉大学".$departmentname.$year."院系学生奖学金情况汇总表.xls";
	header('Content-Disposition: attachment;filename='.$filename);
	header('Cache-Control: max-age=0');
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save('php://output');
	exit;
	
	
?>
