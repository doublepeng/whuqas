<?php
	/** PHPExcel_IOFactory */
	require_once '../Classes/PHPExcel/IOFactory.php';
	/** PHPExcel */
	require_once '../Classes/PHPExcel.php';
	//require_once 'Tjyouxiuscholarship.php';
	
	$hostname = 'localhost';
	$user = 'root';
	$pwd = 'root';
	$encode = "set names 'utf8'";
	$dbname = 'whuqas2011212';
	
	$downloadtable = isset($_POST["downloadtable"]) ? $_POST["downloadtable"] :$_GET["downloadtable"];
	$downloadtable = json_decode($downloadtable,true);
	$year = $downloadtable["year"];//所需要下载年份的表格

	//$year = 2012;
	$currentyear = $year;
	$lastyear = $currentyear-1;
	//测试用例
	//$year = 2012;
	//测试结束

	
	if (!isset($_SESSION))
		session_start();//开启session
	$departmentid = $_SESSION["departmentid"];//学院id
	
	//测试用例
	//$departmentid = 2;

	//获取数据库连接
	$connect = @mysql_connect($hostname, $user, $pwd) or die('连接数据库失败！'); 
    mysql_query($encode);
    mysql_select_db($dbname); 
    
	//生成学生获奖情况汇总表
	$objPHPExcel = new PHPExcel();
	$objReader = PHPExcel_IOFactory::createReader('Excel5');
	//加载模板
	$objPHPExcel = $objReader->load("templates/武汉大学院系所有奖学金情况汇总表.xls");

	//查询学院名称
	$dnameSql = "select departmentname from department where departmentid='$departmentid' limit 1";
	$queryDname =  @mysql_query($dnameSql, $connect) or die("数据库未找到本学院的名称，请联系学工部修改！");;
	$rowDname = mysql_fetch_row($queryDname);
	if($rowDname[0] == false)//表示没有查询到该学院名称
	{
		echo json_encode(array('success'=>true,'message'=>"",'results'=>array()));
        exit;
	}
	$departmentname = $rowDname[0];
	//获得学院名称，用于生成表格的名字
	//生成优秀学生获奖情况汇总表
	//规定甲乙丙，国家励志，国家奖学金的id在1-5范围内
	//这里是1，2，3表示优秀学生奖学金
	
	
	
	
	//基本情况汇总表
	//优秀学生
	$stuSql = "select a.studentid, b.bankid, b.studentname, b.sex, d.type as name,
		d.name as level, d.amount from scholarship a 
		left join scholarshiptype d on d.scholarshipid = a.scholarshipid 
		left join student b on a.studentid = b.studentid 
		where  a.scholarshipid in (1,2,3) and a.year = '$year' and a.departmentid = '$departmentid' 
		order by a.scholarshipid, a.departmentid, a.studentid";
	$queryStu=  @mysql_query($stuSql, $connect) or die("获取优秀学生基本情况汇总表失败") ;//学院相关信息
	//如果没有出现错误
	$baseRow = 2;
	$r = 0;
	//获取选择当前活动sheet 基本情况汇总表
	$objPHPExcel->setActiveSheetIndex(1); 
	$objActSheet = $objPHPExcel->getActiveSheet(); 
	while($row = mysql_fetch_array($queryStu))
	{
		$rowline = $baseRow + $r;
		
		$studentid = $row['studentid'];
		$bankid = $row['bankid'];
		$studentname = $row['studentname'];
		$sex = $row['sex'];
		$name = $row['name'];
		$level = $row['level'];
		$amount = $row['amount'];
		$objActSheet->setCellValue('A'.$rowline, $departmentname);
		$objActSheet->setCellValue('B'.$rowline, "'".$studentid);
		$objActSheet->setCellValue('C'.$rowline, "'".$bankid);
		$objActSheet->setCellValue('D'.$rowline, $studentname);
		$objActSheet->setCellValue('E'.$rowline, $sex);
		$objActSheet->setCellValue('F'.$rowline, $name);
		$objActSheet->setCellValue('G'.$rowline, $level);
		$objActSheet->setCellValue('H'.$rowline, $amount);
		
		$r++;

	}
	
	//向本表格中继续写获得国家励志专项奖学金
	$notyouxiu = "select tmp.studentid, tmp.name, tmp.type, tmp.amount, if(d.name is null, '', d.name) as level, 
				e.departmentname, f.sex, f.bankid, f.studentname from 
				(select a.studentid, a.scholarshipid, a.departmentid, b.name, b.type, b.amount from scholarship a 
				left join scholarshiptype b on b.scholarshipid = a.scholarshipid 
				where a.year='$year' and a.scholarshipid not in(1,2,3) and a.departmentid = '$departmentid' ) tmp 
				left join (select * from scholarship where year='$year' and scholarshipid in(1,2,3)) c on c.studentid = tmp.studentid 
				left join (select scholarshipid, name from scholarshiptype) d on d.scholarshipid = c.scholarshipid 
				left join (select departmentid, departmentname from department)e on e.departmentid = tmp.departmentid
				left join student f on f.studentid = tmp.studentid order by tmp.type, d.scholarshipid, tmp.departmentid, tmp.studentid";
	$queryStu=  @mysql_query($notyouxiu, $connect) or die("生成优秀学生获奖情况表失败2") ;//学院相关信息
	//获得表格写进去位置
	$currentRow = $rowline + 1;//当前位置下一个
	$r = 0;
	//写入
	while($row = mysql_fetch_array($queryStu))
	{
		
		$rowline = $currentRow + $r;
		
		$studentid = $row['studentid'];
		$bankid = $row['bankid'];
		$studentname = $row['studentname'];
		$sex = $row['sex'];
		$name = $row['name'];
		$level = $row['level'];
		$amount = $row['amount'];
		$objActSheet->setCellValue('A'.$rowline, $departmentname);
		$objActSheet->setCellValue('B'.$rowline, "'".$studentid);
		$objActSheet->setCellValue('C'.$rowline, "'".$bankid);
		$objActSheet->setCellValue('D'.$rowline, $studentname);
		$objActSheet->setCellValue('E'.$rowline, $sex);
		$objActSheet->setCellValue('F'.$rowline, $name);
		$objActSheet->setCellValue('G'.$rowline, $level);
		$objActSheet->setCellValue('H'.$rowline, $amount);
		
		$r++;

	}
	
	//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	//$objWriter->save(str_replace('.php', '.xls', __FILE__));
	
	//等级奖汇总表
	$stuSql = "select a.studentid, b.bankid, b.studentname, b.sex, d.type as name,
		d.name as level, d.amount from scholarship a 
		left join scholarshiptype d on d.scholarshipid = a.scholarshipid 
		left join student b on a.studentid = b.studentid 
		where  a.scholarshipid in (1,2,3) and a.year = '$year' and a.departmentid = '$departmentid' 
		order by a.scholarshipid, a.departmentid, a.studentid";

	$queryStu=  @mysql_query($stuSql, $connect) or die("生成优秀学生获奖情况表失败3") ;//学院相关信息
	
	//如果没有出现错误
	$baseRow = 2;
	$r = 0;
	//获取选择当前活动sheet
	$objPHPExcel->setActiveSheetIndex(2); 
	$objActSheet = $objPHPExcel->getActiveSheet(); 
	while($row = mysql_fetch_array($queryStu))
	{
		$rowline = $baseRow + $r;
		
		$studentid = $row['studentid'];
		$bankid = $row['bankid'];
		$studentname = $row['studentname'];
		$sex = $row['sex'];
		$name = $row['name'];
		$level = $row['level'];
		$amount = $row['amount'];
		$objActSheet->setCellValue('A'.$rowline, $departmentname);
		$objActSheet->setCellValue('B'.$rowline, "'".$studentid);
		$objActSheet->setCellValue('C'.$rowline, "'".$bankid);
		$objActSheet->setCellValue('D'.$rowline, $studentname);
		$objActSheet->setCellValue('E'.$rowline, $sex);
		$objActSheet->setCellValue('F'.$rowline, $name);
		$objActSheet->setCellValue('G'.$rowline, $level);
		$objActSheet->setCellValue('H'.$rowline, $amount);
		
		$r++;

	}
	
	
	//专项奖学金  除了1，2，3，4，5
	$stuSql = "select tmp.studentid, tmp.name, tmp.type, tmp.amount, if(d.name is null, '', d.name) as level, 
				e.departmentname, f.sex, f.bankid, f.studentname from 
				(select a.studentid, a.scholarshipid, a.departmentid, b.name, b.type, b.amount from scholarship a 
				left join scholarshiptype b on b.scholarshipid = a.scholarshipid 
				where a.year='$year' and a.scholarshipid not in (1,2,3,4,5) and a.departmentid = '$departmentid' ) tmp 
				left join (select * from scholarship where year='$year' and scholarshipid in(1,2,3)) c on c.studentid = tmp.studentid 
				left join (select scholarshipid, name from scholarshiptype) d on d.scholarshipid = c.scholarshipid 
				left join (select departmentid, departmentname from department)e on e.departmentid = tmp.departmentid
				left join student f on f.studentid = tmp.studentid order by tmp.type, d.scholarshipid, tmp.departmentid, tmp.studentid";
	$queryStu=  @mysql_query($stuSql, $connect) or die("生成优秀学生获奖情况表失败4") ;//学院相关信息
	
	//如果没有出现错误
	$baseRow = 2;
	$r = 0;
	//获取选择当前活动sheet
	$objPHPExcel->setActiveSheetIndex(3); 
	$objActSheet = $objPHPExcel->getActiveSheet(); 
	while($row = mysql_fetch_array($queryStu))
	{
		$rowline = $baseRow + $r;
		
		$studentid = $row['studentid'];
		$bankid = $row['bankid'];
		$studentname = $row['studentname'];
		$sex = $row['sex'];
		$name = $row['name'];
		$level = $row['level'];
		$amount = $row['amount'];
		$objActSheet->setCellValue('A'.$rowline, $departmentname);
		$objActSheet->setCellValue('B'.$rowline, "'".$studentid);
		$objActSheet->setCellValue('C'.$rowline, "'".$bankid);
		$objActSheet->setCellValue('D'.$rowline, $studentname);
		$objActSheet->setCellValue('E'.$rowline, $sex);
		$objActSheet->setCellValue('F'.$rowline, $name);
		$objActSheet->setCellValue('G'.$rowline, $level);
		$objActSheet->setCellValue('H'.$rowline, $amount);
		
		$r++;

	}

		
	//国家汇总表id为4
	$stuSql = "select tmp.studentid, tmp.name, tmp.type, tmp.amount, if(d.name is null, '', d.name) as level, 
				e.departmentname, f.sex, f.bankid, f.studentname from 
				(select a.studentid, a.scholarshipid, a.departmentid, b.name, b.type, b.amount from scholarship a 
				left join scholarshiptype b on b.scholarshipid = a.scholarshipid 
				where a.year='$year' and a.scholarshipid = 4 and a.departmentid = '$departmentid' ) tmp 
				left join (select * from scholarship where year='$year' and scholarshipid in(1,2,3)) c on c.studentid = tmp.studentid 
				left join (select scholarshipid, name from scholarshiptype) d on d.scholarshipid = c.scholarshipid 
				left join (select departmentid, departmentname from department)e on e.departmentid = tmp.departmentid
				left join student f on f.studentid = tmp.studentid order by tmp.type, d.scholarshipid, tmp.departmentid, tmp.studentid";

	$queryStu=  @mysql_query($stuSql, $connect) or die("生成优秀学生获奖情况表失败") ;//学院相关信息
	
	//如果没有出现错误
	$baseRow = 2;
	$r = 0;
	//获取选择当前活动sheet
	$objPHPExcel->setActiveSheetIndex(4); 
	$objActSheet = $objPHPExcel->getActiveSheet(); 
	while($row = mysql_fetch_array($queryStu))
	{
		$rowline = $baseRow + $r;
		
		$studentid = $row['studentid'];
		$bankid = $row['bankid'];
		$studentname = $row['studentname'];
		$sex = $row['sex'];
		$name = $row['name'];
		$level = $row['level'];
		$amount = $row['amount'];
		$objActSheet->setCellValue('A'.$rowline, $departmentname);
		$objActSheet->setCellValue('B'.$rowline, "'".$studentid);
		$objActSheet->setCellValue('C'.$rowline, "'".$bankid);
		$objActSheet->setCellValue('D'.$rowline, $studentname);
		$objActSheet->setCellValue('E'.$rowline, $sex);
		$objActSheet->setCellValue('F'.$rowline, $name);
		$objActSheet->setCellValue('G'.$rowline, $level);
		$objActSheet->setCellValue('H'.$rowline, $amount);
		
		$r++;
	}
	
	//国家励志汇总表 id为5
	$stuSql = "select tmp.studentid, tmp.name, tmp.type, tmp.amount, if(d.name is null, '', d.name) as level, 
				e.departmentname, f.sex, f.bankid, f.studentname from 
				(select a.studentid, a.scholarshipid, a.departmentid, b.name, b.type, b.amount from scholarship a 
				left join scholarshiptype b on b.scholarshipid = a.scholarshipid 
				where a.year='$year' and a.scholarshipid = 5 and a.departmentid = '$departmentid' ) tmp 
				left join (select * from scholarship where year='$year' and scholarshipid in(1,2,3)) c on c.studentid = tmp.studentid 
				left join (select scholarshipid, name from scholarshiptype) d on d.scholarshipid = c.scholarshipid 
				left join (select departmentid, departmentname from department)e on e.departmentid = tmp.departmentid
				left join student f on f.studentid = tmp.studentid order by tmp.type, d.scholarshipid, tmp.departmentid, tmp.studentid";

	$queryStu=  @mysql_query($stuSql, $connect) or die("生成优秀学生获奖情况表失败") ;//学院相关信息
	
	//如果没有出现错误
	$baseRow = 2;
	$r = 0;
	//获取选择当前活动sheet
	$objPHPExcel->setActiveSheetIndex(5); 
	$objActSheet = $objPHPExcel->getActiveSheet(); 
	while($row = mysql_fetch_array($queryStu))
	{
		$rowline = $baseRow + $r;
		
		$studentid = $row['studentid'];
		$bankid = $row['bankid'];
		$studentname = $row['studentname'];
		$sex = $row['sex'];
		$name = $row['name'];
		$level = $row['level'];
		$amount = $row['amount'];
		$objActSheet->setCellValue('A'.$rowline, $departmentname);
		$objActSheet->setCellValue('B'.$rowline, "'".$studentid);
		$objActSheet->setCellValue('C'.$rowline, "'".$bankid);
		$objActSheet->setCellValue('D'.$rowline, $studentname);
		$objActSheet->setCellValue('E'.$rowline, $sex);
		$objActSheet->setCellValue('F'.$rowline, $name);
		$objActSheet->setCellValue('G'.$rowline, $level);
		$objActSheet->setCellValue('H'.$rowline, $amount);
		
		$r++;

	}
	
	

	$departments  = array(
					'GJAlevel'=>0, 'GJBlevel'=>0, 'GJClevel'=>0, 'GJSum'=>0, 'GJMoney'=>0,
					'LZAlevel'=>0, 'LZBlevel'=>0, 'LZClevel'=>0, 'LZSum'=>0, 'LZMoney'=>0,
					'ZXAlevel'=>0, 'ZXBlevel'=>0, 'ZXClevel'=>0, 'ZXSum'=>0, 'ZXMoney'=>0);

	//计算专项奖学金的冲抵情况
	$zhuanxiang = array();
	$sql = "select * from scholarshiptype where departmentid = '$departmentid' and type='专项奖学金' order by scholarshipid";
	//echo '<br/>'.$sql;
	$rets = @mysql_query($sql,$connect) or die("查询专项奖学金类型表格失败！");
	while ($row=mysql_fetch_array($rets))
	{
		if(isset($row['type']) && $row['type'] == '专项奖学金')
			array_push($zhuanxiang, $row['scholarshipid']);
	}
	$zhuanxiang = implode(',', $zhuanxiang); if(empty($zhuanxiang)) $zhuanxiang = 'NULL';

	$sql = "select count(*) as amount, c.name as name from scholarship a 
		left join scholarship b on a.studentid = b.studentid 
		left join scholarshiptype c on c.scholarshipid = b.scholarshipid
		where a.year = '$currentyear' and a.departmentid = '$departmentid' 
		and a.scholarshipid in ($zhuanxiang) and a.scholarshipid != b.scholarshipid 
		GROUP BY c.scholarshipid ORDER BY c.scholarshipid"; 
	$rets = @mysql_query($sql,$connect) or die("查询专项奖学金冲抵情况失败！");
	while ($row=mysql_fetch_array($rets))
	{
		if(isset($row['name']) && $row['name'] == '甲等')
			$departments['ZXAlevel'] = $row['amount'];
		if(isset($row['name']) && $row['name'] == '乙等')
			$departments['ZXBlevel'] = $row['amount'];
		if(isset($row['name']) && $row['name'] == '丙等')
			$departments['ZXClevel'] = $row['amount'];
	}
	$departments['ZXSum'] = $departments['ZXAlevel'] + $departments['ZXBlevel'] +$departments['ZXClevel'];
	//计算专项冲抵的总钱
	$sql = "select sum(c.amount) as amount from scholarship a left join scholarship b on a.studentid = b.studentid 
		left join scholarshiptype c on c.scholarshipid = b.scholarshipid 
		where a.year = '$currentyear' and a.departmentid = '$departmentid' 
		and a.scholarshipid in ($zhuanxiang) and a.scholarshipid != b.scholarshipid";
	$rets = @mysql_query($sql,$connect) or die("查询专项奖学金冲抵总额情况失败！");
	while ($row=mysql_fetch_array($rets))
	{
		if(isset($row['amount']))
			$departments['ZXMoney'] = $row['amount'];
	}

	//计算国家奖学金冲抵情况
	$sql = "select count(*) as amount, c.name as name from scholarship a 
		left join scholarship b on a.studentid = b.studentid 
		left join scholarshiptype c on c.scholarshipid = b.scholarshipid
		where a.year = '$currentyear' and a.departmentid = '$departmentid' 
		and a.scholarshipid in (5) and a.scholarshipid != b.scholarshipid 
		GROUP BY c.scholarshipid ORDER BY c.scholarshipid"; 
	$rets = @mysql_query($sql,$connect) or die("查询国家奖学金冲抵情况失败！");
	while ($row=mysql_fetch_array($rets))
	{
		if(isset($row['name']) && $row['name'] == '甲等')
			$departments['GJAlevel'] = $row['amount'];
		if(isset($row['name']) && $row['name'] == '乙等')
			$departments['GJBlevel'] = $row['amount'];
		if(isset($row['name']) && $row['name'] == '丙等')
			$departments['GJClevel'] = $row['amount'];
	}
	$departments['GJSum'] = $departments['GJAlevel'] + $departments['GJBlevel'] +$departments['GJClevel'];
	//计算国家冲抵的总钱
	$sql = "select sum(c.amount) as amount from scholarship a 
		left join scholarship b on a.studentid = b.studentid 
		left join scholarshiptype c on c.scholarshipid = b.scholarshipid 
		where a.year = '$currentyear' and a.departmentid = '$departmentid' 
		and a.scholarshipid in (5) and a.scholarshipid != b.scholarshipid";
	$rets = @mysql_query($sql,$connect) or die("查询国家奖学金冲抵总额情况失败！");
	while ($row=mysql_fetch_array($rets))
	{
		if(isset($row['amount']))
			$departments['GJMoney'] = $row['amount'];
	}

	//计算国家励志奖学金冲抵情况
	$sql = "select count(*) as amount, c.name as name from scholarship a 
		left join scholarship b on a.studentid = b.studentid 
		left join scholarshiptype c on c.scholarshipid = b.scholarshipid
		where a.year = '$currentyear' and a.departmentid = '$departmentid' 
		and a.scholarshipid in (4) and a.scholarshipid != b.scholarshipid 
		GROUP BY c.scholarshipid ORDER BY c.scholarshipid"; 
	$rets = @mysql_query($sql,$connect) or die("查询国家励志奖学金冲抵情况失败！");
	while ($row=mysql_fetch_array($rets))
	{
		if(isset($row['name']) && $row['name'] == '甲等')
			$departments['LZAlevel'] = $row['amount'];
		if(isset($row['name']) && $row['name'] == '乙等')
			$departments['LZBlevel'] = $row['amount'];
		if(isset($row['name']) && $row['name'] == '丙等')
			$departments['LZClevel'] = $row['amount'];
	}
	$departments['LZSum'] = $departments['LZAlevel'] + $departments['LZBlevel'] +$departments['LZClevel'];
	//计算国家励志冲抵的总钱
	$sql = "select sum(c.amount) as amount from scholarship a 
		left join scholarship b on a.studentid = b.studentid 
		left join scholarshiptype c on c.scholarshipid = b.scholarshipid 
		where a.year = '$currentyear' and a.departmentid = '$departmentid' and a.scholarshipid in (4) and a.scholarshipid != b.scholarshipid";
	$rets = @mysql_query($sql,$connect) or die("查询国家励志奖学金冲抵总额情况失败！");
	while ($row=mysql_fetch_array($rets))
	{
		if(isset($row['amount']))
			$departments['LZMoney'] = $row['amount'];
	}
	//开始写入数据
	//如果没有出现错误
	$name_1 = "(系)";
	$name_2 = "学年度专项、国家、励志学生奖学金冲抵优秀奖金额汇总表";
	$name_3 = "--";
	$titlename = $departmentname.$name_1.($currentyear-1).$name_3.$currentyear.$name_2;
	$rowline = 1;
	
	//获取选择当前活动sheet
	$objPHPExcel->setActiveSheetIndex(6); 
	$objActSheet = $objPHPExcel->getActiveSheet(); 
	$objActSheet->setCellValue('A'.$rowline, $titlename);
	
	$rowline = 3;

	//获取选择当前活动sheet
	$objPHPExcel->setActiveSheetIndex(6); 
	$objActSheet = $objPHPExcel->getActiveSheet(); 
	$objActSheet->setCellValue('B'.$rowline, $departments['ZXAlevel']);
	$objActSheet->setCellValue('C'.$rowline, $departments['GJAlevel']);
	$objActSheet->setCellValue('D'.$rowline, $departments['LZAlevel']);
	$rowline++;
	
	$objActSheet->setCellValue('B'.$rowline, $departments['ZXBlevel']);
	$objActSheet->setCellValue('C'.$rowline, $departments['GJBlevel']);
	$objActSheet->setCellValue('D'.$rowline, $departments['LZBlevel']);
	$rowline++;
	
	$objActSheet->setCellValue('B'.$rowline, $departments['ZXClevel']);
	$objActSheet->setCellValue('C'.$rowline, $departments['GJClevel']);
	$objActSheet->setCellValue('D'.$rowline, $departments['LZClevel']);
	$rowline++;
	
	$objActSheet->setCellValue('B'.$rowline, $departments['ZXSum']);
	$objActSheet->setCellValue('C'.$rowline, $departments['GJSum']);
	$objActSheet->setCellValue('D'.$rowline, $departments['LZSum']);
	$rowline++;
	
	$objActSheet->setCellValue('B'.$rowline, $departments['ZXMoney']);
	$objActSheet->setCellValue('C'.$rowline, $departments['GJMoney']);
	$objActSheet->setCellValue('D'.$rowline, $departments['LZMoney']);

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save(str_replace('.php', '.xls', __FILE__));

	
	$objPHPExcel->setActiveSheetIndex(0); //跳到sheet1
	@mysql_close($connect) or die('关闭数据库连接失败！');
	
	ob_end_clean();
	ob_start();
	
	// Redirect output to a client’s web browser (Excel5)
	header('Content-Type: application/vnd.ms-excel');
	
	$filename = "武汉大学".$departmentname.$year."年院系学生奖学金情况汇总表.xls";
	$encoded_filename = urlencode($filename);
	$encoded_filename = str_replace("+", "%20", $encoded_filename);
	$ua = $_SERVER["HTTP_USER_AGENT"];
	header('Content-Type: application/octet-stream');
	if (preg_match("/MSIE/", $ua)) {
		header('Content-Disposition: attachment; filename="' . $encoded_filename . '"');
	} else if (preg_match("/Firefox/", $ua)) {
		header('Content-Disposition: attachment; filename*="utf8\'\'' . $filename . '"');
	} else {
		header('Content-Disposition: attachment; filename="' . $filename . '"');
	}
			
	header('Cache-Control: max-age=0');
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save('php://output');
	exit;
	
?>
