<?php 
  header('Content-type: text/html;charset=GB2312'); 
	/** Error reporting */
	error_reporting(E_ALL);
	//设置时区为中国上海
	date_default_timezone_set('Asia/Shanghai');
	/** PHPExcel_IOFactory */
	require_once '../Classes/PHPExcel/IOFactory.php';
	/** PHPExcel */
	require_once '../Classes/PHPExcel.php';

	$downloadtable2 = isset($_POST["downloadtable2"]) ? $_POST["downloadtable2"] :$_GET["downloadtable2"];
	$downloadtable2 = json_decode($downloadtable2,true);
	$currentyear = $downloadtable2["year"];
	$tableid = $downloadtable2["tableid"];

	$tablename = "bTable";
	//$tablename = iconv("gb2312","utf-8",$tablename);

	$scholarname = "优秀学生奖学金";
	//$scholarname = iconv("gb2312","utf-8",$scholarname);
  
  //var_dump($downloadtable2);
	if ($tableid == $tablename)
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}

		if (isset($_SESSION["ID"]))//判定用户已登录
		{
			$departmentid = $_SESSION["departmentid"];//学院id
			$encode = "set names 'utf8'";
			$con = @mysql_connect("localhost","root","root") or die("连接数据库失败！");
			@mysql_select_db("whuqas2011212", $con) or die("选择数据库失败！");
			mysql_query($encode);

			// Create new PHPExcel object
			$objPHPExcel = new PHPExcel();
			$objReader = PHPExcel_IOFactory::createReader('Excel5');
			$objPHPExcel = $objReader->load("templates/武汉大学年度优秀学生奖学金获得者基本情况汇总表.xls");
			
			//查询学院名称
			$querydepartment = "select departmentname from department where departmentid='$departmentid' limit 1";
			$departmentrets = @mysql_query($querydepartment,$con) or die("查询学院名称失败！");
			$departmentname = "";
			while ($R=mysql_fetch_array($departmentrets))
			{
				$departmentname = $R["departmentname"];
			}
			//echo $departmentname;
			//查询学院所有的年级专业的班级id
			$i = 0;
			$queryclass = "select classid from whuclass,major where major.majorid=whuclass.majorid and departmentid='$departmentid'";
			$classrets = @mysql_query($queryclass,$con) or die("查询学院所有年级、专业的班级信息失败！");
			$classidarr = array();
			while ($row=mysql_fetch_array($classrets))
			{
				$classidarr[$i] = $row["classid"];//将班级号存储在数组中
				$i++;
			}
			//var_dump($classidarr);
			$j = 0;
			$allstudentinfo = array();
			foreach ($classidarr as $classid)//以班级单位
			{
				$querystudentinfo = "select studentid,studentname,bankid from student where classid='$classid'";
        //echo $querystudentinfo;

				$studentrets = @mysql_query($querystudentinfo,$con) or die("查询班级id".$classid."的学生信息失败！");
				while ($Row=mysql_fetch_array($studentrets))
				{
					$allstudentinfo[$j]["studentid"] = $Row["studentid"];
					$allstudentinfo[$j]["studentname"] = $Row["studentname"];
					$allstudentinfo[$j]["bankid"] = $Row["bankid"];
					$j++;
				}
			}
     // var_dump($allstudentinfo);
			
			$baseRow = 2;
			$r = 0;
			foreach ($allstudentinfo as $datarow)
			{
				$studentid = $datarow["studentid"];
				$studentname = $datarow["studentname"];
				$bankid = $datarow["bankid"];

				$queryscholar = "select name,amount,type from scholarship,scholarshiptype where studentid='$studentid' and year='$currentyear' and												scholarship.scholarshipid=scholarshiptype.scholarshipid and departmentid='$departmentid' and type='$scholarname'";
				$scholarrets = @mysql_query($queryscholar,$con) or die("查询学生学号为".$studentid."的学生基本信息失败！");
        //echo $queryscholar;
				while ($scholarinfo=mysql_fetch_array($scholarrets))
				{
					$rowline = $baseRow + $r;
					$name = $scholarinfo["name"];
					$amount = $scholarinfo["amount"];
					$type = $scholarinfo["type"];
						
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowline, $departmentname);
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowline, "'".$studentid);
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowline, $bankid);
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowline, $studentname);
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$rowline, $type);
					$objPHPExcel->getActiveSheet()->setCellValue('G'.$rowline, $name);
					$objPHPExcel->getActiveSheet()->setCellValue('H'.$rowline, $amount);

					$r++;
				}
			}

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save(str_replace('.php', '.xls', __FILE__));

			mysql_close($con);

			ob_end_clean();
			ob_start();

			// Redirect output to a client’s web browser (Excel5)
			header('Content-Type: application/vnd.ms-excel');
      			//$filename = iconv("gb2312", "utf-8", "武汉大学").$departmentname.$currentyear.iconv("gb2312", "utf-8", "年度优秀学生奖学金获得者基本情况汇总表.xls");
			$filename = "武汉大学".$departmentname.$currentyear."年度优秀学生奖学金获得者基本情况汇总表.xls";
 //edited by pengchao 2012.3.6
     // $filename = iconv("gb2312","utf-8",$filename);
			header('Content-Disposition: attachment;filename='.$filename);
			header('Cache-Control: max-age=0');

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
			exit;
		}
	}
?>
