<?php
  header('Content-type: text/html;charset=GB2312'); 
	/** Error reporting */
	error_reporting(E_ALL);
	//设置时区为中国上海
	date_default_timezone_set('Asia/Shanghai');
	/** PHPExcel_IOFactory */
	require_once '../Classes/PHPExcel/IOFactory.php';
	/** PHPExcel */
	require_once '../Classes/PHPExcel.php';

	$downloadtable4 = isset($_POST["downloadtable4"]) ? $_POST["downloadtable4"] :$_GET["downloadtable4"];
	$downloadtable4 = json_decode($downloadtable4,true);
	$currentyear = $downloadtable4["year"];
	$tableid = $downloadtable4["tableid"];

	$tablename = "dTable";
	//$tablename = iconv("gb2312","utf-8",$tablename);

	$scholarname = "专项奖学金";
	//$scholarname = iconv("gb2312","utf-8",$scholarname);

	if ($tableid == $tablename)
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}

		if (isset($_SESSION["ID"]))//判定用户已登录
		{
			$departmentid = $_SESSION["departmentid"];//学院id

			$encode = "set names 'utf8'";
			$con = mysql_connect("localhost","root","root");
			mysql_select_db("whuqas2011212", $con);
			mysql_query($encode);

			// Create new PHPExcel object
			$objPHPExcel = new PHPExcel();
			$objReader = PHPExcel_IOFactory::createReader('Excel5');
			$objPHPExcel = $objReader->load("templates/武汉大学年度专项奖学金获得者学生基本情况汇总表.xls");
			
			//查询学院名称
			$querydepartment = "select departmentname from department where departmentid='$departmentid'";
			$departmentrets = @mysql_query($querydepartment,$con) or die("查询学院名称失败！");
			$departmentname = "";
			while ($R=mysql_fetch_array($departmentrets))
			{
				$departmentname = $R["departmentname"];
			}

			//查询学院所有的年级专业的班级id
			$i = 0;
			$queryclass = "select classid from whuclass,major where major.majorid=whuclass.majorid and departmentid='$departmentid'";
			$classrets = @mysql_query($queryclass,$con)or die("查询学院所有年级所有专业的班级信息失败！");
			$classidarr = array();
			while ($row=mysql_fetch_array($classrets))
			{
				$classidarr[$i] = $row["classid"];//将班级号存储在数组中
				$i++;
			}
						
			$j = 0;
			$allstudentinfo = array();
			foreach ($classidarr as $classid)//以班级单位
			{
				$querystudentinfo = "select studentid,studentname,bankid,grade,political,phone from student where classid='$classid'";

				$studentrets = @mysql_query($querystudentinfo,$con) or die("查询班级id为".$classid."的学生基本信息失败！");
				while ($Row=mysql_fetch_array($studentrets))
				{
					$allstudentinfo[$j]["studentid"] = $Row["studentid"];
					$allstudentinfo[$j]["studentname"] = $Row["studentname"];
					$allstudentinfo[$j]["bankid"] = $Row["bankid"];
					$allstudentinfo[$j]["grade"] = $Row["grade"];
					$allstudentinfo[$j]["political"] = $Row["political"];
					$allstudentinfo[$j]["phone"] = $Row["phone"];
					$j++;
				}
			}

			$baseRow = 2;
			$r = 0;
			foreach ($allstudentinfo as $datarow)
			{
				$studentid = $datarow["studentid"];
				$studentname = $datarow["studentname"];
				$bankid = $datarow["bankid"];
				$grade = $datarow["grade"];
				$political = $datarow["political"];
				$phone = $datarow["phone"];

				$queryclass = "select classid  from student where studentid='$studentid'";
				$classrets = @mysql_query($queryclass,$con) or die("查询学号为".$studentid."的学生班级号失败！");
				while ($ROW=mysql_fetch_array($classrets))
				{
					$id = $ROW["classid"];
				}

				$querymajorname = "select majorname  from major,whuclass where departmentid='$departmentid'and classid='$id' and whuclass.majorid=major.majorid";
				$majornamerets = @mysql_query($querymajorname,$con) or die("查询学院的专业名称信息失败！");
				while ($majorinfo=mysql_fetch_array($majornamerets))
				{
					$majorname = $majorinfo["majorname"];
				}
				$major_grade = $majorname.$grade;
				
				$queryscholar = "select name,amount from scholarship,scholarshiptype where studentid='$studentid' and year='$currentyear' and											scholarship.scholarshipid=scholarshiptype.scholarshipid and departmentid='$departmentid' and type='$scholarname'";
				$scholarrets = @mysql_query($queryscholar,$con) or die("查询学生的获奖情况失败！");
				while ($scholarinfo=mysql_fetch_array($scholarrets))
				{
					$rowline = $baseRow + $r;
					$name = $scholarinfo["name"];
					$amount = $scholarinfo["amount"];
						
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowline, $departmentname);
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowline, $studentid);
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowline, $bankid);
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowline, $studentname);
					$objPHPExcel->getActiveSheet()->setCellValue('G'.$rowline, $name);
					$objPHPExcel->getActiveSheet()->setCellValue('H'.$rowline, $amount);
					$objPHPExcel->getActiveSheet()->setCellValue('I'.$rowline, $major_grade);
					$objPHPExcel->getActiveSheet()->setCellValue('J'.$rowline, $political);
					$objPHPExcel->getActiveSheet()->setCellValue('M'.$rowline, $phone);

					$r++;
				}
			}

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save(str_replace('.php', '.xls', __FILE__));

			mysql_close($con);

			ob_end_clean();
			ob_start();

			// Redirect output to a client’s web browser (Excel5)
			header('Content-Type: application/vnd.ms-excel');
      			//$filename = iconv("gb2312", "utf-8", "武汉大学").$departmentname.$currentyear.iconv("gb2312", "utf-8", "年度专项奖学金获得者学生基本情况汇总表.xls");
			$filename = "武汉大学".$departmentname.$currentyear."年度专项奖学金获得者学生基本情况汇总表.xls";
       //edited by pengchao 2012.3.6
      //$filename = iconv("utf-8","gb2312",$filename);

			header('Content-Disposition: attachment;filename='.$filename);
			header('Cache-Control: max-age=0');

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
			exit;
		}
	}

?>
