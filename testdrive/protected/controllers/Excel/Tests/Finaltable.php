<?php
  	//header('Content-type: text/html;charset=GB2312'); 
	/** Error reporting */
	error_reporting(E_ALL);
	//设置时区为中国上海
	date_default_timezone_set('Asia/Shanghai');
	/** PHPExcel_IOFactory */
	require_once '../Classes/PHPExcel/IOFactory.php';
	/** PHPExcel */
	require_once '../Classes/PHPExcel.php';

	$downloadtable5 = isset($_POST["downloadtable5"]) ? $_POST["downloadtable5"] :$_GET["downloadtable5"];
	$downloadtable5 = json_decode($downloadtable5,true);
	$currentyear = $downloadtable5["year"];
	$tableid = $downloadtable5["tableid"];
    
	$tablename = "eTable";

	if ($tableid == $tablename)
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}
		if (isset($_SESSION["ID"]))//判定用户已登录
		{
			//$start = microtime(true);

			$encode = "set names 'utf8'";
			$con = @mysql_connect("localhost","root","root") or die("连接数据库失败！");
			@mysql_select_db("whuqas2011212", $con) or die("选择数据库失败！");
			mysql_query($encode);

			//得到全部学院的参评人数和学院的id和name列表（参评人数，只考虑当前年份以前的三届学生）
			$departments = array();
			//$years = ($currentyear-1).",".($currentyear-2).",".($currentyear-3);
			/*$sql = "select d.departmentid, d.departmentname, if(a.studentid is null, 0, count(*)) as count from student as a right JOIN whuclass b on a.classid = b.classid and a.grade in ($years) left join major c on c.majorid = b.majorid right join (select * from department where departmentid != 1 )d on d.departmentid = c.departmentid GROUP BY d.departmentid ORDER BY d.departmentid";*/
			$sql = 'select * from department where departmentid != 1';
			$rets = @mysql_query($sql,$con) or die("查询学院信息以及学院参评人数失败！");
			while ($row=mysql_fetch_array($rets))
			{
				$departments[$row["departmentid"]] = array(
					'name'=>$row["departmentname"], 
					//'canping'=>$row["count"],
					'Alevel'=>0,'Blevel'=>0,'Clevel'=>0,'ABCSum'=>0,'ABCMoney'=>0,
					'GJAlevel'=>0, 'GJBlevel'=>0, 'GJClevel'=>0, 'GJSum'=>0, 'GJMoney'=>0,
					'LZAlevel'=>0, 'LZBlevel'=>0, 'LZClevel'=>0, 'LZSum'=>0, 'LZMoney'=>0,
					'ZXAlevel'=>0, 'ZXBlevel'=>0, 'ZXClevel'=>0, 'ZXSum'=>0, 'ZXMoney'=>0);
			}
			//var_dump($departments);

			//得到各个学院的奖学金的类型情况,得到id即可
			$youxiu = array(); $guojia = array(); $lizhi = array(); $zhuangxiang = array();
			$sql = 'select * from scholarshiptype where departmentid = 1 order by scholarshipid';
			$rets = @mysql_query($sql,$con) or die("查询优秀学生奖学金类型表格失败！");
			while ($row=mysql_fetch_array($rets))
			{
				if(isset($row['type']) && $row['type'] == '优秀学生奖学金')
					array_push($youxiu, $row['scholarshipid']);
				if(isset($row['type']) && $row['type'] == '国家奖学金')
					array_push($guojia, $row['scholarshipid']);
				if(isset($row['type']) && $row['type'] == '国家励志奖学金')
					array_push($lizhi, $row['scholarshipid']);
			}
			$youxiu = implode(',', $youxiu); if(empty($youxiu)) $youxiu = 'NULL';
			$guojia = implode(',', $guojia); if(empty($guojia)) $guojia = 'NULL';
			$lizhi = implode(',', $lizhi); if(empty($lizhi)) $lizhi = 'NULL';

			//针对各个学院开始计算奖学金的实际冲抵获得情况
			foreach($departments as $id=>$arr)
			{
				//查询等级奖的获得情况
				$sql = "select  count(b.amount) as amount, b.name as name from scholarship as a left join scholarshiptype b on a.scholarshipid = b.scholarshipid where a.year='$currentyear' and a.departmentid = '$id' and a.scholarshipid in ($youxiu) GROUP BY a.scholarshipid order by a.scholarshipid";
				$rets = @mysql_query($sql,$con) or die("计算".$arr['name']."等级奖学金实际获得情况失败！");
				while ($row=mysql_fetch_array($rets))
				{
					if(isset($row['name']) && $row['name'] == '甲等')
						$departments[$id]['Alevel'] = $row['amount'];
					if(isset($row['name']) && $row['name'] == '乙等')
						$departments[$id]['Blevel'] = $row['amount'];
					if(isset($row['name']) && $row['name'] == '丙等')
						$departments[$id]['Clevel'] = $row['amount'];
				}
				$departments[$id]['ABCSum'] = $departments[$id]['Alevel'] + $departments[$id]['Blevel'] +$departments[$id]['Clevel'];
				//查询等级奖的总金额
				$sql = "select  sum(b.amount) as amount from scholarship as a left join scholarshiptype b on a.scholarshipid = b.scholarshipid where a.year='$currentyear' and a.departmentid = '$id' and a.scholarshipid in ($youxiu) order by a.scholarshipid";
				$rets = @mysql_query($sql,$con) or die("计算".$arr['name']."等级奖学金总金额情况失败！");
				while ($row=mysql_fetch_array($rets))
				{
					if(isset($row['amount']))
						$departments[$id]['ABCMoney'] = $row['amount'];
				}

				//计算专项奖学金的冲抵情况
				$zhuanxiang = array();
				$sql = "select * from scholarshiptype where departmentid = '$id' and type='专项奖学金' order by scholarshipid";
				$rets = @mysql_query($sql,$con) or die("查询专项奖学金类型表格失败！");
				while ($row=mysql_fetch_array($rets))
				{
					if(isset($row['type']) && $row['type'] == '专项奖学金')
						array_push($zhuanxiang, $row['scholarshipid']);
				}
				$zhuanxiang = implode(',', $zhuanxiang); if(empty($zhuanxiang)) $zhuanxiang = 'NULL';

				$sql = "select count(*) as amount, c.name as name from scholarship a left join scholarship b on a.studentid = b.studentid left join scholarshiptype c on c.scholarshipid = b.scholarshipid
where a.year = '$currentyear' and a.departmentid = '$id' and a.scholarshipid in ($zhuanxiang) and a.scholarshipid != b.scholarshipid GROUP BY c.scholarshipid ORDER BY c.scholarshipid"; 
				$rets = @mysql_query($sql,$con) or die("查询专项奖学金冲抵情况失败！");
				while ($row=mysql_fetch_array($rets))
				{
					if(isset($row['name']) && $row['name'] == '甲等')
						$departments[$id]['ZXAlevel'] = $row['amount'];
					if(isset($row['name']) && $row['name'] == '乙等')
						$departments[$id]['ZXBlevel'] = $row['amount'];
					if(isset($row['name']) && $row['name'] == '丙等')
						$departments[$id]['ZXClevel'] = $row['amount'];
				}
				$departments[$id]['ZXSum'] = $departments[$id]['ZXAlevel'] + $departments[$id]['ZXBlevel'] +$departments[$id]['ZXClevel'];
				//计算专项冲抵的总钱
				$sql = "select sum(c.amount) as amount from scholarship a left join scholarship b on a.studentid = b.studentid left join scholarshiptype c on c.scholarshipid = b.scholarshipid where a.year = '$currentyear' and a.departmentid = '$id' and a.scholarshipid in ($zhuanxiang) and a.scholarshipid != b.scholarshipid";
				$rets = @mysql_query($sql,$con) or die("查询专项奖学金冲抵总额情况失败！");
				while ($row=mysql_fetch_array($rets))
				{
					if(isset($row['amount']))
						$departments[$id]['ZXMoney'] = $row['amount'];
				}

				//计算国家奖学金冲抵情况
				$sql = "select count(*) as amount, c.name as name from scholarship a left join scholarship b on a.studentid = b.studentid left join scholarshiptype c on c.scholarshipid = b.scholarshipid
where a.year = '$currentyear' and a.departmentid = '$id' and a.scholarshipid in ($guojia) and a.scholarshipid != b.scholarshipid GROUP BY c.scholarshipid ORDER BY c.scholarshipid"; 
				$rets = @mysql_query($sql,$con) or die("查询国家奖学金冲抵情况失败！");
				while ($row=mysql_fetch_array($rets))
				{
					if(isset($row['name']) && $row['name'] == '甲等')
						$departments[$id]['GJAlevel'] = $row['amount'];
					if(isset($row['name']) && $row['name'] == '乙等')
						$departments[$id]['GJBlevel'] = $row['amount'];
					if(isset($row['name']) && $row['name'] == '丙等')
						$departments[$id]['GJClevel'] = $row['amount'];
				}
				$departments[$id]['GJSum'] = $departments[$id]['GJAlevel'] + $departments[$id]['GJBlevel'] +$departments[$id]['GJClevel'];
				//计算国家冲抵的总钱
				$sql = "select sum(c.amount) as amount from scholarship a left join scholarship b on a.studentid = b.studentid left join scholarshiptype c on c.scholarshipid = b.scholarshipid where a.year = '$currentyear' and a.departmentid = '$id' and a.scholarshipid in ($guojia) and a.scholarshipid != b.scholarshipid";
				$rets = @mysql_query($sql,$con) or die("查询国家奖学金冲抵总额情况失败！");
				while ($row=mysql_fetch_array($rets))
				{
					if(isset($row['amount']))
						$departments[$id]['GJMoney'] = $row['amount'];
				}

				//计算国家励志奖学金冲抵情况
				$sql = "select count(*) as amount, c.name as name from scholarship a left join scholarship b on a.studentid = b.studentid left join scholarshiptype c on c.scholarshipid = b.scholarshipid
where a.year = '$currentyear' and a.departmentid = '$id' and a.scholarshipid in ($lizhi) and a.scholarshipid != b.scholarshipid GROUP BY c.scholarshipid ORDER BY c.scholarshipid"; 
				$rets = @mysql_query($sql,$con) or die("查询国家励志奖学金冲抵情况失败！");
				while ($row=mysql_fetch_array($rets))
				{
					if(isset($row['name']) && $row['name'] == '甲等')
						$departments[$id]['LZAlevel'] = $row['amount'];
					if(isset($row['name']) && $row['name'] == '乙等')
						$departments[$id]['LZBlevel'] = $row['amount'];
					if(isset($row['name']) && $row['name'] == '丙等')
						$departments[$id]['LZClevel'] = $row['amount'];
				}
				$departments[$id]['LZSum'] = $departments[$id]['LZAlevel'] + $departments[$id]['LZBlevel'] +$departments[$id]['LZClevel'];
				//计算国家励志冲抵的总钱
				$sql = "select sum(c.amount) as amount from scholarship a left join scholarship b on a.studentid = b.studentid left join scholarshiptype c on c.scholarshipid = b.scholarshipid where a.year = '$currentyear' and a.departmentid = '$id' and a.scholarshipid in ($lizhi) and a.scholarshipid != b.scholarshipid";
				$rets = @mysql_query($sql,$con) or die("查询国家励志奖学金冲抵总额情况失败！");
				while ($row=mysql_fetch_array($rets))
				{
					if(isset($row['amount']))
						$departments[$id]['LZMoney'] = $row['amount'];
				}
			}
			@mysql_close($con) or die('关闭数据库连接失败！');
			
			//echo 'after db '.(microtime(true)-$start).'<br/>';
			//加载模板
			$objPHPExcel = new PHPExcel();
			$objReader = PHPExcel_IOFactory::createReader('Excel5');
			$objPHPExcel = $objReader->load("templates/冲抵奖学金总表.xls");

			$baseRow = 5; $currentRow = 0; 
			//定义合计总数
			$canping = 0; $Alevel = 0; $Blevel = 0; $Clevel = 0; $ABCSum = 0; $ABCMoney = 0;
			$ZXSum = 0; $ZXMoney = 0; $GJSum = 0; $GJMoney = 0; $LZSum = 0; $LZMoney = 0;
			$realSum = 0; $realMoney = 0;
			foreach ($departments as $id => $dataRow)
			{
				$rowline = $baseRow + $currentRow;

				//填充学院名称
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowline, $departments[$id]['name']);

				//填充学院参评人数
				//$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowline, $departments[$id]['canping']);

				//填充优秀学生奖学金人数
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowline, $departments[$id]['Alevel']);
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$rowline, $departments[$id]['Blevel']);
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$rowline, $departments[$id]['Clevel']);
				$objPHPExcel->getActiveSheet()->setCellValue('H'.$rowline, $departments[$id]['ABCSum']);
				$objPHPExcel->getActiveSheet()->setCellValue('I'.$rowline, $departments[$id]['ABCMoney']);

				//填充冲抵的专项奖学金人数及总数
				$objPHPExcel->getActiveSheet()->setCellValue('J'.$rowline, $departments[$id]['ZXSum']);
				$objPHPExcel->getActiveSheet()->setCellValue('K'.$rowline, $departments[$id]['ZXMoney']);

				//填充冲抵的国家奖学金人数及总数
				$objPHPExcel->getActiveSheet()->setCellValue('L'.$rowline, $departments[$id]['GJSum']);
				$objPHPExcel->getActiveSheet()->setCellValue('M'.$rowline, $departments[$id]['GJMoney']);

				//填充冲抵的国家励志奖学金人数及总数
				$objPHPExcel->getActiveSheet()->setCellValue('N'.$rowline, $departments[$id]['LZSum']);
				$objPHPExcel->getActiveSheet()->setCellValue('O'.$rowline, $departments[$id]['LZMoney']);

				//填充冲抵的国家励志奖学金人数及总数
				$objPHPExcel->getActiveSheet()->setCellValue('P'.$rowline, ($departments[$id]['ABCSum']-$departments[$id]['ZXSum']-$departments[$id]['GJSum']-$departments[$id]['LZSum']));
				$objPHPExcel->getActiveSheet()->setCellValue('Q'.$rowline, ($departments[$id]['ABCMoney']-$departments[$id]['GJMoney']-$departments[$id]['ZXMoney']-$departments[$id]['LZMoney']));
				
				//$canping += $departments[$id]['canping'];
				$Alevel += $departments[$id]['Alevel']; 
				$Blevel += $departments[$id]['Blevel']; 
				$Clevel += $departments[$id]['Clevel']; 
				$ABCSum += $departments[$id]['ABCSum']; 
				$ABCMoney += $departments[$id]['ABCMoney']; 
				$ZXSum += $departments[$id]['ZXSum']; 
				$ZXMoney += $departments[$id]['ZXMoney']; 
				$GJSum += $departments[$id]['GJSum']; 
				$GJMoney += $departments[$id]['GJMoney'];  
				$LZSum += $departments[$id]['LZSum']; 
				$LZMoney += $departments[$id]['LZMoney']; 
				$realSum += ($departments[$id]['ABCSum']-$departments[$id]['ZXSum']-$departments[$id]['GJSum']-$departments[$id]['LZSum']); 
				$realMoney += ($departments[$id]['ABCMoney']-$departments[$id]['GJMoney']-$departments[$id]['ZXMoney']-$departments[$id]['LZMoney']); 
				$currentRow++;
			}
		
			$rowline = $rowline + 1;
			//填充合计
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowline, "合计");
			//$objPHPExcel->getActiveSheet()->setCellValue('B'.$rowline, $canping);
				
			//填充总计优秀学生奖学金人数甲乙丙等总数及合计总数
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowline, $Alevel);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$rowline, $Blevel);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$rowline, $Clevel);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$rowline, $ABCSum);
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$rowline, $ABCMoney);

			//填充冲抵的专项奖学金合计人数及钱数
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$rowline, $ZXSum);
			$objPHPExcel->getActiveSheet()->setCellValue('K'.$rowline, $ZXMoney);

			//填充冲抵的国家奖学金合计人数及钱数
			$objPHPExcel->getActiveSheet()->setCellValue('L'.$rowline, $GJSum);
			$objPHPExcel->getActiveSheet()->setCellValue('M'.$rowline, $GJMoney);

			//填充冲抵的国家励志奖学金合计人数及钱数
			$objPHPExcel->getActiveSheet()->setCellValue('N'.$rowline, $LZSum);
			$objPHPExcel->getActiveSheet()->setCellValue('O'.$rowline, $LZMoney);

			//填充冲抵的国家励志奖学金合计人数及钱数
			$objPHPExcel->getActiveSheet()->setCellValue('P'.$rowline, $realSum);
			$objPHPExcel->getActiveSheet()->setCellValue('Q'.$rowline, $realMoney);

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save(str_replace('.php', '.xls', __FILE__));

			ob_end_clean();
			ob_start();
			
			// Redirect output to a client’s web browser (Excel5)
			header('Content-Type: application/vnd.ms-excel');
      			//$filename = iconv("gb2312", "utf-8", "武汉大学".$currentyear."年度冲抵奖学金总表.xls");
      			$filename = "武汉大学".$currentyear."年度冲抵奖学金总表.xls";

			$encoded_filename = urlencode($filename);
			$encoded_filename = str_replace("+", "%20", $encoded_filename);
			$ua = $_SERVER["HTTP_USER_AGENT"];
			header('Content-Type: application/octet-stream');
			if (preg_match("/MSIE/", $ua)) {
			    header('Content-Disposition: attachment; filename="' . $encoded_filename . '"');
			} else if (preg_match("/Firefox/", $ua)) {
			    header('Content-Disposition: attachment; filename*="utf8\'\'' . $filename . '"');
			} else {
			    header('Content-Disposition: attachment; filename="' . $filename . '"');
			}


			//header('Content-Disposition: attachment;filename='.$filename);
			header('Cache-Control: max-age=0');
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
			exit;

		}
		else {echo '您还没有登录，请先登录系统！'; exit;}
	}
	else {echo '您没有权限下载该表格！'; exit;}

?>
