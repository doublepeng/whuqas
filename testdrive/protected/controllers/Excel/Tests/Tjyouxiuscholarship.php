<?php
	function StatisticsExcellNums($departmentlable,$Year)//统计学院优秀奖学金甲等人数、优秀学生奖学金乙等人数和优秀奖学金丙等人数
	{
		$departmentid = $departmentlable;
		$year = $Year;

		$scholarname = "优秀学生奖学金";
		//$scholarname = iconv("gb2312","utf-8",$scholarname);
			
		/*
		*初始化优秀学生奖学金甲乙丙等三类的统计值为0，其中，
		*$Alevelnums,$Blevelnums,$Clevelnums依次为甲乙丙；
		*/
		$Alevelnums = 0;
		$Alevel = "甲等";
		//$Alevel = iconv("gb2312","utf-8",$Alevel);
		$Blevelnums = 0;
		$Blevel = "乙等";
		//$Blevel = iconv("gb2312","utf-8",$Blevel);
		$Clevelnums = 0;
		$Clevel = "丙等";
		//$Clevel = iconv("gb2312","utf-8",$Clevel);
		$total = 0;

		$encode = "set names 'utf8'";
		$con = mysql_connect("localhost","root","root");
		mysql_select_db("whuqas2011212", $con);
		mysql_query($encode);

		//查询优秀学生奖学金的等级（甲乙丙）及其id号
		$queryscholarshiptype = " select scholarshipid,name from scholarshiptype where type='$scholarname' and departmentid = '1'";//优秀学生呢奖学金属于学校管理		
		$scholarshiptyprets = mysql_query($queryscholarshiptype,$con);
		$i = 0;
		$schloarrows = array();
		while ($row=mysql_fetch_array($scholarshiptyprets))
		{
			$schloarrows[$i]["scholarshipid"] = $row["scholarshipid"];
			$schloarrows[$i]["name"] = $row["name"];
			//var_dump($schloarrows[$i]);
			$i++;
		}

		foreach ($schloarrows as $scholar)
		{
			$scholarid = $scholar["scholarshipid"];
			$scholarlevel = $scholar["name"];

			//查询学院获得优秀学生奖学金的id号
			$sqlclassid = "select classid from whuclass where majorid in (select majorid from major where departmentid='$departmentid')";
			$sqlstudentid = "select studentid from student where classid in (";
			$sqlquery = "select scholarshipid from scholarship where year='$year' and studentid in (".$sqlstudentid.$sqlclassid."))";
			$scholarshiprets = mysql_query($sqlquery,$con);
			$i = 0;
			$scholarshipidarray = array();
			while ($row=mysql_fetch_array($scholarshiprets))
			{
				$scholarshipidarray[$i]["scholarshipid"] = $row["scholarshipid"];
				//var_dump($scholarshipidarray[$i]);
				$i++;
			}

			foreach ($scholarshipidarray as $val)
			{
				$scholarshipid = $val["scholarshipid"];
							
				if ($scholarlevel==$Alevel)//统计优秀学生奖学金甲等人数
				{
					if ($scholarshipid==$scholarid)
						$Alevelnums++;
				}

				if ($scholarlevel==$Blevel)//统计优秀学生奖学金乙等人数
				{
					if ($scholarshipid==$scholarid)
						$Blevelnums++;
				}
		
				if ($scholarlevel==$Clevel)//统计优秀学生奖学金丙等人数
				{
					if ($scholarshipid==$scholarid)
						$Clevelnums++;
				}
			}
		}

		//计算学院优秀学生奖学金获得总数
		$total += ($Alevelnums+$Blevelnums+$Clevelnums);

		$queryexscholarship = "select * from excestuschoarshiptype where departmentid='$departmentid' and year='$year'";
		$flag = mysql_query($queryexscholarship,$con);
		$mark = false;
		while ($row=mysql_fetch_array($flag))
			{
						//var_dump($row);
				$mark = true;
					
			}
		if (!$mark)//如果优秀学生奖学金统计表中不存在学院对应年份的记录的话，则进行插入记录
		{
			$insertexcestuschoarshiptype = "insert into excestuschoarshiptype(departmentid,year,Alevelnums,Blevelnums,Clevelnums,totalnums) 
												values('$departmentid','$year','$Alevelnums','$Blevelnums','$Clevelnums','$total')";	
			mysql_query($insertexcestuschoarshiptype,$con);
		}
		else //如果优秀生学生奖学金人数统计表中已经存在了学院对应年份的记录的话，直接进行更新即可
		{
			$updateexcestuschoarshiptype = "update excestuschoarshiptype set Alevelnums='$Alevelnums',Blevelnums='$Blevelnums',Clevelnums='$Clevelnums',totalnums='$total' where departmentid='$departmentid' and year='$year'";
			mysql_query($updateexcestuschoarshiptype,$con);
		}

		mysql_close($con);
	}


	/*
	本函数用于统计获得了国家奖学金、国家励志奖学金和专项奖学金的学生同时又获得优秀学生奖学金的冲抵人数，
	其中优秀学生奖学金分为甲乙丙三等；
	统计是以学院为单位进行的；
	*/
	function StatisticsNums($departmentlable,$Year)
	{
		$departmentid = $departmentlable;
		$year = $Year;

		$encode = "set names 'utf8'";
		$con = mysql_connect("localhost","root","root");
		mysql_select_db("whuqas2011212", $con);
		mysql_query($encode);

		/*
		*初始化冲抵的国家、国家励志和专项的甲乙丙等奖学金人数；
		*其中nation为国家，mnation为国际励志,special为专项；
		*A表示甲等，B表示乙等，C表示丙等
		*/
		$Anation = 0;
		$Bnation = 0;
		$Cnation = 0;
		$Amnation = 0;
		$Bmnation = 0;
		$Cmnation = 0;
		$Aspecial = 0;
		$Bspecial = 0;
		$Cspecial = 0;
		
		try
		{
			$Newname = "优秀学生奖学金";
			//$Newname = iconv("gb2312","utf-8",$Newname);

			$Alevel = "甲等";
			//$Alevel = iconv("gb2312","utf-8",$Alevel);

			$Blevel = "乙等";
			//$Blevel = iconv("gb2312","utf-8",$Blevel);

			$Clevel = "丙等";
			//$Clevel = iconv("gb2312","utf-8",$Clevel);

			$nation = "国家奖学金";
			//$nation = iconv("gb2312","utf-8",$nation);

			$mnation = "国家励志奖学金";
			//$mnation = iconv("gb2312","utf-8",$mnation);

			$special = "专项奖学金";
			//$special = iconv("gb2312","utf-8",$special);	
			
      //查询得到奖学金的类型和id
			$queryscholarshiptype = " select scholarshipid,type as name from scholarshiptype where type!='$Newname' and (departmentid='$departmentid' or departmentid = 1)";
			$scholarshiptyperets = mysql_query($queryscholarshiptype,$con);
			$i = 0;
			$scholarshiprets = array();
			while ($row=mysql_fetch_array($scholarshiptyperets))
			{
				$scholarshiprets[$i]["scholarshipid"] = $row["scholarshipid"];
				//var_dump($row["scholarshipid"]);
				$scholarshiprets[$i]["name"] = $row["name"];
				//var_dump($scholarshiprets[$i]);
				$i++;
			}

			foreach ($scholarshiprets  as $datarow)	
			{
				$scholarshipid = $datarow["scholarshipid"];
				$name = $datarow["name"];	
				//echo $name;

				//查询相关学院获得国家（或者国家励志或者专项）奖学金的学生的id号
				$sqlquery = "select distinct studentid from scholarship where year='$year' and scholarshipid='$scholarshipid' and studentid in (select studentid from student where classid in (select classid from whuclass where majorid in (select majorid from major where departmentid='$departmentid')))";
				//echo $sqlquery."<br/>";

				$studentidrets = mysql_query($sqlquery,$con)  or die(mysql_error());
				$i = 0;
				$studentidarray = array();
				while ($row=mysql_fetch_array($studentidrets))
				{
					$studentidarray[$i]["studentid"] = $row["studentid"];
					//var_dump($studentidarray[$i]);
					$i++;
				}

				foreach ($studentidarray as $studentidinfo)
				{
					$studentid = $studentidinfo["studentid"];
					//var_dump($studentid);
					//通过学生id号来查询学生获得了除国家（或者国家励志或者专项）奖学金以外的优秀学生奖学金及其等级
					$querystudentscholar = "select distinct name,type from scholarshiptype,scholarship where studentid='$studentid'
												and scholarship.scholarshipid=scholarshiptype.scholarshipid and 
												scholarshiptype.scholarshipid!='$scholarshipid' and  (departmentid = 1)";
					
					//echo $querystudentscholar;
					$studentscholarrets = mysql_query($querystudentscholar,$con) or die(mysql_error());
					//var_dump($studentscholarrets);
					$i = 0;
					$queryrets = array();
					while ($row=mysql_fetch_array($studentscholarrets))
					{
						$queryrets[$i]["name"] = $row["name"];
						$queryrets[$i]["type"] = $row["type"];
						//var_dump($row);
						//echo "<br/>";
						$i++;
					}

					foreach ($queryrets as $scholarinfo)
					{
						$scholarname = $scholarinfo["type"];
						$scholarlevel = $scholarinfo["name"];

						if ($name==$nation)//统计获得国家奖学金冲抵人数
						{
							if ($scholarlevel==$Alevel)//优秀学生甲等奖学金冲抵人数
							{
								$Anation++;
							}

							if ($scholarlevel==$Blevel)//优秀学生乙等奖学金冲抵人数
							{
								$Bnation++;
							}

							if ($scholarlevel==$Clevel)//优秀学生丙等奖学金冲抵人数
							{
								$Cnation++;
							}
						}

						if ($name==$mnation)//统计获得国家励志奖学金冲抵人数
						{
							if ($scholarlevel==$Alevel)//优秀学生甲等奖学金冲抵人数
							{
								$Amnation++;
							}

							if ($scholarlevel==$Blevel)//优秀学生乙等奖学金冲抵人数
							{
								$Bmnation++;
							}

							if ($scholarlevel==$Clevel)//优秀学生丙等奖学金冲抵人数
							{
								$Cmnation++;
							}
						}

						if ($name==$special)//统计获得专项奖学金冲抵人数
						{
							if ($scholarlevel==$Alevel)//优秀学生甲等奖学金冲抵人数
							{
								$Aspecial++;
							}

							if ($scholarlevel==$Blevel)//优秀学生乙等奖学金冲抵人数
							{
								$Bspecial++;
							}

							if ($scholarlevel==$Clevel)//优秀学生丙等奖学金冲抵人数
							{
								$Cspecial++;
							}
						}
					}
				}
			}
		}
		catch (Exception $e)
		{
			mysql_close($con);
			throw $e;
		}

		//将统计得到冲抵优秀学生奖学金的甲乙丙等人数插入到奖学金人数冲抵表（scholarshipoffset)中
		try
		{
			$queryexscholarship = "select * from scholarshipoffset where departmentid='$departmentid' and year='$year'";
			$flag = mysql_query($queryexscholarship,$con)or die(mysql_error());
			$mark = false;
			while ($row=mysql_fetch_array($flag))
			{
				//var_dump($row);
				$mark = true;
					
			}
			if (!$mark)//如果优秀学生奖学金冲抵统计表中不存在学院对应年份的记录的话，则进行插入记录
			{
				//echo "insert";
				$insertscholarshipoffset = "insert into scholarshipoffset(year,departmentid,
							Alevelspecialscholarnums,Blevelspecialscholarnums,Clevelspecialscholarnums,
							Alevelnationnalscholarnums,Blevelnationnalscholarnums,Clevelnationnalscholarnums,
							Alevelnationalmotivationalscholarnums,Blevelnationalmotivationalscholarnums,Clevelnationalmotivationalscholarnums) values(
							'$year','$departmentid','$Aspecial','$Bspecial','$Cspecial','$Anation','$Bnation',
							'$Cnation','$Amnation','$Bmnation','$Cmnation')";	
				mysql_query($insertscholarshipoffset,$con)or die(mysql_error());
			}
			else //如果优秀生学生奖学金冲抵人数统计表中已经存在了学院对应年份的记录的话，直接进行更新即可
			{
				//echo "update";
				$updatescholarshipoffset = "update scholarshipoffset set																			Alevelspecialscholarnums='$Aspecial',Blevelspecialscholarnums='$Bspecial',Clevelspecialscholarnums='$Cspecial',
				Alevelnationnalscholarnums='$Anation',Blevelnationnalscholarnums='$Bnation',Clevelnationnalscholarnums='$Cnation',
				Alevelnationalmotivationalscholarnums='$Amnation',Blevelnationalmotivationalscholarnums='$Bmnation',Clevelnationalmotivationalscholarnums='$Cmnation'
						 where departmentid='$departmentid' and year='$year'";
				mysql_query($updatescholarshipoffset,$con)or die(mysql_error());
			}
		}
		catch (Exception $e)
		{
			mysql_close($con);
			throw $e;
		}
		mysql_close($con);
	}
?>
