<?php

class ManagerController extends Controller
{
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated users to access all actions
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionSaveEditManager()//超级管理员部分:编辑管理员信息
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}

		if(!isset($_SESSION['ID']))
		{
			echo json_encode(array(
				"success"=>false, 
				"message"=>"您登录的时间超时，请返回首页重新登录！", 
				"results"=>array('timeout'=>true)
			));
			exit();
		}

		if (isset($_SESSION["ID"]))//判定用户已登录
		{
			$saveEditManager = isset($_POST["saveEditManager"])?$_POST["saveEditManager"]:$_GET["saveEditManager"];
			$saveEditManager = json_decode($saveEditManager,true);
			$updatemanager = $saveEditManager["update"];
			$delmanager = $saveEditManager["delete1"];

			try
			{
				$db = Yii::app()->db;

				if (!empty($updatemanager))
				{
					foreach ($updatemanager as $value)
					{
						$managername = $value["managername"];
						$managerid = $value["managerid"];
						$departmentname = $value["departmentname"];

						$sqldepartmentid = "select departmentid from department where departmentname='$departmentname'";
						$sqlrets = $db->createCommand($sqldepartmentid)->queryRow();
						$departmentid = $sqlrets["departmentid"];

						$updatemanagerinfo = "update manager set managername='$managername' where departmentid='$departmentid' and managerid='$managerid'";
						$db->createCommand($updatemanagerinfo)->execute();
					}
				}


				if (!empty($delmanager))
				{
					foreach ($delmanager as $datarow)
					{
						$managerid = $datarow["managerid"];

						$delmanagerinfo = "delete from manager where managerid='$managerid'";
						$db->createCommand($delmanagerinfo)->execute();
					}
				}

				$rets = array("success"=>true,"message"=>"","results"=>array());
				echo json_encode($rets);
			}
			catch (Exception $e)
			{
				$rets = array("success"=>false,"message"=>$e->getMessage(),"results"=>array());
				echo json_encode($rets);
			}
		}
	}


	public function actionSaveDepartment()//超级管理员部分：新增学院
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}

		if(!isset($_SESSION['ID']))
		{
			echo json_encode(array(
				"success"=>false, 
				"message"=>"您登录的时间超时，请返回首页重新登录！", 
				"results"=>array('timeout'=>true)
			));
			exit();
		}

		if (isset($_SESSION["ID"]))//判定用户已登录
		{
			$saveDepartment = isset($_POST["saveDepartment"])?$_POST["saveDepartment"]:$_GET["saveDepartment"];
			$saveDepartment = json_decode($saveDepartment,true);

			try
			{
				$db = Yii::app()->db;

				foreach ($saveDepartment as $value)
				{
					$departmentname = $value["departmentname"];
        
          //防止插入重复
          $sql = "select * from department where departmentname = '$departmentname'"; 
          $query = $db->createCommand($sql)->query();
          $res = $query->readAll();
          if(!empty($res))   //如果已经存在了
          {
            echo json_encode(array("success"=>false,"message"=>'“'.$departmentname."”已经存在，请修改后再提交！","results"=>array()));
            exit;
          }

					$insertdepartment = "insert into department(departmentname) values('$departmentname')";
					$db->createCommand($insertdepartment)->execute();
				}
			
				$rets = array("success"=>true,"message"=>"","results"=>array());
				echo json_encode($rets);
			}
			catch (Exception $e)
			{
				$rets = array("success"=>false,"message"=>$e->getMessage(),"results"=>array());
				echo json_encode($rets);
			}
		}

	}

	public function actionShowDepartment()
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}
		if(!isset($_SESSION['ID']))
		{
			echo json_encode(array(
				"success"=>false, 
				"message"=>"您登录的时间超时，请返回首页重新登录！", 
				"results"=>array('timeout'=>true)
			));
			exit();
		}
		if (isset($_SESSION["ID"]))//判定用户已登录
		{
			try
			{
				$db = Yii::app()->db;//连接mysql数据库
				
				$sqlquery = "select departmentid,departmentname from department where departmentid != '1'";
				$queryinfo= $db->createCommand($sqlquery)->query();
				$results = $queryinfo->readAll();

				$rets = array("success"=>true,"message"=>"","results"=>$results);
				echo json_encode($rets);
			}
			catch (Exception $e)
			{
				$rets = array("success"=>false,"message"=>$e->getMessage(),"results"=>array());
				echo json_encode($rets);
			}
		}
	}

	public function actionSaveEditDepartment()//超级管理员部分：编辑学院信息
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}
		if(!isset($_SESSION['ID']))
		{
			echo json_encode(array(
				"success"=>false, 
				"message"=>"您登录的时间超时，请返回首页重新登录！", 
				"results"=>array('timeout'=>true)
			));
			exit();
		}
		if (isset($_SESSION["ID"]))//判定用户已登录
		{
			$saveEditDepartment = isset($_POST["saveEditDepartment"])?$_POST["saveEditDepartment"]:$_GET["saveEditDepartment"];
			$saveEditDepartment = json_decode($saveEditDepartment,true);
			$updatedepartment = $saveEditDepartment["update"];
			$deldepartment = $saveEditDepartment["delete1"];

			try
			{
				$db = Yii::app()->db;

				if (!empty($updatedepartment))
				{
					foreach ($updatedepartment as $datarow)
					{
						$departmentname = $datarow["departmentname"];
						$departmentid = $datarow["departmentid"];

						$updatemanagerinfo = "update department set departmentname='$departmentname' where departmentid='$departmentid'";
						$db->createCommand($updatemanagerinfo)->execute();
					}
				}

				if (!empty($deldepartment))
				{
					foreach ($deldepartment as $value)
					{
						$departmentid = $value["departmentid"];

						$delmanagerinfo = "delete from department where departmentid='$departmentid'";
						$db->createCommand($delmanagerinfo)->execute();
					}
				}

				$rets = array("success"=>true,"message"=>"","results"=>array());
				echo json_encode($rets);
			}
			catch (Exception $e)
			{
				$rets = array("success"=>false,"message"=>$e->getMessage(),"results"=>array());
				echo json_encode($rets);
			}
		}
	}

	public function actionCheckFactors()//超级管理员部分：查看各个学院的测评系数
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}
		if(!isset($_SESSION['ID']))
		{
			echo json_encode(array(
				"success"=>false, 
				"message"=>"您登录的时间超时，请返回首页重新登录！", 
				"results"=>array('timeout'=>true)
			));
			exit();
		}
		if (isset($_SESSION["ID"]))//判定用户已登录
		{
			if (!isset($_POST["submit"]))//submit为前台测评系数查看按钮名称
			{
				try
				{
					$db = Yii::app()->db;

					$queryfactors = "select departmentname,F1 as factor1,F2 as factor2,F3 as factor3 from department where departmentid != '1'";
					$factorsrets = $db->createCommand($queryfactors)->query();
					$results = $factorsrets->readAll();

					if (!empty($results))
					{
						$rets = array("success"=>true,"message"=>"","results"=>$results);
						echo json_encode($rets);
					}
					else 
					{
						$rets = array("success"=>false,"message"=>"各学院测评系数尚未设定...","results"=>array());
						echo json_encode($rets);
					}
				}
				catch (Exception $e)
				{
					$rets = array("success"=>false,"message"=>$e->getMessage(),"results"=>array());
					echo json_encode($rets);
				}
			}
		}
	}

	public function actionAddManager()//新增学院管理员信息
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}
		if(!isset($_SESSION['ID']))
		{
			echo json_encode(array(
				"success"=>false, 
				"message"=>"您登录的时间超时，请返回首页重新登录！", 
				"results"=>array('timeout'=>true)
			));
			exit();
		}
		if (isset($_SESSION["ID"]))//判定用户已登录
		{
			$saveManager = isset($_POST["saveManager"])?$_POST["saveManager"]:$_GET["saveManager"];
			$saveManager = json_decode($saveManager,true);	

			try
			{
				$db = Yii::app()->db;//链接mysql数据库
				
				foreach ($saveManager as $datarow)
				{
					$departmentid = $datarow["departmentid"];
					$managerid = $datarow["managerid"];
					$managername = $datarow["managername"];
          $password = md5("123456");

					$insertmanager = "insert ignore into manager(managerid,managername,departmentid,password) values('$managerid','$managername','$departmentid','$password')";
					$db->createCommand($insertmanager)->execute();
				}

				$rets = array("success"=>true,"message"=>"","results"=>array());
				echo json_encode($rets);
			}
			catch (Exception $e)
			{
				$rets = array("success"=>false,"message"=>$e->getMessage(),"results"=>array());
				echo json_encode($rets);
			}		
		}
	}

	public function actionShowManger()//显示各个学院管理员信息
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}
		if(!isset($_SESSION['ID']))
		{
			echo json_encode(array(
				"success"=>false, 
				"message"=>"您登录的时间超时，请返回首页重新登录！", 
				"results"=>array('timeout'=>true)
			));
			exit();
		}
		if (isset($_SESSION["ID"]))//判定用户已登录
		{
			try
			{
				$db = Yii::app()->db;//连接mysql数据库
				
				$sqlquery = "select managerid,managername,departmentname from manager,department where manager.departmentid=department.departmentid and manager.departmentid != '1'";
				$queryinfo= $db->createCommand($sqlquery)->query();
				$results = $queryinfo->readAll();

				$rets = array("success"=>true,"message"=>"","results"=>$results);
				echo json_encode($rets);
			}
			catch (Exception $e)
			{
				$rets = array("success"=>false,"message"=>$e->getMessage(),"results"=>array());
				echo json_encode($rets);
			}				
		}
	}
}
