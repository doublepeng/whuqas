<?php
	//创建后台数据库操作类
	class ZingController extends Controller 
	{	
		private $hostname = 'localhost';
		private $user = 'root';
		private $pwd = 'root';
		private $encode = "set names 'utf8'";
		private $dbname = 'whuqas2011212';


		public function filters()
		{
			return array(
				'accessControl', // perform access control for CRUD operations
			);
		}

		/**
		 * Specifies the access control rules.
		 * This method is used by the 'accessControl' filter.
		 * @return array access control rules
		 */
		public function accessRules()
		{
			return array(
				array('allow', // allow authenticated users to access all actions
					'users'=>array('@'),
				),
				array('deny',  // deny all users
					'users'=>array('*'),
				),
			);
		}

		public function actionGetStuType()	//返回学生类型，包括普通班、基地班、农水、测绘班级
		{
			if(!isset($_SESSION)) session_start();//开启缓存
			if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
			{
				echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
				exit;
			}

			if (isset($_SESSION['ID']))//如果存在session['ID']，说明是用户登录，这样做是防止url攻击
			 {
			 	$connect = mysql_connect($this->hostname, $this->user, $this->pwd); 
                mysql_query($this->encode);
                mysql_select_db($this->dbname); 
                try {
                	$ret = array();
                	$SQL = "SELECT stuType, content FROM studenttype order by stuType";
                	$query = mysql_query( $SQL, $connect );
                	$i = 0;
                	$rows = array();
                	while($row = mysql_fetch_row($query)) { 
                		$rows[$i]['stuType'] = $row[0];//增加访问速度，学生类型的id
                		$rows[$i]['content'] = $row[1]; //学生类型
                		$i++; 
                	}  
                	$ret['success'] = true;
                	$ret['message'] = "";
                	$ret['results'] = $rows;
                	echo json_encode($ret);
	                @mysql_close($connect) or die('Fatal Error: close database failed!');
                } catch (Exception $e) {
                	$ret['success'] = false;
                	$ret['message'] = iconv("gb2312","utf-8","学生类型返回失败");
                	$ret['results'] = array();
                	echo json_encode($ret);
					        @mysql_close($connect) or die('Fatal Error: close database failed!');
                }	
			 }
		}
		
		public function actionGetEvaCon()//返回学生自我测评F3选项内容，每个学院都不同
		{			
			if(!isset($_SESSION)) session_start();//开启缓存
			if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
			{
				echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
				exit;
			}
			if (isset($_SESSION['ID']))//如果存在session['ID']，说明是用户登录，这样做是防止url攻击
			{
				//连接数据库
				$connect = mysql_connect($this->hostname, $this->user, $this->pwd); 
				mysql_query($this->encode);
				mysql_select_db($this->dbname); 
				$department = $_SESSION['departmentid'];
				//$departmentname =  iconv("gb2312","utf-8","计算机学院");
				try {
					$ret = array();//结果集返回
					//存在问题，学生自我测评选项中存在年份，跨年的时候需要注意，这里采取返回年份最近的那一个
					//首先寻找数据库存在的最近的年份
					$sqlyear = "select year from selfevacon order by year desc limit 1";
					$queryyear =  mysql_query( $sqlyear, $connect );
					$rowyear = mysql_fetch_row($queryyear);
					$year = $rowyear[0];
					//返回该最近年份的学生创新与实践选项的内容
					$sql = "SELECT evaluateid,evaluatename,maxscore FROM selfevacon
						where  departmentid ='$department'
					  	and year = '$year' order by evaluateid";
					$query = mysql_query( $sql, $connect );
					$i = 0;
					$rows = array();
					while($row = mysql_fetch_row($query)) { 
						$rows[$i]['evaluateid'] = $row[0];//增加访问速度，自评选项id
						$rows[$i]['evaluatename'] = $row[1]; //自评选项类型
						$rows[$i]['maxscore'] = $row[2]; //某自评选项最高分				
						$i++; 
					}  
					$ret['success'] = true;
					$ret['message'] = "";
					$ret['results'] = $rows;
					@mysql_close($connect) or die('Fatal Error: close database failed!');
					echo json_encode($ret);			
				} catch (Exception $e) {
					$ret['success'] = false;
					$ret['message'] = iconv("gb2312","utf-8","实践与创新类型选项内容返回失败");
					$ret['results'] = array();
					echo json_encode($ret);
	        @mysql_close($connect) or die('Fatal Error: close database failed!');
				}			
			}				
		}	
		
		/**获取辅导员所带班级的班级列表，有两种选择
		*辅导员自己查看所带班级的情况 teacherid
		*学院管理员查看辅导员所带班级的情况 teacherid由前台传给后台里面获得
		*/
		//这里测评班级仍然涉及到年份，年份是唯一的，存储在学工部管理员点击学年奖学金开始的年份
		//存储在session里面
		public function actionGetTeClass()
		{
			if(!isset($_SESSION)) session_start();//开启缓存
			if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
			{
				echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
				exit;
			}

      		if($_SESSION['WStart'] == false)
      		{
        		echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","学年测评尚未开始或者已经结束！"), "results"=>array()));
        		exit;
      		}

			if (isset($_SESSION['ID']))//如果存在session['ID']，说明是用户登录，这样做是防止url攻击
			{
				//连接数据库
				$connect = mysql_connect($this->hostname, $this->user, $this->pwd); 
				mysql_query($this->encode);
				mysql_select_db($this->dbname);
				try {
					if(!isset($_REQUEST['TeClass'])) 
						exit;
					$TeClass= isset($_POST["TeClass"])? $_POST["TeClass"]:$_GET["TeClass"];
					$TeClass = json_decode($TeClass,true);//解码为数组
					if($TeClass['teacherid'] == null)//为空表示是教师本人进行查询，不需要teacherid
						$teacherid = $_SESSION['ID'];
					else 
						$teacherid = $TeClass['teacherid'];//表示为老师进行查询，该id需查询的时候给出
					$year = $_SESSION['year'];
					/*$teacherid = "2008301500116";
					$year = "2011";*/
					$ret = array();//结果集返回
					//返回该年份某辅导员所带班级的班级列表
					//辅导员可能带多个测评班级
					$sql = "select t_classid, t_name as name FROM t_class 
						where  teacherid = '$teacherid' and year = '$year'  
						order by t_classid";
					$query = mysql_query($sql, $connect );
					$i = 0;
					$rows = array();
					while($row = mysql_fetch_row($query)) { 
						$rows[$i]['t_classid'] = $row[0];// id
						$rows[$i]['name'] = $row[1]; //自评选项类型			
						$i++; 
					}  
					$ret['success'] = true;
					$ret['message'] = "";
					$ret['results'] = $rows;
					@mysql_close($connect) or die('Fatal Error: close database failed!');
					echo json_encode($ret);			
				} catch (Exception $e) {
					$ret['success'] = false;
					$ret['message'] = iconv("gb2312","utf-8","辅导员所带测评班级内容返回失败");
					$ret['results'] = array();
					@mysql_close($connect) or die('Fatal Error: close database failed!');
					echo json_encode($ret);
				}		
			}							
		}	
		//根据测评班级t_classid返回测评班级所包括的班级列表
		public function actionGetStuClass()//真实学生班级
		{
			if(!isset($_SESSION)) session_start();//开启缓存	
			if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
			{
				echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
				exit;
			}	
			if (isset($_SESSION['ID']))//如果存在session['ID']，说明是用户登录，这样做是防止url攻击
			{
				//连接数据库
				$connect = mysql_connect($this->hostname, $this->user, $this->pwd); 
				mysql_query($this->encode);
				mysql_select_db($this->dbname); 

				if(!isset($_REQUEST['StuClass']))
					exit;
				$StuClass= isset($_POST["StuClass"])? $_POST["StuClass"]:$_GET["StuClass"];
				$StuClass = json_decode($StuClass,true);//解码为数组
				//获得测评班级id
				$t_classid = $StuClass['t_classid'];
				//$t_classid = "1";
				try {
					$ret = array();//结果集返回
					//sql语句
					$sql = "SELECT classid,classname FROM whuclass  where  t_classid = '$t_classid' order by classid";
					$query = mysql_query( $sql, $connect );
					$i = 0;
					$rows = array();
					while($row = mysql_fetch_row($query)) { 
						$rows[$i]['classid'] = $row[0];//班级id
						$rows[$i]['classname'] = $row[1]; //班级名称
						$i++; 
					}  
					$ret['success'] = true;
					$ret['message'] = "";
					$ret['results'] = $rows;
					echo json_encode($ret);	
					@mysql_close($connect) or die('Fatal Error: close database failed!');		
				} catch (Exception $e) {
					$ret['success'] = false;
					$ret['message'] = iconv("gb2312","utf-8","班级列表内容返回失败");
					$ret['results'] = array();
					echo json_encode($ret);
					@mysql_close($connect) or die('Fatal Error: close database failed!');
				}	
			}												
		}
		/**获取某个学院的奖学金奖项列表，由学院管理员进行查看，学院名称从session里中获得，或者是学院id
		 * 查看学校级别奖学金以及各个学院的专项奖学金
		 * 学校级别奖学金用学校代号1标志
		 * 其余学院专项奖学金用各个学院的id标志
		 */
		public function actionGetDepSchType()
		{
		  	if(!isset($_SESSION)) session_start();//开启缓存    		
			  if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
			  {
				  $url = "../../../";
				  echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
				  exit;
			  }
			if (isset($_SESSION['ID']))//如果存在session['ID']，说明是用户登录，这样做是防止url攻击
			{
				//连接数据库
				$connect = mysql_connect($this->hostname, $this->user, $this->pwd); 
				mysql_query($this->encode);
				mysql_select_db($this->dbname); 
				//获得学院id
				$departmentid = $_SESSION['departmentid'];
				//$departmentid = 2;
				try {
					$ret = array();//结果集返回
					//sql语句
					$sql = "SELECT scholarshipid,name,type,amount FROM scholarshiptype 
						 where departmentid = '$departmentid' or departmentid = 1
						 order by scholarshipid";//为1表示为学校级别的奖学金,奖学金顺序还需要注意				
					$query = mysql_query( $sql, $connect );
					$i = 0;
					$rows = array();
					while($row = mysql_fetch_row($query)) { 
						$rows[$i]['scholarshipid'] = $row[0];//奖学金id
						$rows[$i]['name'] = $row[1]; //奖学金名称
						$rows[$i]['type'] = $row[2]; //奖学金类型，优秀奖学金，国奖，专项奖学金
						$rows[$i]['amount'] = $row[3]; //奖学金数额				
						$i++; 
					}  
					$ret['success'] = true;
					$ret['message'] = "";
					$ret['results'] = $rows;
					echo json_encode($ret);	
					@mysql_close($connect) or die('Fatal Error: close database failed!');		
				} catch (Exception $e) {
					$ret['success'] = false;
					$ret['message'] = iconv("gb2312","utf-8","奖学金列表内容返回失败");
					$ret['results'] = array();
					echo json_encode($ret);
					@mysql_close($connect) or die('Fatal Error: close database failed!');
				}													
			}			
		}
		
		//获取全校院系名称
		public function actionSearchAllDep()
		{
      		if(!isset($_SESSION)) session_start();//开启缓存  
			if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
			{
				echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
				exit;
			}
			//查询所有学院的id，name
			$db = Yii::app()->db;
			try {
				 $sqldep = "call search_allDep ()";//视学校为一个特殊学院，除去departmentid为1的
		   		 $resultsdep = $db->createCommand($sqldep)->queryAll();//所有学院信息
		   		 //如果没有出现错误
		   		$ret = array('success'=>true,'message'=>'','results'=>$resultsdep);
				echo json_encode($ret);
			} catch (Exception $e) {
				//echo $e;
				$ret['success'] = false;
				$ret['message'] = iconv("gb2312","utf-8","返回全校学院列表失败");
				$ret['results'] = array();
				echo json_encode($ret);
			}
		 
		    
		}
		
		//根据学院获取指定院系的专业名
		public function actionSearchAllMajor()
		{
      if(!isset($_SESSION)) session_start();//开启缓存  
			if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
			{
				echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
				exit;
			}
			$db = Yii::app()->db;

			if(!isset($_REQUEST['retMajor']))
				exit;
			$retMajor = isset($_POST["retMajor"]) ? $_POST["retMajor"] : $_GET["retMajor"];
			$retMajor = json_decode($retMajor,true);//解码为数组
			$departmentid = $retMajor['departid'];//获得学院id
			//查询该学院所有专业的id，name,传递参数为 in depid int(5)
			//测试用例
			//$departmentid = 2;
			//测试结束
			try {
				$sqlmajor = "call search_allMajor ('$departmentid')";//视学校为一个特殊学院，除去departmentid为1的
		    	$resultsmajor = $db->createCommand($sqlmajor)->queryAll();//所有学院信息
		     	//如果没有出现错误
		  	 	$ret = array('success'=>true,'message'=>'','results'=>$resultsmajor);
				echo json_encode($ret);
			} catch (Exception $e) {
				//echo $e;
				$ret['success'] = false;
				$ret['message'] = iconv("gb2312","utf-8","返回学院专业列表失败");
				$ret['results'] = array();
				echo json_encode($ret);
			}
		
		}
		
		//获取指定专业对应的班级
		public function actionSearchAllClass()
		{
      if(!isset($_SESSION)) session_start();//开启缓存  
			if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
			{
				echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
				exit;
			}
			$db = Yii::app()->db;

			if(!isset($_REQUEST['retClass']))
				exit;
			$retClass = isset($_POST["retClass"]) ? $_POST["retClass"] : $_GET["retClass"];
			$retClass = json_decode($retClass,true);//解码为数组
			$majorid = $retClass['majorid'];//获得专业id
			$grade = $retClass['grade'];
			//查询该专业所有班级的id，name,传递参数为 in majorid int(5)
			//测试用例
			//$majorid = 1;
			//测试结束
			try {
				$sqlclass = "call search_allClass ('$majorid', '$grade')";
		    	$resultsclass = $db->createCommand($sqlclass)->queryAll();//所有学院信息
		     	//如果没有出现错误
		  	 	$ret = array('success'=>true,'message'=>'','results'=>$resultsclass);
				echo json_encode($ret);
			} catch (Exception $e) {
				//echo $e;
				$ret['success'] = false;
				$ret['message'] = iconv("gb2312","utf-8","返回专业班级列表失败");
				$ret['results'] = array();
				echo json_encode($ret);
			}
		
		}
		
		//为学生生成本学年度grade记录
		public function actionAddStuGrade()
		{
			if(!isset($_SESSION)) session_start();
			if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
			{
				echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
				exit;
			}
			$studentid = $_SESSION['ID'];
			$year= $_SESSION['year'];
			//测试用例
			//$studentid = '2008301500116';
			//$year = 2009;
			//测试结束
			$db = Yii::app()->db;
			try {
				$sql = "call add_stuGrade('$studentid','$year')";
				$db->createCommand($sql)->execute();//执行该操作
					//如果没有出现错误
		  	 	$ret = array('success'=>true,'message'=>'','results'=>array());
				echo json_encode($ret);
			} catch (Exception $e) {
				//echo $e;
				$ret['success'] = false;
				$ret['message'] = iconv("gb2312","utf-8","新建grade表记录失败");
				$ret['results'] = array();
				echo json_encode($ret);
			}
		}
		
		/**
		 * 学生接口
		 */
		//Yii框架开发
		public function actionDisplayStuinfo()//显示学生基本信息
		{
			if(!isset($_SESSION)) session_start();
			if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
			{
				echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
				exit;
			}
			$studentid = $_SESSION['ID'];
			$db = Yii::app()->db;

			$departmentid = $_SESSION['departmentid'];
			//var_dump($_SESSION['departmentid']);
			//$studentid = '2008301500116';
			//$departmentid = '2';
		    $ret = array();
		    try {
		    	//执行查询调用存储过程操作,yii框架同一页面可以执行多次存储过程
		    	//查询学生信息
		    	$sql = "call displayStuinfo ('$studentid')";
		    	$results = $db->createCommand($sql)->queryRow();//学生的基本信息，下面还有补充
		    	//var_dump($results);
				
		    	//将学生所属的学院id添加学生信息去
		    	$results['did'] = $departmentid;
		    	
		    	//查询学校学生的所有学生类型的id，名称
		    	$sqlStuType = "call search_allStutype ()";//调用存储过程
		    	$resultsStuType = $db->createCommand($sqlStuType)->queryAll();//所有学生类别信息
		    	
		    	//将学院信息添加到最终结果中去
		    	$results['arrayStuType'] = $resultsStuType;
		    	//$majorid = $results['mid'];
				
		    	//查询该学院所有专业的id，name,传递参数为 in depid int(5)
		    	$sqlmajor = "call search_allMajor ('$departmentid')";//视学校为一个特殊学院，除去departmentid为1的
				
		    	$resultsmajor = $db->createCommand($sqlmajor)->queryAll();//所有学院信息
				
		    	//将学院信息添加到最终结果中去
		    	$results['majors'] = $resultsmajor;
		    	//查询该学院该专业的所有班级的id,名称，将年级和班级名称拼接在一起
		    	//比如 2008 计算机科学与技术四班 拼接为2008级计算机科学与技术四班
		    	$majorid = $results['mid'];
				$grade1=$results['grade'];
		    	$sqlclass = "call search_allClass ('$majorid', '$grade1')";
		    	$resultsclass = $db->createCommand($sqlclass)->queryAll();//所有学院信息
		    	
		    	//将学院信息添加到最终结果中去
		    	$results['classes'] = $resultsclass;
		    	

		    	
		    	//如果没有出现错误
		    	$ret['success'] = true;
				$ret['message'] = iconv("gb2312","utf-8","返回成功");
				$ret['results'] = $results;	
				//json编码返回给前台						
				echo json_encode($ret);
		    } catch (Exception $e) {
		    	//echo $e;
		    	$ret['success'] = false;
				$ret['message'] = iconv("gb2312","utf-8","显示信息失败");
				$ret['results'] = array();
				echo json_encode($ret);	
		    }
		}
		//保存学生修改信息SaveStuinfo
		public function actionSaveStuinfo()
		{
			if(!isset($_SESSION)) session_start();//打开session
			if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
			{
				echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
				exit;
			}
			try {
				if(!isset($_REQUEST['saveStuInfo']))
					exit;

				$StuInfo = isset($_POST["saveStuInfo"]) ? $_POST["saveStuInfo"] : $_GET["saveStuInfo"];
				$StuInfo = json_decode($StuInfo,true);//解码为数组
				//列表参数
				$studentid = $_SESSION['ID'];//获得学生id
				$studentname = $StuInfo['sname'];//姓名
				//edited by pengchao 20120309
				$sex = $StuInfo['sex'];//性别
				$birthday = $StuInfo['birthday'];//出生时间

				$grade = $StuInfo['grade'];//入学年份
				$classid = $StuInfo['classid'];//班级id
				$IDtype = $StuInfo['IDtype'];//证件类型
				$IDnum = $StuInfo['IDnum'];//证件名称
				$nation = $StuInfo['nation'];//民族
				$province = $StuInfo['province'];//省份
				$political = $StuInfo['political'];//政治面貌
				$bankid = $StuInfo['bankid'];//银行账号
				$ispoor =$StuInfo['ispoor'];//贫困生
				$cadre = $StuInfo['cadre'];//班内职务
				$phone = $StuInfo['phone'];//电话号码
				$stuType = $StuInfo['stuType'];//学生类型（普通，基地班,农水测绘）

        
        //edit by pengchao 20120221
        //防止sql注入
		  		$inject = new Inject; 
				$$province  = $inject->injectCheck($province);
				$nation = $inject->injectCheck($nation);
				$phone = $inject->injectCheck($phone);
			    $bankid = $inject->injectCheck($bankid);

				//对学生信息进行更新
				$db = Yii::app()->db;//建立数据库连接
				$sql = "call update_stuinfo ('$studentid','$studentname','$sex','$birthday','$grade','$classid','$IDtype',
'$IDnum','$nation','$province','$political','$bankid','$ispoor','$cadre','$phone','$stuType')";//调用存储过程
				$db->createCommand($sql)->execute();//执行该操作
				
				//2012-01-12 by minzhenyu
				//由于第一次注册的时候，学生没有departmentid，所以这里需要将session填充
				if(empty($_SESSION['departmentid']))
				{
					$sql = "call search_stuinfo ('$studentid')";
					$results = $db->createCommand($sql)->queryRow();
					$_SESSION['departmentid'] = $results['departmentid'];
				}
			
		    	$ret = array();
		    	 //如果没有出现异常错误
			    $ret['success'] = true;
				$ret['message'] = "";
				$ret['results'] = array();
				echo json_encode($ret);
			} catch (Exception $e) {
				//echo $e;
				$ret['success'] = false;
				$ret['message'] = iconv("gb2312","utf-8",'保存修改信息失败');
				$ret['results'] = array();
				echo json_encode($ret);
			}
		}
		//学生修改课程成绩
		/**只有在学年测评开始才能使用，学年测评完毕后关闭此功能。
		*并且只有教师赋予权限才能用。所以这里很重要
		*而且只能修改当年的
		*这是对于那些修了双学位之类的学生做的功能。
		*/
		//编辑本学年课程成绩
		public function actionDisplayReCscore()//显示要编辑的学生课程成绩
		{
			if(!isset($_SESSION)) session_start();//打开session
			if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
			{
				echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
				exit;
			}
			try {
				$ret = array();//储存结果
				$studentid = $_SESSION['ID'];//查询学生的学号
				$year = $_SESSION['year'];//session查询该系统开始测评的时间

				//测试用例
				//$studentid = '2008301500116';
				//$year = '2011';
				
				//根据传递的参数查询学生成绩
				$db = Yii::app()->db;
				$sql = "call search_coscores('$studentid','$year')";
				$results = $db->createCommand($sql)->queryAll();//学生课程成绩基本信息
				//根据传递的参数查询学生成绩是否被审核
				$checksql = "call search_stucheck('$studentid','$year')";
				$checkresult = $db->createCommand($checksql)->queryRow();//学生成绩是否被审核
				//存储在 $checkresult['gpacheck'];//
				
				//根据传递的参数查询学生是否被赋予课程成绩修改的权限
				$authsql = "call search_stuAuth('$studentid')";//
				$authresult = $db->createCommand($authsql)->queryRow();//查询学生权限
				//存储在$authresult['authority'];	//查询该学生成绩是否已经被审核
					//如果该学生已经不存在于grade（奖学金总成汇总表中），那么新建一个该学生的记录
					//如果存在，那么查询该学生的成绩gpacheck是否已经被审核，该记录的初始值为0，表示未被任何人所审核
				if($authresult['authority'] == 0)//并且未被赋予权限允许修改课程成绩，返回空
				{
					$ret['success'] = true;//查询成功，没有权限修改课程成绩
					$ret['message'] = iconv("gb2312","utf-8",'对不起您没有权限修改课程成绩');//
					$ret['results']['hasPrivilege'] =$authresult['authority'];//学生权限
					//var_dump($ret);
					echo json_encode($ret);
				}
				else
				{
					$ret['success'] = true;//查询成功，有权限修改课程成绩
					$ret['message'] = "";//
					$ret['results']['hasPrivilege'] =$authresult['authority'];//学生权限
					$ret['results']['isPassed'] = $checkresult['gpacheck'];//成绩被审核的标志
					$ret['results']['content'] = $results;
					echo json_encode($ret);				
				}

			} catch (Exception $e) {
				//echo $e;
				$ret['success'] = false;
				$ret['message'] = iconv("gb2312","utf-8",'编辑课程成绩信息失败');
				$ret['results'] = array();
				echo json_encode($ret);
			}
		}
		//保存学生修改课程成绩信息
		public function actionSaveReCscore()//编辑学生课程成绩
		{
			if(!isset($_SESSION)) session_start();//打开session
			if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
			{
				echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
				exit;
			}

      		if($_SESSION['WStart'] == false)
      		{
        		echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","学年测评尚未开始或者已经结束！"), "results"=>array()));
        		exit;
      		}		

			$studentid = $_SESSION['ID'];//学生id
			$year = $_SESSION['year'];//年份*/
			$db = Yii::app()->db;
			//测试用例
			//$studentid = "2008301500116";
			//$year = "2011";
			//根据传入参数，查看学生自评审核状态
			$checksql = "call search_stuCheck('$studentid','$year')";
			$checkresult = $db->createCommand($checksql)->queryRow();//学生成绩是否被审核
			if($checkresult['gpacheck'] != 0)//如果自评成绩处于审核状态，不能修改
			{
				$ret['success'] = false;
				$ret['message'] = iconv("gb2312","utf-8",'课程成绩已被审核,信息保存失败');
				$ret['results'] = array();
				echo json_encode($ret);
			}
			else 
			{
				//测试用例
				//$saveScore = json_encode($saveScore);
				try {
					if(!isset($_REQUEST['saveScore']))
						exit;
					$saveScore = isset($_POST["saveScore"]) ? $_POST["saveScore"] : $_GET["saveScore"];
					$saveScore = json_decode($saveScore,true);//解码为数组
					$upscores = $saveScore['update'];//学生更新的课程成绩
					$inscores = $saveScore['insert'];//学生插入的课程成绩
					
					//更新学生课程成绩
					foreach($upscores as $upscore)
					{
						$coureid = $upscore['cid'];//课程id
						$coursescore = $upscore['cscore'];//课程成绩
						$upsql = "update score set coursescore = '$coursescore' 
						where studentid = '$studentid' and year = '$year' and courseid = '$coureid'";
						$db->createCommand($upsql)->execute();//执行该操作
					}
					//插入学生成绩
					//首先插入学生添加的成绩中的课程信息，包括课程的名称,类型，学分
					$incosql = "insert into course(coursename,coursetype,coursepoint) values";
					$temp = $incosql;
					foreach ($inscores as $inscore)
					{
						$coursename = $inscore['cname'];
						$coursetype = $inscore['ctype'];
						$coursepoint = $inscore['cpoint'];
						//根据这三个参数，判断该课程是否存在于数据库之中
						//调用存储过程
						$cosql = "call search_courseId('$coursename','$coursetype','$coursepoint')";
						$corow = $db->createCommand($cosql)->queryRow();//执行该操作
						//课程号信息存在于$corow['courseid']中
						//如果课程号信息不存在，那么直接插入
						if ($corow['courseid'] == null)
							$incosql = $incosql."('$coursename','$coursetype','$coursepoint'),";		
					}
					//如果没有插入课程成绩，那么$incosql语句没有发生变化，将不执行操作
					//否则
					if ($incosql != $temp)//如果发生变化，则说明有插入课程
					{
						$incosql= substr($incosql, 0, -1);//将最后一个逗号去掉
						$db->createCommand($incosql)->execute();//执行该操作
					}
					
					//插入学生课程成绩
					$inscsql = "insert into score(studentid,courseid,year,coursescore)  values";
					$temp = $inscsql;
					foreach ($inscores as $inscore)
					{
						$coursename = $inscore['cname'];
						$coursetype = $inscore['ctype'];
						$coursepoint = $inscore['cpoint'];
						 //根据这三个参数，找到courseid
						$cosql = "call search_courseId('$coursename','$coursetype','$coursepoint')";
						$corow = $db->createCommand($cosql )->queryRow();//执行该操作
						//课程号信息存在于$corow['courseid']中
						$courseid = $corow['courseid'];
						//根据学号，课程号，年份判断该条成绩记录是否存在学生成绩表中
						//调用存储过程查找成绩
						$sescosql = "call search_stucscore('$studentid','$courseid','$year')";
						$sescorow = $db->createCommand($sescosql )->queryRow();//执行该操作
						//成绩存储在$sescorow['coursescore'];
						//如果成绩为空则可以插入
						if ($sescorow['coursescore'] == null)
						{
							$coursescore =  $inscore['cscore'];//课程成绩存在于插入成绩中 
							$inscsql = $inscsql."('$studentid','$courseid','$year','$coursescore'),";		
						}
					}
				//如果没有插入课程成绩，那么$incos ql语句没有发生变化，将不执行操作
					//否则
					if ($inscsql != $temp)//如果发生变化，则说明有插入课程
					{
						$inscsql = substr($inscsql, 0, -1);//将最后一个逗号去掉
						$db->createCommand($inscsql)->execute();//执行该操作
					}
					//到这里仍然没有错误，那么
					$ret['success'] = true;
					$ret['message'] = "";
					$ret['results'] = array();
					echo json_encode($ret);
				} catch (Exception $e) {
					//echo $e;
					$ret['success'] = false;
					$ret['message'] = iconv("gb2312","utf-8",'保存课程成绩信息失败');
					$ret['results'] = array();
					echo json_encode($ret);
				}
			}
		}
		//显示学生课程成绩，根据是否被审核，B1，B2，未参加奖学金评定课程显示
		//计算出成绩最高的最优解
		public function actionGetF2()
		{
			if(!isset($_SESSION)) session_start();//打开session
			if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
			{
				echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
				exit;
			}
			$ret = array();//保存最后结果
			try {
				$db = Yii::app()->db; 
				$studentid = $_SESSION['ID'];//学生id
				$year = $_SESSION['year'];//当前年份
				//测试用例
				//$studentid = "2008301500116";
				//$year = '2011';
				//根据传入参数，查看学生成绩审核状态
				$checksql = "call search_stuCheck('$studentid','$year')";
				$checkresult = $db->createCommand($checksql)->queryRow();//学生成绩是否被审核
				//学生成绩审核状态存储在 $checkresult['gpacheck'];//
				$gpacheck = $checkresult['gpacheck'];

				if ($gpacheck == 0)//如果没有被审核
				{
					//调用存储过程，把所有公共必修，专业必修放在B1里面，即赋值为B1,并且遍历必修成绩，如果出现不及格成绩
					//那么置grade表中学生为不及格
					$scoreB1sql = "call update_B1score('$studentid','$year')";
					$db->createCommand($scoreB1sql)->execute();//执行该操作
					//该程序结果是将所有必修课成绩强制设为B1,并且标志出不不及格的标志
					//根据传入参数，查看学生本年度所有成绩，根据B1，B2，others返回
					$scoresql = "call search_coscores('$studentid','$year')";
					$scoresresults = $db->createCommand($scoresql)->queryAll();//返回学生所有成绩
					//var_dump ($scoresresults);
					//成绩没有被审核，则将所有成绩返回
					$results = array('isPassed'=>"",'B1'=>array(),'B2'=>array(),'others'=>array());
					foreach($scoresresults as $score)//对于其中的每一条记录
					{
						//如果该条成绩是B1则插入到相应的B1中去
						if($score['cchoice'] == 'B1')
						{
							array_push($results['B1'],$score);
						}
						elseif ($score['cchoice'] == 'B2')//如果该条成绩是B2则插入到相应的B2中去
						{
							array_push($results['B2'],$score);
						}
						else array_push($results['others'],$score);
					}
					//至此处理完毕
					$results['isPassed'] = $gpacheck;
					$ret['success'] = true;
					$ret['message'] = "";
					$ret['results'] = $results;

					echo json_encode($ret);	
				}
				else //如果已经被审核，那么只需返回B1，B2的有关内容即可
				{
					//根据传入参数，查看学生本年度所有成绩，根据B1，B2，others返回
					$scoresql = "call search_coscores('$studentid','$year')";
					$scoresresults = $db->createCommand($scoresql)->queryAll();//返回学生所有成绩
					//var_dump ($scoresresults);
					//成绩没有被审核，则将所有成绩返回
					$results = array('isPassed'=>"",'B1'=>array(),'B2'=>array(),'others'=>array());
					foreach($scoresresults as $score)//对于其中的每一条记录
					{
						//如果该条成绩是B1则插入到相应的B1中去
						if($score['cchoice'] == 'B1')
						{
							array_push($results['B1'],$score);
						}
						elseif ($score['cchoice'] == 'B2')//如果该条成绩是B2则插入到相应的B2中去
						{
							array_push($results['B2'],$score);
						}
					}
					//至此处理完毕
					$results['isPassed'] = $gpacheck;
					$ret['success'] = true;
					$ret['message'] = "";
					$ret['results'] = $results;
					echo json_encode($ret);	
				}
				
			} catch (Exception $e) {
				//echo $e;
				$ret['success'] = false;
				$ret['message'] = iconv("gb2312","utf-8",'查询课程成绩信息失败');
				$ret['results'] = array();
				echo json_encode($ret);
			}
		}
		
		//学生保存自评后的F2成绩----前端会做个规则限制，不让学生乱选
		public function actionSaveF2()
		{
			if(!isset($_SESSION)) session_start();//打开session
			if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
			{
				echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
				exit;
			}
			
      		if($_SESSION['WStart'] == false)
      		{
        		echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","学年测评尚未开始或者已经结束！"), "results"=>array()));
        		exit;
      		}

      		$studentid = $_SESSION['ID'];//学生id
			$year = $_SESSION['year'];//当前年份
			//测试用例
			//$studentid = "2008301500116";
			//$year = '2011';
			$db = Yii::app()->db; 
			//根据传入参数，查看学生自评审核状态
			$checksql = "call search_stuCheck('$studentid','$year')";
			$checkresult = $db->createCommand($checksql)->queryRow();//学生成绩是否被审核
			if($checkresult['gpacheck'] != 0)//如果自评成绩处于审核状态，不能修改
			{
				$ret['success'] = false;
				$ret['message'] = iconv("gb2312","utf-8",'课程成绩已被审核,信息保存失败');
				$ret['results'] = array();
				echo json_encode($ret);
			}
			else 
			{
				$ret = array();//保存最后结果
				try {
					if(!isset($_REQUEST['saveF2']))
						exit;

					$saveF2 = isset($_POST["saveF2"]) ? $_POST["saveF2"] : $_GET["saveF2"];
					$saveF2 = json_decode($saveF2,true);//解码为数组
					//对其中的数组进行判断读取
					//对于赋值B1的cid
					$B1scores = $saveF2['B1'];
					foreach($B1scores as $B1score)
					{
						//对于其中的某一条记录
						$B1sql = "update score set cchoice = 'B1' 
							where studentid = '$studentid' and courseid = $B1score and year = '$year'";
						$db->createCommand($B1sql)->execute();//执行该操作
					}
					//B1结束
					//对于赋值B2的cid
					$B2scores = $saveF2['B2'];
					foreach($B2scores as $B2score)
					{
						//对于其中的某一条记录
						$B2sql = "update score set cchoice = 'B2' 
							where studentid = '$studentid' and courseid = $B2score and year = '$year'";
						$db->createCommand($B2sql)->execute();//执行该操作
					}
					//B2结束
					//对学生成绩的grade表更新
					$score = $saveF2['score'];
					$scoresql = "update grade set gpascore = '$score' 
							where studentid = '$studentid' and year = '$year'";
					$db->createCommand($scoresql)->execute();//执行该操作
					//如果没有错误
					$ret['success'] = true;
					$ret['message'] = "";
					$ret['results'] = array();
					echo json_encode($ret);	
				} catch (Exception $e) {
					//echo $e;
					$ret['success'] = false;
					$ret['message'] = iconv("gb2312","utf-8",'保存信息失败');
					$ret['results'] = array();
					echo json_encode($ret);
				}
			}
		}

		//显示创新实践自评列表
		public function actionDisplaySelfScore()
		{
			if(!isset($_SESSION)) session_start();//打开session
			if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
			{
				echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
				exit;
			}
			$ret = array();//保存最后结果
			try {

				if(!isset($_REQUEST['reviseSelfScore']))
						exit;

				$SelfScore = isset($_POST["reviseSelfScore"]) ? $_POST["reviseSelfScore"] : $_GET["reviseSelfScore"];
				$SelfScore = json_decode($SelfScore,true);//解码为数组
				//对其中的参数进行读取判断
				if($SelfScore['sid'] == "")//如果为空，表示是学生自己查询
					$studentid = $_SESSION['ID'];
				else 
					$studentid = $SelfScore['sid'];//学号
				$year = $SelfScore['year'];//年份
				$db = Yii::app()->db; 
				//测试用例
				//$studentid = "2008301500116";
				//$year = '2011';
				//调用存储过程
				//根据传入参数，查看学生自评审核状态
				$checksql = "call search_stuCheck('$studentid','$year')";
				//echo $checksql;
				
				////queryRow有隐患。。。。。。。
				$checkresult = $db->createCommand($checksql)->queryRow();//学生成绩是否被审核
				//var_dump($checkresult);
				//学生自评审核状态存储在 $checkresult['selfcheck'];
				//调用存储过程，查询学生所有自评成绩，包括F1选项中得自评
				$sql = "call search_selfscores('$studentid','$year')";
		    	$resultAll = $db->createCommand($sql)->queryAll();	    	
		    	
		    	$ret = array();
		    	$results = array();//F3个人自评成绩结果存储，相当于$score
		    	$add = array('choose'=>1,'content'=>"",'score'=>"",'selfid'=>"");//加分情况
		    	$sub =array('choose'=>2,'content'=>"",'score'=>"",'selfid'=>"");;//减分情况
		    	foreach ($resultAll as $result)//对每一项成绩进行分析
		    	{
		    		if($result['choose'] == '1')//如果是加分，只允许一项
		    		{
		    			$add = array('choose'=>$result['choose'],
		    			'content'=>$result["content"],
		    			"score"=>$result["score"],
		    			"selfid"=>$result['selfid']);
		    		}
		    		elseif ($result['choose'] == '2')//如果是减分
		    		{
		    			$sub = array('choose'=>$result['choose'],
		    			'content'=>$result["content"],
		    			"score"=>$result["score"],
		    			"selfid"=>$result['selfid']);
		    		}
		    		else//如果不是F1中得加分选项
		    		{	
			    		array_push($results, array('selfid'=>$result['selfid'],'choose'=>$result['choose'],
			    		"content"=>$result["content"],"score"=>$result["score"]));
		    		}
		    	}
		    	//调用存储过程，查询该学院该学年的所有的学生自评成绩选项的参数
		    	$departmentid = $_SESSION['departmentid'];
				//var_dump($departmentid);
		    	//$departmentid = 2;
		    	$selfconsql = "call search_depselfcon('$departmentid','$year')";//
		    	$selfcon = $db->createCommand($selfconsql)->queryAll();	
		    	//如果没有出现错误
		    	$gather['isPassed']= $checkresult['selfcheck'];
		    	$gather['score']=$results;
		    	$gather['chooses']= $selfcon;
		    	$gather['isF1Passed']= $checkresult['selfcheck'];//暂时这样
		    	$gather['add'] = $add;
		    	$gather['sub'] = $sub;
		    	//整理完毕
		    	$ret['success'] = true;
				$ret['message'] = "";
				$ret['results'] = $gather;
				echo json_encode($ret);	
			} catch (Exception $e) {
				//echo $e;
				$ret['success'] = false;
				$ret['message'] = iconv("gb2312","utf-8",'显示信息失败');
				$ret['results'] = array();
				echo json_encode($ret);
			}
		}

	/*存储过程修改search_selfscores
	begin
	select selfscoreid as selfid,evaluateid as choose,content,evascore as score from selfevascore
	where studentid= stuid and year = tyear 
	order by choose;
	end*/

	//保存创新实践自评列表-------
	public function actionSaveSelfscore()
	{
		if(!isset($_SESSION)) session_start();//打开session
		if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
		{
			$url = "../../../";
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
			exit;
		}

		if($_SESSION['WStart'] == false)
		{
		  echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","学年测评尚未开始或者已经结束！"), "results"=>array()));
		  exit;
		}

		$studentid = $_SESSION['ID'];//学生id
		$year = $_SESSION['year'];//年份*/
		//测试用例
		//$studentid = "2008301500116";
		//$year = "2011";
		//用例结束
		$ret = array();//保存最后结果
		$db = Yii::app()->db;	
		//根据传入参数，查看学生自评审核状态
		try {
			$checksql = "call search_stuCheck('$studentid','$year')";
			$checkresult = $db->createCommand($checksql)->queryRow();//学生成绩是否被审核
			if($checkresult['selfcheck'] != 0)//如果自评成绩处于审核状态，不能修改
			{
				$ret['success'] = false;
				$ret['message'] = iconv("gb2312","utf-8",'自评成绩已被审核,信息保存失败');
				$ret['results'] = array();
				echo json_encode($ret);
			}
			else
			{

				if(!isset($_REQUEST['studentSelfEva']))
					exit;
				$selfEva = isset($_POST["studentSelfEva"]) ? $_POST["studentSelfEva"] : $_GET["studentSelfEva"];
				$selfEva  = json_decode($selfEva ,true);//解码为数组
				//测试用例
				//用例结束
				$update = $selfEva['update'];
				$insert = $selfEva['insert'];
				$delete = $selfEva['delete'];
				//对于更新其中的每一条记录
				foreach ($update as $upself) {
					$selfid = $upself['selfid'];
					$content = $upself['content'];

         //edit by pengchao 20120221
         //防止sql注入
         		$inject = new Inject; 
		     	$content  = $inject->injectCheck($content);
         //防止结束

					$score = $upself['score'];
					$upsql = "update selfevascore set content = '$content' ,evascore = '$score' 
					where selfscoreid = '$selfid' ";
					$db->createCommand($upsql)->execute();
				}
				//对于插入其中的记录
				foreach ($insert as $inself)
				{
					$choose = $inself['choose'];
					$content = $inself['content'];
          //edit by pengchao 20120221
          //防止sql注入
          		$inject = new Inject; 
		      	$content  = $inject->injectCheck($content);
          //防止结束
					$score = $inself['score'];
					$insql = "insert into selfevascore (studentid,evaluateid,year,content,evascore)  value
					('$studentid','$choose','$year','$content','$score')";
					$db->createCommand($insql)->execute();
				}
				//对于删除其中的记录
				foreach ($delete as $deself)
				{
					$selfid = $deself;
					$desql = "delete from selfevascore where selfscoreid = '$selfid'";
					$db->createCommand($desql)->execute();
				}
				//如果没有错误
				$ret = array('success'=>true,'message'=>"",'results'=>array());
				echo json_encode($ret);
			}
		} catch (Exception $e) {
			//echo $e;
			$ret['success'] = false;
			$ret['message'] = iconv("gb2312","utf-8",'编辑保存学生自评成绩信息失败');
			$ret['results'] = array();
			echo json_encode($ret);
		}
	}

    //重置密码
		public function actionMoStuPass()//重置密码
		{
			if(!isset($_SESSION)) session_start();
			if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
			{
				$url = "../../../";
				echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
				exit;
			}

			if(!isset($_REQUEST['modifyPass']))
					exit;

			$modifyPass= isset($_POST["modifyPass"])? $_POST["modifyPass"]:$_GET["modifyPass"];
			//测试用例
			//$modifyPass = array(
			//		'oldpass'=>'33a269895846aa3d44384aa89e15a439',
			//		'newpass'=>'123456');
			//$modifyPass = json_encode($modifyPass);
			
			$modifyPass = json_decode($modifyPass,true);//解码为数组
			$type = $_SESSION['type'];//通过session获取登录者角色：学生、教师还是管理员，其中管理员分为学院管理员和学工部管理员
			//测试用例
			//$studentid = "2008301500116";
			$oldpass = $modifyPass['oldpass'];
			$newpass = $modifyPass['newpass'];
			$ret = array();
			try {
				$db =Yii::app()->db;

				if ($type == "student")//登录者为学生
				{
					$studentid = $_SESSION['ID'];//获得学号

					$sql = "select password from student where studentid = '$studentid'";
					$row = $db->createCommand($sql)->queryRow();
					//首先判断学号输入是否正确
					$password = $row['password'];
					if($password != $oldpass)//如果输入的旧密码错误
					{
						$ret['success'] = false;
						$ret['message'] = iconv("gb2312","utf-8",'密码输入错误');
						$ret['results'] = array();
						echo json_encode($ret);	
					}
					else //学号和密码都输入正确,则更新密码
					{
						$sql = "update student set password = '$newpass' where studentid = '$studentid'";
						$db->createCommand($sql)->execute();
						//更新成功
						$ret['success'] = true;
						$ret['message'] = "";
						$ret['results'] = array();
						echo json_encode($ret);	
					}	
				}
				elseif ($type == "teacher")//登录者为教师
				{
					$teacherid = $_SESSION['ID'];//获得教师工号

					$sql = "select password from teacher where teacherid = '$teacherid'";
					$row = $db->createCommand($sql)->queryRow();
					//首先判断教师工号输入是否正确
					$password = $row['password'];
					if($password != $oldpass)//如果输入的旧密码错误
					{
						$ret['success'] = false;
						$ret['message'] = iconv("gb2312","utf-8",'密码输入错误');
						$ret['results'] = array();
						echo json_encode($ret);	
					}
					else //教师工号和密码都输入正确,则更新密码
					{
						$sql = "update teacher set password = '$newpass' where teacherid = '$teacherid'";
						$db->createCommand($sql)->execute();
						//更新成功
						$ret['success'] = true;
						$ret['message'] = "";
						$ret['results'] = array();
						echo json_encode($ret);	
					}
				}
				else //登录者为管理员，管理员分为学院管理员和学工部管理员
				{
					if ($type == "supermanager")//登录者超级管理员也就是学院管理员
					{
						$supermanagerid = $_SESSION['ID'];//获得工号

						$sql = "select password from manager where managerid = '$supermanagerid'";
						$row = $db->createCommand($sql)->queryRow();
						//首先判断工号输入是否正确
						$password = $row['password'];
						if($password != $oldpass)//如果输入的旧密码错误
						{
							$ret['success'] = false;
							$ret['message'] = iconv("gb2312","utf-8",'密码输入错误');
							$ret['results'] = array();
							echo json_encode($ret);	
						}
						else //工号和密码都输入正确,则更新密码
						{
							$sql = "update manager set password = '$newpass' where managerid = '$supermanagerid'";
							$db->createCommand($sql)->execute();
							//更新成功
							$ret['success'] = true;
							$ret['message'] = "";
							$ret['results'] = array();
							echo json_encode($ret);	
						}
					}
					else //登录者为学院管理员
					{
						$managerid = $_SESSION['ID'];//获得工号

						$sql = "select password from manager where managerid = '$managerid'";
						$row = $db->createCommand($sql)->queryRow();
						//首先判断工号输入是否正确
						$password = $row['password'];
						if($password != $oldpass)//如果输入的旧密码错误
						{
							$ret['success'] = false;
							$ret['message'] = iconv("gb2312","utf-8",'密码输入错误');
							$ret['results'] = array();
							echo json_encode($ret);	
						}
						else //工号和密码都输入正确,则更新密码
						{
							$sql = "update manager set password = '$newpass' where managerid = '$managerid'";
							$db->createCommand($sql)->execute();
							//更新成功
							$ret['success'] = true;
							$ret['message'] = "";
							$ret['results'] = array();
							echo json_encode($ret);	
						}
					}
				}

			} catch (Exception $e) {
						$ret['success'] = false;
						$ret['message'] = iconv("gb2312","utf-8",'更新密码失败');
						$ret['results'] = array();
						echo json_encode($ret);	
				
			}																	
		}

		//返回指定学生、指定年份的课程成绩
		public function actionGetCoScores()//返回指定学生指定年份的课程成绩
		{
			if(!isset($_SESSION)) session_start();
			if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
			{
				$url = "../../../";
				echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
				exit;
			}

			if(!isset($_REQUEST['searchGPA']))
				exit;

			$searchGPA = isset($_POST["searchGPA"]) ? $_POST["searchGPA"] : $_GET["searchGPA"];
			$searchGPA = json_decode($searchGPA,true);//解码为数组
			if($searchGPA ['sid'] == "")//为空表示是学生本人进行查询，不需要studentid
				$studentid = $_SESSION['ID'];
			else 
				$studentid = $searchGPA['sid'];
			$year = $searchGPA['year'];
			$ret = array();
			//测试用例
			//$studentid = '2008301500116';
			//$year = '2011';
			
			try {
					$db = Yii::app()->db;
					$sql = "call search_coscores('$studentid','$year')";
					$results = $db->createCommand($sql)->queryAll();
					//如果没有出现异常错误
					$ret['success'] = true;
					$ret['message'] = "";
					$ret['results'] = $results;
					echo json_encode($ret);							
			} catch (Exception $e) {
					$ret['success'] = false;
					$ret['message'] = iconv("gb2312","utf-8",'返回课程成绩信息失败');
					$ret['results'] = array();
					echo json_encode($ret);				
			}			
		}
		
		//返回指定学生、指定年份的基本素质成绩，加入F1的自评加减
		public function actionGetBasicScore()
		{
			if(!isset($_SESSION)) session_start();
			if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
			{
				$url = "../../../";
				echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
				exit;
      }
			if(!isset($_REQUEST['searchBasic']))
				exit;

			$searchBasic= isset($_POST["searchBasic"])? $_POST["searchBasic"]:$_GET["searchBasic"];
			$searchBasic = json_decode($searchBasic,true);//解码为数组
			if($searchBasic['sid'] == "")
				$studentid = $_SESSION['ID'];
			else 
				$studentid = $searchBasic['sid'];
			$year = $searchBasic['year'];
			//测试用例
			//$studentid = '2009302530001';
			//$year = '2011';
			$db = Yii::app()->db;
		    $ret = array();
		    $results = array();
		    $add = array();
		    $sub = array();
		    try {
		    		//查找学生基本素质总得分
		    		$Basicsql = "select qualityscore from grade 
		    		where studentid = '$studentid' and year = '$year'";
		    		$Basicrow = $db->createCommand($Basicsql)->queryRow();//查询
					if(is_array($Basicrow))
		    			$score = $Basicrow['qualityscore'];
					else $score = 0;
		    		//查询互评各个得分
		    		//调用存储过程
		    		$musql = "call search_mutualscores('$studentid','$year')";
					//echo $musql;
		    		$muResults = $db->createCommand($musql)->queryAll();//查询
					//var_dump($muResults);
		    		//查询F1加减分
		    		$F1self = "select evaluateid as choose,content,evascore as score,t_evascore,
		    		if(te_evascore is NULL, t_evascore, te_evascore) as te_evascore, isPassed as 'check' from selfevascore
		    	 where studentid = '$studentid' and year = '$year' and (evaluateid = 1 or evaluateid = 2)";
		    		$F1selfscores = $db->createCommand($F1self)->queryAll();//查询
		    		//对$F1selfscore内容判断分析
		    		foreach ($F1selfscores as $F1selfscore)
		    		{
		    			if($F1selfscore['choose'] == 1)//如果是加分，那么
		    				$add = $F1selfscore;
		    			elseif ($F1selfscore['choose'] == 2)
		    				$sub = $F1selfscore;
		    		}
		    		//处理完毕
		    		$results = array('score'=>$score,'contents'=>$muResults,'add'=>$add,'sub'=>$sub);
		    		//如果没有出现异常错误
					$ret['success'] = true;
					$ret['message'] = "";
					$ret['results'] = $results;
					echo json_encode($ret);	
		    } catch (Exception $e) {
		    		//echo $e;
		    		$ret['success'] = false;
					$ret['message'] = iconv("gb2312","utf-8",'返回信息失败');
					$ret['results'] = array();
					echo json_encode($ret);		
		    }			
		}
		
		//返回指定学生，指定年份的学生的自评成绩列表，减去F1的自评加减
		public function actionGetSelfScore()
		{
			if(!isset($_SESSION)) session_start();
			if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
			{
				$url = "../../../";
				echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
				exit;
			}

			if(!isset($_REQUEST['searchSelf']))
				exit;

			$searchSelf= isset($_POST["searchSelf"])? $_POST["searchSelf"]:$_GET["searchSelf"];
			$searchSelf = json_decode($searchSelf,true);//解码为数组
			if($searchSelf['sid'] == "")
				$studentid = $_SESSION['ID'];
			else 
				$studentid = $searchSelf['sid'];
			$year = $searchSelf['year'];
			//测试用例
			//$studentid = '2008301500116';
			//$year = '2011';
			$db = Yii::app()->db;
		    $ret = array();
	    	try {
	    		//调用存储过程，查询学生所有自评成绩，包括F1选项中得自评
				$sql = "call search_deselfscores('$studentid','$year')";
		    	$resultAll = $db->createCommand($sql)->queryAll();	    	
		    	$choose = -1;//该学生成绩的第一个测评选项的id
		    	$array_count = -1;
		    	$ret = array();
		    	$results = array();//F3个人自评成绩结果存储，相当于$contents
		    	foreach ($resultAll as $result)//对每一项成绩进行分析
		    	{
		    		if($result['evaluateid'] != '1' && $result['evaluateid'] != '2')//如果不是加减分
		    		{
			    		if($choose != $result['evaluateid'])
			    		{
			    			$array_count++;
			    			$results[$array_count]["choose"] = $result['evaluatename'];
			    			$results[$array_count]["score"] =  $result['maxscore'];
			    			$results[$array_count]["contents"] = array();
			    			$choose = $result['evaluateid'];
			    		}		    		
			    		array_push($results[$array_count]["contents"], 
			    		array("content"=>$result["content"],
			    		"score"=>$result["evascore"],
			    		't_evascore'=>$result["t_evascore"],
			    		'te_evascore'=>$result["te_evascore"]
			    		));
		    		}
		    	}
		    	
		    	//查询学生自评成绩总得分以及审核情况
	    		$selfsql = "select selfscore,selfcheck from grade 
	    		where studentid = '$studentid' and year = '$year'";
	    		$selfrow = $db->createCommand($selfsql)->queryRow();//查询
	    		//如果没有出现错误
	    		$ret['results']['score'] = $selfrow['selfscore'];
	    		$ret['results']['check'] = $selfrow['selfcheck'];
	    		$ret['results']['contents']  = $results;
	    		$ret['success'] = true;
				$ret['message'] = "";
				echo json_encode($ret);	
	    	} catch (Exception $e) {
    			//echo $e;
	    		$ret['success'] = false;
				$ret['message'] = iconv("gb2312","utf-8",'返回信息失败');
				$ret['results'] = array();
				echo json_encode($ret);	
	    	}	    	
		}
		
		//返回指定学生，指定年份的学生总评成绩，需要将上述三种数据类型综合返回

		public function actionGetFinal()//得到学生的最终成绩
		{
			if(!isset($_SESSION)) session_start();
			if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
			{
				$url = "../../../";
				echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
				exit;
			}

			if(!isset($_REQUEST['searchFinal']))
				exit;

			$searchFinal= isset($_POST["searchFinal"])? $_POST["searchFinal"]:$_GET["searchFinal"];
			$searchFinal = json_decode($searchFinal,true);//解码为数组
			if($searchFinal['sid'] == "")
				$studentid = $_SESSION['ID'];
			else 
				$studentid = $searchFinal['sid'];
			$year = $searchFinal['year'];
			//测试用例
			//$studentid = '2008301500116';

			//$year = '2011';
			$db = Yii::app()->db;
		    $ret = array();//最后返回结果
		    $finalresults = array();//结果集
		    $gpascore = array();
		    $basicscore = array();
		    $selfscore = array();	    
		    try {
		    	//查询学生各项成绩的审核状态
		    	//根据传递的参数查询学生是否被审核
				$checksql = "call search_stuCheck('$studentid','$year')";
				$checkresult = $db->createCommand($checksql)->queryRow();//学生成绩是否被审核
				//存储在 $checkresult['gpacheck'];  gpacheck,qualitycheck,selfcheck//
				//只有当均被辅导员审核之后才能返回最终成绩
				if($checkresult['gpacheck'] != 0 && $checkresult['qualitycheck'] == 2 && $checkresult['selfcheck'] == 2 )
				{
					//查询具体各项得分
			    	$Fsql = "select gpascore,qualityscore,selfscore,total from grade 
			    	where studentid = '$studentid' and year = '$year'";
			    	$FRow = $db->createCommand($Fsql)->queryRow();
			    	//查询学生所在测评班级的具体得分参数F1，F2，F3,调用存储过程
            		$score = $FRow['total'];
			    	//查询学生选择参与奖学金评定的学生成绩
			    	//查询课程成绩具体得分
			    	$scoresql = "call search_F2coscores('$studentid','$year')";
					$scoreresults = $db->createCommand($scoresql)->queryAll();
					$gpascore = array('score'=>$FRow['gpascore'],'contents'=>$scoreresults);
					//保存课程记录完毕
					
					
					
					//查询学生基本素质得分 
					//查询互评各个得分
					$add = array();//F1加分
					$sub = array();//F1减分
					$contents = array();//
					//如果老师审核通过，那么
		    		$musql = "Select t_a1polotics as t_A1,t_a2morality as t_A2,t_a3attitudes as t_A3,
		    			t_a4discipline as t_A4,t_a5physical as t_A5  from mutualeva 
		    			where studentid = '$studentid' and year = '$year'";
		    		$muResults = $db->createCommand($musql)->queryAll();//查询
		    		//查询F1加减分
		    		$F1self = "select evaluateid as choose,content,te_evascore as score from selfevascore
		    		 where studentid = '$studentid' and year = '$year' and (evaluateid = 1 or evaluateid = 2)";
		    		$F1selfscores = $db->createCommand($F1self)->queryAll();//查询
		    		//对$F1selfscore内容判断分析
		    		foreach ($F1selfscores as $F1selfscore)
		    		{
		    			if($F1selfscore['choose'] == 1)//如果是加分，那么
		    				$add = $F1selfscore;
		    			elseif ($F1selfscore['choose'] == 2)
		    				$sub = $F1selfscore;
		    		}
		    		//处理完毕
		    		$basicscore = array('score'=>$FRow['qualityscore'],'add'=>$add,
		    					'sub'=>$sub,'contents'=>$muResults);
		    		//保存基本素质测评结束
		    		
		    		
		    		
		    		
		    		
		    		//查询学生自评情况
					//调用存储过程，查询学生所有自评成绩，包括F1选项中得自评
					$sql = "call search_deselfscores('$studentid','$year')";
			    	$resultAll = $db->createCommand($sql)->queryAll();	    	
			    	$choose = -1;//该学生成绩的第一个测评选项的id
			    	$array_count = -1;
			    	$results = array();//F3个人自评成绩结果存储，相当于$contents
			    	foreach ($resultAll as $result)//对每一项成绩进行分析
			    	{
			    		if($result['evaluateid'] != '1' && $result['evaluateid'] != '2')//如果不是加减分
			    		{
				    		if($choose != $result['evaluateid'])
				    		{
				    			$array_count++;
				    			$results[$array_count]["choose"] = $result['evaluatename'];
				    			$results[$array_count]["score"] =  $result['maxscore'];
				    			$results[$array_count]["contents"] = array();
				    			$choose = $result['evaluateid'];
				    		}		    		
				    		array_push($results[$array_count]["contents"], 
				    			array("content"=>$result["content"],'te_evascore'=>$result["te_evascore"])
				    		);
			    		}
			    	}
			    	//处理完毕
			    	$selfscore = array('score'=>$FRow['selfscore'],'contents'=>$results);
			    	//
			    	
			    	$finalresults['score'] = $score;

					$finalresults['gpascore'] =$gpascore;
					$finalresults['basicscore'] = $basicscore;
					$finalresults['selfscore'] = $selfscore;
					$ret['success'] = true;
					$ret['message'] = "";
					$ret['results'] = $finalresults;
					echo json_encode($ret);
			    	
			    	
					}
				else 
				{
					$finalresults['score'] = 0;
					$finalresults['gpascore'] = array();
					$finalresults['basicscore'] = array();
					$finalresults['selfscore'] = array();
					$ret['success'] = true;
					$ret['message'] = "";
					$ret['results'] = $finalresults;
					echo json_encode($ret);
				}		
		    } catch (Exception $e) {
		    	//echo $e;
	    		$ret['success'] = false;
				$ret['message'] = iconv("gb2312","utf-8",'返回信息失败');
				$ret['results'] = array();
				echo json_encode($ret);	
		    	
		    }
		}	
		//返回指定学生，指定年份的学生获得奖学金情况
		public function actionSearchScholar()
		{
			if(!isset($_SESSION)) session_start();
			if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
			{
				$url = "../../../";
				echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
				exit;
			}
			if(!isset($_REQUEST['searchScholarship']))
				exit;
			$searchScholarship= isset($_POST["searchScholarship"])? $_POST["searchScholarship"]:$_GET["searchScholarship"];
			$Scholar = json_decode($searchScholarship,true);//解码为数组
			//$studentid = $Scholar['sid'];
      		if($Scholar['sid'] == null)                             
        		$studentid = $_SESSION['ID']; 
      		else
        		$studentid = $Scholar['sid'];  			

      		$year = $Scholar['year'];
			//测试用例
			//$studentid = '2009302530009';
			//$year = '2011';
			$db = Yii::app()->db;
		    $ret = array();//最后返回结果
		    //,getRank('$studentid','$year') as rank
		    try {
		    	$desql = "select distinct student.studentid as sid,student.studentname as sname,qualityscore as basicscore,gpascore,selfscore,total from student,grade where student.studentid = '$studentid' and grade.studentid = '$studentid' and grade.year = '$year'";
		    	$deResults = $db->createCommand($desql)->queryRow();//查询学生相信息
          		//print_r($deResults);
          		if($deResults == false)  //student表或者grade表格中没有该学生，那么表示没有获奖记录
          		{ 
            		echo json_encode(array('success'=>true,'message'=>"",'results'=>array()));
            		exit;
          		}
		    	//var_dump($deResults);
		    	//查询奖学金获奖情况
          		$excellent = iconv("gb2312", "utf-8", "优秀学生奖学金");
		    	$scsql = "select group_concat(if(type = '$excellent', concat(name,type), name)) as scp from scholarshiptype where scholarshipid in (select scholarshipid from scholarship where studentid = '$studentid' and year = '$year')";
		    	$scResults = $db->createCommand($scsql)->queryRow();//查询学生相信息
		    	//将奖学金获奖情况添加到学生信息中去
		    	if($scResults['scp'] != null)
		    	$deResults['scholarship'] = $scResults['scp'];
		    	$ret = array('success'=>true,'message'=>"",'results'=>$deResults);
				echo json_encode($ret);
		    } catch (Exception $e) {
		    	//echo $e;
				$ret['success'] = false;
				$ret['message'] = iconv("gb2312","utf-8",'返回奖学金获奖信息失败');
				$ret['results'] = array();
				echo json_encode($ret);	
		    }
		}
		
	}
?>
