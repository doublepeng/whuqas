<?php

class TeacherController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	//权限修改
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated users to access all actions
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	
	public function actionGetStuByClass()//返回测评班级需要测评的学生列表
	{
		if(!isset($_SESSION)) session_start();//开启缓存
		if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
		{
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
			exit;
		}
		$getStuByClass= isset($_POST["getStuByClass"])? $_POST["getStuByClass"]:$_GET["getStuByClass"];
		$getStuByClass = json_decode($getStuByClass,true);//解码为数组
		$tclassid = $getStuByClass['t_classid'];
		$classid = $getStuByClass['classid'];
		$db = Yii::app()->db;
		$ret = array();
		//测试用例
		//$tclassid = 1;
		//$classid = "1";
		try {
			if($classid == "")//如果没有具体到某一个班级，那么测评班级全部返回
			{
				//根据t_classid查找
				$sql = "select studentid as sid,studentname as sname,whuclass.classname as cname
				from student,whuclass where student.classid = whuclass.classid
				and whuclass.t_classid = '$tclassid' order by student.classid,sid";
				$results = $db->createCommand($sql)->queryAll();//查询	
				//var_dump($results);
				//如果没有出现错误
				$ret = array('success'=>true,'message'=>"",'results'=>$results);
				echo json_encode($ret);		
			}	
			else//如果具体到某一个班级，那么
			{
				$sql = "select studentid as sid,studentname as sname,whuclass.classname as cname
				from student,whuclass where student.classid = whuclass.classid
				and whuclass.classid = '$classid' order by student.classid,sid";
				$results = $db->createCommand($sql)->queryAll();//查询	
				//如果没有出现错误
				$ret = array('success'=>true,'message'=>"",'results'=>$results);
				echo json_encode($ret);	
			}			
		} catch (Exception $e) {
			//echo $e;
    		$ret['success'] = false;
			$ret['message'] = iconv("gb2312","utf-8",'返回学生信息失败');
			$ret['results'] = array();
			echo json_encode($ret);	
		}
	}
	
	public function actionSearchTestTeam()//测评班级所对应的测评小组
	{
		if(!isset($_SESSION)) session_start();//开启缓存
		if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
		{
			$url = "../../../";
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
			exit;
		}
		$TestTeam= isset($_POST["searchTestTeam"])? $_POST["searchTestTeam"]:$_GET["searchTestTeam"];
		$TestTeam = json_decode($TestTeam,true);//解码为数组
		$tclassid = $TestTeam['t_classid'];
		$departmentid = $_SESSION['departmentid'];
		//测试用例
		//$tclassid = 1;
		//$departmentid = 2;
		//测试结束
		$db = Yii::app()->db;
		$ret = array();
		try {
			$teachername = iconv('gb2312',"utf-8","测评小组");
			$sql = "select student.studentid as sid,student.studentname as sname,whuclass.classname as cname
			from student,whuclass where student.studentid in (select teacherid from teacher 
			where teachername = '$teachername' and departmentid = '$departmentid')
			and student.classid = whuclass.classid and whuclass.t_classid = '$tclassid' ";
			$results = $db->createCommand($sql)->queryAll();//查询	
			//如果没有出现错误
			$ret = array('success'=>true,'message'=>"",'results'=>$results);
			echo json_encode($ret);	
		} catch (Exception $e) {
			//echo $e;
			$ret = array('success'=>false,'message'=>iconv("gb2312","utf-8",'返回测评小组列表信息失败'),
			'results'=> array());	
			echo json_encode($ret);	
		}
	}
	
		//保存测评小组列表 ，设置为每年都重新添加
	public function actionAddStuTeam()//根据测评班级id，测评小组成员学号存储到教师中去
	{
		if(!isset($_SESSION)) session_start();//开启缓存
		if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
		{
			$url = "../../../";
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
			exit;
		}
    if($_SESSION['WStart'] == false)
    {
      echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","学年测评尚未开始或者已经结束！"), "results"=>array()));
      exit;
    }

		$StudentTeam= isset($_POST["addStudentTeam"])? $_POST["addStudentTeam"]:$_GET["addStudentTeam"];
		$StudentTeam = json_decode($StudentTeam,true);//解码为数组
		$tclassid = $StudentTeam['t_classid'];
		$sids = $StudentTeam['sids'];
		$departmentid = $_SESSION['departmentid'];
		$teachername = iconv('gb2312','utf-8',"测评小组");
		//密码设置为MD5(123456)
		$password = md5(123456);
		$db = Yii::app()->db;
		$ret = array();
		//测试用例
		/*$sids = array(
			array('sid'=>'2009302530001'),
			array('sid'=>'2009302530002'),
			array('sid'=>'2009302530005'),
			array('sid'=>'2009302530008'));
		$departmentid = 2;
		$tclassid = 1;*/
		$teachername = iconv('gb2312','utf-8',"测评小组");
		//$password = md5(123456);
		//测试用例结束
    $year = $_SESSION['year'];
		try {
			//用来获取密码的查询
      //只有当测评班级中的班级成员中的基本素质和加减分数都没有被审核的时候，才能重新添加测评班级
      //根据前端传来的测评班级id，再根据所包含的班级成员的t_evascore进行判断是否已经开始测评,自我测评
        $tsql = "select studentid from selfevascore where studentid in (select studentid from student where classid in (select classid from whuclass where       t_classid = '$tclassid')) and t_evascore is not NULL and year = '$year'";//查询进行修改的数目
				$trow = $db->createCommand($tsql)->queryRow();//查询
        
        //基本素质
        $basql = "select studentid from mutualeva where studentid in (select studentid from student where classid in (select classid from whuclass where       t_classid = '$tclassid')) and year = '$year' and a1polotics != 0";
        $barow = $db->createCommand($basql)->queryRow();//查询
        
        if($trow != false | $barow!= false)//如果有返回值，那么说明已经开始测评
        {
           $ret = array('success'=>false,'message'=>
					 	iconv("gb2312","utf-8",'测评小组成员已开始进行测评,不允许修改'),'results'=> array());	
					 	echo json_encode($ret); 
					 	exit();
        }



      //
      
			if(count($sids) != 0)
			{
				$getpass = 'select studentid, password from student where studentid in (';
				foreach ($sids as $sid)//对于其中的每一条记录
				{
					$getpass = $getpass."'$sid[sid]',";
				}
				$getpass = substr($getpass, 0, -1);  $getpass = $getpass.");";
				$sids = $db->createCommand($getpass)->queryAll();//查询	
			}
			
			//插入测评小组
			$instusql = "insert into teacher (teacherid,departmentid,teachername,password)  values";
			$temp = $instusql;		
			foreach ($sids as $sid)//对于其中的每一条记录
			{
				//得到$teacherid
				$teacherid = $sid['studentid'];//
				$password = $sid["password"];
				//拼接$sql
				$instusql = $instusql."('$teacherid','$departmentid','$teachername','$password'),";	
			}
			if ($instusql != $temp)//如果发生变化，则说明有插入
			{

				$desql ="delete from teacher where teacherid in (select studentid from student,whuclass
				where student.classid = whuclass.classid and whuclass.t_classid = '$tclassid')";
				$db->createCommand($desql)->execute();
				$instusql = substr($instusql, 0, -1);//将最后一个逗号去掉
				$db->createCommand($instusql)->execute();//执行该操作
			}
			//到这里仍然没有错误，那么
			$ret['success'] = true;
			$ret['message'] = "";
			$ret['results'] = array();
			echo json_encode($ret);	
		} catch (Exception $e) {
			//echo $e;
			$ret['success'] = false;
			$ret['message'] = iconv("gb2312","utf-8",'重复保存,保存信息失败');
			$ret['results'] = array();
			echo json_encode($ret);
		}
	}	
	
	/*
	 * （3）为测评小组成员分配任务
		（3.1）指派F1，基本素质测评人员
		---用来显示已经分配的人员结果的接口
	 */
	//重要
	public function actionGetStuArranged()
	{
		if(!isset($_SESSION)) session_start();//开启缓存
		if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
		{
			$url = "../../../";
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
			exit;
		}
		$StuArranged= isset($_POST["getStuArranged"])? $_POST["getStuArranged"]:$_GET["getStuArranged"];
		$StuArranged = json_decode($StuArranged,true);//解码为数组
		$type = $StuArranged['type'];
		$sid = $StuArranged['sid'];
		$year = $_SESSION['year'];

		if($sid == null)
		{
			echo json_encode(array("success"=>true, "message"=>"", "results"=>array()));
			exit;
		}
		//测试用例
		//$type = "F2";
		//$sid = "2008301500116";
		//$year = 2011;
		//测试结束
		$db = Yii::app()->db;
		try {
			if ($type == 'F1')//如果查询该学生评测基本素质成绩学生的列表,在mutualeva中查找
			{
				$sql = "select distinct student.studentid as sid,student.studentname as sname,whuclass.classname
				as cname from student,mutualeva,whuclass where mutualeva.judgestudentid = '$sid' and
				mutualeva.studentid = student.studentid and mutualeva.year = '$year' and 
				student.classid = whuclass.classid order by sid";
				$results = $db->createCommand($sql)->queryAll();//查询	
				//如果没有出现错误
				$ret = array('success'=>true,'message'=>"",'results'=>$results);
				echo json_encode($ret);
			}	
			else 
			{
				$sql = "select student.studentid as sid,student.studentname as sname,whuclass.classname
				as cname from student,stu_check_list,whuclass where stu_check_list.schid = '$sid' and
				stu_check_list.studentid = student.studentid and stu_check_list.year = '$year' and 
				student.classid = whuclass.classid order by sid";
				$results = $db->createCommand($sql)->queryAll();//查询	
				//如果没有出现错误
				$ret = array('success'=>true,'message'=>"",'results'=>$results);
				echo json_encode($ret);
			}
		} catch (Exception $e) {
			//echo $e;
			$ret = array('success'=>false,'message'=>iconv("gb2312","utf-8",'返回测评成员列表信息失败'),
			'results'=> array());	
			echo json_encode($ret);	
			
		}
	}
	//保存分配结果
	public function actionSaveStuArranged()
	{
		if(!isset($_SESSION)) session_start();//开启缓存
		if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
		{
			$url = "../../../";
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
			exit;
		}

    if($_SESSION['WStart'] == false)
    {
      echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","学年测评尚未开始或者已经结束！"), "results"=>array()));
      exit;
    }

		$StuArranged= isset($_POST["saveStuArranged"])? $_POST["saveStuArranged"]:$_GET["saveStuArranged"];
		$StuArranged = json_decode($StuArranged,true);//解码为数组
		$tclassid = $StuArranged['t_classid'];
		$type = $StuArranged['type'];
		$arranged = $StuArranged['arranged'];
		$year = $_SESSION['year'];
		//测试用例
		/*$type = "F1";
		$year = 2011;
		$arranged = array(
		array('sid'=>'2008301500117','sids'=>array(
			array('sid'=>'2009302530001'),
			array('sid'=>'2009302530002'),
			array('sid'=>'2009302530006'),
			array('sid'=>'2009302530008'),
		) ));
		$tclassid = 1;*/
		//测试结束
		$db = Yii::app()->db;
		try {
			if($type == "F23")//
			{
				$sql = "insert ignore into stu_check_list(studentid,year,schid) values";
				$temp = $sql;
				//批量插入


        //方法一 根据前端传来的测评班级参数，利用其中的测评小组id所审核的班级成员的t_evascore进行判断是否已经开始测评
				/*foreach ($arranged as $stus)
				{
					$jusid = $stus['sid'];//测评小组id
          //根据测评小组id判断测评F3是否开始
          $f3tesql = "select studentid from selfevascore where studentid in (select studentid from stu_check_list where schid = '$jusid' and year = '$year') and t_evascore is not NULL and year = '$year'";
        //如果找到某学生的自我素质测评成绩被测评小组成员修改，说明测评已经开始。
          $frow = $db->createCommand($f3tesql)->queryRow();//查询
					if($frow != null )//表示学生有进行评价，不可以更改删除
          {
            $ret = array('success'=>false,'message'=>
					 	iconv("gb2312","utf-8",'测评小组成员已开始进行测评,不允许修改'),'results'=> array());	
					 	echo json_encode($ret); 
					 	exit();
          }
        }*/

        //方法二 根据前端传来的测评班级id，再根据所包含的班级成员的t_evascore进行判断是否已经开始测评
        $tsql = "select studentid from selfevascore where studentid in (select studentid from student where classid in (select classid from whuclass where  t_classid = '$tclassid')) and t_evascore is not NULL and year = '$year'";//查询进行修改的数目
				$row = $db->createCommand($tsql)->queryRow();//查询
				if($row != false )//表示学生有进行评价，不可以更改删除
        {
            $ret = array('success'=>false,'message'=>
					 	iconv("gb2312","utf-8",'测评小组成员已开始进行测评,不允许修改'),'results'=> array());	
					 	echo json_encode($ret); 
					 	exit();
        }
        foreach ($arranged as $stus)
				{
					$jusid = $stus['sid'];//测评小组id
					//删除该学生所对应的审核关系
					//首先要删除该测评班级$sid中所对应的F23的审核关系再插入
					$desql ="delete from stu_check_list where schid = '$jusid' and year = '$year'";
					$db->createCommand($desql)->execute();
					foreach ($stus['sids'] as $stu)
					{
						$sid = $stu['sid'];
						$sql = $sql."('$sid','$year','$jusid'),";
					}
				}
				//删除该学生的全部自我评价记录，在执行重新插入之前
				if ($sql != $temp)//不相等表示有修改
				{
					$sql = substr($sql, 0, -1);//将最后一个逗号去掉
					$db->createCommand($sql)->execute();//执行该操作
				}
				$ret = array('success'=>true,'message'=>"",'results'=>array());
				echo json_encode($ret); 
			}
			else //如果是F1
			{
				$sql = "insert into mutualeva(studentid,judgestudentid,year) values";
				$temp = $sql;
				//批量插入
				foreach ($arranged as $stus)
				{
					$jusid = $stus['sid'];
					//首先判断该学生是否给其他人评分，如果评分，那么不允许对这个学生进行修改
					$ifmuoth = "select a1polotics from mutualeva where  judgestudentid = '$jusid' and year = '$year' 
					and a1polotics != 0";//查询进行修改的数目
					$row = $db->createCommand($ifmuoth)->queryRow();//查询
					if($row == null )//表示学生没有进行评价，可以更改删除
					{
						//删除该学生所对应的审核关系
						//首先要删除该测评班级$sid中所对应的F1的审核关系再插入，只有当记录为空的时候才可以删除
						$desql ="delete from mutualeva where judgestudentid = '$jusid' and year = '$year'";//a1polotics为空，才可以删除
						$db->createCommand($desql)->execute();
						foreach ($stus['sids'] as $stu)
						{
							$sid = $stu['sid'];
							$sql = $sql."('$sid','$jusid','$year'),";
						}
					}
					else
					 {
					 	$ret = array('success'=>false,'message'=>
					 	iconv("gb2312","utf-8",'测评小组成员已开始进行测评,不允许修改'),'results'=> array());	
					 	echo json_encode($ret); 
					 	exit();
					 }
				}
				//删除该学生的全部自我评价记录，在执行重新插入之前
				if ($sql != $temp)//不相等表示有修改
				{
					$sql = substr($sql, 0, -1);//将最后一个逗号去掉
					$db->createCommand($sql)->execute();//执行该操作
				}
				$ret = array('success'=>true,'message'=>"",'results'=>array());
				echo json_encode($ret); 
			}
		} catch (Exception $e) {
			//echo $e;
			$ret = array('success'=>false,'message'=>iconv("gb2312","utf-8",'可能重复插入，返回测评成员列表信息失败'),
			'results'=> array());	
			echo json_encode($ret);
		}
	}
	
	
	public function actionCheckRight()//判断所有学生是否已经分配完
	{
		if(!isset($_SESSION)) session_start();//开启缓存

		if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
		{
			$url = "../../../";
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
			exit;
		}
		$checkRight= isset($_POST["checkRight"])? $_POST["checkRight"]:$_GET["checkRight"];
		$checkRight = json_decode($checkRight,true);//解码为数组
		$tclassid = $checkRight['t_classid'];
		$year = $checkRight['year'];
		//$type = $checkRight['type'];
		
		//测试用例
		//$tclassid = 1;
		//$year = 2011;
		//$type = 'F1';
		$db = Yii::app()->db;
		try {
			$sql = "select student.studentid as sid,student.studentname as sname,whuclass.classname as cname
			 from student,whuclass where student.classid = whuclass.classid and whuclass.t_classid = '$tclassid'
			  and studentid not in (select distinct studentid from mutualeva where year = '$year' )";
			$results = $db->createCommand($sql)->queryAll();//查询	
			//如果没有出现错误
			$ret = array('success'=>true,'message'=>"",'results'=>$results);
			echo json_encode($ret);
		} catch (Exception $e) {
			//echo $e;
			$ret = array('success'=>false,'message'=>iconv("gb2312","utf-8",'返回未测评学生列表信息失败'),
			'results'=> array());	
			echo json_encode($ret);
			
		}
	}
	
	//（4）审核一个测评小组成员的评定的基本素质成绩   --- 审核一个或者多个
	public function actionCheckF1()//单个审核
	{
		if(!isset($_SESSION)) session_start();//开启缓存
		if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
		{
			$url = "../../../";
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
			exit;
		}
		$checkF1= isset($_POST["checkF1"])? $_POST["checkF1"]:$_GET["checkF1"];
		$checkF1 = json_decode($checkF1,true);//解码为数组
		$sid = $checkF1['sid'];
		$stus = $checkF1['F1'];
		$add = $checkF1['add'];
		$sub = $checkF1['sub'];
		$year = $_SESSION['year'];
		$tcheck = 2;
    $type = "teacher";
		//测试数据
		/*$sid = '2009302530001';
		$stus = array(
		array('sid'=>'2008301500116','A1'=>'10','A2'=>'10','A3'=>'10','A4'=>'10','A5'=>'10'),
		array('sid'=>'2008301500117','A1'=>'10','A2'=>'10','A3'=>'10','A4'=>'10','A5'=>'10'),
		array('sid'=>'2009302530001','A1'=>'10','A2'=>'10','A3'=>'10','A4'=>'10','A5'=>'10'),
		array('sid'=>'2009302530002','A1'=>'10','A2'=>'10','A3'=>'10','A4'=>'10','A5'=>'10'),
		array('sid'=>'2009302530003','A1'=>'10','A2'=>'10','A3'=>'10','A4'=>'10','A5'=>'10'),
		);
		$add = array('score'=>100);
		$sub = array('score'=>2);
		$year = 2011;
		$type = "teacher";
		$tcheck = 2;*/
		//测试结束
		$db = Yii::app()->db;
		try {
			//辅导员审核互评成绩
			foreach ($stus as $stu)
			{
				$jusid = $stu['sid'];
				$A1 = $stu['A1'];
	    		$A2 = $stu['A2'];$A3 = $stu['A3'];
	    		$A4 = $stu['A4'];$A5 = $stu['A5'];//更新操作的同时，将学生的审核状态设置为2
	    		$insertBasql = "call update_Basicscore('$type','$sid','$jusid','$year','$A1','$A2','$A3','$A4','$A5')";
	    		//执行更新操作
	    		$db->createCommand($insertBasql)->execute();//执行该操作
	    		//
			}
			//教师审核F1自评加减分成绩
			//如果不为空，那么执行操作加分操作
			if(!empty($add))//如果不为空，那么执行操作
			{
				$addscore = $add['score'];
				$addsql = "update selfevascore set te_evascore = '$addscore', isPassed = '2' where studentid = '$sid' 
				and year = '$year' and evaluateid = 1";//加分
				$db->createCommand($addsql)->execute();//执行该操作
			}
			//如果不为空，那么执行操作减分操作
			if(!empty($sub))//如果不为空，那么执行操作
			{
				$subscore = $sub['score'];
				$subsql = "update selfevascore set te_evascore = '$subscore', isPassed='2' where studentid = '$sid' 
				and year = '$year' and evaluateid = 2";//减分
				$db->createCommand($subsql)->execute();//执行该操作
			}
			//如果没有出现错误
			//将学生的审核状态设置为2
			$sql = "call update_stuQuacheck('$sid','$year','$tcheck')";
			$db->createCommand($sql)->execute();//执行该操作	
			
			$ret = array('success'=>true,'message'=>"",'results'=>array());
			echo json_encode($ret);
		} catch (Exception $e) {
			//echo $e;
			$ret = array('success'=>false,'message'=>iconv("gb2312","utf-8",'审核学生基本素质成绩信息失败'),
			'results'=> array());	
			echo json_encode($ret);
		}
		
	}
	
	
	
	
	
	
	//批量审核学生的F1成绩，后台需要处理，将测评小组的打分复制给辅导员打分
	//然后置辅导员审核通过标志
	public function actionPassMultiF1()
	{
		if(!isset($_SESSION)) session_start();//开启缓存
		if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
		{
			$url = "../../../";
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
			exit;
		}
		$passMultiF1= isset($_POST["passMultiF1"])? $_POST["passMultiF1"]:$_GET["passMultiF1"];
		$sids = json_decode($passMultiF1,true);//解码为数组
		$year = $_SESSION["year"];
		//测试用例
		/*$sids = array(
		array('sid'=>'2009302530001'),
		array('sid'=>'2009302530002'),
		array('sid'=>'2009302530006'),
		array('sid'=>'2009302530008'));
		$year = 2011;*/
		//测试用例结束
		$db = Yii::app()->db;
		try {
			foreach ($sids as $sid)
			{
			//首先将学生F1教师成绩复制为测评小组审核成绩
			//其次将学生自评F1的加减分进行复制
			//最后将学生grade表中的qualitycheck记录设置为2，表示教师审核通过
			//以上三条表现在存储过程中
				//$studentid = $sid['sid'];
        $studentid = $sid;
				$musql = "call update_PassMultiF1('$studentid','$year')";
				$db->createCommand($musql)->execute();//执行该操作
			}
			$ret = array('success'=>true,'message'=>"",'results'=>array());
			echo json_encode($ret);
			
		} catch (Exception $e) {
			//echo $e;
			$ret = array('success'=>false,'message'=>iconv("gb2312","utf-8",'批量审核学生基本素质成绩信息失败'),
			'results'=> array());	
			echo json_encode($ret);
		}
	}
	
	
	//教师显示学生自评成绩测评接口
	public function actionDisplaySelfScore()
	{
		if(!isset($_SESSION)) session_start();
		if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
		{
			$url = "../../../";
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
			exit;
		}
		$searchSelf= isset($_POST["showF3"])? $_POST["showF3"]:$_GET["showF3"];
		$searchSelf = json_decode($searchSelf,true);//解码为数组
		if($searchSelf['sid'] == "")
			$studentid = $_SESSION['ID'];
		else 
			$studentid = $searchSelf['sid'];
		$year = $searchSelf['year'];
		//测试用例
		//$studentid = '2008301500116';
		//$year = '2011';
		$db = Yii::app()->db;
	    $ret = array();
    	try {    	
	    	//查询学生自评成绩总得分以及审核情况
    		$selfsql = "select selfscore,selfcheck from grade 
    		where studentid = '$studentid' and year = '$year'";
    		$selfrow = $db->createCommand($selfsql)->queryRow();//查询
    		//调用存储过程，查询学生所有自评成绩，包括F1选项中得自评
			$sql = "call search_deselfscores('$studentid','$year')";
	    	$resultAll = $db->createCommand($sql)->queryAll();	    	
	    	$choose = -1;//该学生成绩的第一个测评选项的id
	    	$array_count = -1;
	    	$ret = array();
	    	$add = array();
	    	$sub = array();
	    	$results = array();//F3个人自评成绩结果存储，相当于$contents
	    	//如果$selfrow['selfcheck']为2表示教师已经审核通过，那么显示原始成绩
	    	//否则选择测评小组成绩
	    	if($selfrow['selfcheck'] == 2)//已审核通过
	    	{
		    	foreach ($resultAll as $result)//对每一项成绩进行分析
		    	{
			    	if($result['evaluateid'] == '1')//如果是加分，只允许一项
		    		{
		    			/*$add = array('choose'=>$result['evaluateid'],
		    			'content'=>$result["content"],
		    			"score"=>$result["evascore"]);*/
		    		}
		    		elseif ($result['evaluateid'] == '2')//如果是减分
		    		{
		    			/*$sub = array('choose'=>$result['evaluateid'],
		    			'content'=>$result["content"],
		    			"score"=>$result["evascore"]);*/
		    		}
		    		else//如果不是加减分
	    			{
			    		if($choose != $result['evaluateid'])
			    		{
			    			$array_count++;
			    			$results[$array_count]["choose"] = $result['evaluateid'];
			    			$results[$array_count]["score"] =  $result['maxscore'];
			    			$results[$array_count]["contents"] = array();
			    			$choose = $result['evaluateid'];
			    		}		    		
			    		array_push($results[$array_count]["contents"], 
			    		array('selfscoreid'=>$result['selfscoreid'],"content"=>$result["content"],
			    		"score"=>$result["evascore"],
			    		't_evascore'=>$result["t_evascore"],
			    		'te_evascore'=>$result["te_evascore"]
			    		));
		    		}
		    	}
	    	}


	    	else 
	    	{
	    		foreach ($resultAll as $result)//对每一项成绩进行分析
		    	{
			    	if($result['evaluateid'] == '1')//如果是加分，只允许一项
		    		{
		    			/*$add = array('choose'=>$result['evaluateid'],
		    			'content'=>$result["content"],
		    			"score"=>$result["evascore"]);*/
		    		}
		    		elseif ($result['evaluateid'] == '2')//如果是减分
		    		{
		    			/*$sub = array('choose'=>$result['evaluateid'],
		    			'content'=>$result["content"],
		    			"score"=>$result["evascore"]);*/
		    		}
		    		else//如果不是加减分
	    			{
			    		if($choose != $result['evaluateid'])
			    		{
			    			$array_count++;
			    			$results[$array_count]["choose"] = $result['evaluateid'];
			    			$results[$array_count]["score"] =  $result['maxscore'];
			    			$results[$array_count]["contents"] = array();
			    			$choose = $result['evaluateid'];
			    		}		    		
			    		array_push($results[$array_count]["contents"], 
			    		array('selfscoreid'=>$result['selfscoreid'],"content"=>$result["content"],
			    		"score"=>$result["evascore"],
			    		't_evascore'=>$result["t_evascore"],
			    		'te_evascore'=>$result["t_evascore"]
			    		));
		    		}
		    	}

	    	}
    		//如果没有出现错误
    		$ret['success'] = true;
			$ret['message'] = "";
    		$ret['results']['isPassed'] = $selfrow['selfcheck'];
    		$ret['results']['score'] = $selfrow['selfscore'];
    		$ret['results']['contents']  = $results;
    		$ret['results']['isF1Passed'] = $selfrow['selfcheck'];
    		$ret['add'] = $add;
    		$ret['sub'] = $sub;
			echo json_encode($ret);	
	    } catch (Exception $e) {
    		//echo $e;
	    	$ret['success'] = false;
			$ret['message'] = iconv("gb2312","utf-8",'返回信息失败');
			$ret['results'] = array();
			echo json_encode($ret);	
	    }	    	
	}	
	
	
	//教师保存单个审核的创新实践成绩
	public function actionSaveSelfscore()
	{
		if(!isset($_SESSION)) session_start();
		if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
		{
			$url = "../../../";
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
			exit;
		}
		$saveF3= isset($_POST["saveF3"])? $_POST["saveF3"]:$_GET["saveF3"];
		$saveF3 = json_decode($saveF3,true);//解码为数组
		$studentid = $saveF3['sid'];
		$year = $saveF3['year'];
		$type = $saveF3['type'];
		$score = $saveF3['score'];
		$contents = $saveF3['contents'];
		//测试用例
		/*$studentid = '2008301500116';
		$year = '2011';
		$score = 89.9999;
		$contents = array(
		array(
		'contents'=>array(
		array('selfscoreid'=>20,'t_score'=>2.222),
		array('selfscoreid'=>21,'t_score'=>2.222),
		array('selfscoreid'=>22,'t_score'=>2.222),
		array('selfscoreid'=>23,'t_score'=>2.222)
		)
		)
		);*/
		$db = Yii::app()->db;
		//只有当未被老师审核通过的时候才进行修改
			//根据传入参数，查看学生自评审核状态
		try {
				//修改审核后的创新实践成绩以及审核状态
				$tcheck = 2;//表示教师审核
				$stusql = "call update_stuSelfinfo('$studentid','$year','$tcheck','$score')";
		    	$db->createCommand($stusql)->execute();//执行该操作
		    	//对具体自评成绩进行修改
		    	foreach ($contents as $content)
		    	{
		    		//foreach ($content['contents'] as $selfscore)
		    		{
		    			//根据selfscoreid更新相应的学生审核成绩
		    			$selfscoreid = $content['selfscoreid'];
		    			$score = $content['t_score'];
		    			$selfsql = "call update_teacherSelfscore('$selfscoreid','$score')";
		    			$db->createCommand($selfsql)->execute();//执行该操作	
		    		}
		    	}
		    	//没有出现错误
		    		//如果没有出现错误
				$ret['success'] = true;
				$ret['message'] = iconv("gb2312","utf-8","辅导员保存审核信息成功");
				$ret['results'] = array();					
				echo json_encode($ret);
			} catch (Exception $e) {
				//echo $e;
	    		$ret['success'] = false;
				$ret['message'] = iconv("gb2312","utf-8",'辅导员保存审核信息失败');
				$ret['results'] = array();
				echo json_encode($ret);	
		}
	}
	
	//教师批量审核学生自评成绩
   public function actionTeacherBadudit()
	{
		if(!isset($_SESSION)) session_start();//开启缓存
		if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
		{
			$url = "../../../";
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
			exit;
		}
		$passF3 = isset($_POST["passF3"])? $_POST["passF3"]:$_GET["passF3"];
		$passF3  = json_decode($passF3 ,true);//解码为数组
		$type = $passF3['type'];
		$sids = $passF3['members'];
		$year = $_SESSION['year'];
		//测试用例
		/*$sids =array(
			array('sid'=>'2009302530001'),
			array('sid'=>'2009302530002'),
			array('sid'=>'2009302530006'),
			array('sid'=>'2009302530008'),
			);
		$year = 2011;*/
		//用例结束
		$db = Yii::app()->db;
		$ret = array();
		try {
			foreach($sids as $sid)
			{
				$studentid = $sid['sid'];
				//对学生权限进行更新
				//调用存储过程
				$sql = "call  update_TBaudit('$studentid','$year')";
				$db->createCommand($sql)->execute();//执行该操作
			}
			$ret = array('success'=>true,'message'=>"",'results'=>array());
			echo json_encode($ret);
		} catch (Exception $e) {
			//echo $e;
			$ret['success'] = false;
			$ret['message'] = iconv("gb2312","utf-8",'批量审核学生自评成绩失败');
			$ret['results'] = array();
			echo json_encode($ret);	
		}
	}
	
	public function actionTePassBscore()//教师审核通过测评小组评的基本素质成绩
	{
		if(!isset($_SESSION)) session_start();//开启缓存
		if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
		{
			$url = "../../../";
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
			exit;
		}
		/*$passAMem= isset($_POST["passAMem"])? $_POST["passAMem"]:$_GET["passAMem"];
		$passAMem = json_decode($passAMem,true);//解码为数组
		$type = $passAMem['type'];
		$role = $passAMem['role'];
		$sids = $passAMem['sids'];
		$year = $_SESSION['year'];
		$tcheck = 2;//审核状态设置为2，教师*/
		$db = Yii::app()->db;
		//测试用例
		$sids = array(
		array('sid'=>'2009302530001'),
		array('sid'=>'2009302530002'),
		array('sid'=>'2009302530006'),
		array('sid'=>'2009302530008'));
		$type = 'F3';
		$role = 'student';
		$year = 2011;
		//测试用例结束
		$ret = array();
		try {
			if($role == 'student')//如果是按照审核某几个学生
			{
				//根据type类型，对学生的审核状态进行标志
				if($type == 'F1')//互评成绩审核，教师审核通过状态设置为2
				{
					foreach ($sids as $sid)
					{
						$studentid = $sid['sid'];//获得审核学生的学号
						$sql = "call update_stuQuacheck('$studentid','$year','$tcheck')";
			    		$db->createCommand($sql)->execute();//执行该操作	
					}
				}
				elseif ($type == 'F2')		//课程成绩审核，教师审核通过状态设置为2
				{
					foreach ($sids as $sid)
					{
						$studentid = $sid['sid'];//获得审核学生的学号
						$sql = "call update_stuGpacheck('$studentid','$year','$tcheck')";
			    		$db->createCommand($sql)->execute();//执行该操作	
					}
				}
				else //自评成绩审核，教师审核通过状态设置为2
				{
					foreach ($sids as $sid)
					{
						$studentid = $sid['sid'];//获得审核学生的学号
						$sql = "call update_stuSelfcheck('$studentid','$year','$tcheck')";
			    		$db->createCommand($sql)->execute();//执行该操作	
					}
				}
				
			}//如果是按照某几个学生审核，那么通过，否则按照team审核通过
			else
			{
			//根据type类型，对学生的审核状态进行标志
				if($type == 'F1')//互评成绩审核，教师审核通过状态设置为2
				{
				}
				elseif ($type == 'F2')		//课程成绩审核，教师审核通过状态设置为2
				{
				}
				else //自评成绩审核，教师审核通过状态设置为2
				{
				}
			};
			$ret['success'] = true;
			$ret['message'] = "";
			$ret['results'] = array();
			echo json_encode($ret);
		}
		catch (Exception $e) {
			//echo $e;
			$ret['success'] = false;
			$ret['message'] = iconv("gb2312","utf-8",'保存审核信息失败');
			$ret['results'] = array();
			echo json_encode($ret);
		}
	}	
	
	
	//（7）计算班级基本素质成绩
	public function actionCalClassBasic()
	{
		if(!isset($_SESSION)) session_start();//开启缓存
		if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
		{
			$url = "../../../";
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
			exit;
		}
		$ClassBasic= isset($_POST["calClassBasic"])? $_POST["calClassBasic"]:$_GET["calClassBasic"];
		$ClassBasic = json_decode($ClassBasic,true);//解码为数组
		$tclassid = $ClassBasic['t_classid'];
		$year = $_SESSION['year'];
		//测试用例
		//$tclassid = 1;
		//$year = 2011;
		//测试用例结束
		$db = Yii::app()->db;
		$ret = array();
		try {
			$sql = "call update_stuQuaScore('$tclassid','$year')";
			$db->createCommand($sql)->execute();//执行该操作	
			//如果没有出现错误
			$ret['success'] = true;
			$ret['message'] = "";
			$ret['results'] = array();
			echo json_encode($ret);
		} catch (Exception $e) {

			//echo $e;
			$ret['success'] = false;
			$ret['message'] = iconv("gb2312","utf-8",'计算学生基本素质成绩失败');
			$ret['results'] = array();
			echo json_encode($ret);
		}
	}
	
	//查看指定班级的基本素质成绩----
	//此处查看的是经过互评和教师审核后的成绩
	public function actionSearchClassBasic()
	{
		if(!isset($_SESSION)) session_start();//开启缓存
		if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
		{
			$url = "../../../";
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
			exit;
		}
		/*$searchClassBasic = isset($_POST["searchClassBasic "])? $_POST["searchClassBasic "]:$_GET["searchClassBasic "];
		$searchClassBasic = json_decode($searchClassBasic,true);//解码为数组
		$tclassid = $searchClassBasic['t_classid'];
		$classid = $searchClassBasic['classid'];
		$year = $searchClassBasic['year'];*/
		$db = Yii::app()->db;
		$ret = array();
		try {
			if($classid == null)//如果没有具体到某一个班级，那么测评班级全部返回
			{
				//根据t_classid查找
				$sql = "select student.studentid as sid,student.studentname as sname,whuclass.classname 
				,t_a1polotics as A1,t_a2morality as A2,t_a3attitudes as A3,t_a4discipline as A4,t_a5physical as A5
				from student,whuclass,mutualeva where student.classid in (select classid
				from whuclass where whuclass.t_classid = '$tclassid') 
				and mutualeva.studentid =student.studentid and mutualeva.year = '$year'
				order by student.classid,sid";
				$results = $db->createCommand($sql)->queryAll();//查询	
				//如果没有出现错误
				$ret = array('success'=>true,'message'=>"",'results'=>$results);
				echo json_encode($ret);		
			}	
			else//如果具体到某一个班级，那么
			{
				$sql = "select student.studentid as sid,student.studentname as sname,whuclass.classname 
				,t_a1polotics as A1,t_a2morality as A2,t_a3attitudes as A3,t_a4discipline as A4,t_a5physical as A5
				from student,whuclass,mutualeva where student.classid = whuclass.classid and whuclass.classid = '$classid'
				and mutualeva.studentid =student.studentid and mutualeva.year = '$year' 
				order by student.classid,sid";
				$results = $db->createCommand($sql)->queryAll();//查询	
				//如果没有出现错误
				$ret = array('success'=>true,'message'=>"",'results'=>$results);
				echo json_encode($ret);	
			}			
		} catch (Exception $e) {
			//echo $e;
    		$ret['success'] = false;
			$ret['message'] = iconv("gb2312","utf-8",'返回学生信息失败');
			$ret['results'] = array();
			echo json_encode($ret);	
		}
	}
	
	
	
	/*
	 * （9）首先调用接口searchTestTeam获取测评小组列表，
	 * 然后查看某个成员评定的学生F2成绩列表，并且审核。
	 */

	/*
	 * （10）审核通过或者批量审核通过学生的F2
	 * 成绩调用接口passAMem
		type=F2,role=student/team
	 */
	
	/*
	 *(11) 查看指定班级的课程评定成绩F2
	 * 此处查看的是经过互评和教师审核后的成绩
	 */
	
//查看指定班级的课程成绩----此处查看的是教师审核后的成绩
	public function actionSearchClassCourse()
	{
		if(!isset($_SESSION)) session_start();//开启缓存
		if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
		{
			$url = "../../../";
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
			exit;
		}
		/*$searchClassCourse = isset($_POST["searchClassCourse"])? $_POST["searchClassCourse"]:$_GET["searchClassCourse"];
		$searchClassCourse  = json_decode($searchClassCourse ,true);//解码为数组
		$tclassid = $searchClassCourse['t_classid'];
		$classid = $searchClassCourse['classid'];
		$year = $_SESSION['year'];*/
		$db = Yii::app()->db;
		$ret = array();
		//测试用例
		try {
			if($classid == null)//如果没有具体到某一个班级，那么测评班级全部返回
			{
				//根据t_classid查找
				$sql = "select student.studentid as sid,student.studentname as sname,whuclass.classname 
				,gpascore as total from student,whuclass,grade 
				where student.classid in (select classid
				from whuclass where whuclass.t_classid = '$tclassid') 
				and grade.studentid =student.studentid and grade.year = '$year'
				order by student.classid,sid";
				$results = $db->createCommand($sql)->queryAll();//查询	
				//如果没有出现错误
				$ret = array('success'=>true,'message'=>"",'results'=>$results);
				echo json_encode($ret);		
			}	
			else//如果具体到某一个班级，那么
			{
				$sql = "select student.studentid as sid,student.studentname as sname,whuclass.classname 
				,gpascore as total
				from student,whuclass,grade where student.classid = whuclass.classid and whuclass.classid = '$classid'
				and grade.studentid =student.studentid and grade.year = '$year' 
				order by student.classid,sid";
				$results = $db->createCommand($sql)->queryAll();//查询	
				//如果没有出现错误
				$ret = array('success'=>true,'message'=>"",'results'=>$results);
				echo json_encode($ret);	
			}			
		} catch (Exception $e) {
			//echo $e;
    		$ret['success'] = false;
			$ret['message'] = iconv("gb2312","utf-8",'返回学生信息失败');
			$ret['results'] = array();
			echo json_encode($ret);	
		}
	}
	
	
	
	
//查看班级的创新综合实践成绩F3此处查看的是经过教师审核后的成绩。
	public function actionSearchClassCreativity()
	{
		if(!isset($_SESSION)) session_start();//开启缓存
		if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
		{
			$url = "../../../";
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
			exit;
		}
			$searchClassCreativity = isset($_POST["searchClassCreativity"])? $_POST["searchClassCreativity"]:$_GET["searchClassCreativity"];
			$searchClassCreativity  = json_decode($searchClassCreativity ,true);//解码为数组
			$tclassid = $searchClassCreativity['t_classid'];
			$classid = $searchClassCreativity['classid'];
			$year = $_SESSION['year'];
			$db = Yii::app()->db;
			$ret = array();
			//测试用例
			try {
				if($classid == null)//如果没有具体到某一个班级，那么测评班级全部返回
				{
					//根据t_classid查找
					$sql = "select student.studentid as sid,student.studentname as sname,whuclass.classname, 
					grade.selfscore as total from student,whuclass,grade 
					where student.classid in (select classid
					from whuclass where whuclass.t_classid = '$tclassid') 
					and grade.studentid =student.studentid and grade.year = '$year'
					order by student.classid,sid";
					$results = $db->createCommand($sql)->queryAll();//查询	
					//如果没有出现错误
					$ret = array('success'=>true,'message'=>"",'results'=>$results);
					echo json_encode($ret);		
				}	
				else//如果具体到某一个班级，那么
				{
					$sql = "select student.studentid as sid,student.studentname as sname,whuclass.classname 
					,grade.selfscore as total
					from student,whuclass,grade where student.classid = whuclass.classid and whuclass.classid = '$classid'
					and grade.studentid =student.studentid and grade.year = '$year' 
					order by student.classid,sid";
					$results = $db->createCommand($sql)->queryAll();//查询	
					//如果没有出现错误
					$ret = array('success'=>true,'message'=>"",'results'=>$results);
					echo json_encode($ret);	
				}			
			} catch (Exception $e) {
				//echo $e;
	    		$ret['success'] = false;
				$ret['message'] = iconv("gb2312","utf-8",'返回学生信息失败');
				$ret['results'] = array();
				echo json_encode($ret);	
			}
	}

	//给班级设定奖学金
	/*
	 * 首先获得测评班级列表和奖学金列表
	 */
	public function actionGetTclassScpList()
	{
		if(!isset($_SESSION)) session_start();//开启缓存
		
		$teacherid = $_SESSION['ID'];
		$year = $_SESSION['year'];
		$departmentid = $_SESSION['departmentid'];

		$db = Yii::app()->db;
		$ret = array();
		$results = array();
		try {
			$clasql = "select t_classid,t_name as classname from t_class where
			teacherid = '$teacherid' and year = '$year'";
			$clarets = $db->createCommand($clasql)->queryAll();//查询全部
			
			//查询本学院的奖学金列表
			$scpsql = "select scholarshipid,name,type,amount from scholarshiptype
			 where departmentid = 1 or departmentid = '$departmentid' 
			 order by scholarshipid";
			$scprets = $db->createCommand($scpsql)->queryAll();//查询全部
			
			$results = array('classes'=>$clarets,'scholars'=>$scprets);
			$ret = array('success'=>true,'message'=>"",'results'=>$results);
			echo json_encode($ret);
		} catch (Exception $e) {
			//echo $e;
    		$ret['success'] = false;
			$ret['message'] = iconv("gb2312","utf-8",'返回奖学金信息失败');
			$ret['results'] = array();
			echo json_encode($ret);	
		}
	}
	
	//(2)获得测评班级对应的有资格评定奖学金的所有学生排名，降序
	public function actionGetRankByClass()
	{
		if(!isset($_SESSION)) session_start();//开启缓存
		if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
		{
			$url = "../../../";
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
			exit;
		}
		$RankByClass = isset($_POST["getRankByClass"])? $_POST["getRankByClass"]:$_GET["getRankByClass"];
		$RankByClass  = json_decode($RankByClass ,true);//解码为数组
		$tclassid = $RankByClass['t_classid'];//根据测评班级整体排序
		$year = $RankByClass['year'];
		$departmentid = $_SESSION['departmentid'];
		//测试用例
		/*$tclassid = 1;

		$year = 2011;
		$departmentid = 2;*/
		//用例结束


		$db = Yii::app()->db;
		$ret = array();
		$results = array();
		try {
			//首先查询是否被学院管理员审核通过
			$tcsql = "select gcheck,F1,F2,F3 from t_class where t_classid = '$tclassid'";
			$tcret = $db->createCommand($tcsql)->queryRow();//查询
			$isPassed = $tcret['gcheck'];//是否被学院管理员审核通过
			$bf = $tcret['F1'];//基本素质测评参数
			$gf= $tcret['F2'];//课程成绩参数
			$sf = $tcret['F3'];//自评成绩参数
			
			//查询学生排名以及具体信息包括获奖信息scholarshipid，用到了左连接
			$stusql = "call search_RankStuRets('$tclassid','$year','$bf','$gf','$sf')" ;
			$sturets = $db->createCommand($stusql)->queryAll();//查询
			//对数组进行遍历
			foreach ($sturets as &$stu)//引用赋值
			{
				//提取获奖信息
				$schostring = $stu['scholarship'];
				//这是一个字符串或者为空
				$stu['scholars'] = array();
				if($schostring  != null)//如果不为空，进行处理，为一个用，间断的字符串
				{
					$scholarshipid = explode(',',$schostring); 
					$stu['scholars'] = $scholarshipid;
				}
			}
			$results = array('isPassed'=>$isPassed,'contents'=>$sturets);
			$ret = array('success'=>true,'message'=>"",'results'=>$results);
			echo json_encode($ret);
		} catch (Exception $e) {
			//echo $e;
    		$ret['success'] = false;
			$ret['message'] = iconv("gb2312","utf-8",'返回学生信息失败');
			$ret['results'] = array();
			echo json_encode($ret);	
		}
	}
	
	//2012-01-12 minzhenyu
	//支持前端的基本搜索功能
	public function actionSearchRank()
	{
		if(!isset($_SESSION)) session_start();//开启缓存
		if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
		{
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
			exit;
		}
		$searchRank = isset($_POST["searchRank"])? $_POST["searchRank"]:$_GET["searchRank"];
		$searchRank  = json_decode($searchRank ,true);//解码为数组
		$departmentid = $_SESSION['departmentid'];
		/*
			para: searchRank {t_classid:'1', year: '2012', ispoor:'0', compare:'0/1/2' //0表示'>=' 1表示'<=' 2表示'='
										type:'F1/F2/F3/total', rankID: '11', exscholarship:'0/1'//0表示不排除已获奖的，1表示排除已获奖的}
		*/
		
		//测试用例
		//{"t_classid":"1","year":2012,"ispoor":1,"exscholarship":1,"condition1":{"compare":0,"rankID":3,"type":"F1"},"condition2":{"compare":0,"num":4,"type":"research"}}
		/*>= 0,10
		<= 10-1,18446744073709551615
		== 10-1,1
		"1=1" ispoor!="否"
		
		"1=1" "scholarshipid  is null"*/
		//ispoor compare type exscholarship rankid 是用来条件、组合查询的
		//调用存储过程search_BasicSearchRank
		//入口参数in tclassid int(10), in tyear varchar(5), in bf decimal(5,3),in gf decimal(5,3), in sf decimal(5,3), in type varchar(6), 
    //      in rank varchar(25), in ispoor varchar(15), in exscholarship varchar(25), in did int(5), in item varchar(10), 
    //      in num varchar(25), in condition1 int(1), in condition2 int(1)

		
    /*$searchRank = array(
      "t_classid" => '1',
      "year" => '2012',
      'ispoor' => 1,
      'exscholarship' => 1,
      'condition1' => array('compare'=>0, "rankID"=>3, "type"=>'F1'),
      'condition2' => array('compare'=>0, "num"=>4, "type"=>"paper")//paper, artical, research    
    );
		$departmentid = 2;*/

    $tclassid = $searchRank['t_classid'];//根据测评班级整体排序
		$year = $searchRank['year'];
		$exscholarship = $searchRank['exscholarship'];
		$ispoor = $searchRank['ispoor'];
     
    //用于条件查询
    $condition1 = 0;
    $condition2 = 0;
    $_ispoor = '1=1';
		$_exscholarship = '1=1';
    $type = ""; $rank = ""; $did = $_SESSION['departmentid']; $item = ""; $num = "1=1";

		if($ispoor == 1)
		{
			$_ispoor = iconv("gb2312","utf-8", 'ispoor!="否"');
		}
		if($exscholarship == 1)
		{
			$_exscholarship = "scholarshipid  is null";
		}

    if(!empty($searchRank["condition1"]))
    {
      $condition1 = 1;
      $compare = $searchRank["condition1"]['compare'];
		  $type = $searchRank["condition1"]['type'];  
      $rankID = $searchRank["condition1"]['rankID'];
      
      //用于成绩条件查询
		  //用于limit查询
		  $rank = "0, 18446744073709551615";   //默认查询全部的 
      if($rankID <= 0) //防止溢出
        $rankID = 1;
		  if($compare == 0) // >=
		  {
			  $rank = "0, $rankID";
		  }
		  else if($compare == 1) // <=
		  {
			  $rank = ($rankID-1).", 18446744073709551615";
		  }
		  else if($compare == 2)  //==
		  {
			  $rank = ($rankID-1).", 1";
		  }
    }
    if(!empty($searchRank["condition2"]))
    {
      $condition2 = 1;
      $compare = $searchRank["condition2"]['compare'];
		  $type1 = $searchRank["condition2"]['type'];  
      $rankID1 = $searchRank["condition2"]['num'];

      if($type1 == "paper")
        $item = iconv("gb2312","utf-8", '论文');   
      else if($type1 == "artical")
        $item = iconv("gb2312","utf-8", '著作');
      else if($type1 == "research")
        $item = iconv("gb2312","utf-8", '科研');
      
      //用于成绩条件查询
		  //用于limit查询
		  $num = "1=1";   //默认查询全部的 
		  if($compare == 0) // >=
		  {
			  $num = "num >= $rankID1";
		  }
		  else if($compare == 1) // <=
		  {
			  $num = "num <= $rankID1";
		  }
		  else if($compare == 2)  //==
		  {
			  $num = "num = $rankID1";
		  }
    }
		//用例结束
		$db = Yii::app()->db;
		$ret = array();
		$results = array();
		try {
			//首先查询是否被学院管理员审核通过
			$tcsql = "select gcheck,F1,F2,F3 from t_class where t_classid = '$tclassid'";
			$tcret = $db->createCommand($tcsql)->queryRow();//查询
			$isPassed = $tcret['gcheck'];//是否被学院管理员审核通过
			$bf = $tcret['F1'];//基本素质测评参数
			$gf= $tcret['F2'];//课程成绩参数
			$sf = $tcret['F3'];//自评成绩参数
			
			//查询学生排名以及具体信息包括获奖信息scholarshipid，用到了左连接
			$stusql = "call search_BasicSearchRank('$tclassid','$year','$bf','$gf','$sf', '$type', '$rank', "
                  ."'$_ispoor', '$_exscholarship', '$did', '$item', '$num', '$condition1', '$condition2')" ;
			
			//echo $stusql;
			
			$sturets = $db->createCommand($stusql)->queryAll();//查询
      //var_dump($sturets);

			//对数组进行遍历
			foreach ($sturets as &$stu)//引用赋值
			{
				//提取获奖信息
				$schostring = $stu['scholarship'];
				//这是一个字符串或者为空
				$stu['scholars'] = array();
				if($schostring  != null)//如果不为空，进行处理，为一个用，间断的字符串
				{
					$scholarshipid = explode(',',$schostring); 
					$stu['scholars'] = $scholarshipid;
				}
			}
			$results = array('isPassed'=>$isPassed,'contents'=>$sturets);
			$ret = array('success'=>true,'message'=>"",'results'=>$results);
			echo json_encode($ret);
		} catch (Exception $e) {
    		$ret['success'] = false;
			$ret['message'] = iconv("gb2312","utf-8",'返回学生信息失败').$e->getMessage();
			$ret['results'] = array();
			echo json_encode($ret);	
		}
	}
	
	
	//设置学生奖学金信息，前端会一次性把信息传给后台
	public function actionSetStuScholar()
	{
		if(!isset($_SESSION)) session_start();//开启缓存
		if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
		{
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
			exit;
		}
		$ScholarByClass = isset($_POST["setScholarByClass"])? $_POST["setScholarByClass"]:$_GET["setScholarByClass"];
		$ScholarByClass  = json_decode($ScholarByClass ,true);//解码为数组
		$year = $ScholarByClass['year'];
		$contents = $ScholarByClass['contents'];
		//测试用例
		/*$year = 2011;
		$contents =array(
			array('sid'=>'2009302530001','scholar'=>array(array('schid'=>1),array('schid'=>3))),
			array('sid'=>'2009302530002','scholar'=>array(array('schid'=>1),array('schid'=>2))),
			array('sid'=>'2009302530006','scholar'=>array(array('schid'=>1),array('schid'=>2))),
			array('sid'=>'2009302530008','scholar'=>array(array('schid'=>1),array('schid'=>2))),
		);*/
		//用例结束
		$db = Yii::app()->db;
		$ret = array();
		$results = array();
		//2012-03-23 minzhenyu add departmentid
		$departmentid = $_SESSION["departmentid"];
		try {
			$sql = "insert ignore into scholarship(studentid,year,scholarshipid, departmentid) values";
			$temp = $sql;
			foreach ($contents as $stu)
			{
				//对每一个学生
				$sid = $stu['sid'];
				//foreach ($stu['scholar'] as $scholar)//对于每条奖学金记录
				{
					$schid = $stu['scholar'];
					//$schid = $scholar['schid'];
					//添加奖学金记录
					$sql = $sql."('$sid','$year','$schid', '$departmentid'),";
				}	
			}
			if ($sql != $temp)//如果发生变化，则说明有插入
			{
				$sql = substr($sql, 0, -1);//将最后一个逗号去掉
				$db->createCommand($sql)->execute();//执行该操作
			}
			$ret = array('success'=>true,'message'=>"",'results'=>array());
			echo json_encode($ret);
		} catch (Exception $e) {
			//echo $e;
			$ret['success'] = false;
			$ret['message'] = iconv("gb2312","utf-8",'保存学生信息失败');
			$ret['results'] = array();
			echo json_encode($ret);	
		}
	}
	
	//取消奖学金
	public function actionResetStuScholar()
	{
		if(!isset($_SESSION)) session_start();//开启缓存
		if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
		{
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
			exit;
		}
		$ScholarByClass = isset($_POST["resetScholar"])? $_POST["resetScholar"]:$_GET["resetScholar"];
		$ScholarByClass  = json_decode($ScholarByClass ,true);//解码为数组
		$year = $ScholarByClass['year'];
		$contents = $ScholarByClass['contents'];
		//测试用例
		/*$year = 2011;
		$contents =array(
			array('sid'=>'2009302530001','scholar'=>array(array('schid'=>1),array('schid'=>2))),
			array('sid'=>'2009302530002','scholar'=>array(array('schid'=>1),array('schid'=>2))),
			array('sid'=>'2009302530006','scholar'=>array(array('schid'=>1),array('schid'=>2))),
			array('sid'=>'2009302530008','scholar'=>array(array('schid'=>1),array('schid'=>2))),
		);*/
		//用例结束
		$db = Yii::app()->db;
		$ret = array();
		$results = array();
		try {
			foreach ($contents as $stu)
			{
				//对每一个学生
				$sid = $stu['sid'];
				//foreach ($stu['scholar'] as $scholar)//对于每条奖学金记录
				{
					$schid = $stu['scholar'];
					//$schid = $scholar['schid'];
					//删除奖学金记录
					$sql = "delete from scholarship where studentid = '$sid' and year = '$year' and 
					scholarshipid = '$schid'";
					$db->createCommand($sql)->execute();//执行该操作
				}	
			}
			$ret = array('success'=>true,'message'=>"",'results'=>array());
			echo json_encode($ret);
		} catch (Exception $e) {
			//echo $e;
			$ret['success'] = false;
			$ret['message'] = iconv("gb2312","utf-8",'保存学生信息失败');
			$ret['results'] = array();
			echo json_encode($ret);	
		}
	}

	//辅导员赋予学生权限添加课程成绩
	public function actionAddPrivilege()
	{
		if(!isset($_SESSION)) session_start();//开启缓存
		if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
		{
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
			exit;
		}
		$addPrivilege = isset($_POST["addPrivilege"])? $_POST["addPrivilege"]:$_GET["addPrivilege"];
		$addPrivilege  = json_decode($addPrivilege ,true);//解码为数组
		$tclassid = $addPrivilege['t_classid'];
		$sids = $addPrivilege['sids'];
		//测试用例
		/*$sids =array(
			array('sid'=>'2009302530001'),
			array('sid'=>'2009302530002'),
			array('sid'=>'2009302530006'),
			array('sid'=>'2009302530008'),
		);*/
		//用例结束
		$db = Yii::app()->db;
		$ret = array();
		try {
			foreach($sids as $sid)
			{
				$studentid = $sid;
				//对学生权限进行更新
				$sql = "update student set authority = 1 where studentid = '$studentid'";
				$db->createCommand($sql)->execute();//执行该操作
			}
			$ret = array('success'=>true,'message'=>"",'results'=>array());
			echo json_encode($ret);
		} catch (Exception $e) {
			//echo $e;
			$ret['success'] = false;
			$ret['message'] = iconv("gb2312","utf-8",'增加学生权限失败');
			$ret['results'] = array();
			echo json_encode($ret);	
		}
	}
	
	//得到测评班级的学生列表，并且包含了权限字段
	public function actionGetStuPrivilege()//返回测评班级需要测评的学生列表
	{
		if(!isset($_SESSION)) session_start();//开启缓存
		if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
		{
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
			exit;
		}
		$getStuPrivilege= isset($_POST["getStuPrivilege"])? $_POST["getStuPrivilege"]:$_GET["getStuPrivilege"];
		$getStuPrivilege = json_decode($getStuPrivilege,true);//解码为数组
		$tclassid = $getStuPrivilege['t_classid'];
		$classid = $getStuPrivilege['classid'];
		$db = Yii::app()->db;
		$ret = array();
		//测试用例
		//$tclassid = 1;
		//$classid = "1";
		try {
			if($classid == "")//如果没有具体到某一个班级，那么测评班级全部返回
			{
				//根据t_classid查找
				$sql = "select studentid as sid,studentname as sname,whuclass.classname as cname,
				authority as hasPrivilege
				from student,whuclass where student.classid = whuclass.classid
				and whuclass.t_classid = '$tclassid' order by student.classid,sid";
				$results = $db->createCommand($sql)->queryAll();//查询	
				//如果没有出现错误
				$ret = array('success'=>true,'message'=>"",'results'=>$results);
				echo json_encode($ret);		
			}	
			else//如果具体到某一个班级，那么
			{
				$sql = "select studentid as sid,studentname as sname,whuclass.classname as cname
				,authority as hasPrivilege from student,whuclass where student.classid = whuclass.classid
				and whuclass.classid = '$classid' order by student.classid,sid";
				$results = $db->createCommand($sql)->queryAll();//查询	
				//如果没有出现错误
				$ret = array('success'=>true,'message'=>"",'results'=>$results);
				echo json_encode($ret);	
			}			
		} catch (Exception $e) {
			//echo $e;
    		$ret['success'] = false;
			$ret['message'] = iconv("gb2312","utf-8",'返回学生信息失败');
			$ret['results'] = array();
			echo json_encode($ret);	
		}
	}

  //2012-01-15 minzhenyu
  //for manager
  public function actionSaveTeacherInfo()//保存新添加辅导员信息，接口4.1
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}
		if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
		{
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
			exit;
		}
		
		if (isset($_SESSION["departmentid"]))
		{
			$departmentid = $_SESSION["departmentid"];//通过session获取学院id	
			
			$saveTeacherInfo = isset($_POST["saveTeacherInfo"])?$_POST["saveTeacherInfo"]:$_GET["saveTeacherInfo"];
			$saveTeacherInfo = json_decode($saveTeacherInfo,true);

			try
			{
				$db = Yii::app()->db;//连接mysql数据库

				foreach($saveTeacherInfo as $value)//逐条插入教师信息
				{
					$teacherid = $value["teacherid"];//教师号
					$teachername = $value["teachername"];//教师名字
          $password = md5("123456");
					//$teachername = iconv("gb2312","utf-8",$teachername);
					
					if(Teacher::exist($teacherid))//exists
					{
						echo json_encode(array('success'=>false, 'message'=>Teacher::$err));
						exit;
					}
			
					$sqlinsert = "insert into teacher(teacherid,departmentid,teachername,password) values('$teacherid','$departmentid','$teachername','$password')";
					$db->createCommand($sqlinsert)->execute();
				}
        $rets = array("success"=>true,"message"=>"","results"=>array());
			  echo json_encode($rets);
			}
			catch(Exception $e)
			{
				$rets = array("success"=>false,"message"=>$e->getMessage(),"results"=>array());
				echo json_encode($rets);
			}
		}
	}

	public function actionDiaplayTeacherInfo()//显示辅导员信息，接口4.2
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}
		if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
		{
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
			exit;
		}
		if (isset($_SESSION["departmentid"]))
		{
			$departmentid = $_SESSION["departmentid"];//通过session获得学院id
			
			try
			{
				$db = Yii::app()->db;//连接mysql数据库
				
				//查询学院所有的教师信息，包括教师号，教师名
				$sqlquery = "select teacherid,teachername from teacher where departmentid='$departmentid' and teachername!='测评小组'";
        $sqlquery = iconv("gb2312","utf-8",$sqlquery);
				$queryinfo = $db->createCommand($sqlquery)->query();
				$results = $queryinfo->readAll();
			
				$rets = array("success"=>true,"message"=>"","results"=>$results);
				echo json_encode($rets);
			}
			catch(Exception $e)
			{
				$rets = array("success"=>false,"message"=>$e->getMessage(),"results"=>array());
				echo json_encode($rets);
			}
		}
	}

	public function actionSaveEditTeacherInfo()//保存编辑后的辅导员信息,接口4.3
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}
		if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
		{
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
			exit;
		}

		if (isset($_SESSION["departmentid"]))
		{
			$departmentid = $_SESSION["departmentid"];//通过session获取学院id
			$saveEditTeacherInfo = isset($_POST["saveEditTeacherInfo"])?$_POST["saveEditTeacherInfo"]:$_GET["saveEditTeacherInfo"];
			$saveEditTeacherInfo = json_decode($saveEditTeacherInfo,true);
			$updateteacher = $saveEditTeacherInfo["update"];
			$delteacher = $saveEditTeacherInfo["delete1"];

			try
			{
				$db = Yii::app()->db;//连接mysql数据库
				
				if (!empty($updateteacher))//如果要更新的教师信息不为空，则进行更新
				{
					foreach($updateteacher as $datarow)//逐行更新记录

					{
						$teacherid = $datarow["teacherid"];//教师id
						$teachername = $datarow["teachername"];//教师名
						//$teachername = iconv("gb2312","utf-8",$teachername);
						$sqlupdate = "UPDATE teacher SET teachername='$teachername' WHERE teacherid='$teacherid' and departmentid='$departmentid'";
						$db->createCommand($sqlupdate)->execute();
					}
				}
				
				if (!empty($delteacher))//如果要删除的教师信息不为空，则进行删除
				{
					foreach($delteacher as $value)//记录逐条删除
					{
						$teacherid = $value["teacherid"];//要删除教师信息的教师id
						$sqldel = "DELETE FROM teacher WHERE teacherid='$teacherid' and departmentid='$departmentid'";
						$db->createCommand($sqldel)->execute();
					}
				}

				$rets = array("success"=>true,"message"=>"","results"=>array());
				echo json_encode($rets);
			}
			catch(Exception $e)
			{
				$rets = array("success"=>false,"message"=>$e->getMessage(),"results"=>array());
				echo json_encode($rets);
			}
		}
	}

}
?>
