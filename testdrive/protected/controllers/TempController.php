<?php
	//创建测评小组后台数据库操作类
	class TempController extends Controller 
	{
		public function filters()
		{
			return array(
				'accessControl', // perform access control for CRUD operations
			);
		}

		/**
		 * Specifies the access control rules.
		 * This method is used by the 'accessControl' filter.
		 * @return array access control rules
		 */
		public function accessRules()
		{
			return array(
				array('allow', // allow authenticated users to access all actions
					'users'=>array('@'),
				),
				array('deny',  // deny all users
					'users'=>array('*'),
				),
			);
		}

		public function actionGetStuByMem()//返回测评小组需要测评的学生列表
		{
			if(!isset($_SESSION)) session_start();//开启缓存
			if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
			{
				$url = "../../../";
				echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
				exit;
			}
			//if (!isset($_SESSION['ID']))//如果存在session['ID']，说明是用户登录，这样做是防止url攻击
      		if($_SESSION['WStart'] == false)
      		{
        		echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","学年测评尚未开始或者已经结束！"), "results"=>array()));
        		exit;
      		}
				
			$StuByMem= isset($_POST["getStuByMem"])? $_POST["getStuByMem"]:$_GET["getStuByMem"];
			$StuByMem = json_decode($StuByMem,true);//解码为数组
			if($StuByMem['sid'] == "")
				$studentid = $_SESSION['ID'];
			else 
				$studentid = $StuByMem['sid'];
			$type = $StuByMem['type'];
			$year = $_SESSION['year'];

			$ret = array();
			try {
				$db = Yii::app()->db;
				//根据$type类型选择不同操作
				$sql = "call search_JuStuList('$studentid','$year','$type')";
				if (!$results = $db->createCommand($sql)->queryAll())
					$results = array();//查询
					//var_dump($results);
					$ret = array('success'=>true,'message'=>'','results'=>$results);
					echo json_encode($ret);								
			} catch (Exception $e) {
					//echo $e;
					$ret['success'] = false;
					$ret['message'] = iconv("gb2312","utf-8",'返回信息失败');
					$ret['results'] = array();
					echo json_encode($ret);	
			}
		}
		
		//审核基本素质成绩F1 
		//返回指定测评小组成员评定的班级其他成员的基本素质成绩
		public function actionSearchBasicMember()
		{
			if(!isset($_SESSION)) session_start();
			if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
			{
				$url = "../../../";
				echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
				exit;
			}
			if($_SESSION['WStart'] == false)
			{
				echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","学年测评尚未开始或者已经结束！"), "results"=>array()));
				exit;
			}

			$searchBasicMember= isset($_POST["searchBasicMember"])? $_POST["searchBasicMember"]:$_GET["searchBasicMember"];
			$BasicMember = json_decode($searchBasicMember,true);//解码为数组
			if($BasicMember['sid'] == "")
				$studentid = $_SESSION['ID'];
			else 
				$studentid = $BasicMember['sid'];
			$year = $_SESSION["year"];
			$db = Yii::app()->db;
		    $ret = array();//最后返回结果
		    try {
		    	$sql = "call search_otherMuscores('$studentid','$year')";
		    	$results = $db->createCommand($sql)->queryAll();//查询
		    	//对查询的数据判断其check值，是否被老师审核
		    	//如果没有出现错误
		    	$ret['success'] = true;
				$ret['message'] = "";
				//$ret['results']['isPassed']= $isPassed;
				$ret['results']['score'] = $results;
				echo json_encode($ret);
		    } catch (Exception $e) {
		    	//echo $e;
	    		$ret['success'] = false;
				$ret['message'] = iconv("gb2312","utf-8",'返回信息失败');
				$ret['results'] = array();
				echo json_encode($ret);	
		    }
		}
		
		//insertBasicScore  保存测评小组评定的F1成绩
		//第一条mutualevascore已经在老师选择学生所对应的测评对象上确定生成
		public function actionSaveBscores()//
		{
			if(!isset($_SESSION)) session_start();
			if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
			{
				$url = "../../../";
				echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
				exit;
			}
			$BasicScore= isset($_POST["insertBasicScore"])? $_POST["insertBasicScore"]:$_GET["insertBasicScore"];
			$BasicScore = json_decode($BasicScore,true);//解码为数组
			$justudentid = $_SESSION['ID'];
			$year = $_SESSION['year'];
			$type = $BasicScore['type'];
			$results = $BasicScore['results'];

			$db = Yii::app()->db;
		    $ret = array();//最后返回结果
		    try {
		    	foreach($results as $result)
		    	{
		    		$sid = $result['sid'];$A1 = $result['A1'];
		    		$A2 = $result['A2'];$A3 = $result['A3'];
		    		$A4 = $result['A4'];$A5 = $result['A5'];
		    		$insertBasql = "call update_Basicscore('$type','$sid','$justudentid','$year','$A1','$A2','$A3','$A4','$A5')";
		    		//执行更新操作
		    		$db->createCommand($insertBasql)->execute();//执行该操作
		    		//
		    	}
	    		$ret['success'] = true;
				$ret['message'] = "";
				$ret['results'] = array();
				echo json_encode($ret);	
		    } catch (Exception $e) {
		    	//echo $e;
	    		$ret['success'] = false;
				$ret['message'] = iconv("gb2312","utf-8",'保存信息失败');
				$ret['results'] = array();
				echo json_encode($ret);	
		    }
		}	
		
		//审核学生课程成绩F2    只有在学年测评开始才能使用，学年测评完毕后关闭此功能。
		 //测评小组只查看，不能修改成绩。
		 //必须得通知所有被评的学生在规定时间内完成自评，否则视为放弃测评。
		public function actionDisplayCscores()//显示学生课程成绩
		{
			if(!isset($_SESSION)) session_start();
			if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
			{
				$url = "../../../";
				echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
				exit;
			}
			$showF2= isset($_POST["showF2"])? $_POST["showF2"]:$_GET["showF2"];
			$showF2 = json_decode($showF2,true);//解码为数组
			$studentid = $showF2['sid'];
			$year = $showF2['year'];
		    $ret = array();//最后返回结果
		    $results = array();
		    try {
	    		$db = Yii::app()->db;
	   			 //根据传入参数，查看学生本年度所有成绩，根据B1，B2，others返回
				$scoresql = "call search_coscores('$studentid','$year')";
				$scoresresults = $db->createCommand($scoresql)->queryAll();//返回学生所有成绩
				//var_dump ($scoresresults);
				//将所有成绩返回
				
				//查询具体各项得分
				
			    $Fsql = "select gpascore,gpacheck from grade 
			    	where studentid = '$studentid' and year = '$year'";
			    $FRow = $db->createCommand($Fsql)->queryRow();
			    if($FRow['gpacheck'] != null)
					$results = array('isPassed'=> $FRow['gpacheck'],'B1'=>array(),'B2'=>array(),'score'=>$FRow['gpascore']);
				foreach($scoresresults as $score)//对于其中的每一条记录
				{
					//如果该条成绩是B1则插入到相应的B1中去
					if($score['cchoice'] == 'B1')
					{
						array_push($results['B1'],$score);
					}
					elseif ($score['cchoice'] == 'B2')//如果该条成绩是B2则插入到相应的B2中去
					{
						array_push($results['B2'],$score);
					}
				}
				//如果没有出现错误
				$ret['success'] = true;
				$ret['message'] = iconv("gb2312","utf-8","返回信息成功");
				$ret['results'] = $results;	//判断是否为空
				//print_r($results);
				//json编码返回给前台						
				echo json_encode($ret);
		    } catch (Exception $e) {
		    	//echo $e;
	    		$ret['success'] = false;
				$ret['message'] = iconv("gb2312","utf-8",'返回信息失败');
				$ret['results'] = array();
				echo json_encode($ret);	
		    }
		}
		
		//审核通过成绩   --可以审核单个，也可以审核多个
		//将grade中得gpacheck置为1，表示测评小组审核通过学生成绩，
		//其中第一个score中gpascore由actionSaveF2赋值确定
		public function actionTeamPassF2()
		{
			
			if(!isset($_SESSION)) session_start();
			if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
			{
				$url = "../../../";
				echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
				exit;
			}
			$passF2= isset($_POST["passF2"])? $_POST["passF2"]:$_GET["passF2"];
			$results = json_decode($passF2,true);//解码为数组
			$year = $_SESSION['year'];
			$tcheck = 1;//状态设置为1，表示测评小组审核通过
			//测试用例
		    $ret = array();//最后返回结果
		    try {
		    	$db = Yii::app()->db;
				///////以下有逻辑问题，最好是重新整理下
		    	foreach ($results as $result)
		    	{
		    		$studentid = $result['sid'];
		    		//根据传入参数，查看学生课程审核状态
					$checksql = "call search_stuCheck('$studentid','$year')";
					$checkresult = $db->createCommand($checksql)->queryRow();//学生成绩是否被审核
					//var_dump($checkresult);
					if(is_array($checkresult) && isset($checkresult['gpacheck']) && $checkresult['gpacheck'] != '2')	//如果该学生成绩未被教师审核，那么允许修改
					{
		    			$sql = "call update_stugpacheck('$studentid','$year','$tcheck')";
		    			$db->createCommand($sql)->execute();//执行该操作
					}
		    	}
		    	//如果没有出现错误
				$ret['success'] = true;
				$ret['message'] = iconv("gb2312","utf-8","保存审核信息成功");
				$ret['results'] = array();	
				//json编码返回给前台						
				echo json_encode($ret);
		    } catch (Exception $e) {
		    	//echo $e;
	    		$ret['success'] = false;
				$ret['message'] = iconv("gb2312","utf-8",'保存审核信息失败');
				$ret['results'] = array();
				echo json_encode($ret);	
		    }
		}
		
		
		//审核学生的创新实践成绩F3
		//查看
		//返回指定学生，指定年份的学生的自评成绩列表
		//显示创新实践自评列表
		public function actionDisplaySelfScore()
		{
			if(!isset($_SESSION)) session_start();
			if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
			{
				$url = "../../../";
				echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
				exit;
			}
			$searchSelf= isset($_POST["showF3"])? $_POST["showF3"]:$_GET["showF3"];
			$searchSelf = json_decode($searchSelf,true);//解码为数组
			if($searchSelf['sid'] == "")
				$studentid = $_SESSION['ID'];
			else 
				$studentid = $searchSelf['sid'];
			$year = $searchSelf['year'];

			$db = Yii::app()->db;
		    $ret = array();
	    	try {
	    		//调用存储过程，查询学生所有自评成绩，包括F1选项中得自评
				$sql = "call search_deselfscores('$studentid','$year')";
		    	$resultAll = $db->createCommand($sql)->queryAll();	    	
		    	$choose = -1;//该学生成绩的第一个测评选项的id
		    	$array_count = -1;
		    	$ret = array();
		    	$add = array();
		    	$sub = array();
		    	$results = array();//F3个人自评成绩结果存储，相当于$contents
		    	foreach ($resultAll as $result)//对每一项成绩进行分析
		    	{
			    	if($result['evaluateid'] == '1')//如果是加分，只允许一项
			    		{
			    			$add = array(
							'selfscoreid'=>$result['selfscoreid'],
							'choose'=>$result['evaluateid'],
			    			'content'=>$result["content"],
			    			"score"=>$result["evascore"],
							"t_evascore"=> $result["t_evascore"]);
			    		}
			    		elseif ($result['evaluateid'] == '2')//如果是减分
			    		{
			    			$sub = array(
							'selfscoreid'=>$result['selfscoreid'],
							'choose'=>$result['evaluateid'],
			    			'content'=>$result["content"],
			    			"score"=>$result["evascore"],
							"t_evascore"=> $result["t_evascore"]);
			    		}
			    		else//如果不是加减分
		    			{
			    		if($choose != $result['evaluateid'])
			    		{
			    			$array_count++;
			    			$results[$array_count]["choose"] = $result['evaluateid'];
			    			$results[$array_count]["score"] =  $result['maxscore'];
			    			$results[$array_count]["contents"] = array();
			    			$choose = $result['evaluateid'];
			    		}		    		
			    		array_push($results[$array_count]["contents"], 
			    		array('selfscoreid'=>$result['selfscoreid'],"content"=>$result["content"],
			    		"score"=>$result["evascore"],
			    		't_evascore'=>$result["t_evascore"],
			    		'te_evascore'=>$result["te_evascore"]
			    		));
		    		}
		    	}
		    	
		    	//查询学生自评成绩总得分以及审核情况
	    		$selfsql = "select selfscore,selfcheck from grade 
	    		where studentid = '$studentid' and year = '$year'";
	    		$selfrow = $db->createCommand($selfsql)->queryRow();//查询
	    		
	    		$ret['success'] = true;
				$ret['message'] = "";
	    		$ret['results']['isPassed'] = $selfrow['selfcheck'];
	    		$ret['results']['score'] = $selfrow['selfscore'];
	    		$ret['results']['contents']  = $results;
	    		$ret['results']['isF1Passed'] = $selfrow['selfcheck'];
	    		$ret['results']['add'] = $add;
	    		$ret['results']['sub'] = $sub;
	    		
				echo json_encode($ret);	
	    	} catch (Exception $e) {
    			//echo $e;
	    		$ret['success'] = false;
				$ret['message'] = iconv("gb2312","utf-8",'返回信息失败');
				$ret['results'] = array();
				echo json_encode($ret);	
	    	}	    	
		}
		
		
		//测评小组保存审核的创新实践成绩
		public function actionSaveSelfscore()
		{
			if(!isset($_SESSION)) session_start();
			if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
			{
				$url = "../../../";
				echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
				exit;
			}
			$saveF3= isset($_POST["saveF3"])? $_POST["saveF3"]:$_GET["saveF3"];
			$saveF3 = json_decode($saveF3,true);//解码为数组
			$studentid = $saveF3['sid'];
			$year = $saveF3['year'];
			$type = $saveF3['type'];
			$score = $saveF3['score'];
			$contents = $saveF3['contents'];
			//测试用例

			$db = Yii::app()->db;
			//只有当未被老师审核通过的时候才进行修改
				//根据传入参数，查看学生自评审核状态
			try {
				$checksql = "call search_stuCheck('$studentid','$year')";
				$checkresult = $db->createCommand($checksql)->queryRow();//学生成绩是否被审核
				if($checkresult['selfcheck'] != 2)//如果自评成绩未处于老师审核状态，处于0或者1，可以修改
				{
					//修改审核后的创新实践成绩以及审核状态
					$tcheck = 1;
					$stusql = "call update_stuSelfinfo('$studentid','$year','$tcheck','$score')";
			    	$db->createCommand($stusql)->execute();//执行该操作
			    	//对具体自评成绩进行修改
			    	foreach ($contents as $selfscore)
			    	{
			    		//foreach ($content['contents'] as $selfscore)
			    		{
			    			//根据selfscoreid更新相应的学生审核成绩
						//2012-02-18 modified by minzhenyu
			    			$selfscoreid = $selfscore['selfscoreid'];
						if($selfscoreid != '-9' && $selfscoreid != '-99')
						{
			    				$score = $selfscore['t_score'];
			    				//根据selfscoreid判断成绩是否被修改，做一次查询
			    				$seidsql = "update grade set schange = 1
			    				where $score !=(select evascore from selfevascore where selfscoreid = '$selfscoreid')
			    				and studentid = '$studentid' and year = '$year'";
			    				//判断是否被修改结束
			    				$db->createCommand($seidsql)->execute();//执行该操作
			    				$selfsql = "call update_teamSelfscore('$selfscoreid','$score')";
			    				$db->createCommand($selfsql)->execute();//执行该操作	
						}
						else //add  or  sub
						{
							if ($selfscoreid == '-9')
								$choose = '1';
							else if($selfscoreid == '-99')
								$choose = '2';
							$content = $selfscore['content'];
							$score = $selfscore['t_score'];
							$insql = "insert into selfevascore (studentid,evaluateid,year,content,evascore,t_evascore,isPassed)  value ('$studentid','$choose','$year','$content',0,'$score',1)";
							$db->createCommand($insql)->execute();
						}
			    		}
			    	}
			    	//没有出现错误
			    		//如果没有出现错误
					$ret['success'] = true;
					$ret['message'] = iconv("gb2312","utf-8","保存审核信息成功");
					$ret['results'] = array();					
					echo json_encode($ret);
				}
				else {
					$ret['success'] = false;
					$ret['message'] = iconv("gb2312","utf-8",'课程成绩已被老师审核,信息保存失败');
					$ret['results'] = array();
					echo json_encode($ret);
					}
				} catch (Exception $e) {
					//echo $e;
		    		$ret['success'] = false;
					$ret['message'] = iconv("gb2312","utf-8",'保存审核信息失败');
					$ret['results'] = array();
					echo json_encode($ret);	
			}
		}
		
		
		//显示全部F3
		public function actionDisplayAllSelfScore()
		{
			if(!isset($_SESSION)) session_start();
			if(!isset($_SESSION['ID']))//如果不存在变量，则说明登录超时，需要重新登录
			{
				$url = "../../../";
				echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
				exit;
			}

      		if($_SESSION['WStart'] == false)
      		{
        		echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","学年测评尚未开始或者已经结束！"), "results"=>array()));
        		exit;
      		}

			$showAllF3= isset($_POST["showAllF3"])? $_POST["showAllF3"]:$_GET["showAllF3"];
			$showAllF3 = json_decode($showAllF3,true);//解码为数组
			$year= $showAllF3['year'];
			$studentid = $_SESSION['ID'];
			$db = Yii::app()->db;
			//$studentid = '2008301500116';
			$ret = array();
			try {

				$tclsql = "call search_stu_Tclassid('$studentid')";
				$tclrow = $db->createCommand($tclsql)->queryRow();//查询
				//测评班级id号存储在tclrow['t_classid']中
				$tclassid = $tclrow['t_classid'];
				$sql = "call search_AllSelfScore('$tclassid')";//这里面做一个比较。判断成绩是否被修改过
				$results = $db->createCommand($sql)->queryAll();//查询
				
				//如果没有出现错误
				$ret['success'] = true;
				$ret['message'] = "";
				$ret['results'] = $results;						
				echo json_encode($ret);
			} catch (Exception $e) {
				//echo $e;
	    		$ret['success'] = false;
				$ret['message'] = iconv("gb2312","utf-8",'返回信息失败');
				$ret['results'] = array();
				echo json_encode($ret);	
			}
		}
	}
?>

