<?php

class T_classController extends Controller
{
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated users to access all actions
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionDisplayMajorToList()//显示专业信息到下拉列表框中,接口5.1
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}
		if(!isset($_SESSION['ID']))	//如果不存在变量，咋舌说明登录超时，请重新登录
		{
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array("timeout"=>true)));
			exit();
		}
		if (isset($_SESSION["ID"]))
		{
			$departmentid = $_SESSION["departmentid"];//通过session获取学院id
			
			try
			{
				$db = Yii::app()->db;//连接mysql数据库
				
				//查询学院全部专业信息，包括专业id，专业名
				$sqlquery = "select majorid,majorname from major where departmentid='$departmentid'";
				$sqlinfo = $db->createCommand($sqlquery)->query();
				$results = $sqlinfo->readAll();

				$rets = array("success"=>true,"message"=>"","results"=>$results);
				echo json_encode($rets);
				//var_dump($rets);
			}
			catch (Exception $e)
			{
				$rets = array("success"=>false,"message"=>$e->getMessage(),"results"=>array());
				echo json_encode($rets);
			}
		}
	}


	public function actionShowClass()//显示对应学院对应年级对应专业的全部班级信息,接口5.3
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}
		if(!isset($_SESSION['ID']))	//如果不存在变量，咋舌说明登录超时，请重新登录
		{
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array("timeout"=>true)));
			exit();
		}
		if (isset($_SESSION["ID"]))
		{
			$departmentid = $_SESSION["departmentid"];//通过session获得学院id
			
			$showClass = isset($_POST["showClass"])?$_POST["showClass"]:$_GET["showClass"];
			$showClass = json_decode($showClass,true);	
			//var_dump($showClass);
			$grade = $showClass["grade"];//学院某一年级
			$majorid = $showClass["majorid"];//学院某一专业id

			try
			{
				$db = Yii::app()->db;//连接mysql数据库
				
				//如果专业id和年均不为空时，查询学院对应专业对应年级的班级id及其名称，按照班级号排序
				if ((!empty($majorid))&&(!empty($grade)))
				{
					$queryclass = "select classid,classname,grade,majorname from whuclass,major where departmentid='$departmentid' and major.majorid=whuclass.majorid and whuclass.majorid='$majorid' and grade='$grade' and whuclass.t_classid is NULL order by classid";
					$sqlinfo = $db->createCommand($queryclass)->query();
					$results = $sqlinfo->readAll();

					$rets = array("success"=>true,"message"=>"","results"=>$results);
					echo json_encode($rets);
				}
				else if ((!empty($majorid))&&empty($grade))//如果专业不为空而年级为空时，则查询学院某专业所有年级的班级id及其名称，按照班级号排序
					 {
							$queryclass = "select classid,classname,grade,majorname from whuclass,major where departmentid='$departmentid' and major.majorid=whuclass.majorid and whuclass.majorid='$majorid' and whuclass.t_classid is NULL order by classid";
							$sqlinfo = $db->createCommand($queryclass)->query();
							$results = $sqlinfo->readAll();

							$rets = array("success"=>true,"message"=>"","results"=>$results);
							echo json_encode($rets);
					 }
					 else if ((empty($majorid))&&(!empty($grade)))//如果专业为空而年不为空，则查询学院某年级所有专业的班级id及其名称，按学号排序
						  {
							 	$queryclass = "select classid,classname,grade,majorname from whuclass,major where departmentid='$departmentid' and major.majorid=whuclass.majorid and grade='$grade' and whuclass.t_classid is NULL order by classid";
								$sqlinfo = $db->createCommand($queryclass)->query();
								$results = $sqlinfo->readAll();

								$rets = array("success"=>true,"message"=>"","results"=>$results);
								echo json_encode($rets);
						  }
						  else //默认状态：专业号、年级均为空时，则查询学院所有专业所有年级的班级id及其名称，按学号排序
						  {
								$results = array();

								$querymajor  = "select majorid, majorname from major where departmentid='$departmentid'";
								$majorinfo = $db->createCommand($querymajor)->queryAll();
								foreach ($majorinfo as $item)
								{
									//var_dump($item);
									$majorid = $item["majorid"];
									$majorname = $item["majorname"];
									$queryclass = "select classid,classname,grade,'$majorname' as majorname from whuclass where majorid='$majorid' and t_classid is NULL order by classid";
									$classinfo = $db->createCommand($queryclass)->queryAll();
									//$classinfo = $sqlinfo->readAll();
									//var_dump($classinfo);
									//print_r($classinfo);
									//var_dump($classinfo);
									$results = array_merge($results,$classinfo);
								}

								$rets = array("success"=>true,"message"=>"","results"=>$results);
								echo json_encode($rets);
						  }

			}
			catch (Exception $e)
			{
				$rets = array("success"=>false,"message"=>$e->getMessage(),"results"=>array());
				echo json_encode($rets);
			}
		}
	}


	public function actionDisplayTeacherInfo()//显示辅导员信息到下拉列表框里,接口5.4
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}
		if(!isset($_SESSION['ID']))	//如果不存在变量，咋舌说明登录超时，请重新登录
		{
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array("timeout"=>true)));
			exit();
		}
		if (isset($_SESSION["ID"]))
		{
			$departmentid = $_SESSION["departmentid"];//通过session获取学院id

			try
			{
				$db = Yii::app()->db;//连接mysql数据库
				
				//查询学院所有辅导员id及名字
				$sqlquery = "select teacherid,teachername from teacher where departmentid='$departmentid' and teachername != '测评小组'";
				$sqlquery = iconv("gb2312", "utf-8", $sqlquery);
				$sqlinfo = $db->createCommand($sqlquery)->query();
				$results = $sqlinfo->readAll();

				$rets = array("success"=>true,"message"=>"","results"=>$results);
				echo json_encode($rets);
			}
			catch (Exception $e)
			{
				$rets = array("success"=>false,"message"=>$e->getMessage(),"results"=>array());
				echo json_encode($rets);
			}
		}
	}

	public function actionAddExistT_Class()//接口5.5 将班级加入到已存在测评班级中去
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}
		if(!isset($_SESSION['ID']))	//如果不存在变量，咋舌说明登录超时，请重新登录
		{
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array("timeout"=>true)));
			exit();
		}
		if (isset($_SESSION["ID"]))
		{
			$departmentid = $_SESSION["departmentid"];//通过session获取学院id

			$year = date("Y");//获取当前系统是年份//设置测评班级的默认年份为当前系统年份

			$saveT_class = isset($_POST["addExistT_Class"])?$_POST["addExistT_Class"]:$_GET["addExistT_Class"];
			$saveT_class = json_decode($saveT_class,true);
			$t_classid = $saveT_class["t_classid"];
			$classarray = $saveT_class["classArray"];//构成测试班级的班级号

			try
			{
				$db = Yii::app()->db;//连接mysql数据库
				foreach ($classarray as $value)//逐条设置新组成测评班级的真是班级中的测评班级号
				{
					$classid = $value;
					$updatewhuclass = "update whuclass set t_classid='$t_classid' where classid='$classid'";
					$db->createCommand($updatewhuclass)->execute();
				}
				$rets = array("success"=>true,"message"=>"","results"=>array());
				echo json_encode($rets);
			}
			catch (Exception $e)
			{
				$rets = array("success"=>false,"message"=>$e->getMessage(),"results"=>array());
				echo json_encode($rets);
			}
		}
	}
	
	public function actionSaveT_Class()//接口5.5 保存测评班级信息
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}
		if(!isset($_SESSION['ID']))	//如果不存在变量，咋舌说明登录超时，请重新登录
		{
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array("timeout"=>true)));
			exit();
		}
		if (isset($_SESSION["ID"]))
		{
			$departmentid = $_SESSION["departmentid"];//通过session获取学院id

			$year = date("Y");//获取当前系统是年份//设置测评班级的默认年份为当前系统年份

			$saveT_class = isset($_POST["saveT_class"])?$_POST["saveT_class"]:$_GET["saveT_class"];
			$saveT_class = json_decode($saveT_class,true);
			$t_classid = $saveT_class["t_classid"];//如果t_classid为了-9的话，则t_classid为前台标记号表示新建测试班级；如果t_classid不为-9，则更新测试班级相关信息
			$t_classname = $saveT_class["t_classname"];//测试班级名
			//$t_classname = iconv("gb2312","utf-8",$t_classname);
			$teacherid = $saveT_class["teacherid"];//辅导员号
			$classarray = $saveT_class["classArray"];//构成测试班级的班级号

			if ($t_classid != -9)//修改测评班级信息
			{
				try
				{
					$db = Yii::app()->db;//连接mysql数据库
					
					//更新测评班级班级名及辅导员号
					$updatet_class = "update t_class set t_name='$t_classname',teacherid='$teacherid' where t_classid='$t_classid'";
					$db->createCommand($updatet_class)->execute();
					
					//将以前测评班级的真是班级中的测评班级置空
					$updatetclass = "update whuclass set t_classid=NULL where t_classid='$t_classid'";
					$db->createCommand($updatetclass)->execute();

					foreach ($classarray as $value)//逐条设置新组成测评班级的真是班级中的测评班级号
					{
						$classid = $value;
						
						$updatewhuclass = "update whuclass set t_classid='$t_classid' where classid='$classid'";
						$db->createCommand($updatewhuclass)->execute();
					}

					$results = array("t_classid"=>$t_classid);
					$rets = array("success"=>true,"message"=>"","results"=>$results);
					echo json_encode($rets);
				}
				catch (Exception $e)
				{
					$rets = array("success"=>false,"message"=>$e->getMessage(),"results"=>array());
					echo json_encode($rets);
				}
			}
			else //新建测评班级
			{
				try
				{
					$db = Yii::app()->db;
					
					//2012-02-17 需要判断一个老师是否有两个相同的测评班级名称
					$queryt_classid = "select t_classid from t_class where t_name='$t_classname' and teacherid='$teacherid' and year='$year'";
					$queryrets = $db->createCommand($queryt_classid)->queryRow();
					if($queryrets != false)  //表示该辅导员已经存在一个测评班级了，不能有相同的名字
					{
						$message = iconv("gb2312", "utf-8","辅导员ID:")."$teacherid".iconv("gb2312", "utf-8","已经拥有名称为'")."$t_classname".iconv("gb2312", "utf-8","'的测评班级！请重新命名测评班级名称！");
						$rets = array("success"=>false,"message"=>$message,"results"=>array());
						echo json_encode($rets);
						exit;
					}

          //由于新创建的测评班级的F1-F3选项的初始化系数应该和学院的保持一致，这里需要注意
          //2012-02-19 modified by minzhenyu
          $sql = "select F1, F2, F3 from department where departmentid = '$departmentid'";
          $res = $db->createCommand($sql)->queryRow();
          if($res == false)
          {
            echo json_encode(array("success"=>false,"message"=>iconv("gb2312", "utf-8", "学院测评系数尚未设定，请先设定F1、F2、F3系数后，再建立测评班级！"),"results"=>array()));
            exit;
          }
          
					//插入新建的测评班级信息，包括班级名，辅导员id和年份
          $F1 = $res['F1']; $F2 = $res['F2']; $F3 = $res['F3'];
					$insertt_class = "insert into t_class(t_name,teacherid,year,F1,F2,F3) values('$t_classname','$teacherid','$year','$F1','$F2','$F3')";
					$db->createCommand($insertt_class)->execute();
					
					//查询新建测评班级的班级号
					$queryt_classid = "select t_classid from t_class where t_name='$t_classname' and teacherid='$teacherid' and year='$year'";
					$queryrets = $db->createCommand($queryt_classid)->queryRow();
					$tclassid = $queryrets["t_classid"];

					foreach ($classarray as $value)//逐条设置新组成测评班级的真是班级中的测评班级号
					{
						$classid = $value;
					
						$updatewhuclass = "update whuclass set t_classid='$tclassid' where classid='$classid'";
						$db->createCommand($updatewhuclass)->execute();
					}

					$results = array("t_classid"=>$tclassid);
					$rets = array("success"=>true,"message"=>"","results"=>$results);
					echo json_encode($rets);
				}
				catch (Exception $e)
				{
					$rets = array("success"=>false,"message"=>$e->getMessage(),"results"=>array());
					echo json_encode($rets);
				}
			}
		}
	}


	public function actionDisplayTClass()//显示下拉菜单里的测评班级信息,接口5.6
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}
		if(!isset($_SESSION['ID']))	//如果不存在变量，咋舌说明登录超时，请重新登录
		{
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array("timeout"=>true)));
			exit();
		}
		if (isset($_SESSION["ID"]))
		{
			$departmentid = $_SESSION["departmentid"];//通过session获取学院id

			$displayT_class = isset($_POST["displayT_class"])?$_POST["displayT_class"]:$_GET["displayT_class"];
			$displayT_class = json_decode($displayT_class,true);	
			$grade = $displayT_class["grade"];//年级
			$majorid = $displayT_class["majorid"];//专业id

			$year = date("Y");//设置默认年份
			
			try
			{
				$db = Yii::app()->db;//连接mysql数据库
				
				if ((!empty($grade))&&(!empty($majorid)))
				{
					//查询学院某年级某专业的所有测评班级班级号及班级名
					$sqlquery = "select distinct t_class.t_classid,t_name as t_classname, teacherid, grade from t_class,whuclass where grade='$grade' and majorid='$majorid' and whuclass.t_classid=t_class.t_classid and year='$year' order by grade, teacherid;";
					$sqlinfo = $db->createCommand($sqlquery)->query();
					$results = $sqlinfo->readAll();
				}
				else if((!empty($grade))&&empty($majorid))
					{
						$sqlquery = "select distinct t_class.t_classid,t_name as t_classname, grade, teacherid from t_class,whuclass,major where major.majorid= whuclass.majorid and grade='$grade' and year='$year' and whuclass.t_classid=t_class.t_classid and major.departmentid='$departmentid' order by grade, teacherid;";
						$sqlinfo = $db->createCommand($sqlquery)->query();
						$results = $sqlinfo->readAll();	
					}
					else if (empty($grade)&&(!empty($majorid)))
						{
							$sqlquery = "select distinct t_class.t_classid,t_name as t_classname, grade,teacherid from t_class,whuclass where majorid='$majorid' and whuclass.t_classid=t_class.t_classid and year='$year' order by grade, teacherid;";
							$sqlinfo = $db->createCommand($sqlquery)->query();
							$results = $sqlinfo->readAll();
						}
						else
						{
							$sqlquery = "select distinct t_class.t_classid,t_name as t_classname,teacherid,grade from t_class,whuclass,major where major.majorid= whuclass.majorid  and	year='$year' and whuclass.t_classid=t_class.t_classid and major.departmentid='$departmentid' order by grade, teacherid;";
							$sqlinfo = $db->createCommand($sqlquery)->query();
							$results = $sqlinfo->readAll();	
						}
				$rets = array("success"=>true,"message"=>"","results"=>$results);
				echo json_encode($rets);
				//var_dump($rets);
			}
			catch (Exception $e)
			{
				$rets = array("success"=>false,"message"=>$e->getMessage(),"results"=>array());
				echo json_encode($rets);
			}
		}
	}

	public function actionDisplayTClassAll()//显示对应年级的测评班级信息,接口5.7
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}
		if(!isset($_SESSION['ID']))	//如果不存在变量，咋舌说明登录超时，请重新登录
		{
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array("timeout"=>true)));
			exit();
		}
		if (isset($_SESSION["ID"]))
		{
			$showClassByTclass = isset($_POST["showClassByTclass"])?$_POST["showClassByTclass"]:$_GET["showClassByTclass"];
			$showClassByTclass = json_decode($showClassByTclass,true);	
			$year = $showClassByTclass["year"];//年份
			$t_classid = $showClassByTclass["t_classid"];//测评班级号

			try
			{
				$db = Yii::app()->db;//连接mysql数据库
				
				//查询测评班级的辅导员号
				$queryteacherid = "select teacherid from t_class where year='$year' and t_classid='$t_classid'";
				$queryrets = $db->createCommand($queryteacherid)->queryRow();
				$teacherid = $queryrets["teacherid"];

				if (!empty($teacherid))//如果测评班级已经分配了辅导员，则取出组成测评班级的所有真实班级号及名称
				{
					$querywhuclass = "select classid,classname from whuclass where t_classid='$t_classid'";
					$classinfo = $db->createCommand($querywhuclass)->query();
					$classarray = $classinfo->readAll();

					$results["teacherid"] = $teacherid;
					$results["classarray"] = $classarray;

					$rets = array("success"=>true,"message"=>"","results"=>$results);
					echo json_encode($rets);
				}
				else//如果尚未分配辅导员，则不处理
				{
					$results["teacherid"] = "";
					$results["classarray"] = "";

					$rets = array("success"=>true,"message"=>"","results"=>$results);
					echo json_encode($rets);
				}
			}
			catch (Exception $e)
			{
				$rets = array("success"=>false,"message"=>$e->getMessage(),"results"=>array());
				echo json_encode($rets);
			}
		}
	}


	public function actionDeleteTClass()//删除测评班级  接口5.8
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}
		if(!isset($_SESSION['ID']))	//如果不存在变量，咋舌说明登录超时，请重新登录
		{
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array("timeout"=>true)));
			exit();
		}
		if (isset($_SESSION["ID"]))

		{
			$deleteTClass = isset($_POST["deleteTClass"])?$_POST["deleteTClass"]:$_GET["deleteTClass"];
			$deleteTClass = json_decode($deleteTClass,true);
			$t_classid = $deleteTClass["t_classid"];
			$year = $deleteTClass["year"];

			try
			{
				$db = Yii::app()->db;//连接mysql数据库
				
				//删除对应年份对应测评班级号的测评班级记录
				$delt_class = "delete from t_class where t_classid='$t_classid' and year='$year'";
				$db->createCommand($delt_class)->execute();
				
				$delt_class = "update whuclass set t_classid = NULL where t_classid='$t_classid'";
				$db->createCommand($delt_class)->execute();

				$rets = array("success"=>true,"message"=>"","results"=>array());
				echo json_encode($rets);
			}
			catch (Exception $e)
			{
				$rets = array("success"=>false,"message"=>$e->getMessage(),"results"=>array());
				echo json_encode($rets);
			}
		}
	}


}
