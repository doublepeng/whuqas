<?php

class WhuClassController extends Controller
{
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated users to access all actions
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionDisplayClassByMajor()//接口1.2 显示对应学院对应年份对应年级对应专业的班级列表信息
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}
		if(!isset($_SESSION['ID']))	//如果不存在变量，咋舌说明登录超时，请重新登录
		{
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array("timeout"=>true)));
			exit();
		}

		if (isset($_SESSION["ID"]))
		{
			$departmentid = $_SESSION["departmentid"];//通过session获取学院id

			$displayClassByMajor = isset($_POST["displayClassByMajor"]) ? $_POST["displayClassByMajor"] :$_GET["displayClassByMajor"];
			$displayClassByMajor = json_decode($displayClassByMajor,true);
			$grade = $displayClassByMajor["grade"];	//获取前台传送的年级数据
			$year = $displayClassByMajor["year"];//获取年份			
			$majorname = $displayClassByMajor["major"];//获取专业名称
			//$majorname = iconv("gb2312","utf-8",$majorname);

			try
			{
				$db = Yii::app()->db;
				
				//查询学院专业的专业id号
				$querymajorid = "select majorid from major where departmentid='$departmentid' and majorname='$majorname'";
				$queryrets = $db->createCommand($querymajorid)->queryAll();

				foreach ($queryrets as $value)
				{
					$majorid = $value["majorid"];
				}
			}
			catch (Exception $e)
			{
				$rets = array("success"=>false,"message"=>$e->getMessage(),"results"=>array());
				echo json_encode($rets);
			}

			try
			{
				//查询班级名称，班级id和辅导员姓名
				$queryclass = "select classid,classname,teachername from whuclass,t_class,teacher where	majorid='$majorid' and grade='$grade' and whuclass.t_classid=							t_class.t_classid and t_class.teacherid=teacher.teacherid and teacher.departmentid='$departmentid'";
				$queryrets = $db->createCommand($queryclass)->query();
				$results = $queryrets->readAll();

				$rets = array("success"=>true,"message"=>"","results"=>$results);
				echo json_encode($rets);
			}
			catch (Exception $e)
			{
				$rets = array("success"=>false,"message"=>$e->getMessage(),"results"=>array());
				echo json_encode($rets);
			}

		}
	}

	public function actionSaveClass()//保存新增加班级的列表信息,接口3.1
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}
		if(!isset($_SESSION['ID']))	//如果不存在变量，咋舌说明登录超时，请重新登录
		{
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array("timeout"=>true)));
			exit();
		}

		if (isset($_SESSION["ID"]))
		{
			$departmentid = $_SESSION["departmentid"];//通过session获取学院id

			$saveClass = isset($_POST["saveClass"])?$_POST["saveClass"]:$_GET["saveClass"];
			$saveClass = json_decode($saveClass,true);

			$db = Yii::app()->db;//连接mysql数据库

			foreach($saveClass as $value)
			{
				$grade = $value["grade"];
				$majorname = $value["major"];
				$classname = $value["classname"];

				try
				{
					//查询学院专业对应的专业id号
					$sqlquery = "select majorid from major where majorname='$majorname' and departmentid='$departmentid'";
					$queryrets = $db->createCommand($sqlquery)->queryRow();
					$majorid = $queryrets["majorid"];

					if(Whuclass::exist($classname, $grade, $majorid))//exists
					{
						echo json_encode(array('success'=>false, 'message'=>Whuclass::$err, 'results'=>array()));
						exit;
					}
					
					//往whuclass表中插入班级名称，年级和专业id
					$sqlinsert ="insert into whuclass(classname,grade,majorid) values('$classname','$grade','$majorid')";
					$db->createCommand($sqlinsert)->execute();
				}
				catch (Exception $e)
				{
					$rets = array("success"=>false,"message"=>$e->getMessage(),"results"=>array());
					echo json_encode($rets);	
				}
			}

			$rets = array("success"=>true,"message"=>"","results"=>array());
			echo json_encode($rets);
		}	
	}

	public function actionDisplayMajorToList()//显示专业信息并添加到下拉列表中，接口3.2
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}
		if(!isset($_SESSION['ID']))	//如果不存在变量，咋舌说明登录超时，请重新登录
		{
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array("timeout"=>true)));
			exit();
		}

		if (isset($_SESSION["ID"]))
		{
			$departmentid = $_SESSION["departmentid"];//通过session获取学院id

			try
			{
				$db = Yii::app()->db;//连接mysql数据库
				
				//通过连接查询major、whuclass表获取学院某一年的所有专业名称，并读出查询结果存放到数组中
				$sqlquery = "select majorid,majorname from major where departmentid='$departmentid'";
				$queryinfo= $db->createCommand($sqlquery)->query();
				$retMajor = $queryinfo->readAll();

				$rets = array("success"=>true,"message"=>"","results"=>$retMajor);
				echo json_encode($rets);
			}
			catch (Exception $e)
			{
				$rets = array("success"=>false,"message"=>$e->getMessage(),"results"=>array());
				echo json_encode($rets);
			}
		}
	}


	public function actionDisplayClassByYearByMajor()//显示对应学院对应年级对应专业的全部班级信息,接口3.3
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}
		if(!isset($_SESSION['ID']))	//如果不存在变量，咋舌说明登录超时，请重新登录
		{
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array("timeout"=>true)));
			exit();
		}

		if (isset($_SESSION["ID"]))
		{
			$departmentid = $_SESSION["departmentid"];	//通过session获取学院id

			$displayClassByYearByMajor = isset($_POST["displayClassByYearByMajor"])?$_POST["displayClassByYearByMajor"]																					:$_GET["displayClassByYearByMajor"];
			$displayClassByYearByMajor = json_decode($displayClassByYearByMajor,true);
			$grade = $displayClassByYearByMajor["grade"];
			$majorname = $displayClassByYearByMajor["major"];

			try
			{
				$db = Yii::app()->db;//连接mysql数据库

				$querymajor = "select majorid from major where departmentid='$departmentid' and majorname='$majorname'";
				$majoridinfo = $db->createCommand($querymajor)->queryRow();
				$majorid = $majoridinfo["majorid"];

				//通过连接查询whuclass表来读取出学院某一年级的全部班级id及班级名称
				$sqlquery = "select classid,classname from whuclass where grade='$grade' and majorid='$majorid'";
				$queryinfo = $db->createCommand($sqlquery)->query();
				$results = $queryinfo->readAll();

				$rets = array("success"=>true,"message"=>"","results"=>$results);
				echo json_encode($rets);
			}
			catch(Exception $e)
			{
				$rets = array("success"=>false,"message"=>$e->getMessage(),"results"=>array());
				echo json_encode($rets);
			}
		}
	}

	public function actionSaveEditClass()//保存编辑后的班级信息,接口3.4
	{
		if (!isset($_SESSION))
		{
			session_start();//开启session
		}
		if(!isset($_SESSION['ID']))	//如果不存在变量，咋舌说明登录超时，请重新登录
		{
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array("timeout"=>true)));
			exit();
		}

		if (isset($_SESSION["ID"]))
		{
			$departmentid = $_SESSION["departmentid"];

			$saveEditClass = isset($_POST["saveEditClass"])?$_POST["saveEditClass"]:$_GET["saveEditClass"];
			$saveEditClass = json_decode($saveEditClass,true);
			$grade = $saveEditClass["grade"];//年级
			$majorname = $saveEditClass["majorname"];//专业名称
			$saveeditclassbymajor = $saveEditClass["saveeditclassbymajor"];
			$updateclass = $saveeditclassbymajor["update"];//要更新的班级信息
			$delclass = $saveeditclassbymajor["delete1"];//要删除的班级信息

			try
			{
				$db = Yii::app()->db;//连接mysql数据库
				
				//读取学院某专业的专业id
				$querymajorid = "select majorid from major where departmentid='$departmentid' and majorname='$majorname'";
				$queryrets = $db->createCommand($querymajorid)->queryRow();
				$majorid = $queryrets["majorid"];
				
				if (!empty($updateclass))//如果要更新的班级不为空，则进行更新
				{	
					foreach($updateclass as $datarow)//记录逐条更新
					{
						$classid = $datarow["classid"];
						$classname = $datarow["classname"];
						//$classname = iconv("gb2312","utf-8",$classname);
						$sqlupdate = "UPDATE whuclass SET classname='$classname' WHERE classid='$classid' and grade='$grade' and majorid='$majorid'";
						$db->createCommand($sqlupdate)->execute();
					}
				}
				
				if (!empty($delclass))//如果要删除的班级信息不为空，则进行删除
				{
					foreach($delclass as $dataRow)//记录逐条删除
					{
						$classid = $dataRow["classid"];
						$sqldel = "DELETE FROM whuclass WHERE classid='$classid' and grade='$grade' and majorid='$majorid'";
						$db->createCommand($sqldel)->execute();
					}
				}
			
				$rets = array("success"=>true,"message"=>"","results"=>array());
				echo json_encode($rets);
			}
			catch(Exception $e)
			{
				$rets = array("success"=>false,"message"=>$e->getMessage(),"results"=>array());
				echo json_encode($rets);
			}
		}
	}


}
