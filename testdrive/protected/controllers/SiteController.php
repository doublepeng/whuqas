<?php

//header("Content-Type: text/html; charset=UTF-8");

class SiteController extends Controller
{
	private $hostname = 'localhost';
	private $user = 'root';
	private $pwd = 'root';
	private $encode = "set names 'utf8'";
	private $dbname = 'whuqas2011212';

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to access 'index' and 'view' actions.
				'actions'=>array('login', 'err'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated users to access all actions
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionLogin()
	{		
		if(!isset($_SESSION)) session_start();//开启缓存 
		if(!isset($_REQUEST["loginPara"]))
		{
			exit;
		}
		//数据库连接
		$connect = mysql_connect($this->hostname, $this->user, $this->pwd); 
        mysql_query($this->encode);
        mysql_select_db($this->dbname,$connect);
    
		$loginPara = isset($_POST["loginPara"]) ? $_POST["loginPara"] : $_GET["loginPara"];
		$loginPara = json_decode($loginPara, true);

   		//防止sql注入
   		$inject = new Inject; 
		$password = $inject->injectCheck($loginPara["pwd"]);
		$username = $inject->injectCheck($loginPara["sid"]);
		$type = $inject->injectCheck($loginPara["type"]);
        /*$username="1";
        $password="123456";
        $type='manager';*/
		//存储用户登录信息
		//$password = '';
		//$password = md5($password);
		$ret = array();
		try {
			//学生奖学金评定的年份，由学校管理员点击开始学年测评的时间确定
			$sqlyear = "select YEAR(starttime),starttime,endtime from checklist order by starttime desc limit 1";
			$queryyear = mysql_query( $sqlyear);
			if (!$queryyear)   
                	throw new Exception(iconv("gb2312","utf-8","服务器连接失败，请稍候重试"));				
			$rowyear = mysql_fetch_row($queryyear);		
			$year = $rowyear[0];//返回年份，参数
			$_SESSION['year'] = $year;
			//根据当前时间判断是否处于starttime与endtime之间
			$starttime = $rowyear[1];
			$endtime = $rowyear[2];
			$nowtime =  date("Y-m-d",time());
			////如果为空并且当前时间在starttime之后
      		//2012-01-13 minzhenyu
			if(empty($endtime) and $nowtime >=  date("Y-m-d",strtotime($starttime) ) )
			{
				$_SESSION['WStart'] = true;   //用来标记测评是否还是的session
			}
			else 
			{
				$_SESSION['WStart'] = false;
			}

			if($type == "student")//如果登录者类型为学生
			{
				//根据学生是否填写入学年份即年级，判断学生是否填写信息
				//处理的太多了，要拆开。。！！！
				$sql = "call search_stuinfo ('$username')";
				$query= mysql_query($sql);
				if (!$query)   
                	throw new Exception(iconv("gb2312","utf-8","登录失败，用户名不存在~"));	
				$row = mysql_fetch_array($query);		
				$rows = array(); //保存结果  
				if($row['studentid'] == null)//如果对象不存在，说明数据库无此人
				{
					$ret['success'] = false;
					$ret['message'] = iconv("gb2312","utf-8","帐号：")."$username".iconv("gb2312","utf-8"," 不存在，如果使用该帐号，请联系学院学工办管理员添加！");	
					$ret['results'] = array();						
					echo json_encode($ret);	
					@mysql_close($connect) or die('Server error, please try again later');	
					exit;				
				}
				elseif($row['password'] != $password)//如果密码错误
				{
					$ret['success'] = false;
					$ret['message'] = "$username".iconv("gb2312","utf-8","密码错误");
					$ret['results'] = array();							
					echo json_encode($ret);	
					@mysql_close($connect) or die('Server error, please try again later');
					exit;
				}
				else//如果不为空，并且密码输入正确
				{
					$rows['name'] = $row['studentname'];//返回学生姓名，并且返回学生是否注册信息
                	//如果classid为空，则表示学生未填写初始信息
					if ($row['classid'] == null)
						$rows['registered'] = false;//未注册
				 	else
						$rows['registered'] = true;//注册
          			//根据ccol中的online方法判断是否达到网站访问的上线
         			$ccol = new ccol; 
		     		$online  = $ccol->online($username);
          			if ($online)//如果为真，可以访问
         			{
						$ret['success'] = true;
						$ret['message'] = iconv("gb2312","utf-8","登录成功");	
						$ret['results'] = $rows;	
						echo json_encode($ret);
					  @mysql_close($connect) or die('Server error, please try again later');
				  	}	
				  	else 
				  	{
				    	$ret['success'] = false;
						$ret['message'] = iconv("gb2312","utf-8","当前访问人数过多，请稍候重试！");
						$ret['results'] = array();							
						echo json_encode($ret);	
				  	@mysql_close($connect) or die('Server error, please try again later');
						exit;
				  	}
          
					$_SESSION['ID'] = $row['studentid'];
					$_SESSION['type'] = "student";
					$_SESSION['departmentid'] = $row['departmentid'];//参数为学院id		
					Yii::app()->user->login(new UserIdentity('student','student'),0);						
				}
				
			}
			elseif ($type == "teacher")//如果登录者为教师的话
			{
				$sql = "select teacherid,teachername,departmentid,password from teacher where teacherid = '$username'";
				$query = mysql_query( $sql, $connect );
                $rows = array(); //保存结果    
                if (!$query)   
                	throw new Exception(iconv("gb2312","utf-8","连接失败，请稍候重试！"));
                $row = mysql_fetch_array($query);//已经查询出教室的教师号，教师名，所属学院id，密码
				$testname = '测评小组';
				$testname = iconv("gb2312","UTF-8",$testname);
				if($row['teacherid'] == null)//如果对象不存在，说明数据库无此人
				{
					$ret['success'] = false;
					$ret['message'] = iconv("gb2312","utf-8","帐号：")."$username".iconv("gb2312","utf-8"," 不存在，如果使用该帐号，请联系学院辅导员添加！");	
					$ret['results'] = array();						
					echo json_encode($ret);	
					@mysql_close($connect) or die('Server error, please try again later');
					exit;
					
				}
				elseif($row['password'] != $password)//如果密码错误
				{
					$ret['success'] = false;
					$ret['message'] = "$username".iconv("gb2312","utf-8","密码错误");	
					$ret['results'] = array();						
					echo json_encode($ret);	
					@mysql_close($connect) or die('Server error, please try again later');
					exit;
				}
				else//如果不为空，并且密码输入正确
				{
					if($row['teachername'] != $testname )//如果不是测评小组成员的话
					{
						$ret['success'] = true;
						$ret['message'] = iconv("gb2312","utf-8","登录成功");	
						$teachername = 	$row['teachername'];
						$ret['results'] = array('name'=>$teachername);	
				  	@mysql_close($connect) or die('Server error, please try again later');
						echo json_encode($ret);	
						$_SESSION['ID'] = $row['teacherid'];
						$_SESSION['type'] = $type;
						$_SESSION['departmentid'] = $row['departmentid'];	
						Yii::app()->user->login(new UserIdentity('teacher','teacher'),0);					
					}
					else//如果是测评小组成员的话
					{ 					
						$ret['success'] = true;
						$ret['message'] = iconv("gb2312","utf-8","登录成功");	
						$ret['results'] = array('name'=>$testname);		
						$_SESSION['ID'] = $row['teacherid'];
						$_SESSION['type'] = 'tempteacher';
						$_SESSION['departmentid'] = $row['departmentid'];		
						Yii::app()->user->login(new UserIdentity('temp','temp'),0);			
						echo json_encode($ret);	
					  @mysql_close($connect) or die('Server error, please try again later');
					}
				}	
										
			}
			elseif($type == "manager")//如果是管理员
			{
				$sql = "select managerid,departmentid,managername,password from manager where managerid = '$username'";
				 $query = mysql_query( $sql, $connect );
                $rows = array(); //保存结果    
                if (!$query)   
                	throw new Exception(iconv("gb2312","utf-8","连接失败，请稍候重试！"));
                $row = mysql_fetch_array($query);//只查询一次
                		
				if($row['managerid'] == null)//如果对象不存在，说明数据库无此人
				{
					$ret['success'] = false;
					$ret['message'] = iconv("gb2312","utf-8","帐号：")."$username".iconv("gb2312","utf-8"," 不存在，如果使用该帐号，请联系学工部管理员添加！");	
					$ret['results'] = array();				
					echo json_encode($ret);	
					@mysql_close($connect) or die('Server error, please try again later');
					exit;
				}
				elseif($row['password'] != $password)//如果密码错误
				{
					$ret['success'] = false;
					$ret['message'] = "$username".iconv("gb2312","utf-8","密码错误");	
					$ret['results'] = array();						
					echo json_encode($ret);	
					@mysql_close($connect) or die('Server error, please try again later');
					exit;
				}
				else//如果不为空，并且密码输入正确
				{	
					//判断登录者为学院管理员还是学工部管理员
					if($row['departmentid'] == 1)//如果所属学院为1，即表示为学工部管理员
					{
							$_SESSION['type'] = "supermanager";	
							Yii::app()->user->login(new UserIdentity('admin','admin'),0);	
					}	
					else 
					{
							$_SESSION['type'] = "manager";//为默认的学校管理员
							Yii::app()->user->login(new UserIdentity('manager','manager'),0);
					}
					$ret['success'] = true;
					$ret['message'] = iconv("gb2312","utf-8","登录成功");
					$managername = $row['managername'];
					$ret['results'] = array("name"=>$managername);		
					echo json_encode($ret);	
					$_SESSION['ID'] = $row['managerid'];
					$_SESSION['departmentid'] = $row['departmentid'];
					@mysql_close($connect) or die('Server error, please try again later');
				}
			}			
		} catch (Exception $e) {
				$ret['success'] = false;
				$ret['message'] = $e->getMessage();					
				echo json_encode($ret);	
				@mysql_close($connect) or die('Server error, please try again later');
		}				
	}

	public function actionLogout()
	{
		if(!isset($_SESSION)) session_start();
		unset($_SESSION);
		session_destroy();
		Yii::app()->user->logout();		
		echo("<meta http-equiv=refresh content='0; url=../../../../'>");  /*index.html*/
		//echo json_encode(array("success"=>true));
		//exit;
	}

	public function actionErr()
	{
		if(!isset($_SESSION)) session_start();
		unset($_SESSION);
		session_destroy();
		Yii::app()->user->logout();		
		echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","您登录的时间超时，请返回首页重新登录！"), "results"=>array('timeout'=>true)));
		exit;
	}
	
	//获取学年测评是否开始
	public function actionQASStart()
	{
		$db = Yii::app()->db;
		try {
			$year = date("Y", time());
			$starttime = date("Y-m-d", time());
			$sqlclass = "select * from checklist where starttime like '$year-%' and starttime <= '$starttime' and endtime is null;";
			$resultsclass = $db->createCommand($sqlclass)->queryAll();//查询结果
			if(!empty($resultsclass))
			{
				$ret = array('success'=>true,'message'=>iconv("gb2312","utf-8",'学年测评中......'),'results'=>array());
			}
			else
			{
				$ret = array('success'=>false,'message'=>iconv("gb2312","utf-8",'尚未开始学年测评'),'results'=>array());
			}
			echo json_encode($ret);
		} catch (Exception $e) {
			//echo $e;
			$ret['success'] = false;
			$ret['message'] = iconv("gb2312","utf-8","查询是否开始学年测评失败！");
			$ret['results'] = array();
			echo json_encode($ret);
		}
	}

  //获取学年测评是否结束
	public function actionQASEnd()
	{
		if(!isset($_SESSION)) session_start();
		if(!isset($_SESSION["ID"]))
		{
		  echo json_encode(array("success"=>false,"message"=>iconv("gb2312","utf-8", "您没有权限！无法进行此操作！"),"results"=>array()));
		  exit;
		}  

		$db = Yii::app()->db;
		try {
			$currentyear = date("Y", time());

			  //首先判断学年测评是否开始
			  $sql = "select * from checklist where starttime like '$currentyear-%'";
			  $query = $db->createCommand($sql)->query();
				$res = $query->readAll();
			  if(empty($res))   //如果没开始
			  {
				echo json_encode(array("success"=>false,"message"=>iconv("gb2312","utf-8", $currentyear."度学年测评尚未开始，您暂时无法下载测评相关表格！"),"results"=>array()));
				exit;
			  }
				  
			  //学年测评已经开始，那么需要判断学年测评是否已经结束
			  if(empty($res[0]["endtime"]))   //表示学年测评尚未结束
			  {
				echo json_encode(array("success"=>false,"message"=>iconv("gb2312","utf-8", $currentyear."度学年测评尚未结束，您暂时无法下载测评相关表格！"),"results"=>array()));
				exit;
			  }

			echo json_encode(array("success"=>true,"message"=>"","results"=>array()));
		} catch (Exception $e) {
			$ret['success'] = false;
			$ret['message'] = iconv("gb2312","utf-8","判断学年测评是否结束失败！");
			$ret['results'] = array();
			echo json_encode($ret);
		}
	}
}

?>
