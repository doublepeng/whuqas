<?php

class SelfevaconController extends Controller
{
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated users to access all actions
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionSaveScoreOption()//±£ŽæÐÂÔöµÃ·ÖÑ¡Ïî,œÓ¿Ú8.1
	{
		if (!isset($_SESSION))
		{
			session_start();//¿ªÆôsession
		}
		if(!isset($_SESSION['ID']))	//Èç¹û²»ŽæÔÚ±äÁ¿£¬ÕŠÉàËµÃ÷µÇÂŒ³¬Ê±£¬ÇëÖØÐÂµÇÂŒ
		{
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","ÄúµÇÂŒµÄÊ±Œä³¬Ê±£¬Çë·µ»ØÊ×Ò³ÖØÐÂµÇÂŒ£¡"), "results"=>array("timeout"=>true)));
			exit();
		}
		if (isset($_SESSION["ID"]))//ÅÐ¶šÓÃ»§ÒÑµÇÂŒ
		{
			$departmentid = $_SESSION["departmentid"];

			$saveScoreOption = isset($_POST["saveScoreOption"]) ? $_POST["saveScoreOption"] :$_GET["saveScoreOption"];
			$saveScoreOption = json_decode($saveScoreOption,true);

			$year = date("Y");//»ñÈ¡ÏµÍ³Äê·Ý

			$db = Yii::app()->db;
			
			foreach($saveScoreOption as $value)
			{
				$scoreoption = $value["scoreOption"];
				$max = $value["maxscore"];

				try
				{
					if(Selfevacon::existEvaCon($scoreoption, $departmentid, $year))//exists
					{
						echo json_encode(array('success'=>false, 'message'=>'该测评选项已存在！'));
						exit;
					}
					
					$sqlinsert = "insert into selfevacon(evaluatename,departmentid,maxscore,year) values('$scoreoption','$departmentid','$max','$year')";
					$db->createCommand($sqlinsert)->execute();
				}
				catch (Exception $e)
				{
					$rets = array("success"=>false,"message"=>$e->getMessage(),"results"=>array());
					echo json_encode($rets);
				}
			}
			$rets = array("success"=>true,"message"=>"","results"=>array());
			echo json_encode($rets);
		}
	}


	public function actionDisplayScoreOption()//ÏÔÊŸŽŽÐÂµÃ·ÖÑ¡Ïî,œÓ¿Ú8.2
	{
		if (!isset($_SESSION))
		{
			session_start();//¿ªÆôsession
		}
		if(!isset($_SESSION['ID']))	//Èç¹û²»ŽæÔÚ±äÁ¿£¬ÕŠÉàËµÃ÷µÇÂŒ³¬Ê±£¬ÇëÖØÐÂµÇÂŒ
		{
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","ÄúµÇÂŒµÄÊ±Œä³¬Ê±£¬Çë·µ»ØÊ×Ò³ÖØÐÂµÇÂŒ£¡"), "results"=>array("timeout"=>true)));
			exit();
		}
		if (isset($_SESSION["ID"]))//ÅÐ¶šÓÃ»§ÒÑµÇÂŒ
		{
			$departmentid = $_SESSION["departmentid"];
      		$year = $_SESSION["year"];

			try
			{
				$db = Yii::app()->db;

				$sqlquery = "select evaluateid as scoreOptionid,evaluatename as scoreOption,maxscore from selfevacon where departmentid='$departmentid' and year='$year'";
				$queryinfo = $db->createCommand($sqlquery)->query();
				$displayScoreOption = $queryinfo->readAll();

							
				$rets = array("success"=>true,"message"=>"","results"=>$displayScoreOption);
				echo json_encode($rets);
			}
			catch (Exception $e)
			{
				$rets = array("success"=>false,"message"=>$e->getMessage(),"results"=>array());
				echo json_encode($rets);
			}
		}
	}

	public function actionSaveEditScoreOption()//±£Žæ±àŒ­ºóµÄ×šÒµÐÅÏ¢£¬œÓ¿Ú8.3
	{
		if (!isset($_SESSION))
		{
			session_start();//¿ªÆôsession
		}
		if(!isset($_SESSION['ID']))	//Èç¹û²»ŽæÔÚ±äÁ¿£¬ÕŠÉàËµÃ÷µÇÂŒ³¬Ê±£¬ÇëÖØÐÂµÇÂŒ
		{
			echo json_encode(array("success"=>false, "message"=>iconv("gb2312","utf-8","ÄúµÇÂŒµÄÊ±Œä³¬Ê±£¬Çë·µ»ØÊ×Ò³ÖØÐÂµÇÂŒ£¡"), "results"=>array("timeout"=>true)));
			exit();
		}
		if (isset($_SESSION["ID"]))//ÅÐ¶šÓÃ»§ÒÑµÇÂŒ
		{
			$departmentid = $_SESSION["departmentid"];

			$saveEditScoreOption = isset($_POST["saveEditScoreOption"]) ? $_POST["saveEditScoreOption"] :$_GET["saveEditScoreOption"];
			$saveEditScoreOption = json_decode($saveEditScoreOption,true);
			$updatescoreoption = $saveEditScoreOption["update"];
			$delscoreoption = $saveEditScoreOption["delete1"];

			try
			{
				$db = Yii::app()->db;
				
				if (!empty($updatescoreoption))
				{
					foreach($updatescoreoption as $datarow)
					{
						$evaluateid = $datarow["scoreoptionid"];
						//echo $evaluateid;
						$evaluatename = $datarow["scoreoption"];
						//$evaluatename = iconv("gb2312","utf-8",$evaluatename);
						$max = $datarow["max"];

						$sqlupdate = "UPDATE selfevacon SET evaluatename='$evaluatename',maxscore='$max' WHERE evaluateid='$evaluateid' and													departmentid='$departmentid'";
						$db->createCommand($sqlupdate)->execute();
					}
				}
				
				if (!empty($delscoreoption))
				{
					foreach($delscoreoption as $value)
					{
						$evaluateid = $value["scoreoptionid"];
						$sqldel = "DELETE FROM selfevacon WHERE evaluateid='$evaluateid' and departmentid='$departmentid'";
						$db->createCommand($sqldel)->execute();
					}
				}

				$rets = array("success"=>true,"message"=>"","results"=>array());
				echo json_encode($rets);
			}
			catch(Exception $e)
			{
				$rets = array("success"=>false,"message"=>$e->getMessage(),"results"=>array());
				echo json_encode($rets);
			}
		}
	}


}
