// JavaScript Document
//首先加载测评班级的下拉框
addSelectClass();

var selectionArray = {}; //记录F3测评选项的id和maxscore
var mydata = {};

//待审核学生列表
var totalRecord = 0;
var pages = 0;
var currentPage = 1;
//已添加学生列表分页
var totalRecord1 = 0;
var pages1 = 0;
var currentPage1 = 1;

var currentMem;   //当前审核的学生的html行

//用来标记一个学生的F1成绩是否能审核通过，
//只要有一个没有被测评小组审核或者学生没有自评就不能审核通过
var flagCanPass = 2;   //只有为1的时候，才能审核通过，为0表示有人没有测评，为2表示已审核通过

$(document).ready(function() {
	//调整屏幕的高度
	parent.parent.document.getElementById("frame_content").height=420;	
	//首先要隐藏错误输出提示、基本素质成绩提示和页面
	$("#error").hide(); $("#div-listF3").hide(); $("#div-showF3").hide();
	//隐藏模板tr
	$("#stu-table tbody tr").eq(0).hide();
	//隐藏翻页按钮
	$("#btLast").hide(); $("#btNext").hide(); $("#btFormer").hide(); $("#btFirst").hide(); 
	
	$("#btLast1").hide(); $("#btNext1").hide(); $("#btFormer1").hide(); $("#btFirst1").hide(); 
	//隐藏第一行
	$("#tb tbody tr").eq(0).hide();

	//当下拉框中得测评班级改变时
	changeClass();

	//学生列表的翻页控制
	stuPage();

	//自评列表的分页控制
	selfPage();

	//审核通过一个学生的成绩
	passAStu();  //这里要将老师审核的成绩一个一个传给后台

	//批量审核，直接传学号给后台，后台处理就行
	passMultiStu();

	//选择所有行
	$("#CKA").click(function() {
		
		$("#stu-table tbody tr:gt(0)").each(function() {
			if(!$(this).is(":hidden")) 	
				$(this).find("#CK").get(0).checked = $("#CKA").get(0).checked;
		});
	});

	$("#BtStuBack").click(function(){
		$("#tb tbody tr:gt(0)").remove();
		$("#div-listF3").hide();
		$("#div-showF3").hide();
		$("#students").show();
		$("#div-class").show();
		adjustHeight();
	});

	$("#BtSelfBack").click(function(){
		$("#tb-self tbody tr").remove();
		$("#div-listF3").show();
		$("#div-showF3").hide();
		adjustHeight();
	});

	btSave();

})

function passMultiStu()
{
	$("#btMutiCheck").click(function (){
		var passF3 = {};
		passF3["type"] = "teacher";
		passF3["members"] = [];
		var alreadyPassedMem = [];
		$("#stu-table tbody tr:gt(0)").each(function() {
			if ($(this).find("#CK").get(0).checked == true && $(this).find("#link").html() != "已审核") {
				passF3["members"].push({"sid": $(this).find("#stuID").html()});
				alreadyPassedMem.push($(this).find("#link"));
			}
		});
		if(passF3["members"].length == 0)
		{
			alert("请您选择审核的学生~");
			return false;
		}
		
		//alert($.toJSON(passF3));
		if(confirm('您将确认审核通过？'))
		{
      $("#layer").height(parent.parent.document.getElementById("frame_content").height);
			$("#layer").mask("正在提交审核，请稍等...");
			$.get(
				"../../../testdrive/index.php?r=teacher/teacherBadudit",
				{passF3: $.toJSON(passF3)},
				function(data)
				{
					var ret = $.evalJSON(data);
					if(ret.success == true)
					{
						alert("提交成功~");
						$("#BtStuBack").click();
						
						$.each(alreadyPassedMem, function(key, value){
							value.html("已审核");
							value.removeAttr("onclick");
							value.removeAttr("href");
						});
					}
					else
					{
						//alert('服务器端返回错误信息=>"' +ret.message+'"');
if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
					{
						alert(ret.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href='../../../';
						return false;
					}
						alert(ret.message);
					}
          $("#layer").unmask();
				}
			);
		}
		
		$("#CKA").attr("checked", false);
		$("#CKA").click();
		$("#CKA").attr("checked", false);
	});
}

//审核通过一个学生的成绩,此时需要将该学生的审核分数传给后台，后台需要去修改，并且置为已测评小组审核标志
function passAStu()
{
	$("#BtSelfConfirm").click(function(){
		var saveF3 = {};
		var date = new Date();
		var vYear = date.getFullYear();
	
		saveF3["sid"] = currentMem.find("#stuID").html();
		saveF3["year"] = vYear;
		saveF3["type"] = "teacher";
		saveF3["score"] = parseFloat($("#tb-self th:last span").html());
		saveF3["contents"] = [];

		for(var key in mydata)
		{
			for (var i = 0; i < mydata[key].length; i++)
			{
				var _item = {};
				_item["selfscoreid"] = mydata[key][i].selfscoreid;
				_item["t_score"] = mydata[key][i].score;
				saveF3["contents"].push(_item);
			}
		}
		
		//alert($.toJSON(saveF3));		
		
		if(confirm('您将确认审核通过？'))
		{
      $("#layer").height(parent.parent.document.getElementById("frame_content").height);
			$("#layer").mask("正在提交审核，请稍等...");
			$.get(
				"../../../testdrive/index.php?r=teacher/saveSelfscore",
				{saveF3: $.toJSON(saveF3)},
				function(data)
				{
					var ret = $.evalJSON(data);
					if(ret.success == true)
					{
						alert("提交成功~");
						$("#BtStuBack").click();

						currentMem.find("#link").html("已审核");
						currentMem.find("#link").removeAttr("onclick");
						currentMem.find("#link").removeAttr("href");
					}
					else
					{
						//alert('服务器端返回错误信息=>"' +ret.message+'"');
if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
					{
						alert(ret.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href='../../../';
						return false;
					}
						alert(ret.message);
					}
          $("#layer").unmask();
				}
			);
	
		}
	});
}

//保存记录
function btSave()
{
	$("#BtSave").click(function() {
		if(flagCanPass == 0)
		{
			alert("该学生的创新实践能力测评成绩还未经过测评小组审核，您无需审核~");
			return false;
		}
		else if(flagCanPass == 2)
		{
			alert("该学生的创新实践能力测评成绩已通过审核~无需重复审核~");
			return false;
		}

		if(validator())
		{
			mydata = {};
			$("#tb tbody tr:gt(0)").each(function() {			
				var name = $(this).find("#selectType").val();
				var content = $(this).find("#TContent").html();
				var score = $(this).find("#te_evascore").val();
				var selfscoreid = $(this).find("#TContent").attr("name");

				if(mydata.hasOwnProperty(name) == false)
				{
					mydata[name] = [];
				}
				mydata[name].push({content:content, score:score, selfscoreid:selfscoreid});
			});
			
			$("#div-listF3").hide();
			$("#div-showF3").show();
			showSelfEva();
		}
	});
}

//显示学生自我测评，被审核后的结果
function showSelfEva()
{
	$("#tb-self tbody tr").remove();
	var scripts = "";
	var totalScore = 0;
	for(var key in mydata)
	{
		var score = 0;
		for(var i = 0; i < mydata[key].length; i++)
		{
			score += parseFloat(mydata[key][i].score);
		}

		//验证分数
		if(selectionArray[key] && score > parseFloat(selectionArray[key][1]))
		{
			score = parseFloat(selectionArray[key][1]);
		}
		
		totalScore+=score;
		scripts = "<tr class='ui-widget-header'><th><a href='javascript:void(0)' onclick='currentLink = $(this); showDetail()' class='initA'></a>&nbsp;"+selectionArray[key][0]+"  :  "+score+" 分</th></tr>";
		scripts += '<tr id="content"><td style="padding:0"><table id="tb-self" class="ui-widget ui-widget-content" style="margin:0"><thead><tr><th style="width:170px">选项</th><th style="width:315px">成果</th><th style="width:60px">审核分</th></tr></thead><tbody>';
		
		for(var i = 0; i < mydata[key].length; i++)
		{
			scripts += '<tr><td>'+selectionArray[key][0]+'</td><td>'+
				mydata[key][i].content+'</td><td>'+mydata[key][i].score+'</td></tr>';
		}
		
		scripts += '</tbody></table></td></tr>';
		
		$("#tb-self tbody:first").append(scripts);
	}
	scripts = '<tr class="ui-widget-header"><th style="text-align: center">创新实践审核分：<span style="color:black">'+totalScore+'</span> 分</th></tr>';
	$("#tb-self tbody:first").append(scripts);
	$("#tb-self tbody #content:gt(0)").hide();
	$("#tb-self tbody .ui-widget-header:first th a").removeClass("initA");
	$("#tb-self tbody .ui-widget-header:first th a").addClass("visited");
	
	adjustHeight();
}

//点击下拉图表时，则显示隐藏的表格，学生自评查看时用到
function showDetail()
{
	if(currentLink.attr("class") == "visited")
	{
		currentLink.removeClass("visited");
		currentLink.addClass("initA");
		
		var parent = currentLink.parent().parent();
		parent.nextUntil(".ui-widget-header").hide();
		
		adjustHeight();
	}
	else if(currentLink.attr("class") == "initA")
	{
		$("#tb-self tbody #content").hide();
		$("#tb-self tbody .ui-widget-header th a").removeClass("visited");
		$("#tb-self tbody .ui-widget-header th a").addClass("initA");
		
		currentLink.removeClass("initA");
		currentLink.addClass("visited");
		
		var parent = currentLink.parent().parent();
		parent.nextUntil(".ui-widget-header").show();
		
		adjustHeight();
	}
}

function changeClass()
{
	$("#selectClass").change(function(){
		$("#stu-table tbody tr:gt(0)").remove();
		showStu();
	});
}

//保留两位小数
function formatFloat(src, pos)
{
    return Math.round(src*Math.pow(10, pos))/Math.pow(10, pos);
}

//审核一个学生的F1成绩
function checkAMem()
{
	//隐藏选择测评班级下拉框和学生列表
	$("#div-class").hide(); $("#students").hide();

	//显示学生基本素质成绩列表
	$("#div-listF3").show(); 

	loadSelfEva();
}

//将学生的自评成绩加载到表中
function loadSelfEva()
{
	parent.parent.document.getElementById("frame_content").height=420;	
	
	var date = new Date();
	var vYear = date.getFullYear();
	
	var showF3 = {};
	showF3["sid"] = currentMem.find("#stuID").html();
	showF3["year"] = vYear;
	
	$.get(
		"../../../testdrive/index.php?r=teacher/displaySelfScore",
		{"showF3": $.toJSON(showF3)},
		function(data)
		{
			var ret = $.evalJSON(data);

			if(ret.success = false)
			{
if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
					{
						alert(ret.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href='../../../';
						return false;
					}
				alert('服务器端返回错误信息=>"'+ret.message+'"');
				return false;
			}
			
			if(ret.results.length == 0 || ret.results.contents.length == 0)
			{
				alert("该学生该学年没有创新实践自评成绩~");
				$("#BtStuBack").click();
				return false;
			}

			var score = ret.results.score;
			var count = 0;
			var tempRecords = 0;
			var items = ret.results.contents;
			for(var i = 0; i < items.length; i++)
			{
				var choose = items[i].choose;
				for(var j = 0; j < items[i].contents.length; j++)
				{
					var tr = $("#tb tr").eq(2).clone();//复制一行
					tr.find("#selectType").val(choose); 
					tr.find("#TContent").html(items[i].contents[j].content);
					tr.find("#TContent").attr("name", items[i].contents[j].selfscoreid);
					
					if(items[i].contents[j].score === "")
						tr.find("#TScore").html("未自评");
					else
						tr.find("#TScore").html(parseFloat(items[i].contents[j].score));
					
					if(items[i].contents[j].t_evascore === "")
						tr.find("#t_evascore").html("未自评");
					else
						tr.find("#t_evascore").html(parseFloat(items[i].contents[j].t_evascore));
					
					if(items[i].contents[j].te_evascore === "")
						tr.find("#te_evascore").html("未审核");
					else
						tr.find("#te_evascore").val(parseFloat(items[i].contents[j].te_evascore));
					
					tr.show();
					tr.appendTo("#tb");
					tempRecords++;
				}
			}
			
			flagCanPass = ret.results.isPassed;
			
			setRecordPage1(tempRecords);

			currentPage1 = 1;
			$("#tb tbody tr:gt(10)").each(function() {
				$(this).hide();
			});

			$("#showPageSelf").html(currentPage1+" 共"+pages1+"页");
			
			adjustHeight();
			$('html,body', window.parent.parent.document).animate({
				scrollTop: $("#mark").offset().top+135}, 
				600
			);
		}
	);
}

//学生列表的翻页控制
function stuPage()
{
	$("#btLast").click(function() 
	{
		if(currentPage != pages)
		{
			//alert("即将跳转到最后一页");
			currentPage = pages;
			ShowPage(currentPage*10-9, totalRecord);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btNext").click(function() 
	{
		if(currentPage+1<=pages)
		{
			//alert("即将跳转到下一页");
			currentPage += 1;
			ShowPage(currentPage*10-9, currentPage*10);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btFormer").click(function() 
	{
		if(currentPage-1>0)
		{
			//alert("即将跳转到上一页");
			currentPage -= 1;
			ShowPage(currentPage*10-9, currentPage*10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
	$("#btFirst").click(function() 
	{
		if(currentPage != 1)
		{
			//alert("即将跳转到首页");
			currentPage = 1;
			ShowPage(1, 10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
}

//传入需要被显示的数据的位置
//仅显示_min 和 _max之间的数据
function ShowPage(_min, _max)
{
	$("#stu-table tbody tr").each(function() {
		$(this).show();
	});
	
	var variable = "#stu-table tbody tr:lt("+_min+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	variable = "#stu-table tbody tr:gt("+_max+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	$("#showPageStu").html(currentPage+"/"+pages);
	
	adjustHeight();
}

//设置页数和总记录数
function setRecordPage(number)
{
	totalRecord = number;
	if(number <= 0)
	{
		totalRecord = 0;
		pages = 1;
	}
	else
	{//每页有10条记录
		pages = parseInt(number/10);
		if(number%10 != 0)
			pages += 1;
	}
	
	//页数>1 需翻页按钮
	if(pages > 1)
	{
		$("#btLast").show(); $("#btNext").show(); $("#btFormer").show(); $("#btFirst").show(); 
	}
	else
	{
		$("#btLast").hide(); $("#btNext").hide(); $("#btFormer").hide(); $("#btFirst").hide(); 
	}
}

//显示测评班级对应的学生列表
function showStu()
{
	var getStuByClass = {};
	getStuByClass["t_classid"] = $("#selectClass").val();
	getStuByClass["classid"] = "";
	
	$.get(
		"../../../testdrive/index.php?r=teacher/getStuByClass",
		{getStuByClass: $.toJSON(getStuByClass)},
		function(data)
		{
			var ret = $.evalJSON(data);
			if(ret.success == true)//&& ret.results.length != 0
			{
				if(ret.results.length != 0)
				{
					loadSelection();

					//根据返回的结果数目计算分页
					setRecordPage(ret.results.length);
					
					for(var i = 0; i < ret.results.length; i++)
					{
						var row = $("#stu-table tr").eq(1).clone();
						row.find("td").get(1).innerHTML = ret.results[i].sid;
						row.find("td").get(2).innerHTML = ret.results[i].sname;
						row.find("td").get(3).innerHTML = ret.results[i].cname;
						row.show();
						row.appendTo("#stu-table");
					}
					
					currentPage = 1;
					$("#stu-table tbody tr:gt(10)").each(function() {
						$(this).hide();
					});
					
					$("#showPageStu").html(currentPage+"/"+pages);
					
					adjustHeight();
				}
				else
				{
					setRecordPage(1);
					$("#showPageStu").html("0/1");
					alert("该测评班级学生列表为空~");
				}
			}
			else
			{
				//alert('服务器端返回错误信息=>"'+ret.message+'"');
if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
					{
						alert(ret.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href='../../../';
						return false;
					}
				alert(ret.message);
			}
		}
	);
}

function addSelectClass()
{
	var TeClass = {};
	TeClass["teacherid"] = "";
	var date = new Date();
	TeClass["year"] = date.getFullYear();
	$.get(
		"../../../testdrive/index.php?r=zing/getTeClass",
		{TeClass: $.toJSON(TeClass)},
		function(data)
		{
			var obj = $.evalJSON(data);
			if(obj.success == true)
			{
				if(obj.results.length != 0)
				{
					var selector=$('#selectClass');  
					for (var i = 0; i < obj.results.length; i++)
					{
						selector.append('<option value="'+obj.results[i].t_classid+'">'+obj.results[i].name+'</option>');
					}
				
					//显示测评班级对应的学生列表
					showStu();
					return true;
				}
				else
				{
					alert("本学年度，您暂无测评任务！");
				}
			}
			else
			{
				//alert('服务器端返回错误信息=>"'+obj.message+'"');
if(obj.results.hasOwnProperty("timeout") && obj.results.timeout)
					{
						alert(obj.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href='../../../';
						return false;
					}
				alert(obj.message);
			}
			$("#students").hide(); $("#div-class").hide();
			$("#error").show();
			return false;
		}
	);
}

//加载得分选项
function loadSelection()
{	
	$.ajax({
		url: '../../../testdrive/index.php?r=zing/getEvaCon',
		async: false, 
		success: function (data)
		{
			var ret = $.evalJSON(data);
			
			if(ret.success == true)
			{
				if(ret.results.length != 0)
				{
					var selector=$("#selectType");  
					for(var i = 0; i < ret.results.length; i++)
					{
						selectionArray[ret.results[i].evaluateid] = Array(ret.results[i].evaluatename, ret.results[i].maxscore);
						selector.append('<option value="'+ret.results[i].evaluateid+'">'+ret.results[i].evaluatename+'</option>');  
					}
				}
				else
				{
					alert('服务器端返回测评选择项失败~');
				}
			}
			else
			{
				//alert('服务器端返回错误信息=>"'+ret.message+'"');
if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
					{
						alert(ret.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href='../../../';
						return false;
					}
				alert(ret.message);
			}
		}
	});
}

//自评列表的分页控制
function selfPage()
{
	$("#btLast1").click(function() 
	{
		if(currentPage1 != pages1)
		{
			//alert("即将跳转到最后一页");
			currentPage1 = pages1;
			ShowPage1(currentPage1*10-9, totalRecord1);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btNext1").click(function() 
	{
		if(currentPage1+1<=pages1)
		{
			//alert("即将跳转到下一页");
			currentPage1 += 1;
			ShowPage1(currentPage1*10-9, currentPage1*10);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btFormer1").click(function() 
	{
		if(currentPage1-1>0)
		{
			//alert("即将跳转到上一页");
			currentPage1 -= 1;
			ShowPage1(currentPage1*10-9, currentPage1*10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
	$("#btFirst1").click(function() 
	{
		if(currentPage1 != 1)
		{
			//alert("即将跳转到首页");
			currentPage1 = 1;
			ShowPage1(1, 10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
}

//传入需要被显示的数据的位置
//仅显示_min 和 _max之间的数据
function ShowPage1(_min, _max)
{
	$("#tb tbody tr").each(function() {
		$(this).show();
	});
	
	var variable = "#tb tbody tr:lt("+_min+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	variable = "#tb tbody tr:gt("+_max+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	$("#showPageSelf").html(currentPage1+" 共"+pages1+"页");
	
	adjustHeight();
}

//设置页数和总记录数
function setRecordPage1(number)
{
	totalRecord1 = number;
	if(number <= 0)
	{
		totalRecord1 = 0;
		pages1 = 1;
	}
	else
	{//每页有10条记录
		pages1 = parseInt(number/10);
		if(number%10 != 0)
			pages1 += 1;
	}
	//页数>1 需翻页按钮
	//alert(pages1);
	if(pages1 > 1)
	{
		$("#btLast1").show(); $("#btNext1").show(); $("#btFormer1").show(); $("#btFirst1").show(); 
	}
	else
	{
		$("#btLast1").hide(); $("#btNext1").hide(); $("#btFormer1").hide(); $("#btFirst1").hide(); 
	}
}

//检查已添加条目的正确性
function updateTips( t ) {
	$( ".validateTips")
		.text( t )
		.addClass( "ui-state-highlight" );
	setTimeout(function() {
		$( ".validateTips").removeClass( "ui-state-highlight", 1500 );
		$( ".validateTips" ).text("");
	}, 1500 );
}

function checkLength( o, n, min, max ) {
	if ( o.val().length > max || o.val().length < min ) {
		o.addClass( "ui-state-error" );
		updateTips( '提示: " '+n+' "' + " 的长度应该在 " +
			min + " 和 " + max + "之间." );
		return false;
	} else {
		return true;
	}
}

function checkValue( o, n, min, max ) {
	if ( parseFloat(o.val()) > max || parseFloat(o.val()) < min) {
		o.addClass( "ui-state-error" );
		updateTips( '" '+n+' "' + " 的值 " +
			min + " 和 " + max + "之间." );
		return false;
	} else {
		return true;
	}
}

function checkRegexp( o, regexp, n ) {
	if ( !( regexp.test( o.val() ) ) ) {
		o.addClass( "ui-state-error" );
		updateTips( n );
		return false;
	} else {
		return true;
	}
}

function validator()
{
	var success = true;
	$("#tb tbody tr:gt(0)").each(function() 
	{
		var TScore = $(this).find("#te_evascore"),
		allFields = $( [] ).add( TScore );

		var bValid = true;
		allFields.removeClass( "ui-state-error" );
		bValid = bValid && checkLength( TScore, "辅导员审核分", 1, 8);
		bValid = bValid && checkRegexp( TScore, /^\d+(.\d+$|\d*$)/, "\"辅导员审核分\"应该输入合法数字" );
		bValid = bValid && checkValue( TScore, "辅导员审核分", 0, 20);
		
		if ( ! bValid ) {
			success = false;
			return false;
		}
	});
	
	return success;
}

function adjustHeight()
{
	var height = document.body.clientHeight;
	//alert(height);
	if(height + 30 > 420)
		parent.parent.document.getElementById("frame_content").height=height + 50;
	else
		parent.parent.document.getElementById("frame_content").height=420;	
}
