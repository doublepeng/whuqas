// JavaScript Document
//首先加载测评班级的下拉框
//addSelectClass();

//待审核学生列表
var totalRecord = 0;
var pages = 0;
var currentPage = 1;
//已添加学生列表分页
var totalRecord1 = 0;
var pages1 = 0;
var currentPage1 = 1;

var currentMem;   //当前审核的学生的html行

//用来标记一个学生的F1成绩是否能审核通过，
//只要有一个没有被测评小组审核或者学生没有自评就不能审核通过
var flagCanPass = 2;   //只有为1的时候，才能审核通过，为0表示有人没有测评，为2表示已审核通过

//用来记录是否有奖励加分和惩处减分
var markAdd = 0, markSub = 0;

$(document).ready(function() {
	//调整屏幕的高度
	parent.parent.document.getElementById("frame_content").height=420;	
	//首先要隐藏错误输出提示、基本素质成绩提示和页面
	$("#error").hide(); $("#div-basic").hide(); $("#div-info").hide();
	//隐藏模板tr
	$("#stu-table tbody tr").eq(0).hide();
	
	//隐藏翻页按钮
	$("#btLast").hide(); $("#btNext").hide(); $("#btFormer").hide(); $("#btFirst").hide(); 
	
	addSelectClass();
	//当下拉框中得测评班级改变时
	changeClass();

	//学生列表的翻页控制
	stuPage();

	//返回学生列表按钮
	btBackToStu();

	//审核通过一个学生的成绩
	btPass();  //这里要将老师审核的成绩一个一个传给后台

	//批量审核，直接传学号给后台，后台处理就行
	btMutiCheck();

  //btCalClassBasic计算该测评班级的基本素质成绩
  btCalClassBasic();

	//选择所有行
	$("#CKA").click(function() {
		
		$("#stu-table tbody tr:gt(0)").each(function() {
			if(!$(this).is(":hidden")) 	
				$(this).find("#CK").get(0).checked = $("#CKA").get(0).checked;
		});
	});

})

//计算该测评班级的基本素质成绩
function btCalClassBasic()
{
  $("#btCalClassBasic").click(function() {
    if(confirm("请您确认该测评班级的基本素质成绩全部审核通过。确认？"))  
    {
      var calClassBasic = {};
      calClassBasic["t_classid"] = $("#selectClass").val();
      var date = new Date();
      calClassBasic["year"] = date.getFullYear();

      $("#layer").height(parent.parent.document.getElementById("frame_content").height);
			$("#layer").mask("正在计算，请稍等...");
      $.get(
        "../../../testdrive/index.php?r=teacher/calClassBasic",
        {calClassBasic: $.toJSON(calClassBasic)},
        function(data)
        {
          var ret = $.evalJSON(data);
					if(ret.success == true)
					{
						alert("计算测评班级基本素质测评成绩成功！");
            //return true;
					}
					else
					{
						//alert('服务器端返回错误信息"'+ret.message+'"');
if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
					{
						alert(ret.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href='../../../';
						return false;
					}
						alert(ret.message);
						//return false;
					}
          $("#layer").unmask();
        }   
      );
    }
  });
}

function changeClass()
{
	$("#selectClass").change(function(){
		$("#stu-table tbody tr:gt(0)").remove();
    flagCanPass = 2;
		showStu();
	});
}

//审核通过一个学生的成绩
function btPass()
{
	$("#btPass").click(function(){
    //alert(flagCanPass);
		if(flagCanPass == 0) //有学生的成绩还没测评
		{
			alert("提示：由于该生的基本素质成绩没有全部通过测评小组评审，您无需审核~");
			return false;
		}
		if(flagCanPass == 2) //已通过审核
		{
			alert("提示：由于该生的基本素质成绩已通过审核，您无需审核~");
			return false;
		}
	//alert(flagCanPass);	
		//开始向后台传这个学生的成绩
		if(validator())
		{
			var checkF1 = {};
			checkF1["sid"] = currentMem.find("#stuID").html();
			checkF1["F1"] = []; checkF1["add"] = {}; checkF1["sub"] = {};
			$("#tb tbody tr:gt(0)").slice(0,-3).each(function(){
				checkF1["F1"].push({
					"sid": $(this).find("td").eq(0).attr("title"),
					"A1": $(this).find("#t_A1").val(),
					"A2": $(this).find("#t_A2").val(),
					"A3": $(this).find("#t_A3").val(),
					"A4": $(this).find("#t_A4").val(),
					"A5": $(this).find("#t_A5").val()
				});
			});

			if(markAdd == 1 && $("#tb #td_add").length != 0)
			{
				checkF1["add"]["score"] = $("#in_add").val();
			}
			if(markSub == 1 && $("#tb #td_sub").length != 0)
			{
				checkF1["sub"]["score"] = $("#in_del").val();
			}
			
			//alert($.toJSON(checkF1));
			if(confirm('您将确认审核通过？'))
			{
        $("#layer").height(parent.parent.document.getElementById("frame_content").height-10);
			  $("#layer").mask("正在提交审核，请稍等...");
				$.post(
					"../../../testdrive/index.php?r=teacher/CheckF1",
					{checkF1: $.toJSON(checkF1)},
					function(data)
					{
						var ret = $.evalJSON(data);
						if(ret.success == true)
						{
							alert("提交成功！");
							$("#btBackToStu").click();
							currentMem.find("#link").html("已审核");
							currentMem.find("#link").removeAttr("onclick");
							currentMem.find("#link").removeAttr("href");
						}
						else
						{
							//alert('服务器端返回错误信息"'+ret.message+'"');
if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
					{
						alert(ret.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href='../../../';
						return false;
					}
							alert(ret.message);
							//return false;
						}
            					$("#layer").unmask();
						//adjustHeight();
					}
				);
			}
		}
	});
}

//批量审核，直接传学号给后台，后台处理就行
function btMutiCheck()
{
	$("#btMutiCheck").click(function (){
		var passMultiF1 = [];
		var alreadyPassedMem = [];
		$("#stu-table tr:gt(1)").each(function() {
			if ($(this).find("#CK").get(0).checked == true && $(this).find("#link").html()!="已审核") {
				passMultiF1.push($(this).find("#stuID").html());
				alreadyPassedMem.push($(this).find("#link"));
			}
		});

		if(passMultiF1.length == 0)
		{
			alert("请您选择要审核的学生，或者您选择的学生已通过审核~");
			return false;
		}
		
		//alert($.toJSON(passMultiF1));
		if(confirm('您将确认批量审核通过？'))
		{
      			$("#layer").height(parent.parent.document.getElementById("frame_content").height-10);
			$("#layer").mask("正在提交审核，请稍等...");

			$.get(
				"../../../testdrive/index.php?r=teacher/passMultiF1",
				{passMultiF1: $.toJSON(passMultiF1)},
				function(data)
				{
					var ret = $.evalJSON(data);
					
					if(ret.success == true)
					{
						//TO DO
						alert("提交成功~");
						$("#btBackToStu").click();
						
						for(var i = 0; i < alreadyPassedMem.length; i++)
						{
							alreadyPassedMem[i].html("已审核");
							alreadyPassedMem[i].removeAttr("onclick");
							alreadyPassedMem[i].removeAttr("href");
						}
					}
					else
					{
						//alert('服务器端返回错误信息=>"' +ret.message+'"');
if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
					{
						alert(ret.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href='../../../';
						return false;
					}
						alert(ret.message);
					}
          				$("#layer").unmask();
				}
			);

		}
		
		$("#CKA").attr("checked", false);
		$("#CKA").click();
		$("#CKA").attr("checked", false);
	});
}

function btBackToStu()
{
	$("#btBackToStu").click(function() {
		$("#tb tbody tr:gt(0)").remove();
		//隐藏选择测评班级下拉框和学生列表
		$("#div-class").show(); $("#students").show(); $("#div-info1").show();
		//显示学生基本素质成绩列表
		$("#div-basic").hide(); $("#div-info").hide();

		adjustHeight();
	});
}

//保留两位小数
function formatFloat(src, pos)
{
    return Math.round(src*Math.pow(10, pos))/Math.pow(10, pos);
}

//绘制当前学生的F1成绩表
function drawF1Table()
{
	parent.parent.document.getElementById("frame_content").height=420;	
	
	$("#tb tbody tr").eq(0).hide();
	var searchBasic = {};
	searchBasic["sid"] = currentMem.find("#stuID").html();
	
	var date = new Date();
	searchBasic["year"] = date.getFullYear();
	//alert(searchBasic);
	
	$.get(
		"../../../testdrive/index.php?r=zing/getBasicScore",
		{"searchBasic": $.toJSON(searchBasic)},
		function (data)
		{
			var ret = $.evalJSON(data);
			
			if(ret.success == true)
			{
				if(ret.results.length != 0 && ret.results.contents.length != 0)
				{
					$("#tb tr:gt(2)").remove();
					
					var contents = ret.results.contents;
					for(var i = 0; i < contents.length; i++)
					{
						var tr = $("#tb tr").eq(2).clone();//复制一行
						tr.show();
						tr.appendTo("#tb");
						tr.find("td").get(0).innerHTML = contents[i].sname;
						tr.find("td").eq(0).attr("title", contents[i].sid);
						tr.find("td").get(1).innerHTML = parseFloat(contents[i].A1);
						tr.find("#t_A1").val(parseFloat(contents[i].t_A1));
						tr.find("td").get(3).innerHTML = parseFloat(contents[i].A2);
						tr.find("#t_A2").val(parseFloat(contents[i].t_A2));
						tr.find("td").get(5).innerHTML = parseFloat(contents[i].A3);
						tr.find("#t_A3").val(parseFloat(contents[i].t_A3));
						tr.find("td").get(7).innerHTML = parseFloat(contents[i].A4);
						tr.find("#t_A4").val(parseFloat(contents[i].t_A4));
						tr.find("td").get(9).innerHTML = parseFloat(contents[i].A5);
						tr.find("#t_A5").val(parseFloat(contents[i].t_A5));
						
						if(parseInt(contents[i].check) == 0)   //测评小组还没评
						{
							flagCanPass = 0;

							tr.find("td").get(1).innerHTML = '<h1 style="font-size:12px; margin:0;font-weight:normal">未测评</h1>';
							tr.find("td").get(3).innerHTML = '<h1 style="font-size:12px; margin:0;font-weight:normal">未测评</h1>';
							tr.find("td").get(5).innerHTML = '<h1 style="font-size:12px; margin:0;font-weight:normal">未测评</h1>';
							tr.find("td").get(7).innerHTML = '<h1 style="font-size:12px; margin:0;font-weight:normal">未测评</h1>';
							tr.find("td").get(9).innerHTML = '<h1 style="font-size:12px; margin:0;font-weight:normal">未测评</h1>';

							tr.find("td").get(2).innerHTML = '<h1 style="font-size:12px; margin:0;font-weight:normal">暂无</h1>';
							tr.find("td").get(4).innerHTML = '<h1 style="font-size:12px; margin:0;font-weight:normal">暂无</h1>';
							tr.find("td").get(6).innerHTML = '<h1 style="font-size:12px; margin:0;font-weight:normal">暂无</h1>';
							tr.find("td").get(8).innerHTML = '<h1 style="font-size:12px; margin:0;font-weight:normal">暂无</h1>';
							tr.find("td").get(10).innerHTML = '<h1 style="font-size:12px; margin:0;font-weight:normal">暂无</h1>';
						}
						else if(parseInt(contents[i].check) == 1)   //测评小组评了
						{
						  tr.find("#t_A1").val(parseFloat(contents[i].A1));
						  tr.find("#t_A2").val(parseFloat(contents[i].A2));
						  tr.find("#t_A3").val(parseFloat(contents[i].A3));
						  tr.find("#t_A4").val(parseFloat(contents[i].A4));
						  tr.find("#t_A5").val(parseFloat(contents[i].A5));
							if(flagCanPass == 2)
								flagCanPass = 1;
						}
						else if(parseInt(contents[i].check) == 2)   //辅导员审核通过了
						{
							tr.find("td").get(2).innerHTML = parseFloat(contents[i].t_A1);
							tr.find("td").get(4).innerHTML = parseFloat(contents[i].t_A2);
							tr.find("td").get(6).innerHTML = parseFloat(contents[i].t_A3);
							tr.find("td").get(8).innerHTML = parseFloat(contents[i].t_A4);
							tr.find("td").get(10).innerHTML = parseFloat(contents[i].t_A5);
						}
					}
					adjustHeight();
				}
				else
				{
					$("#tb tr:gt(2)").remove();

					alert("该生没有该学年的基本素质测评成绩~");
					
					$("#btBackToStu").click();

					return false;
				}
				
				$("#tb tbody").append('<tr class="ui-widget-header"><td colspan="2">评项</td><td colspan="3">测评内容</td><td colspan="2">自评分</td><td colspan="2">测评小组审核分</td><td colspan="2">辅导员审核分</td></tr>');
				$("#tb tbody").append('<tr id="td_add"><td colspan="2">奖励加分</td><td colspan="3">无</td><td colspan="2">无</td><td colspan="2">无</td><td colspan="2"><input type="text" id="in_add" style="width:90px" value="无" /></td></tr>');
				$("#tb tbody").append('<tr id="td_sub"><td colspan="2">惩处扣分</td><td colspan="3">无</td><td colspan="2">无</td><td colspan="2">无</td><td colspan="2"><input type="text" id="in_del" style="width:90px" value="无" /></td></tr>');
				if(ret.results["add"].length != 0)
				{
          markAdd = 1;
					//$("#td_add").find("td").get(0).innerHTML = ret.results["add"]["choose"];
					$("#td_add").find("td").get(1).innerHTML = ret.results["add"]["content"];
					$("#td_add").find("td").get(2).innerHTML = parseFloat(ret.results["add"]["score"]);
					
					if(parseInt(ret.results["add"]["check"]) == 0)   //自评了
					{
						flagCanPass = 0;
						$("#td_add").find("td").get(3).innerHTML = "未审核";
						$("#td_add").find("td").get(4).innerHTML = "暂无";
					}
					else if(parseInt(ret.results["add"]["check"]) == 1)   //测评小组过了
					{
						if(flagCanPass == 2)
							flagCanPass = 1;

						$("#td_add").find("td").get(3).innerHTML = parseFloat(ret.results["add"]["t_evascore"]);
						$("#td_add").find("#in_add").val(parseFloat(ret.results["add"]["te_evascore"]));
					}
					else if(parseInt(ret.results["add"]["check"]) == 2)   //辅导员审核通过
					{
						$("#td_add").find("td").get(3).innerHTML = parseFloat(ret.results["add"]["t_evascore"]);
						$("#td_add").find("td").get(4).innerHTML = parseFloat(ret.results["add"]["te_evascore"]);
					}
				}

				if(ret.results["sub"].length != 0)
				{
          markSub = 1;
					//$("#td_sub").find("td").get(0).innerHTML = ret.results["sub"]["choose"];
					$("#td_sub").find("td").get(1).innerHTML = ret.results["sub"]["content"];
					$("#td_sub").find("td").get(2).innerHTML = parseFloat(ret.results["sub"]["score"]);

					if(parseInt(ret.results["sub"]["check"]) == 0)   //自评了
					{
						flagCanPass = 0;
						$("#td_sub").find("td").get(3).innerHTML = "未审核";
						$("#td_sub").find("td").get(4).innerHTML = "暂无";
					}
					else if(parseInt(ret.results["sub"]["check"]) == 1)   //测评小组过了
					{
						if(flagCanPass == 2)
							flagCanPass = 1;
						$("#td_sub").find("td").get(3).innerHTML = parseFloat(ret.results.sub["t_evascore"]);
						$("#td_sub").find("#in_del").val(parseFloat(ret.results.sub["te_evascore"]));
					}
					else if(parseInt(ret.results["sub"]["check"]) == 2)   //辅导员审核通过
					{
						$("#td_sub").find("td").get(3).innerHTML = parseFloat(ret.results.sub["t_evascore"]);
						$("#td_sub").find("td").get(4).innerHTML = parseFloat(ret.results.sub["te_evascore"]);
					}
				}
				
				adjustHeight();

				$('html,body', window.parent.parent.document).animate({
					scrollTop: $("#tb").offset().top+135}, 
					400
				);
			}
			else
			{
				//alert("服务器端返回错误信息=>\""+ret.message+'"');
if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
					{
						alert(ret.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href='../../../';
						return false;
					}
				alert(ret.message);
			}
		}
	);
}

//审核一个学生的F1成绩
function checkAMem()
{
	//隐藏选择测评班级下拉框和学生列表
	$("#div-class").hide(); $("#students").hide(); $("#div-info1").hide();

	//显示学生基本素质成绩列表
	$("#div-basic").show(); $("#div-info").show();

	//初始化flag
	flagCanPass = 2;
	markAdd = 0; markSub = 0;
	drawF1Table();
}

//学生列表的翻页控制
function stuPage()
{
	$("#btLast").click(function() 
	{
		if(currentPage != pages)
		{
			//alert("即将跳转到最后一页");
			currentPage = pages;
			ShowPage(currentPage*10-9, totalRecord);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btNext").click(function() 
	{
		if(currentPage+1<=pages)
		{
			//alert("即将跳转到下一页");
			currentPage += 1;
			ShowPage(currentPage*10-9, currentPage*10);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btFormer").click(function() 
	{
		if(currentPage-1>0)
		{
			//alert("即将跳转到上一页");
			currentPage -= 1;
			ShowPage(currentPage*10-9, currentPage*10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
	$("#btFirst").click(function() 
	{
		if(currentPage != 1)
		{
			//alert("即将跳转到首页");
			currentPage = 1;
			ShowPage(1, 10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
}

//传入需要被显示的数据的位置
//仅显示_min 和 _max之间的数据
function ShowPage(_min, _max)
{
	$("#stu-table tbody tr").each(function() {
		$(this).show();
	});
	
	var variable = "#stu-table tbody tr:lt("+_min+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	variable = "#stu-table tbody tr:gt("+_max+")";
	$(variable).each(function() {

		$(this).hide();
	});
	
	$("#showPageStu").html(currentPage+"/"+pages);
	
	adjustHeight();
}

//设置页数和总记录数
function setRecordPage(number)
{
	totalRecord = number;
	if(number <= 0)
	{
		totalRecord = 0;
		pages = 1;
	}
	else
	{//每页有10条记录
		pages = parseInt(number/10);
		if(number%10 != 0)
			pages += 1;
	}
	
	//页数>1 需翻页按钮
	if(pages > 1)
	{
		$("#btLast").show(); $("#btNext").show(); $("#btFormer").show(); $("#btFirst").show(); 
	}
	else
	{
		$("#btLast").hide(); $("#btNext").hide(); $("#btFormer").hide(); $("#btFirst").hide(); 
	}
}

//显示测评班级对应的学生列表
function showStu()
{
	var getStuByClass = {};
	getStuByClass["t_classid"] = $("#selectClass").val();
	getStuByClass["classid"] = "";
	
	$.get(
		"../../../testdrive/index.php?r=teacher/getStuByClass",
		{getStuByClass: $.toJSON(getStuByClass)},
		function(data)
		{
			var ret = $.evalJSON(data);
			if(ret.success == true)// && ret.results.length != 0
			{
				if(ret.results.length != 0)
				{
					//根据返回的结果数目计算分页
					setRecordPage(ret.results.length);
					
					for(var i = 0; i < ret.results.length; i++)
					{
						var row = $("#stu-table tr").eq(1).clone();
						row.find("td").get(1).innerHTML = ret.results[i].sid;
						row.find("td").get(2).innerHTML = ret.results[i].sname;
						row.find("td").get(3).innerHTML = ret.results[i].cname;
						row.show();
						row.appendTo("#stu-table");
					}
					
					currentPage = 1;
					$("#stu-table tbody tr:gt(10)").each(function() {
						$(this).hide();
					});
					
					$("#showPageStu").html(currentPage+"/"+pages);
					
					adjustHeight();
          
          $('html,body', window.parent.parent.document).animate({
					  scrollTop: $("#stu-table").offset().top+135}, 
					  400
				  );
				}
				else
				{
					setRecordPage(0);
					ShowPage(1,10);
					alert("该测评班级学生列表为空~");
				}
			}
			else
			{
				//alert('服务器端返回错误信息=>"'+ret.message+'"');
if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
					{
						alert(ret.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href='../../../';
						return false;
					}
				alert(ret.message);
			}
		}
	);
}

function addSelectClass()
{
	var TeClass = {};
	TeClass["teacherid"] = "";
	var date = new Date();
	TeClass["year"] = date.getFullYear();
	$.get(
		"../../../testdrive/index.php?r=zing/getTeClass",
		{TeClass: $.toJSON(TeClass)},
		function(data)
		{
			var obj = $.evalJSON(data);
			if(obj.success == true)
			{
				if(obj.results.length != 0)
				{
					var selector=$('#selectClass');  
					for (var i = 0; i < obj.results.length; i++)
					{
						selector.append('<option value="'+obj.results[i].t_classid+'">'+obj.results[i].name+'</option>');
					}
				
					//显示测评班级对应的学生列表
					showStu();
					return true;
				}
				else
				{
					alert("本学年度，您暂无测评任务！");
				}
			}
			else
			{
				//alert('服务器端返回错误信息=>"'+obj.message+'"');
if(obj.results.hasOwnProperty("timeout") && obj.results.timeout)
					{
						alert(obj.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href='../../../';
						return false;
					}
				alert(obj.message);
			}
			$("#students").hide(); $("#div-class").hide(); $("#div-info1").hide();
			$("#error").show();
			return false;
		}
	);
}

function adjustHeight()
{
	var height = document.documentElement.scrollHeight<document.body.scrollHeight
						? document.body.scrollHeight:document.documentElement.scrollHeight; 
	//height = document.body.clientHeight > height ? document.body.clientHeight : height;
	//alert(document.documentElement.scrollHeight); alert(document.body.scrollHeight); alert(document.body.clientHeight);
	//alert(height);
	if(height > 420)
		parent.parent.document.getElementById("frame_content").height=height;
	else
		parent.parent.document.getElementById("frame_content").height=420;	
}

//2012-02-20 modified by minzhenyu
//添加校验功能
//检查已添加条目的正确性
function updateTips( t ) {
	$( ".validateTips")
		.text( t )
		.addClass( "ui-state-highlight" );
	setTimeout(function() {
		$( ".validateTips").removeClass( "ui-state-highlight", 1500 );
		$( ".validateTips" ).text("");
	}, 1500 );
}

function checkLength( o, n, min, max ) {
	if ( o.val().length > max || o.val().length < min ) {
		o.addClass( "ui-state-error" );
		updateTips( '提示: " '+n+' "' + " 的长度应该在 " +
			min + " 和 " + max + "之间." );
		return false;
	} else {
		return true;
	}
}

function checkValue( o, n, min, max ) {
	if ( parseFloat(o.val()) > max || parseFloat(o.val()) < min) {
		o.addClass( "ui-state-error" );
		updateTips( '" '+n+' "' + " 的值 " +
			min + " 和 " + max + "之间." );
		return false;
	} else {
		return true;
	}
}

function checkRegexp( o, regexp, n ) {
	if ( !( regexp.test( o.val() ) ) ) {
		o.addClass( "ui-state-error" );
		updateTips( n );
		return false;
	} else {
		return true;
	}
}

function validator()
{
	if($("#tb tbody tr:gt(0)").slice(0,-3).length <= 0)
	{
		updateTips( "该生没有基本素质测评成绩，可能是您没有为测评小组分配F1任务。" );
                return false;
	}

	var success = true;
	$("#tb tbody tr:gt(0)").slice(0,-3).each(function(){
		var A1 = $(this).find("#t_A1"),
		A2 = $(this).find("#t_A2"),
		A3 = $(this).find("#t_A3"),
		A4 = $(this).find("#t_A4"),
		A5 = $(this).find("#t_A5"),
		allFields = $( [] ).add( A1 ).add( A2 ).add( A3 ).add( A4 ).add( A5 );
	
		var bValid = true;
		allFields.removeClass( "ui-state-error" );
		bValid = bValid && checkLength( A1, "思想政治表现", 1, 10);
		bValid = bValid && checkRegexp( A1, /^\d+(.\d+$|\d*$)/, "\"思想政治表现\"应该输入合法数字" );
		bValid = bValid && checkValue( A1, "思想政治表现", 0, 20);
		
		bValid = bValid && checkLength( A2, "个人品德修养", 1, 10);
		bValid = bValid && checkRegexp( A2, /^\d+(.\d+$|\d*$)/, "\"个人品德修养\"应该输入合法数字" );
		bValid = bValid && checkValue( A2, "个人品德修养", 0, 20);
		
		
		bValid = bValid && checkLength( A3, "学习态度状况", 1, 10);
		bValid = bValid && checkRegexp( A3, /^\d+(.\d+$|\d*$)/, "\"学习态度状况\"应该输入合法数字" );
		bValid = bValid && checkValue( A3, "学习态度状况", 0, 20);
		
		bValid = bValid && checkLength( A4, "组织纪律观念", 1, 10);
		bValid = bValid && checkRegexp( A4, /^\d+(.\d+$|\d*$)/, "\"组织纪律观念\"应该输入合法数字" );
		bValid = bValid && checkValue( A4, "组织纪律观念", 0, 20);
		
		bValid = bValid && checkLength( A5, "身心健康素质", 1, 10);
		bValid = bValid && checkRegexp( A5, /^\d+(.\d+$|\d*$)/, "\"身心健康素质\"应该输入合法数字" );
		bValid = bValid && checkValue( A5, "身心健康素质", 0, 20);
		
		if ( ! bValid ) {
			success = false;
			return false;
		}
	});

	
	if(markAdd == 1) //表示有加分成绩
	{
		var selfAdd = $("#in_add"),
		allFields = $( [] ).add( selfAdd );
	
		var bValid = true;
		allFields.removeClass( "ui-state-error" );
		bValid = bValid && checkRegexp( selfAdd, /^\d+(.\d+$|\d*$)/, "\"奖励加分\"应该输入合法数字" );
		bValid = bValid && checkValue( selfAdd, "奖励加分", 0, 20);
		
		if ( ! bValid ) {
			success = false;
			return false;
		}
	}
	
	if(markSub == 1)
        {
                var selfSub = $("#in_del"),
                allFields = $( [] ).add( selfSub );

                var bValid = true;
                allFields.removeClass( "ui-state-error" );
                bValid = bValid && checkRegexp( selfSub, /^\d+(.\d+$|\d*$)/, "\"惩处减分\"应该输入合法数字" );
                bValid = bValid && checkValue( selfSub, "惩处减分", 0, 20);

                if ( ! bValid ) {
                        success = false;
                        return false;
                }
        }

	return success;
}
