// JavaScript Document
//添加测评班级列表
addSelectClass();

//待选择学生列表分页
var totalRecord = 0;
var pages = 0;
var currentPage = 1;
//已添加学生列表分页
var totalRecord1 = 0;
var pages1 = 0;
var currentPage1 = 1;

$(function() {
	parent.parent.document.getElementById("frame_content").height=420;	
	$("#error").hide();
	//隐藏翻页按钮
	$("#btLast").hide(); $("#btNext").hide(); $("#btFormer").hide(); $("#btFirst").hide(); 
	//隐藏翻页按钮
	$("#btLast1").hide(); $("#btNext1").hide(); $("#btFormer1").hide(); $("#btFirst1").hide(); 
	//隐藏模板tr
	$("#tbToChoose tr").eq(1).hide();
	//隐藏模板tr
	 $("#tbChoosed #hide-th").hide();  $("#tbChoosed #hide-td").hide(); $("#tbChoosed tr").eq(1).hide();
	
	//班级列表下拉菜单变化时
	changeClass();
	
	//初始化右边的已添加表格
	initChoosedTable();
	
	//add a member
	btAdd();
	
	//delete a member
	btDel();

	btConfirm();
	
	//选择所有行
	$("#CKA").click(function() {
		$("#tbToChoose tbody tr").each(function() {
			if(!$(this).is(":hidden")) 	
				$(this).find("#CK").get(0).checked = $("#CKA").get(0).checked;
		});
	});
	
	//选择所有行
	$("#CKA1").click(function() {
		$("#tbChoosed tbody tr").each(function() {
			if(!$(this).is(":hidden")) 	
				$(this).find("#CK1").get(0).checked = $("#CKA1").get(0).checked;
		});
	});
	
	toChooseTable();
	choosedTable();
});

//初始化已添加表格
function initChoosedTable()
{
	for(var i = 0; i < 10; i++)
	{
		var row = $("#tbChoosed tr").eq(1).clone();
		row.show();
		row.appendTo("#tbChoosed");
	}
	totalRecord1 += 10;
}

//已添加的分页按钮控制
function choosedTable()
{
	$("#btLast1").click(function() 
	{
		if(currentPage1 != pages1)
		{
			//alert("即将跳转到最后一页");
			currentPage1 = pages1;
			ShowPage1(currentPage1*10-9, totalRecord1);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btNext1").click(function() 
	{
		if(currentPage1+1<=pages1)
		{
			//alert("即将跳转到下一页");
			currentPage1 += 1;
			ShowPage1(currentPage1*10-9, currentPage1*10);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btFormer1").click(function() 
	{
		if(currentPage1-1>0)
		{
			//alert("即将跳转到上一页");
			currentPage1 -= 1;
			ShowPage1(currentPage1*10-9, currentPage1*10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
	$("#btFirst1").click(function() 
	{
		if(currentPage1 != 1)
		{
			//alert("即将跳转到首页");
			currentPage1 = 1;
			ShowPage1(1, 10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
}

//重新分页
function rePage(_totalRecord, _currentPage)
{
	var tempPage = _currentPage;
	setRecordPage1(_totalRecord);
	
	if (tempPage < pages1)  
	{
		currentPage1 = tempPage;
		ShowPage1(tempPage*10-9, tempPage*10);
	}
	else
	{
		currentPage1 = pages1;
		ShowPage1(pages1*10-9, _totalRecord);
	}
}
//传入需要被显示的数据的位置
//仅显示_min 和 _max之间的数据
function ShowPage1(_min, _max)
{
	$("#tbChoosed tbody tr").each(function() {
		$(this).show();
	});
	
	var variable = "#tbChoosed tbody tr:lt("+_min+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	variable = "#tbChoosed tbody tr:gt("+_max+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	$("#showPage1").html(currentPage1+"/"+pages1+" 页");
	adjustHeight();
}

//设置页数和总记录数
function setRecordPage1(number)
{
	totalRecord1 = number;
	if(number <= 0)
	{
		totalRecord1 = 0;
		pages1 = 1;
	}
	else
	{//每页有10条记录
		pages1 = parseInt(number/10);
		if(number%10 != 0)
			pages1 += 1;
	}
	//页数>1 需翻页按钮
	//alert(pages1);
	if(pages1 > 1)
	{
		$("#btLast1").show(); $("#btNext1").show(); $("#btFormer1").show(); $("#btFirst1").show(); 
	}
	else
	{
		$("#btLast1").hide(); $("#btNext1").hide(); $("#btFormer1").hide(); $("#btFirst1").hide(); 
	}
}

//左边的翻页
function toChooseTable()
{
	$("#btLast").click(function() 
	{
		if(currentPage != pages)
		{
			//alert("即将跳转到最后一页");
			currentPage = pages;
			ShowPage(currentPage*10-9, totalRecord);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btNext").click(function() 
	{
		if(currentPage+1<=pages)
		{
			//alert("即将跳转到下一页");
			currentPage += 1;
			ShowPage(currentPage*10-9, currentPage*10);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btFormer").click(function() 
	{
		if(currentPage-1>0)
		{
			//alert("即将跳转到上一页");
			currentPage -= 1;
			ShowPage(currentPage*10-9, currentPage*10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
	$("#btFirst").click(function() 
	{
		if(currentPage != 1)
		{
			//alert("即将跳转到首页");
			currentPage = 1;
			ShowPage(1, 10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
}

//传入需要被显示的数据的位置
//仅显示_min 和 _max之间的数据
function ShowPage(_min, _max)
{
	$("#tbToChoose tbody tr").each(function() {
		$(this).show();
	});
	
	var variable = "#tbToChoose tbody tr:lt("+_min+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	variable = "#tbToChoose tbody tr:gt("+_max+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	$("#showPage").html(currentPage+" 共"+pages+"页");
	
	adjustHeight();
}

//设置页数和总记录数
function setRecordPage(number)
{
	totalRecord = number;
	if(number <= 0)
	{
		totalRecord = 0;
		pages = 1;
	}
	else
	{//每页有10条记录
		pages = parseInt(number/10);
		if(number%10 != 0)
			pages += 1;
	}
	
	//页数>1 需翻页按钮
	if(pages > 1)
	{
		$("#btLast").show(); $("#btNext").show(); $("#btFormer").show(); $("#btFirst").show(); 
	}
	else
	{
		$("#btLast").hide(); $("#btNext").hide(); $("#btFormer").hide(); $("#btFirst").hide(); 
	}
}

function btConfirm()
{
	$("#btConfirm").click(function() {
			var success = false;
			$("#tbChoosed tbody tr:gt(0)").each(function() {
				if($(this).find("td").get(1).innerHTML != "&nbsp;")
				{
					success = true;
					return false;
				}
			});
			if(!success)
			{
				alert("您还没有选择测评人员，无需提交~");
				resetSelection();
				return false;
			}
			
			if(confirm('您将提交测评小组信息，确定继续？'))
			{
				var addStudentTeam = {};
				addStudentTeam["t_classid"] = $("#selectClass").val();
				addStudentTeam["sids"] = [];
				$("#tbChoosed tbody tr:gt(0)").each(function() {
					if($(this).find("td").get(1).innerHTML != "&nbsp;")
						addStudentTeam["sids"].push({"sid": $(this).find("td").get(1).innerHTML});
				});
				
				//alert($.toJSON(addStudentTeam));
				$("#layer").height(parent.parent.document.getElementById("frame_content").height);
				$("#layer").mask("正在提交，请稍等...");
				$.post(
					"../../../testdrive/index.php?r=teacher/addStuTeam",
					{"addStudentTeam": $.toJSON(addStudentTeam)},
					function(data)
					{
						var ret = $.evalJSON(data);
						if(ret.success == true)
						{
							alert("提交成功~");
						}
						else
						{
							//alert('服务器端返回错误信息=>"'+ret.message+'"');
if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
					{
						alert(ret.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href='../../../';
						return false;
					}
							alert(ret.message);
						}
						$("#layer").unmask();
						resetSelection();
					}
				);
			}
		});
}

function btDel()
{
	$("#btDel").click(function() {
		
		var rows = 0; //记录空的行数
		var success = false;
		$("#tbChoosed tbody tr:gt(0)").each(function() {
			if(!$(this).is(":hidden") && $(this).find("#CK1").get(0).checked == true) 
			{
				if($(this).find("td").get(1).innerHTML != "&nbsp;")
				{
					$(this).remove();
					var row = $("#tbChoosed tr").eq(1).clone();
					row.show();
					row.appendTo("#tbChoosed");
					success = true;
				}
			}
		});
		
		if(!success)

		{
			alert("您还没有选择要删除的测评人员~");
			resetSelection();
			return false;
		}
			
		$("#tbChoosed tbody tr:gt(0)").each(function() {
			if($(this).find("td").get(1).innerHTML == "&nbsp;")
			{
				rows++;
			}
		});
		if(pages1 >1 && rows >= 10)
		{
			$("#tbChoosed tbody tr:gt("+(totalRecord1-10)+")").remove();
			setRecordPage1(totalRecord1-10);
		}

		rePage(totalRecord1, currentPage1);
		resetSelection();
	});
}

//充值选择单选按钮
function resetSelection()
{
	$("#CKA1").attr("checked", false);
	$("#CKA1").click();
	$("#CKA1").attr("checked", false);
	$("#CKA").attr("checked", false);
	$("#CKA").click();
	$("#CKA").attr("checked", false);
}

function btAdd()
{
	$("#btAdd").click(function(){
		
		var realAdd = []; //记录真正要添加的行
		//获取选中的行号
		$("#tbToChoose tbody tr").each(function() {
			if(!$(this).is(":hidden") && $(this).find("#CK").get(0).checked == true) 
			{
				if($(this).find("td").get(1).innerHTML != "&nbsp;")
				{
					//将姓名和班级加进去
					realAdd.push(Array(
						$(this).find("td").get(1).innerHTML,
						$(this).find("td").get(2).innerHTML,
						$(this).find("td").get(3).innerHTML
					));
				}
			}
		});
		
		if(realAdd.length <= 0)
		{
			alert("您还没有选择要增加的测评人员~");
			resetSelection();
			return false;
		}
		
		//将选中的元素插入到selectMem中
		for(var i = 0; i < realAdd.length; i ++) {   
			var canAdd = true;
			$("#tbChoosed tr:gt(1)").each(function()
			{
				if($(this).find("td").get(1).innerHTML == realAdd[i][0])
				{
					alert("学号为"+realAdd[i][0]+"的学生已经添加到测评小组中~");
					canAdd = false;
					return false;
				}
			});
			
			if(canAdd)
			{
				if($("#tbChoosed tr:last").find("td").get(1).innerHTML != "&nbsp;")  //需要分页
				{
					initChoosedTable();
					setRecordPage1(totalRecord1);	
					rePage(totalRecord1, pages1);
				}
				$("#tbChoosed tr:gt(1)").each(function()
				{
					if($(this).find("td").get(1).innerHTML == "&nbsp;")
					{
						$(this).find("td").get(1).innerHTML = realAdd[i][0];
						$(this).find("td").get(2).innerHTML = realAdd[i][1];
						$(this).find("td").get(3).innerHTML = realAdd[i][2];
						return false;
					}
				});
			}
		 }
		 resetSelection();
	});
}

function changeClass()
{
	$("#selectClass").change(function(){	
		
		//待选择学生列表分页
		totalRecord = 0;
		pages = 0;
		currentPage = 1;
		//已添加学生列表分页
		totalRecord1 = 0;
		pages1 = 0;
		currentPage1 = 1;
	
		$("#tbChoosed tbody tr:gt(0)").remove();
		initChoosedTable();
		//重绘可选择学生表格
		$("#tbToChoose tbody tr:gt(0)").remove();
		showTable();
	
	});
}

//////
function addMemToChoosed()
{
	var searchTestTeam = {};
	searchTestTeam["t_classid"] = $("#selectClass").val();

	$.get(
		"../../../testdrive/index.php?r=teacher/searchTestTeam",
		{"searchTestTeam":$.toJSON(searchTestTeam)},
		function (data)
		{
			var ret = $.evalJSON(data);
			if(ret.success == true)
			{
				$.each(ret.results, function(key1, val1){
					if($("#tbChoosed tr:last").find("td").get(1).innerHTML != "&nbsp;")  //需要分页
					{
						initChoosedTable();
						setRecordPage1(totalRecord1);	
						rePage(totalRecord1, pages1);
					}
					$("#tbChoosed tr:gt(1)").each(function()
					{
						if($(this).find("td").get(1).innerHTML == "&nbsp;")
						{
							$(this).find("td").get(1).innerHTML = val1.sid;
							$(this).find("td").get(2).innerHTML = val1.sname;
							$(this).find("td").get(3).innerHTML = val1.cname;  //预留接口，得改
							return false;
						}
					});
				});
			}
			else
			{
				//alert("服务器端返回错误信息=>\""+ret.message+'"');
if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
					{
						alert(ret.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href='../../../';
						return false;
					}
				alert(ret.message);
			}
		}
	);
}


function addSelectClass()
{
	var TeClass = {};
	TeClass["teacherid"] = "";
	var date = new Date();
	TeClass["year"] = date.getFullYear();
	$.get(
		"../../../testdrive/index.php?r=zing/getTeClass",
		{"TeClass": $.toJSON(TeClass)},
		function(data)
		{
			var obj = $.evalJSON(data);
			//2012-02-18 modified by minzhenyu
			if(obj.success == true)
			{
				if(obj.results.length != 0)
                        	{
                                	var selector=$('#selectClass');
                                	for (var i = 0; i < obj.results.length; i++)
                                	{
                                        	selector.append('<option value="'+obj.results[i].t_classid+'">'+obj.results[i].name+'</option>');
                               	 	}

                                	//显示左边的带选择表格
                               		showTable();
                                	return true;
				}
				else
				{
					alert("本学年度，您暂无测评任务！");
					$("#main").hide();
                                	$("#error").show();
                                	return false;
				}
                        }
                        else
                        {
                                //alert('服务器端返回错误信息=>"'+obj.message+'"');
if(obj.results.hasOwnProperty("timeout") && obj.results.timeout)
					{
						alert(obj.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href='../../../';
						return false;
					}
								alert(obj.message);
                                $("#main").hide();
                                $("#error").show();
                                return false;
                        }

		}
	);
}

//显示左边的带选择表格
function showTable()
{
	parent.parent.document.getElementById("frame_content").height=420;	
	
	var getStuByClass = {};
	getStuByClass["t_classid"] = $("#selectClass").val();
	getStuByClass["classid"] = "";
	
	$.get(
		"../../../testdrive/index.php?r=teacher/getStuByClass",
		{getStuByClass: $.toJSON(getStuByClass)},
		function(data)
		{
			var ret = $.evalJSON(data);
			if(ret.success == true)
			{
				//根据返回的结果数目计算分页，分页不足一页的，总是保证每页10条，不足的用空行补充
				setRecordPage(ret.results.length);
				
				for(var i = 0; i < ret.results.length; i++)
				{
					var row = $("#tbToChoose tr").eq(1).clone();
					row.find("td").get(1).innerHTML = ret.results[i].sid;
					row.find("td").get(2).innerHTML = ret.results[i].sname;
					row.find("td").get(3).innerHTML = ret.results[i].cname;
					row.show();
					row.appendTo("#tbToChoose");
				}
				for(var i = ret.results.length; i < pages*10; i++)
				{
					var row = $("#tbToChoose tr").eq(1).clone();
					row.show();
					row.appendTo("#tbToChoose");
				}
				setRecordPage(pages*10);
				currentPage = 1;
				$("#tbToChoose tbody tr:gt(10)").each(function() {
					$(this).hide();
				});
				
				$("#showPage").html(currentPage+" 共"+pages+"页");
				
				adjustHeight();
				
				//接下来加载右边的已选择表格
				addMemToChoosed();
			}
			else
			{
				//alert('服务器端返回错误信息=>"'+ret.message+'"');
if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
					{
						alert(ret.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href='../../../';
						return false;
					}
				alert(ret.message);
			}
		}
	);
}

function adjustHeight()
{
	var height = document.documentElement.scrollHeight<document.body.scrollHeight
						? document.body.scrollHeight:document.documentElement.scrollHeight; 
	if(height > 420)
		parent.parent.document.getElementById("frame_content").height=height;
	else
		parent.parent.document.getElementById("frame_content").height=420;	
}
