// JavaScript Document
//首先加载测评班级的下拉框
addSelectClass();

//待审核学生列表
var totalRecord = 0;
var pages = 0;
var currentPage = 1;

var currentMem;   //当前审核的学生的html行

$(document).ready(function() {
	//调整屏幕的高度
	parent.parent.document.getElementById("frame_content").height=420;	
	//首先要隐藏错误输出提示、基本素质成绩提示和页面
	$("#error").hide();
	//隐藏模板tr
	$("#stu-table tbody tr").eq(0).hide();
	
	//隐藏翻页按钮
	$("#btLast").hide(); $("#btNext").hide(); $("#btFormer").hide(); $("#btFirst").hide(); 
	
	//当下拉框中得测评班级改变时
	changeClass();

	//学生列表的翻页控制
	stuPage();

	//批量授权
	btMutiCheck();

	//选择所有行
	$("#CKA").click(function() {
		
		$("#stu-table tbody tr:gt(0)").each(function() {
			if(!$(this).is(":hidden")) 	
				$(this).find("#CK").get(0).checked = $("#CKA").get(0).checked;
		});
	});

})

function changeClass()
{
	$("#selectClass").change(function(){
		$("#stu-table tbody tr:gt(0)").remove();
		showStu();
	});
}

//审核通过一个学生的成绩
function funcAddPrivilege(addPrivilege, alreadyPassedMem)
{
	if(confirm('您确认要为学生授权编辑课程成绩？'))
	{
		//alert($.toJSON(addPrivilege));
		$.get(
			"../../../testdrive/index.php?r=teacher/addPrivilege",
			{addPrivilege: $.toJSON(addPrivilege)},
			function(data)
			{
				var ret = $.evalJSON(data);
				
				if(ret.success == true)
				{
					//TO DO
					alert("授权成功~");
					
					for(var i = 0; i < alreadyPassedMem.length; i++)
					{
						alreadyPassedMem[i].html("已授权");
						alreadyPassedMem[i].removeAttr("onclick");
						alreadyPassedMem[i].removeAttr("href");
					}
				}
				else
				{
					//alert('服务器端返回错误信息=>"' +ret.message+'"');
					if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
					{
						alert(ret.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href='../../../';
						return false;
					}
					alert(ret.message);
				}
			}
		);
	}
}

//批量审核，直接传学号给后台，后台处理就行
function btMutiCheck()
{
	$("#btMutiCheck").click(function (){
		var addPrivilege = {};
		var alreadyPassedMem = [];
		addPrivilege["t_classid"] = $("#selectClass").val();
		addPrivilege["sids"] = [];
		$("#stu-table tr:gt(1)").each(function() {
			if ($(this).find("#CK").get(0).checked == true && $(this).find("#link").html()!="已授权") {
				addPrivilege["sids"].push($(this).find("#stuID").html());
				alreadyPassedMem.push($(this).find("#link"));
			}
		});
		
		if(alreadyPassedMem.length == 0)
		{
			alert("请您选择要授权的学生，或者您选择的学生已授权~");
			$("#CKA").attr("checked", false);
			$("#CKA").click();
			$("#CKA").attr("checked", false);
			return false;
		}
		
		//批量审核
		funcAddPrivilege(addPrivilege, alreadyPassedMem);
		
		$("#CKA").attr("checked", false);
		$("#CKA").click();
		$("#CKA").attr("checked", false);
	});
}

//审核一个学生的F1成绩
function checkAMem()
{
	var addPrivilege = {};
	var alreadyPassedMem = [];
	addPrivilege["t_classid"] = $("#selectClass").val();
	addPrivilege["sids"] = [];

	addPrivilege["sids"].push(currentMem.find("#stuID").html());
	alreadyPassedMem.push(currentMem.find("#link"));
	
	//批量审核
	funcAddPrivilege(addPrivilege, alreadyPassedMem);
}

//学生列表的翻页控制
function stuPage()
{
	$("#btLast").click(function() 
	{
		if(currentPage != pages)
		{
			//alert("即将跳转到最后一页");
			currentPage = pages;
			ShowPage(currentPage*10-9, totalRecord);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btNext").click(function() 
	{
		if(currentPage+1<=pages)
		{
			//alert("即将跳转到下一页");
			currentPage += 1;
			ShowPage(currentPage*10-9, currentPage*10);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btFormer").click(function() 
	{
		if(currentPage-1>0)
		{
			//alert("即将跳转到上一页");
			currentPage -= 1;
			ShowPage(currentPage*10-9, currentPage*10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
	$("#btFirst").click(function() 
	{
		if(currentPage != 1)
		{
			//alert("即将跳转到首页");
			currentPage = 1;
			ShowPage(1, 10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
}

//传入需要被显示的数据的位置
//仅显示_min 和 _max之间的数据
function ShowPage(_min, _max)
{
	$("#stu-table tbody tr").each(function() {
		$(this).show();
	});
	
	var variable = "#stu-table tbody tr:lt("+_min+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	variable = "#stu-table tbody tr:gt("+_max+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	$("#showPageStu").html(currentPage+"/"+pages);
	
	adjustHeight();
}

//设置页数和总记录数
function setRecordPage(number)
{
	totalRecord = number;
	if(number <= 0)
	{
		totalRecord = 0;
		pages = 1;
	}
	else
	{//每页有10条记录
		pages = parseInt(number/10);
		if(number%10 != 0)
			pages += 1;
	}
	
	//页数>1 需翻页按钮
	if(pages > 1)
	{
		$("#btLast").show(); $("#btNext").show(); $("#btFormer").show(); $("#btFirst").show(); 
	}
	else
	{
		$("#btLast").hide(); $("#btNext").hide(); $("#btFormer").hide(); $("#btFirst").hide(); 
	}
}

//显示测评班级对应的学生列表
function showStu()
{
	var getStuPrivilege = {};
	getStuPrivilege["t_classid"] = $("#selectClass").val();
	getStuPrivilege["classid"] = "";
	
	$.get(
		"../../../testdrive/index.php?r=teacher/getStuPrivilege",
		{getStuPrivilege: $.toJSON(getStuPrivilege)},
		function(data)
		{
			var ret = $.evalJSON(data);
			if(ret.success == true)//&& ret.results.length != 0
			{
				if(ret.results.length != 0)
				{
					//根据返回的结果数目计算分页
					setRecordPage(ret.results.length);
					
					for(var i = 0; i < ret.results.length; i++)
					{
						var row = $("#stu-table tr").eq(1).clone();
						row.find("td").get(1).innerHTML = ret.results[i].sid;
						row.find("td").get(2).innerHTML = ret.results[i].sname;
						row.find("td").get(3).innerHTML = ret.results[i].cname;
						if(parseInt(ret.results[i].hasPrivilege) != 0)
						{
							row.find("td").eq(4).find("#link").removeAttr("href");
							row.find("td").eq(4).find("#link").removeAttr("onclick");
							row.find("td").eq(4).find("#link").html("已授权");
						}
						row.show();
						row.appendTo("#stu-table");
					}
					
					currentPage = 1;
					$("#stu-table tbody tr:gt(10)").each(function() {
						$(this).hide();
					});
					
					$("#showPageStu").html(currentPage+"/"+pages);
					
					adjustHeight();
				}
				else
				{
					setRecordPage(1);
					$("#showPageStu").html("0/1");
					alert("该测评班级列表为空~");
				}
			}
			else
			{
				//alert('服务器端返回错误信息=>"'+ret.message+'"');
				if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
					{
						alert(ret.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href='../../../';
						return false;
					}
				alert(ret.message);
			}
		}
	);
}

function addSelectClass()
{
	var TeClass = {};
	TeClass["teacherid"] = "";
	var date = new Date();
	TeClass["year"] = date.getFullYear();
	$.get(
		"../../../testdrive/index.php?r=zing/getTeClass",
		{TeClass: $.toJSON(TeClass)},
		function(data)
		{
			var obj = $.evalJSON(data);
			if(obj.success == true)
			{
				if(obj.results.length != 0)
				{
					var selector=$('#selectClass');  
					for (var i = 0; i < obj.results.length; i++)
					{
						selector.append('<option value="'+obj.results[i].t_classid+'">'+obj.results[i].name+'</option>');
					}
				
					//显示测评班级对应的学生列表
					showStu();
					return true;
				}
				else
				{
					alert("本学年度，您暂无测评任务！");
				}
			}
			else
			{
        			//alert('服务器端返回错误信息=>"'+obj.message+'"');
					if(obj.results.hasOwnProperty("timeout") && obj.results.timeout)
					{
						alert(obj.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href='../../../';
						return false;
					}
					alert(obj.message);
			}
			$("#students").hide(); $("#div-class").hide(); $("#div-info").hide();
			$("#error").show();
			return false;
		}
	);
}

function adjustHeight()
{
	var height = document.body.clientHeight;
	if(height > 420)
		parent.parent.document.getElementById("frame_content").height=height + 40;
	else
		parent.parent.document.getElementById("frame_content").height=420;	
}
