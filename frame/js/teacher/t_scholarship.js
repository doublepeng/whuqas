// JavaScript Document
var searchMark = 0; //搜索的标志，如果为0，表示基本搜索，否则是高级搜索
//首先加载测评班级的下拉框
addSelectClass();

$.validator.setDefaults({
	submitHandler: function() { 
		if(searchMark == 0)
			basicSearch();
		else if(searchMark == 1)
			//高级搜索
			furtherSearch();
	},
	highlight: function(input) {
		$(input).addClass("ui-state-highlight");
	},
	unhighlight: function(input) {
		$(input).removeClass("ui-state-highlight");
	}
});

//待审核学生列表
var totalRecord = 0;
var pages = 0;
var currentPage = 1;

//审核是否通过标志
var isPassed = 0;

$(document).ready(function() {
	//调整屏幕的高度
	parent.parent.document.getElementById("frame_content").height=420;	
	//首先要隐藏错误输出提示、基本素质成绩提示和页面
	$("#error").hide(); $("#furtherSearch").hide();
	//隐藏模板tr
	$("#stu-table tbody tr").eq(0).hide();
	//隐藏翻页按钮
	$("#btLast").hide(); $("#btNext").hide(); $("#btFormer").hide(); $("#btFirst").hide(); 

	$( "#tabs" ).tabs();
	
	//当下拉框中得测评班级改变时
	changeClass();

	//学生列表的翻页控制
	stuPage();

	//显示高级搜索栏
	btShowSearch();

	//为学生设置奖学金
	btSetScholarship();

	//取消为学生设置的奖学金
	btCancelScholarship();
	
	//显示所有学生列表
	btShowAllStu();
	
	//重置基本搜索
	btResetSearchBasic()

	//选择所有行
	$("#CKA").click(function() {
		
		$("#stu-table tbody tr:gt(0)").each(function() {
			if(!$(this).is(":hidden")) 	
				$(this).find("#CK").get(0).checked = $("#CKA").get(0).checked;
		});
	});

	//高级搜索的验证功能
	jQuery.validator.addMethod("score", function(value, element){
		if($(element).prev().val() != '不限')
		{
			var reg = /^\d+(.\d+$|\d*$)/;
			return (value.length >= 1 && value.length <= 7 && reg.test(value) && parseFloat(value) <= 100 && parseFloat(value) >= 0);
		}
		else
		{
			return true;
		}
	}, "请输入长度不超过7位且数值不超过100的数字");
	
	jQuery.validator.addMethod("number", function(value, element){
		if($(element).prev().val() != '不限')
		{
			var reg = /^([0-9]+)$/;
			return (value.length >= 1 && value.length <= 4 && reg.test(value));
		}
		else
		{
			return true;
		}
	}, "请输入长度不超过4位的数字");

	jQuery.validator.addMethod("percent", function(value, element){
		if($(element).prev().val() != '不限')
		{
			var reg = /^([0-9]+)$/;
			return (value.length >= 1 && value.length <= 3 && reg.test(value));
		}
		else
		{
			return true;
		}
	}, "请输入长度不超过3位的数字，用来表示百分比。(例如输入5，则表示5%)");

	jQuery.validator.addMethod("content", function(value, element){
		if($(element).prev().val() != '不限')
		{
			return (value.length >= 1 && value.length <= 25);
		}
		else
		{
			return true;
		}
	}, "请输入正确的班级名称(长度小于25)~");

  jQuery.validator.addMethod("rank", function(value, element){
		if($(element).prev().prev().prev().children().get(0).checked == true)
		{
			var reg = /^([0-9]+)$/;
			return (value.length >= 1 && value.length <= 4 && reg.test(value));
		}
		else
		{
			return true;
		}
	}, "请输入长度不超过4位的数字");
	
	$("#basicSearchForm").validate({
		rules:{
			i_choose: {
				rank: true
			},
      i_choose1: {
				rank: true
			}
		}						   
	});
	
	// validate signup form on keyup and submit
	$("#signupForm").validate({
		rules: {
			i_score: {
				score: true
			},
			i_F1: {
				score: true
			},
			i_F2: {
				score: true
			},
			i_F3: {
				score: true
			},
			i_paper: {
				number: true
			},
			i_tclassRank: {
				percent: true
			},
			i_classRank: {
				percent: true
			},
			i_cname: {
				content: true
			}
		}
	});
	//搜索校验
	validator();
})

//重置基本搜索
function btResetSearchBasic()
{
	$("#btResetSearchBasic").click(function(){
		$("#s_choose").val("F1");
		$("#s_compare").val("0");
		$("#i_choose").val("");
		$("#exScholarship").get(0).checked = false;
		$("#includePoor").get(0).checked = false;
    $("#s_choose1").val("paper");
		$("#s_compare1").val("0");
		$("#i_choose1").val("");
    $("#c_choose").get(0).checked = false;
    $("#c_choose1").get(0).checked = false;
    searchMark = -1;
	});
}

//显示所有学生列表
function btShowAllStu()
{
	$("#btShowAllStu").click(function(){
		showStu();
	});
}

//用来做搜索下拉框的onchange事件
function _onchange(selector, input)
{
	if(selector.val() == '不限') 
	{
		input.attr('readonly', 'readonly'); 
		input.val(''); 
	}
	else
	{
		input.removeAttr('readonly'); 
	}
}

function validator()
{
	$("#btSearch").click(function(){
	    searchMark = 1;
	//$("#btSearch").submit();
	});
	
	$("#btSearchBasic").click(function(){
	    searchMark = 0;
	//$("#btSearch").submit();
	});
}

//基本搜索
function basicSearch()
{
  //searchRank["condition1"] //表示对于基本成绩的查询条件组合
　//searchRank["condition2"] //表示对论文、著作、科研等加分项的查询
	var searchRank = {};
	searchRank["t_classid"] = $("#selectClass").val();
	var date = new Date();
	searchRank["year"] = date.getFullYear();

	searchRank["ispoor"] = 0;
	if($("#includePoor").get(0).checked == true)
		searchRank["ispoor"] = 1;

  searchRank['exscholarship'] = 0;
	if($("#exScholarship").get(0).checked == true)
		searchRank['exscholarship'] = 1;

  //表示对于基本成绩的查询条件组合
  searchRank["condition1"] = {};
  if($("#c_choose").get(0).checked == true)
  {
    searchRank["condition1"]['compare'] = parseInt($("#s_compare").val());
    searchRank["condition1"]["rankID"] = parseInt($("#i_choose").val());
	  searchRank["condition1"]['type'] = $("#s_choose").val();
  }
  
  //表示对论文、著作、科研等加分项的查询
  searchRank["condition2"] = {};
  if($("#c_choose1").get(0).checked == true)
  {
    searchRank["condition2"]['compare'] = parseInt($("#s_compare1").val());
    searchRank["condition2"]["num"] = parseInt($("#i_choose1").val());  //表示有几项
	  searchRank["condition2"]['type'] = $("#s_choose1").val();
  }
	
	//alert($.toJSON(searchRank));
	beginSearch(searchRank);
}

//开始基本搜索逻辑
function beginSearch(searchRank)
{
	$.post(
		"../../../testdrive/index.php?r=teacher/searchRank",
		{searchRank: $.toJSON(searchRank)},
		function(data){
			var ret = $.evalJSON(data);
			if(ret.success == true)
			{
				if(ret.results.length == 0)
				{
          $("#stu-table tbody tr:gt(0)").remove();
					alert("搜索结果为空");
					return false;
				}
				
				var contents = ret.results.contents;
				isPassed = parseInt(ret.results.isPassed);
				if(contents.length == 0)
				{
          $("#stu-table tbody tr:gt(0)").remove();
					alert("搜索结果为空");
					return false;
				}
				
				$("#stu-table tbody tr:gt(0)").remove();
				//根据返回的结果数目计算分页
				setRecordPage(contents.length);
				
				for(var i = 0; i < contents.length; i++)
				{
					var row = $("#stu-table tr").eq(1).clone();
					row.find("td").get(1).innerHTML = contents[i].sid;
					row.find("td").get(2).innerHTML = contents[i].sname;
					row.find("td").eq(2).attr("title", contents[i].cname);
					row.find("td").get(3).innerHTML = parseFloat(contents[i].F1);
					row.find("td").get(4).innerHTML = parseFloat(contents[i].F2);
					row.find("td").get(5).innerHTML = parseFloat(contents[i].F3);
					row.find("td").get(6).innerHTML = parseFloat(contents[i].total);
					
					if(contents[i].scholars.length != 0)
					{
						for(var id in contents[i].scholars)
						{
							var selectScholar = $("#scholarship").clone();
							row.find("td").eq(7).append(selectScholar);
							var newScho = row.find("td").eq(7).find('select:last');
							newScho.removeAttr("name");
							newScho.removeAttr("id");
							newScho.css("width", "90px");

							newScho.val(contents[i].scholars[id]);
							row.find("td").eq(7).append('&nbsp;<a href="javascript:void(0)" onclick="cancelOne($(this))">取消</a>');
							if(isPassed != 0)
							{
								newScho.attr("disabled", "disabled");
								row.find("td").eq(7).find("a").removeAttr("onclick");
								row.find("td").eq(7).find("a").removeAttr("href");
							}
						}
					}
					row.show();
					row.appendTo("#stu-table");
				}

				currentPage = 1;
				$("#stu-table tbody tr:gt(20)").each(function() {
					$(this).hide();
				});
				
				$("#showPageStu").html(currentPage+"/"+pages);
				
				adjustHeight();

				$('html,body', window.parent.parent.document).animate({
					scrollTop: $("#search").offset().top+135}, 
					400
				);
			}
			else
			{
				//alert('服务器端返回错误信息=>"'+ret.message+'"');
if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
					{
						alert(ret.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href='../../../';
						return false;
					}
				alert(ret.message);
				$("#students").hide(); $("#div-class").hide(); $("#search").hide(); 
				$("#error").show();
				return false;
			}
		}
	);
}

//高级搜索
function furtherSearch()
{
	var conditions = {};
	//score, F1, F2, F3, paper, scholarship, tRank, cRank, cname, poor;
	if($("#s_score").val() != '不限')
	{
		conditions['score'] = Array($("#s_score").val(), parseFloat($("#i_score").val()));
	}

	if($("#s_F1").val() != '不限')
	{
		conditions['F1'] = Array($("#s_F1").val(), parseFloat($("#i_F1").val()));
	}

	if($("#s_F2").val() != '不限')
	{
		conditions['F2'] = Array($("#s_F2").val(), parseFloat($("#i_F2").val()));
	}

	if($("#s_F3").val() != '不限')
	{
		conditions['F3'] = Array($("#s_F3").val(), parseFloat($("#i_F3").val()));
	}

	if($("#s_paper").val() != '不限')
	{
		conditions['paper'] = Array($("#s_paper").val(), parseInt($("#i_paper").val()));
	}

	if($("#s_scholar").val() != '不限')
	{
		conditions['scholarship'] = $("#s_scholar").val();
	}

	if($("#s_tclassRank").val() != '不限')
	{
		conditions['tRank'] = Array($("#s_tclassRank").val(), parseInt($("#i_tclassRank").val()));
	}

	if($("#s_classRank").val() != '不限')
	{
		conditions['cRank'] = Array($("#s_classRank").val(), parseInt($("#i_classRank").val()));
	}

	if($("#s_cname").val() != '不限')
	{
		conditions['cname'] = Array($("#s_cname").val(), $("#i_cname").val());
	}

	if($("#s_poor").val() != '不限')
	{
		conditions['poor'] = $("#s_poor").val();
	}

	//alert($.toJSON(conditions));
	alert("该功能接口暂未开放~");
	//真正的搜索逻辑
	//postSearch();
}

//真正的搜索逻辑
function postSearch(conditions)
{
	$.post(
		"./interface/getSearch.php",
		{conditions: $.toJSON(conditions)},
		function(data){
			var ret = $.evalJSON(data);
			if(ret.success == true && ret.results.length != 0)
			{
        $("#stu-table tbody tr:gt(0)").remove();

				var contents = ret.results.contents;
				isPassed = parseInt(ret.results.isPassed);
				if(contents.length == 0)
				{
					alert("搜索结果为空");
					return false;
				}
				
				//$("#stu-table tbody tr:gt(0)").remove();
				//根据返回的结果数目计算分页
				setRecordPage(contents.length);
				
				for(var i = 0; i < contents.length; i++)
				{
					var row = $("#stu-table tr").eq(1).clone();
					row.find("td").get(1).innerHTML = contents[i].sid;
					row.find("td").get(2).innerHTML = contents[i].sname;
					row.find("td").eq(2).attr("title", contents[i].cname);
					row.find("td").get(3).innerHTML = parseFloat(contents[i].F1);
					row.find("td").get(4).innerHTML = parseFloat(contents[i].F2);
					row.find("td").get(5).innerHTML = parseFloat(contents[i].F3);
					row.find("td").get(6).innerHTML = parseFloat(contents[i].total);
					
					if(contents[i].scholars.length != 0)
					{
						for(var id in contents[i].scholars)
						{
							var selectScholar = $("#scholarship").clone();
							row.find("td").eq(7).append(selectScholar);
							var newScho = row.find("td").eq(7).find('select:last');
							newScho.removeAttr("name");
							newScho.removeAttr("id");
							newScho.css("width", "90px");
							newScho.val(contents[i].scholars[id]);
							row.find("td").eq(7).append('&nbsp;<a href="javascript:void(0)" onclick="cancelOne($(this))">取消</a>');
							if(isPassed != 0)
							{
								newScho.attr("disabled", "disabled");
								row.find("td").eq(7).find("a").removeAttr("onclick");
								row.find("td").eq(7).find("a").removeAttr("href");
							}
						}
					}
					row.show();
					row.appendTo("#stu-table");
				}

				currentPage = 1;
				$("#stu-table tbody tr:gt(20)").each(function() {
					$(this).hide();
				});
				
				$("#showPageStu").html(currentPage+"/"+pages);
				
				adjustHeight();

				$('html,body', window.parent.parent.document).animate({
					scrollTop: $("#search").offset().top+135}, 
					400
				);
			}
			else
			{
				//alert('服务器端返回错误信息=>"'+ret.message+'"');
if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
					{
						alert(ret.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href='../../../';
						return false;
					}
				alert(ret.message);
				$("#students").hide(); $("#div-class").hide(); $("#search").hide(); 
				$("#error").show();
				return false;
			}
		}
	);
}

///显示高级搜索栏
function btShowSearch()
{
	$("#btShowSearch").click(function() {
		if($("#btShowSearch span").html() == "显示高级搜索")
		{
			$("#furtherSearch").show();
			$("#btShowSearch span").html("隐藏高级搜索");
			adjustHeight();
		}
		else if($("#btShowSearch span").html() == "隐藏高级搜索")
		{
			$("#furtherSearch").hide();
			$("#btShowSearch span").html("显示高级搜索");
			adjustHeight();
		}
	});
}

//取消为学生设置的奖学金
function btCancelScholarship()
{
	$("#btCancel").click(function() {

		if(isPassed != 0)
		{
			alert("该测评班级的奖学金评定结果已经审核通过，不能修改~");
			return false;
		}

		var selected = false;
		$("#stu-table tbody tr:gt(0)").each(function(){
			if ($(this).find("#CK").get(0).checked == true) {
				selected = true;
			}
		});
		if(!selected)
		{
			alert("您还没有选择要取消设定奖学金等级的学生~");
			return false;
		}
		
		if(confirm('您将确认取消奖学金设置？'))
		{

		var resetScholar = {};
		var date = new Date();
		resetScholar["year"] = date.getFullYear();
		resetScholar["contents"] = [];
		$("#stu-table tbody tr:gt(0)").each(function(){
			if ($(this).find("#CK").get(0).checked == true) {
				//遍历已添加奖学金列表,查看是否存在需要取消的设置，如果没有则直接过滤
				var id = $(this).find("td");
				id.eq(7).find("select").each(function(){
					if($(this).val() == $("#scholarship").val())
					{
						//取消设置~
						$(this).nextUntil('select').remove();
						$(this).remove();
						resetScholar["contents"].push({'sid':id.eq(1).html(), 'scholar':$("#scholarship").val()});
					}
				});
			}
		});
		
		resetScholarship(resetScholar);

		adjustHeight();

		$("#CKA").attr("checked", false);
		$("#CKA").click();
		$("#CKA").attr("checked", false);
		}
	});
}

//点击每行后面的取消奖学金设置
function cancelOne(selector)
{
	var scholarid = selector.prev('select').val(); 
	var sid = selector.parent().parent().find("td").eq(1).html(); 

	var resetScholar = {};
	var date = new Date();
	resetScholar["year"] = date.getFullYear();
	resetScholar["contents"] = [];
	
	resetScholar["contents"].push({"sid": sid, 'scholar': scholarid});
	resetScholarship(resetScholar);

	selector.prev('select').remove(); 
	selector.remove();
	adjustHeight(); 
}

//取消奖学金调用的公共接口
function resetScholarship(resetScholar)
{
	//if(confirm('您将确认取消奖学金设置？'))
	{
		//alert($.toJSON(resetScholar));
		$.post(
			"../../../testdrive/index.php?r=teacher/resetStuScholar",
			{resetScholar: $.toJSON(resetScholar)},
			function (data){
				var ret = $.evalJSON(data);
				if(ret.success == true)
				{
					//TO DO
				}
				else
				{
					if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
					{
						alert(ret.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href='../../../';
						return false;
					}
					alert("取消奖学金失败~服务器返回错误信息\""+ret.message+'"');
					return false;
				}	
			}
		);
	}
}

//为学生设置奖学金
function btSetScholarship()
{
	$("#btSetScholarship").click(function() {
		
		if(isPassed != 0)
		{
			alert("该测评班级的奖学金评定结果已经审核通过，不能修改~");
			return false;
		}

		var selected = false;
		$("#stu-table tbody tr:gt(0)").each(function(){
			if ($(this).find("#CK").get(0).checked == true) {
				selected = true;
			}
		});
		if(!selected)
		{
			alert("您还没有选择要设置奖学金等级的学生~");
			return false;
		}
		

		if(confirm('您将确认设置奖学金？'))
		{
		var setScholarByClass = {};
		var date = new Date();
		setScholarByClass["year"] = date.getFullYear();
		setScholarByClass["contents"] = [];

		$("#stu-table tbody tr:gt(0)").each(function(){
			if ($(this).find("#CK").get(0).checked == true) {
				//遍历已添加奖学金列表，去重
				var id = $(this).find("td");
				var canAdd = true;
				id.eq(7).find("select").each(function(){
					if($(this).val() == $("#scholarship").val())
					{
						alert('姓名为"'+id.eq(2).html()+'"的学生已经设定了"'+$("#scholarship option:selected").text()+'"~');
						canAdd = false;
						return false;
					}
				});

				if(canAdd)
				{
					setScholarByClass["contents"].push({"sid":id.eq(1).html(), "scholar":$("#scholarship").val()});
					var selectScholar = $("#scholarship").clone();
					id.eq(7).append(selectScholar);
					var newScho = id.eq(7).find('select:last');
					newScho.removeAttr("name");
					newScho.removeAttr("id");
					newScho.css("width", "90px");
					newScho.val($("#scholarship").val());
					id.eq(7).append('&nbsp;<a href="javascript:void(0)" onclick="cancelOne($(this))">取消</a>');
				}
			}
		});

		//alert($.toJSON(setScholarByClass));
		$.post(
			"../../../testdrive/index.php?r=teacher/setStuScholar",
			{setScholarByClass: $.toJSON(setScholarByClass)},
			function (data){
				var ret = $.evalJSON(data);
				if(ret.success == true)
				{
					//TO DO
				}
				else
				{
					if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
					{
						alert(ret.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href='../../../';
						return false;
					}
					alert("设置奖学金失败~服务器返回错误信息\""+ret.message+'"');
					return false;
				}	
			}
		);

		adjustHeight();

		$("#CKA").attr("checked", false);
		$("#CKA").click();
		$("#CKA").attr("checked", false);
		}
	});
}

function changeClass()
{
	$("#selectClass").change(function(){
		$("#stu-table tbody tr:gt(0)").remove();
		showStu();
	});
}

//学生列表的翻页控制
function stuPage()
{
	$("#btLast").click(function() 
	{
		if(currentPage != pages)
		{
			//alert("即将跳转到最后一页");
			currentPage = pages;
			ShowPage(currentPage*20-19, totalRecord);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btNext").click(function() 
	{
		if(currentPage+1<=pages)
		{
			//alert("即将跳转到下一页");
			currentPage += 1;
			ShowPage(currentPage*20-19, currentPage*20);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btFormer").click(function() 
	{
		if(currentPage-1>0)
		{
			//alert("即将跳转到上一页");
			currentPage -= 1;
			ShowPage(currentPage*20-19, currentPage*20);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
	$("#btFirst").click(function() 
	{
		if(currentPage != 1)
		{
			//alert("即将跳转到首页");
			currentPage = 1;
			ShowPage(1, 20);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
}

//传入需要被显示的数据的位置
//仅显示_min 和 _max之间的数据
function ShowPage(_min, _max)
{
	$("#stu-table tbody tr").each(function() {
		$(this).show();
	});
	
	var variable = "#stu-table tbody tr:lt("+_min+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	variable = "#stu-table tbody tr:gt("+_max+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	$("#showPageStu").html(currentPage+"/"+pages);
	
	adjustHeight();
}

//设置页数和总记录数
function setRecordPage(number)
{
	totalRecord = number;
	if(number <= 0)
	{
		totalRecord = 0;
		pages = 1;
	}
	else
	{//每页有25条记录
		pages = parseInt(number/20);
		if(number%20 != 0)
			pages += 1;
	}
	
	//页数>1 需翻页按钮
	if(pages > 1)
	{
		$("#btLast").show(); $("#btNext").show(); $("#btFormer").show(); $("#btFirst").show(); 
	}
	else
	{
		$("#btLast").hide(); $("#btNext").hide(); $("#btFormer").hide(); $("#btFirst").hide(); 
	}
}

//显示测评班级对应的学生列表
function showStu()
{
	var getRankByClass = {};
	getRankByClass["t_classid"] = $("#selectClass").val();
	var date = new Date();
	getRankByClass["year"] = date.getFullYear();
	
	$.get(
		"../../../testdrive/index.php?r=teacher/getRankByClass",
		{getRankByClass: $.toJSON(getRankByClass)},
		function(data)
		{
			var ret = $.evalJSON(data);
			if(ret.success == true)
			{
				var contents = ret.results.contents;
				isPassed = parseInt(ret.results.isPassed);
				if(ret.results.length == 0 || contents.length == 0)
				{
					alert("暂时没有学生列表，无法进行奖学金评定~");
					$("#students").hide(); $("#div-class").hide(); $("#search").hide(); 
					$("#error").show();
					return false;
				}
				
				$("#stu-table tbody tr:gt(0)").remove();
				
				//根据返回的结果数目计算分页
				setRecordPage(contents.length);
				
				for(var i = 0; i < contents.length; i++)
				{
					var row = $("#stu-table tr").eq(1).clone();
					row.find("td").get(1).innerHTML = contents[i].sid;
					row.find("td").get(2).innerHTML = contents[i].sname;
					row.find("td").eq(2).attr("title", contents[i].cname);
					row.find("td").get(3).innerHTML = parseFloat(contents[i].F1);
					row.find("td").get(4).innerHTML = parseFloat(contents[i].F2);
					row.find("td").get(5).innerHTML = parseFloat(contents[i].F3);
					row.find("td").get(6).innerHTML = parseFloat(contents[i].total);
					
					if(contents[i].scholars.length != 0)
					{
						for(var id in contents[i].scholars)
						{
							var selectScholar = $("#scholarship").clone();
							row.find("td").eq(7).append(selectScholar);
							var newScho = row.find("td").eq(7).find('select:last');
							newScho.removeAttr("name");
							newScho.removeAttr("id");
							newScho.css("width", "90px");
							newScho.val(contents[i].scholars[id]);
							row.find("td").eq(7).append('&nbsp;<a href="javascript:void(0)" onclick="cancelOne($(this))">取消</a>');
							if(isPassed != 0)
							{
								newScho.attr("disabled", "disabled");
								row.find("td").eq(7).find("a").removeAttr("onclick");
								row.find("td").eq(7).find("a").removeAttr("href");
							}
						}
					}
					row.show();
					row.appendTo("#stu-table");
				}

				currentPage = 1;
				$("#stu-table tbody tr:gt(20)").each(function() {
					$(this).hide();
				});
				
				$("#showPageStu").html(currentPage+"/"+pages);
				
				adjustHeight();

				$('html,body', window.parent.parent.document).animate({
					scrollTop: $("#search").offset().top+135}, 
					400
				);
			}
			else
			{
				//alert('服务器端返回错误信息=>"'+ret.message+'"');
				alert(ret.message);
				$("#students").hide(); $("#div-class").hide(); $("#search").hide(); 
				$("#error").show();
				return false;
			}
		}
	);
}

//添加奖学金
function addScholarship()
{
	$.get(
		"../../../testdrive/index.php?r=zing/getDepSchType",
		function(data)
		{
			var obj = $.evalJSON(data);
			if(obj.success == true)
			{
				if(obj.results.length != 0)
				{
					var selector = $("#scholarship");
					for (var i = 0; i < obj.results.length; i++)
					{
            var scholarshipname = "";
            if(obj.results[i].type == "优秀学生奖学金")
              scholarshipname = obj.results[i].name+obj.results[i].type;
            else
              scholarshipname = obj.results[i].name;
						$("#s_scholar").append('<option value="'+obj.results[i].scholarshipid
																			 +'">'+scholarshipname+'</option>');
						selector.append('<option value="'+obj.results[i].scholarshipid+'">'+
																	  scholarshipname+'</option>');
					}
					
					//显示测评班级对应的学生列表
					showStu();
					return true;
				}
				else
				{
					alert("暂时没有奖学金列表，无法进行奖学金评定，请联系学院管理员进行添加~");
					$("#students").hide(); $("#div-class").hide(); $("#search").hide(); 
					$("#error").show();
					return false;
				}
			}
			else
			{
				$("#students").hide(); $("#div-class").hide(); $("#search").hide(); 
				$("#error").show();
				return false;
			}
		}
	);
}

//填充测评班级下拉框和奖学金下拉框
function addSelectClass()
{
	var TeClass = {};
	TeClass["teacherid"] = "";
	var date = new Date();
	TeClass["year"] = date.getFullYear();
	$.get(
		"../../../testdrive/index.php?r=zing/getTeClass",
		{TeClass: $.toJSON(TeClass)},
		function(data)
		{
			var obj = $.evalJSON(data);
			if(obj.success == true)
			{
				if(obj.results.length != 0)
				{
					var selector=$('#selectClass');  
					for (var i = 0; i < obj.results.length; i++)
					{
						selector.append('<option value="'+obj.results[i].t_classid+'">'+obj.results[i].name+'</option>');
					}
					
					//添加奖学金
					addScholarship();
				}
				else
				{
					alert("您暂时没有测评班级列表，无需进行奖学金评定~");
					$("#students").hide(); $("#div-class").hide(); $("#search").hide(); 
					$("#error").show();
					return false;
				}
			}
			else
			{
				//alert('服务器端返回错误信息=>"'+obj.message+'"');
				if(obj.results.hasOwnProperty("timeout") && obj.results.timeout)
					{
						alert(obj.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href='../../../';
						return false;
					}
				alert(obj.message);
				$("#students").hide(); $("#div-class").hide(); $("#search").hide(); 
				$("#error").show();
				return false;
			}
		}
	);
}

function adjustHeight()
{
	var height = document.body.clientHeight;
	//alert(height);
	if(height + 25 > 420)
		parent.parent.document.getElementById("frame_content").height=height + 50;
	else
		parent.parent.document.getElementById("frame_content").height=420;	
}
