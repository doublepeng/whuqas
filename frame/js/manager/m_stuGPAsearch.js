//设置班级列表的分页
var totalRecord = 0;
var pages = 0;
var currentPage = 1;
//设置学生成绩列表
var totalRecord1 = 0;
var pages1 = 0;
var currentPage1 = 1;
   
//记录当前选择的是哪个班级
var currentMem = "";    
$(document).ready(function() {
	parent.parent.document.getElementById("frame_content").height=420;
	//获取查询年级
	setSearchGrade();
	//获取查询学年
	setSearchYear();
	//载入一个学院的全部专业
	loadMajor();
	//显示班级
	
	//改变年份-
	changeYear();
	//改变年级
	changeGrade();
	//改变专业
	changeMajor();
	
	//设置记录页面数
	setRecordPage1();
	setRecordPage();
	//调整屏幕的高度
	parent.parent.document.getElementById("frame_content").height=420;	
	//设置班级列表的分页显示
	//隐藏翻页按钮
	$("#btLast").hide(); $("#btNext").hide(); $("#btFormer").hide(); $("#btFirst").hide(); 
	//隐藏第一行
	$("#stu-table tr").eq(1).hide();
	//$("#classes").hide();
	//设置学生成绩列表的分页显示
	$("#users-contain").hide();
	//隐藏翻页按钮
	$("#btLast1").hide(); $("#btNext1").hide(); $("#btFormer1").hide(); $("#btFirst1").hide(); 
	//隐藏第一行
	$("#tb tr").eq(1).hide();
	//初始化时，需要加载一个默认的表格
	
	
	//返回到班级列表
/*	$("#btBack").click(function(){
		$("#tb tbody tr").each(function(){ $(this).remove(); });
		$("#users-contain").hide();
		$("#classes").show();
		var height = document.documentElement.scrollHeight>document.body.scrollHeight?document.body.scrollHeight:document.documentElement.scrollHeight; 
		if(height > 460)
			parent.parent.document.getElementById("frame_content").height=height;
		else
			parent.parent.document.getElementById("frame_content").height=465;	
	});*/
	
	$("#btLast").click(function() 
	{
		if(currentPage != pages)
		{
			//alert("即将跳转到最后一页");
			currentPage = pages;
			ShowPage(currentPage*10-9, totalRecord);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btNext").click(function() 
	{
		if(currentPage+1<=pages)
		{
			//alert("即将跳转到下一页");
			currentPage += 1;
			ShowPage(currentPage*10-9, currentPage*10);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btFormer").click(function() 
	{
		if(currentPage-1>0)
		{
			//alert("即将跳转到上一页");
			currentPage -= 1;
			ShowPage(currentPage*10-9, currentPage*10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
	$("#btFirst").click(function() 
	{
		if(currentPage != 1)
		{
			//alert("即将跳转到首页");
			currentPage = 1;
			ShowPage(1, 10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
	$("#btLast1").click(function() 
	{
		if(currentPage1 != pages1)
		{
			//alert("即将跳转到最后一页");
			currentPage1 = pages1;
			ShowPage1(currentPage1*10-7, totalRecord1);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btNext1").click(function() 
	{
		if(currentPage1+1<=pages1)
		{
			//alert("即将跳转到下一页");
			currentPage1 += 1;
			ShowPage1(currentPage1*10-7, currentPage1*10);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btFormer1").click(function() 
	{
		if(currentPage1-1>0)
		{
			//alert("即将跳转到上一页");
			currentPage1 -= 1;
			ShowPage1(currentPage1*10-7, currentPage1*10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
	$("#btFirst1").click(function() 
	{
		if(currentPage1 != 1)
		{
			//alert("即将跳转到首页");
			currentPage1 = 1;
			ShowPage1(1, 10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
	$("#BtStuBack").click(function(){
		$("#tb tr:gt(1)").each(function(){$(this).remove();});
		$("#users-contain").hide();
		$("#classes").show();
		adjustHeight();
	});
})

function checkAMem()
{
	//alert(currentMem);
	$("#classes").hide();
	$("#users-contain").show();
	loadSelfEva();
}
//将学生成绩加载到表中
function loadSelfEva()
{
	parent.parent.document.getElementById("frame_content").height=420;	
	var displayScore = {};
	displayScore["classid"] = currentMem;
	displayScore["year"] = $("#selectYear").find("option:selected").val();
	displayScore["grade"] = $("#selectGrade").find("option:selected").text();  
	//alert($.toJSON(displayScore));
	$.get(
		
		 "../../../testdrive/index.php?r=Score/DisplayScore",
		{displayScore:$.toJSON(displayScore)},
		function(data)
		{
			//alert(data);
			var ret = $.evalJSON(data);
			if(ret.success = false)
			{
if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
					{
						alert(ret.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href = '../../../';
						return false;
					}
				alert('服务器端返回错误信息=>"'+ret.message+'"');
			}
			var tempRecords1 = 0;
			var normalStudent = ret.results;
			$("#tb tbody tr:gt(0)").remove();
			if (normalStudent.length ==0)
			{alert("该班级没有学生成绩信息，请添加~~");}
			else
			{
				for(var i = 0; i < normalStudent.length; i++)
				{
					var row = $("#tb tbody tr:first").clone();	
					row.find("td").get(0).innerHTML = normalStudent[i]["studentid"];
					row.find("td").get(1).innerHTML = normalStudent[i]["studentname"];
					row.find("td").get(2).innerHTML = normalStudent[i]["score1"];
					row.find("td").get(3).innerHTML = normalStudent[i]["score2"];
					row.find("td").get(4).innerHTML = normalStudent[i]["score3"];
					row.find("td").get(5).innerHTML = normalStudent[i]["score4"];
					row.show();
					row.appendTo("#tb tbody");
					tempRecords1++;
				}
				setRecordPage1(tempRecords1);
				currentPage1 = 1;
				$("#tb tbody tr:gt(10)").each(function() {
					$(this).hide();
				});
				$("#showPage1").html(currentPage1+"/"+pages1);
			}
			adjustHeight();
		}
	);
}

//设置页数和总记录数
function setRecordPage(number)
{
	totalRecord = number;
	if(number <= 0)
	{
		totalRecord = 0;
		pages = 0;
	}
	else
	{//每页有10条记录
		pages = parseInt(number/10);
		if(number%10 != 0)
			pages += 1;
	}
	//页数>1 需翻页按钮
	if(pages > 1)
	{
		$("#btLast").show(); $("#btNext").show(); $("#btFormer").show(); $("#btFirst").show(); 
	}
}
//设置页数和总记录数
function setRecordPage1(number)
{
	totalRecord1 = number;
	if(number <= 0)
	{
		totalRecord1 = 0;
		pages1 = 1;
	}
	else
	{//每页有10条记录
		pages1 = parseInt(number/10);
		if(number%10 != 0)
			pages1 += 1;
	}
	//页数>1 需翻页按钮
	//alert(pages1);
	if(pages1 > 1)
	{
		$("#btLast1").show(); $("#btNext1").show(); $("#btFormer1").show(); $("#btFirst1").show(); 
	}
	/*else
	{
		$("#btLast1").hide(); $("#btNext1").hide(); $("#btFormer1").hide(); $("#btFirst1").hide(); 
	}*/
}

//设置下拉框中的学年
function setSearchYear()
{
	  var years;
	  var today;
	  today = new Date();
	  Years = today.getFullYear();
	  var intYears;
	  intYears = parseInt(Years);
	  var selector=$('#selectYear'); 
	  var j;
	  for(var i=intYears-4; i< intYears +4;i++)
	  {
		 j=i+1;
		 if(i == intYears)
			selector.append('<option selected="selected" value='+j+'>'+i+'-'+j+'</option>');  	 
		  else
			selector.append('<option value='+j+'>'+i+'-'+j+'</option>');  
	  } 
}

//设置下拉框中的年级
function setSearchGrade()
{
	  var years;
	  var today;
	  today = new Date();
	  Years = today.getFullYear();
	  var intYears;
	  intYears = parseInt(Years);
	  var selector=$('#selectGrade'); 
	  for(var i=intYears-4; i< intYears +4;i++)
	  {
		  if(i == intYears)
			selector.append('<option selected="selected" value="intYears">'+i+'</option>');  	 
		  else
			selector.append('<option value="intYears">'+i+'</option>');  
	  }     
}

//载入一个学院的全部专业
function loadMajor()
{
		$.get(  
			////frame/js/manager/m_PHP/m_stuGPAsearch.php
			 "../../../testdrive/index.php?r=Major/DisplayMajorByGrade",
			
			function(data)
			{
					var obj = $.evalJSON(data);
					if(obj.success == false)
					{
if(obj.results.hasOwnProperty("timeout") && obj.results.timeout)
					{
						alert(obj.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href = '../../../';
						return false;
					}
						alert("服务器端返回错误信息=>\""+obj.message+'"');
						return false;
					}
					var selector=$('#selectMajor'); 
					for (var i = 0; i < obj.results.length; i++)
					{
						//alert(obj.results[i]["majorid"]);
						selector.append('<option value="'+obj.results[i]["majorid"]+'">'+obj.results[i]["majorname"]+'</option>'); 
					}
					loadClass();
			}							
		);	
}


function loadClass()
{
	//$("#selectMajor").change(function(){
		var selected =   $("#selectMajor").find("option:selected").text();
		var value = $("#selectMajor").find("option:selected").val();
		//alert(value);
		/*if(value == -1)
		{		
			$("#stu-table tbody tr:gt(0)").each(function(){ $(this).remove(); });
		}
		else*/
		/*$("#stu-table tbody tr:gt(0)").each(function(){ $(this).remove(); });*/
		var displayClassByMajor = {};
		displayClassByMajor["year"] = $('#selectYear').find('option:selected').val();
		displayClassByMajor["grade"] = $('#selectGrade').find('option:selected').text();
		displayClassByMajor["major"] = $('#selectMajor').find('option:selected').text();
		//alert($.toJSON(displayClassByMajor));
		$.get(
			  
			  "../../../testdrive/index.php?r=WhuClass/DisplayClassByMajor",
			  {displayClassByMajor:$.toJSON(displayClassByMajor)},
			  function(data)
					{
						var obj = $.evalJSON(data);
						if(obj.success == false)
						{
if(obj.results.hasOwnProperty("timeout") && obj.results.timeout)
					{
						alert(obj.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href = '../../../';
						return false;
					}
							alert("服务器端返回错误信息=>\""+obj.message+'"');
							return false;
						}
						setRecordPage(obj.results.length);
						if(obj.results.length==0)
						{alert("没有相应的班级信息，请重新选定年份、年级及专业~~");}
						else
						{
							for (var i = 0; i < obj.results.length; i++)
							{
								var tr = $("#stu-table tr").eq(1).clone();
								tr.find("td").get(0).innerHTML = '<h1 style="font-size:12px; font-weight:normal" name="'+ obj.results[i]['classid']+'">'+obj.results[i]['classname']+'</h1>';
								//tr.find("td").get(0).val() ;
								
								tr.find("td").get(1).innerHTML = obj.results[i]['teachername'];
								tr.show();
								tr.appendTo("#stu-table tbody");
							}
							currentPage = 1;
							$("#stu-table tbody tr:gt(10)").each(function() {
								$(this).hide();
							});
							$("#showPage").html(currentPage+"/"+pages);
						}
						adjustHeight();
					}
		);
		
	//});		
}
//下拉框改变时，
function changeYear()
{
	$("#selectYear").change(function(){		
		$("#stu-table tbody tr:gt(0)").each(function(){ $(this).remove(); });
		loadClass();
		//changeMajor();		
	});
}
function changeGrade()
{
	$("#selectGrade").change(function(){		
		$("#stu-table tbody tr:gt(0)").each(function(){ $(this).remove(); });
		loadClass();
		//changeMajor();
	});		
}
function changeMajor()
{
	$("#selectMajor").change(function(){
		//var selected =   $("#selectGrade").find("option:selected").text();
		$("#stu-table tbody tr:gt(0)").each(function(){ $(this).remove(); });
		loadClass();
		//changeMajor();
	});		
}
function adjustHeight()
{
	var height = document.documentElement.scrollHeight>document.body.scrollHeight
						? document.body.scrollHeight:document.documentElement.scrollHeight; 
	//alert("before "+height);
	if(height + 50 > 420)
		parent.parent.document.getElementById("frame_content").height=height + 50;
	else
		parent.parent.document.getElementById("frame_content").height=420;	
}
//传入需要被显示的数据的位置
//仅显示_min 和 _max之间的数据
function ShowPage(_min, _max)
{
	$("#stu-table tbody tr").each(function() {
		$(this).show();
	});
	
	var variable = "#stu-table tbody tr:lt("+_min+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	variable = "#stu-table tbody tr:gt("+_max+")";
	$(variable).each(function() {
		$(this).hide();
	});
	$("#showPage").html(currentPage+"/"+pages);
	adjustHeight();
}
//传入需要被显示的数据的位置
//仅显示_min 和 _max之间的数据
function ShowPage1(_min, _max)
{
	$("#tb tbody tr").each(function() {
		$(this).show();
	});
	
	var variable = "#tb tbody tr:lt("+_min+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	variable = "#tb tbody tr:gt("+_max+")";
	$(variable).each(function() {
		$(this).hide();
	});
	$("#showPage1").html(currentPage1+"/"+pages1);
	adjustHeight();
}




//检查已添加条目的正确性
function updateTips( t ) {
	$( ".validateTips")
		.text( t )
		.addClass( "ui-state-highlight" );
	setTimeout(function() {
		$( ".validateTips").removeClass( "ui-state-highlight", 1500 );
		$( ".validateTips" ).text("");
	}, 1500 );
}

function checkLength( o, n, min, max ) {
	if ( o.val().length > max || o.val().length < min ) {
		o.addClass( "ui-state-error" );
		updateTips( '提示: " '+n+' "' + " 的长度应该在 " +
			min + " 和 " + max + "之间." );
		return false;
	} else {
		return true;
	}
}

function checkValue( o, n, min, max ) {
	if ( parseFloat(o.val()) > max || parseFloat(o.val()) <= min) {
		o.addClass( "ui-state-error" );
		updateTips( '" '+n+' "' + " 的值 " +
			min + " 和 " + max + "之间." );
		return false;
	} else {
		return true;
	}
}

function checkRegexp( o, regexp, n ) {
	if ( !( regexp.test( o.val() ) ) ) {
		o.addClass( "ui-state-error" );
		updateTips( n );
		return false;
	} else {
		return true;
	}
}

function validator()
{
/*	if($("#tb tr").length <= 2)
	{
		updateTips( "您还没有添加新纪录，无需保存" );
		return false;
	}
		
	var totalRows = new Array();
	var success = true;
	$("#tb tr:gt(1)").each(function() 
	{
		var selectType = $(this).find("#selectType"),
		TContent = $(this).find("#TContent"),
		TScore = $(this).find("#TScore"),
		allFields = $( [] ).add( selectType ).add( TContent ).add( TScore );
	
		var type = selectType.find("option:selected").text();
		var content = TContent.val();
		var score = TScore.val();
	
		var bValid = true;
		allFields.removeClass( "ui-state-error" );
		bValid = bValid && checkLength( TContent, "成果", 2, 50 );
		bValid = bValid && checkLength( TScore, "得分", 1, 5);
		bValid = bValid && checkRegexp( TContent, /^[\u4e00-\u9fa5\da-zA-Z\-\_]+$/, "\"成果名称\"不能包含特殊字符，请输入中文、英文单词或者数字" );
		bValid = bValid && checkRegexp( TScore, /^[0-9]+.?[0-9]*$/, "\"得分\"应该输入合法数字" );
		bValid = bValid && checkValue( TScore, "得分", 0, 20);
		
		if ( ! bValid ) {
			success = false;
			return false;
		}
	});*/
	return success;
}








