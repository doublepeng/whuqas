// JavaScript Document
$(document).ready(function() {
	parent.parent.document.getElementById("frame_content").height=420;	

	$("#BtSave").click(function ()
	{
		if(validator())
		{
			modifyPass();
		}
	});
	
	$("#BtDel").click(function ()
	{
		window.location.href = "../s_standard.html";
	});
})

function modifyPass()
{
	var modifyPass = {};
	modifyPass["managerid"] = ""; //获取管理员教工号
	modifyPass["oldpass"] = $.md5($("#oldpass").val());
	modifyPass["newpass"] = $.md5($("#newpass1").val());
	//alert($.toJSON(modifyPass));
	$.get(
		"../../../testdrive/index.php?r=zing/moStuPass",
		{"modifyPass": $.toJSON(modifyPass)},
		function (data)
		{
			var ret = $.evalJSON(data);
			if(ret.success == true)
			{
				alert("修改密码成功");
				$("#oldpass").val("");
				$("#newpass1").val("");
				$("#newpass2").val("");
			}
			else
			{
if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
					{
						alert(ret.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href = '../../../';
						return false;
					}
				alert('服务器端返回错误信息=>"'+ret.message+'"');
			}
		}
	);
}

//检查已添加条目的正确性

function updateTips( t ) {
	$( ".validateTips")
		.text( t )
		.addClass( "ui-state-highlight" );
	setTimeout(function() {
		$( ".validateTips").removeClass( "ui-state-highlight", 1500 );
		$( ".validateTips" ).text("");
	}, 1500 );
}

function checkLength( o, n, min, max ) {
	if ( o.val().length > max || o.val().length < min ) {
		o.addClass( "ui-state-error" );
		updateTips( '提示: " '+n+' "' + " 的长度应该在 " +
			min + " 和 " + max + "之间." );
		return false;
	} else {
		return true;
	}
}

function checkValue( o, n, min, max ) {
	if ( parseFloat(o.val()) > max || parseFloat(o.val()) <= min) {
		o.addClass( "ui-state-error" );
		updateTips( '" '+n+' "' + " 的值 " +
			min + " 和 " + max + "之间." );
		return false;
	} else {
		return true;
	}
}

function checkRegexp( o, regexp, n ) {
	if ( !( regexp.test( o.val() ) ) ) {
		o.addClass( "ui-state-error" );
		updateTips( n );
		return false;
	} else {
		return true;
	}
}

function validator()
{
	var oldpass = $("#oldpass"),
	newpass1 = $("#newpass1"),
	newpass2 = $("#newpass2"),
	allFields = $( [] ).add( oldpass ).add( newpass1 ).add( newpass2 );

	var bValid = true;
	allFields.removeClass( "ui-state-error" );
	bValid = bValid && checkLength( oldpass, "旧密码", 6, 25 );
	bValid = bValid && checkLength( newpass1, "新密码", 6, 25);
	bValid = bValid && checkLength( newpass2, "新密码", 6, 25);

	if ( ! bValid ) {
		return false;
	}
	
	if(newpass1.val() != newpass2.val())
	{
		updateTips("您输入的新密码不一致~");
		return false;
	}
	
	return true;
}

