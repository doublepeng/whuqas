// JavaScript Document
var allFields;
var currentMem;  //用来记录当前是哪一行的select框改变了。

$(document).ready(function() {
	parent.parent.document.getElementById("frame_content").height=420;	
	
	//隐藏模板tr
	$("#tb tr").eq(1).hide();
	var i = 0;
		
	//加载年级
	loadGrade();
	
	$("#BtAdd").click(function() {
　　//复制一行
		var tr = $("#tb tr").eq(1).clone();
		tr.find("td").get(0).innerHTML = ++i;
		tr.show();
		tr.appendTo("#tb tbody");
		
		$("#tb tbody tr:last :input:last").keypress(function(event){
			//tab enter
			if(event.keyCode==13 || event.keyCode==9){
				$("#BtAdd").click();
			}
		});
		
		adjustHeight();
	});
	
	//删除所选记录按钮
	$("#BtDel").click(function() {
		var selected = false;
		$("#tb tr:gt(1)").each(function() {
			if ($(this).find("#CK").get(0).checked == true) {
				selected = true;
				return false;
			}
		 });
		if(!selected)
		{
			alert("请您选择要删除的学生信息");
			return false;
		}
		
		$("#tb tr:gt(1)").each(function() {
			if ($(this).find("#CK").get(0).checked == true) {
				$(this).remove();
			}
		});
		
		adjustHeight();
			
		i = 0;
		$("#tb tr:gt(1)").each(function() {
			$(this).find("td").get(0).innerHTML = ++i;
		});
		$("#CKA").attr("checked", false);
		
	});
	
	//选择所有行
	$("#CKA").click(function() {
		$("#tb tr:gt(1)").each(function() {
			$(this).find("#CK").get(0).checked = $("#CKA").get(0).checked;
		});
	});
	
	//保存记录
	$("#BtSave").click(function() {		
		if(validator())
		{
      var saveStudentInfo = [];
      var success = true;
			$("#tb tr:gt(1)").each(function() {
				if ($(this).find("#selectStudentName").attr("name")=="1")	
				{
          //2012-02-19 modified by minzhenyu
          if($(this).find("#selectMajor").val() == null)
          {
            alert("您还没有选择专业！");
            success = false;
            return false;
          }		
          if($(this).find("#selectClass").val() == null)
          {
            alert("您还没有选择班级！");
            success = false;
            return false;
          }	

					var SaveRow = {};				
					SaveRow['studentid'] = $(this).find("#selectStudentNum").val();
					SaveRow['studentname'] = $(this).find("#selectStudentName").val();
					SaveRow['grade'] = $(this).find("#selectGrade").find('option:selected').text();
					SaveRow['major'] = $(this).find("#selectMajor").val();
					SaveRow['class'] = $(this).find("#selectClass").val();
					saveStudentInfo.push(SaveRow);
				}
				//$(this).find("#selectStudentName").attr("name","0");
			});
			if(!success) return false;

			var json = $.toJSON(saveStudentInfo);
			alert(json);
			$.post(
				"../../../testdrive/index.php?r=Student/SaveStudentInfo",
			   {
				   saveStudentInfo: json
			   },
			   function(data)
			   {
					var obj = $.evalJSON(data);
					if(obj.success == false)
					{
	if(obj.results.hasOwnProperty("timeout") && obj.results.timeout)
					{
						alert(obj.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href = '../../../';
						return false;
					}
						alert("保存记录失败！"+obj.message);
						return false;
					}
					else
					{
						alert("保存学生基本信息成功！");
						$("#tb tr:gt(1)").each(function() {
              if ($(this).find("#selectStudentName").attr("name")=="1")	
              {
                $(this).find("#selectStudentName").attr("name","0");
              }
            });
					}
					
				}
			);
		}
	});
})

//设置下拉框中的年级
function loadGrade()
{
	  var years;
	  var today;
	  today = new Date();
	  Years = today.getFullYear();
	  var intYears;
	  intYears = parseInt(Years);
	  var selector=$('#selectGrade'); 
	  for(var i=intYears-4; i< intYears +4;i++)
	  {
		  if(i == intYears)
			selector.append('<option selected="selected" value="intYears">'+i+'</option>');  	 
		  else
			selector.append('<option value="intYears">'+i+'</option>');  
	  } 
	  loadMajor();
}
//载入一个学院的全部专业
function loadMajor()
{
	$.get(  
		"../../../testdrive/index.php?r=WhuClass/DisplayMajorToList",
		function(data)
		{
			var obj = $.evalJSON(data);
			if(obj.success == false)
			{
if(obj.results.hasOwnProperty("timeout") && obj.results.timeout)
					{
						alert(obj.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href = '../../../';
						return false;
					}
				alert("服务器端返回错误信息=>\""+obj.message+'"');
				return false;
			}
			if(obj.results.length == 0)
			{
        updateTips("学院专业信息列表为空，请选择“添加专业信息”进行添加！");
			}
			var selector=$('#selectMajor'); 
			for (var i = 0; i < obj.results.length; i++)
			{
				selector.append('<option value="'+obj.results[i]["majorid"]+'">'+obj.results[i]["majorname"]+'</option>'); 
			}
      
      currentMem = $("#tb tbody tr").eq(0);
			loadClass();
			$("#BtAdd").click();	
		}							
	);	
}

function loadClass()
{	
  currentMem.find('#selectClass').eq(0).find("option").remove(); 
	var displayClassByYearByMajor = {};
	displayClassByYearByMajor['grade'] = currentMem.find('#selectGrade').eq(0).find('option:selected').text();
	displayClassByYearByMajor['major'] = currentMem.find('#selectMajor').eq(0).find('option:selected').text();
	//alert( $.toJSON(displayClassByYearByMajor));
        
	$.ajax({
		url: '../../../testdrive/index.php?r=WhuClass/DisplayClassByYearByMajor',
		data: {displayClassByYearByMajor : $.toJSON(displayClassByYearByMajor)},
		async: false, 
		type: 'post',
		success: function(data)
		{
			var obj = $.evalJSON(data);
			if(obj.success == false)
			{
if(obj.results.hasOwnProperty("timeout") && obj.results.timeout)
					{
						alert(obj.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href = '../../../';
						return false;
					}
				alert("服务器端返回错误信息=>"+obj.message);
				return false;
			}
			if(obj.results.length == 0)
			{
        updateTips("班级信息列表为空，请选择“添加班级信息”进行添加！");
			}
			var selector=currentMem.find('#selectClass').eq(0); 
			for (var i = 0; i < obj.results.length; i++)
			{
				selector.append('<option value="'+obj.results[i]["classid"]+'">'+obj.results[i]["classname"]+'</option>');
			}
			
		}
	});
}


//检查已添加条目的正确性

function updateTips( t ) {
	$( ".validateTips")
		.text( t )
		.addClass( "ui-state-highlight" );
	setTimeout(function() {
		$( ".validateTips").removeClass( "ui-state-highlight", 1500 );
		$( ".validateTips" ).text("");
	}, 3000 );
}

function checkLength( o, n, min, max ) {
	if ( o.val().length > max || o.val().length < min ) {
		o.addClass( "ui-state-error" );
		updateTips( '提示: " '+n+' "' + " 的长度应该在 " +
			min + " 和 " + max + "之间." );
		return false;
	} else {
		return true;
	}
}

function checkRegexp( o, regexp, n ) {
	if ( !( regexp.test( o.val() ) ) ) {
		o.addClass( "ui-state-error" );
		updateTips( n );
		return false;
	} else {
		return true;
	}
}

function validator()
{
	if($("#tb tr").length <= 2)
	{
		updateTips( "您还没有添加新纪录，无需保存" );
		return false;
	}

	var success = true;
	$("#tb tr:gt(1)").each(function() 
	{
		var selectStudentName = $(this).find("#selectStudentName"),
		selectStudentNum = $(this).find("#selectStudentNum"),

		allFields = $( [] ).add( selectStudentName ).add(selectStudentNum); 
	
		var bValid = true;
		allFields.removeClass( "ui-state-error" );
		bValid = bValid && checkLength( selectStudentNum, "学生学号", 2, 20);
		bValid = bValid && checkRegexp( selectStudentNum, /^\d+(.\d+$|\d*$)/, "\"学生学号\"应该输入合法数字" );
		bValid = bValid && checkLength( selectStudentName, "学生姓名", 2, 40);
		//bValid = bValid && checkRegexp( selectStudentName, /^[\u4e00-\u9fa5\da-zA-Z\-\_]+$/, "\"学生姓名\"不能包含特殊字符，请输入中文、英文单词或者数字" );
		
		if ( ! bValid ) {
			success = false;
			return false;
		}
	});
	return success;
}

function adjustHeight()
{
	var height = document.body.clientHeight;
	//alert(height);
	if(height > 420)
		parent.parent.document.getElementById("frame_content").height=height + 40;
	else
		parent.parent.document.getElementById("frame_content").height=420;	
}




