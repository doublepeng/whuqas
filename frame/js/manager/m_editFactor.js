var allFields;

$(function() {
	parent.parent.document.getElementById("frame_content").height=420;
	
	//隐藏测评班级测评系数
	$("#tclass").hide();
	
	//获取查询年级
	setSearchGrade();
	
	showTable();
	
	//改变年级
	changeGrade();
	//改变测评班级
	changeTclass();
	
	//保存记录
	$("#BtSave").click(function() {
		if(validatorTB1())   /* */
		{
			//var saveEditFactor = [];
			var SaveRow = {};
			SaveRow['factor1'] = $("#f1 #factor1").val();
			SaveRow['factor2'] = $("#f2 #factor2").val();
			SaveRow['factor3'] = $("#f3 #factor3").val();
			//saveEditFactor.push(SaveRow);
			//var json = $.toJSON(saveEditFactor);
			var json = $.toJSON(SaveRow);
			//alert(json);
			$.post(
			   "../../../testdrive/index.php?r=department/SaveEditFactor",
			   {
				   saveEditFactor: json
			   },
			   function(data)
			   {
					var obj = $.evalJSON(data);
					if(obj.success == false)
					{
if(obj.results.hasOwnProperty("timeout") && obj.results.timeout)
					{
						alert(obj.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href = '../../../';
						return false;
					}
						alert("保存记录失败！"+obj.message);
						return false;
					}
					else
					{
						alert("保存测评系数成功~~");
						//return true;
					}
				}
			);
		}
	});
	
	$("#BtSave1").click(function() {
		if(validatorTB2())   /* */
		{
			//var saveEditTclassFactor = [];
			var SaveRow = {};
			SaveRow['grade'] = $('#selectGrade').find('option:selected').text();
			SaveRow['t_classid'] = $('#selectT_class').find('option:selected').val();
			SaveRow['factor1'] = $("#fa1 #factor1").val();
			SaveRow['factor2'] = $("#fa2 #factor2").val();
			SaveRow['factor3'] = $("#fa3 #factor3").val();
			//saveEditTclassFactor.push(SaveRow);
			//var json = $.toJSON(saveEditTclassFactor);
			var json = $.toJSON(SaveRow);
			//alert(json);
			$.post(
			  "../../../testdrive/index.php?r=department/SaveEditTclassFactor",
			   {
				   saveEditTclassFactor: json
			   },
			   function(data)
			   {
					var obj = $.evalJSON(data);
					if(obj.success == false)
					{
if(obj.results.hasOwnProperty("timeout") && obj.results.timeout)
					{
						alert(obj.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href = '../../../';
						return false;
					}
						alert("保存记录失败！"+obj.message);
						return false;
					}
					else
					{
						alert("保存测评系数成功！");
						//return true;
					}
				}
			);
		}
	});
	
	$("#look").click(function(){
		$("#tclass").show();
		$("#dep").hide();
		//进入到查看测评班级系数的逻辑
		//加载测评班级
		loadTclass();
	});
	
	$("#BtStuBack").click(function(){
		$("#tclass").hide();
		$("#dep").show();						   
	});
});

function showTable()
{
	$.get(
		
		"../../../testdrive/index.php?r=Department/SetFactor",
		function(data)
		{
			var obj = $.evalJSON(data);
			if(obj.success == false)
			{
if(obj.results.hasOwnProperty("timeout") && obj.results.timeout)
					{
						alert(obj.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href = '../../../';
						return false;
					}
				alert("服务器端返回错误信息=>"+obj.message);
				return false;
			}
			
			if(obj.results.length == 0)
			{
        updateTips("学院尚未设置测评系数！");
        return false;
      }
        
			$("#f1 input").val(obj.results[0]['F1']);				
			$("#f2 input").val(obj.results[0]['F2']);
			$("#f3 input").val(obj.results[0]['F3']);
		}
	);
}
//设置下拉框中的年级
function setSearchGrade()
{
	  var years;
	  var today;
	  today = new Date();
	  Years = today.getFullYear();
	  var intYears;
	  intYears = parseInt(Years);
	  var selector=$('#selectGrade'); 
	  for(var i=intYears-4; i< intYears +4;i++)
	  {
		  if(i == intYears)
			selector.append('<option selected="selected" value="'+i+'">'+i+'</option>');  	 
		  else
			selector.append('<option value="'+i+'">'+i+'</option>');  
	  }     
}

//载入测评班级
function loadTclass()
{
  $("#selectT_class option").remove();
  
	var displayT_class = {};
	displayT_class['grade'] = $('#selectGrade').find('option:selected').text();
	//alert( $.toJSON(displayT_class));
	$.get(  
		"../../../testdrive/index.php?r=Department/DisplayTClass",
		{
			displayT_class : $.toJSON(displayT_class)
		},
		function(data)
		{
			var obj = $.evalJSON(data);
			if(obj.success == false)
			{
if(obj.results.hasOwnProperty("timeout") && obj.results.timeout)
					{
						alert(obj.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href = '../../../';
						return false;
					}
				alert("服务器端返回错误信息=>\""+obj.message+'"');
				return false;
			}
			if(obj.results.length != 0)
			{
				var selector=$('#selectT_class'); 
				for (var i = 0; i < obj.results.length; i++)
				{
					/*alert(obj.results[i]["majorid"]);*/
					selector.append('<option value="'+obj.results[i]["t_classid"]+'">'+obj.results[i]["t_classname"]+'</option>'); 
				}
				//加载测评班级系数
				loadTclassFactor();
			}
			else
			{
				updateTips1("对不起，没有"+$("#selectGrade").val()+"级的测评班级信息~");
			}
		}							
	);	
}
function loadTclassFactor()
{
  $("#fa1 input").val("");				
	$("#fa2 input").val("");
	$("#fa3 input").val("");
			
	var displayFactorByT_class = {};
	displayFactorByT_class['grade'] = $('#selectGrade').find('option:selected').text();
	displayFactorByT_class['t_classid'] = $('#selectT_class').val();
	//alert($.toJSON(displayFactorByT_class));
	$.get(
		"../../../testdrive/index.php?r=Department/DisplayFactorByTClass",
		{
			displayFactorByT_class : $.toJSON(displayFactorByT_class)
		},
		function(data)
		{
			//alert(data);
			var obj = $.evalJSON(data);
			if(obj.success == false)
			{
if(obj.results.hasOwnProperty("timeout") && obj.results.timeout)
					{
						alert(obj.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href = '../../../';
						return false;
					}
				alert("服务器端返回错误信息=>"+obj.message);
				return false;
			}
			
			if(obj.results.length == 0)
			{
        updateTips1("测评班级尚未设置测评系数！");
				return false;
			}
			
			$("#fa1 input").val(obj.results[0]['factor1']);				
			$("#fa2 input").val(obj.results[0]['factor2']);
			$("#fa3 input").val(obj.results[0]['factor3']);
		}
	);
}

//改变学年
function changeGrade()
{
	$("#selectGrade").change(function(){
    $("#fa1 input").val("");				
    $("#fa2 input").val("");
    $("#fa3 input").val("");
		loadTclass();
	});		
}

function changeTclass()
{
	$("#selectT_class").change(function(){
		loadTclassFactor();
	});
}

//检查已添加条目的正确性
function updateTips1( t ) {
	$( ".validateTips1")
		.text( t )
		.addClass( "ui-state-highlight" );
	setTimeout(function() {
		$( ".validateTips1").removeClass( "ui-state-highlight", 1500 );
		$( ".validateTips1" ).text("");
	}, 3000 );
}

function checkLength1( o, n, min, max ) {
	if ( o.val().length > max || o.val().length < min ) {
		o.addClass( "ui-state-error" );
		updateTips1( '提示: " '+n+' "' + " 的长度应该在 " +
			min + " 和 " + max + "之间." );
		return false;
	} else {
		return true;
	}
}

function checkValue1( o, n, min, max ) {
	if ( parseFloat(o.val()) > max || parseFloat(o.val()) < min) {
		o.addClass( "ui-state-error" );
		updateTips1( '" '+n+' "' + " 的值 " +
			min + " 和 " + max + "之间." );
		return false;
	} else {
		return true;
	}
}

function checkRegexp1( o, regexp, n ) {
	if ( !( regexp.test( o.val() ) ) ) {
		o.addClass( "ui-state-error" );
		updateTips1( n );
		return false;
	} else {
		return true;
	}
}

//检查已添加条目的正确性
function updateTips( t ) {
	$( ".validateTips")
		.text( t )
		.addClass( "ui-state-highlight" );
	setTimeout(function() {
		$( ".validateTips").removeClass( "ui-state-highlight", 1500 );
		$( ".validateTips" ).text("");
	}, 3000 );
}

function checkLength( o, n, min, max ) {
	if ( o.val().length > max || o.val().length < min ) {
		o.addClass( "ui-state-error" );
		updateTips( '提示: " '+n+' "' + " 的长度应该在 " +
			min + " 和 " + max + "之间." );
		return false;
	} else {
		return true;
	}
}

function checkValue( o, n, min, max ) {
	if ( parseFloat(o.val()) > max || parseFloat(o.val()) < min) {
		o.addClass( "ui-state-error" );
		updateTips( '" '+n+' "' + " 的值 " +
			min + " 和 " + max + "之间." );
		return false;
	} else {
		return true;
	}
}

function checkRegexp( o, regexp, n ) {
	if ( !( regexp.test( o.val() ) ) ) {
		o.addClass( "ui-state-error" );
		updateTips( n );
		return false;
	} else {
		return true;
	}
}

function validatorTB1()
{
	var F1 = $("#f1 #factor1"),
		F2 = $("#f2 #factor2"),
		F3 = $("#f3 #factor3"),
		allFields = $( [] ).add( F1 ).add( F2 ).add( F3 );

	var bValid = true;
	allFields.removeClass( "ui-state-error" );
	
	bValid = bValid && checkLength( F1, "F1", 1, 5);
	bValid = bValid && checkRegexp( F1, /^\d+(.\d+$|\d*$)/, "\"F1\"应该输入合法数字" );
	
	bValid = bValid && checkLength( F2, "F2", 1, 5);
	bValid = bValid && checkRegexp( F2, /^\d+(.\d+$|\d*$)/, "\"F2\"应该输入合法数字" );
	
	bValid = bValid && checkLength( F3, "F3", 1, 5);
	bValid = bValid && checkRegexp( F3, /^\d+(.\d+$|\d*$)/, "\"F3\"应该输入合法数字" );
		
	if ( ! bValid ) {
		return false;
	}
	
	
	var f1 = parseFloat($("#f1 #factor1").val());
	var f2 = parseFloat($("#f2 #factor2").val());
	var f3 = parseFloat($("#f3 #factor3").val());
	if(f1 + f2 + f3 != 1)
	{
		bValid = false;
		updateTips( "测评系数总和须为1" );
	}
	
	return bValid;
}
function validatorTB2()
{
  if($("#selectT_class").val() == null)
  {
    updateTips1("由于测评班级列表为空，您无法为指定测评班级设置测评系数！");
    return false;
  }
  
	var FF1 = $("#fa1 #factor1"),
		FF2 = $("#fa2 #factor2"),
		FF3 = $("#fa3 #factor3"),
		allFields = $( [] ).add( FF1 ).add( FF2 ).add( FF3 );

	var bValid = true;
	allFields.removeClass( "ui-state-error" );
	
	bValid = bValid && checkLength1( FF1, "F1", 1, 5);
	bValid = bValid && checkRegexp1( FF1, /^\d+(.\d+$|\d*$)/, "\"F1\"应该输入合法数字" );
	
	bValid = bValid && checkLength1( FF2, "F2", 1, 5);
	bValid = bValid && checkRegexp1( FF2, /^\d+(.\d+$|\d*$)/, "\"F2\"应该输入合法数字" );
	
	bValid = bValid && checkLength1( FF3, "F3", 1, 5);
	bValid = bValid && checkRegexp1( FF3, /^\d+(.\d+$|\d*$)/, "\"F3\"应该输入合法数字" );
		
	if ( ! bValid ) {
		return false;
	}
	
	
	var ff1 = parseFloat($("#fa1 #factor1").val());
	var ff2 = parseFloat($("#fa2 #factor2").val());
	var ff3 = parseFloat($("#fa3 #factor3").val());
	if(ff1 + ff2 + ff3 != 1)
	{
		bValid = false;
		updateTips1( "测评系数总和须为1" );
	}
	
	return bValid;
}


