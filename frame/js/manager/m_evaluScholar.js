//设置班级列表的分页
var totalRecord = 0;
var pages = 0;
var currentPage = 1;

//设置学生成绩列表
var totalRecord1 = 0;
var pages1 = 0;
var currentPage1 = 1;

//记录当前选择的是哪个班级
var currentMem;   

$(document).ready(function() {
	//调整屏幕的高度
	parent.parent.document.getElementById("frame_content").height=420;	
	//隐藏错误提示
	$("#error").hide();

	//隐藏翻页按钮
	$("#btLast").hide(); $("#btNext").hide(); $("#btFormer").hide(); $("#btFirst").hide(); 
	//隐藏第一行
	$("#stu-table tr").eq(1).hide();

	//设置学生成绩列表的分页显示
	$("#users-contain").hide();
	//隐藏翻页按钮
	$("#btLast1").hide(); $("#btNext1").hide(); $("#btFormer1").hide(); $("#btFirst1").hide(); 
	//隐藏第一行
	$("#tb tr").eq(1).hide();

	//获取查询年级
	setSearchGrade();

	//改变专业
	changeMajor();

	//改变年级
	changeGrade();

	//改变专项奖学金的下拉框
	changeSelectZX()
	
	//studentPage的分页控制
	studentPage();

	//奖学金审核分页控制
	scholarshipPage();

	//单个审核
	btConfirm();

	//批量审核
	btMultiCheck();

	$("#BtStuBack").click(function(){
		$("#tb tr:gt(1)").remove();
		$("#users-contain").hide();
		$("#classes").show();
		adjustHeight();
	});

	//选择所有行
	$("#CKA").click(function() {
		$("#stu-table tbody tr:gt(0)").each(function() {
			if(!$(this).is(":hidden")) 	
				$(this).find("#CK").get(0).checked = $("#CKA").get(0).checked;
		});
	});
});

//批量审核
function btMultiCheck()
{
	$("#btMutiCheck").click(function(){
		var passScholar = [];
		var alreadyPassedMem = [];
		$("#stu-table tr:gt(1)").each(function() {
			if ($(this).find("#CK").get(0).checked == true && $(this).find("#link").html()!="已审核") {
				passScholar.push($(this).find("#cl_name").attr("name"));
				alreadyPassedMem.push($(this).find("#link"));
			}
		});

		if(passScholar.length == 0)
		{
			alert("请您选择要审核的测评班级，或者您选择的测评班级已通过审核~");
			return false;
		}

		check(passScholar, alreadyPassedMem);
	});
}

//单个审核
function btConfirm()
{
	$("#agree").click(function(){
		var passScholar = [];
		var alreadyPassedMem = [];
		passScholar.push(currentMem.find("#cl_name").attr("name"));
		alreadyPassedMem.push(currentMem.find("#link"));

		check(passScholar, alreadyPassedMem);
	});
}

//审核的公共接口
function check(passScholar, alreadyPassedMem)
{
	//alert($.toJSON(passScholar));
	if(confirm('您将审核通过选中测评班级的奖学金评定结果，确定继续？'))
	{
		$.get(
			 "../../../testdrive/index.php?r=Scholarshiptype/PassScholar",
			
			{passScholar: $.toJSON(passScholar)},
			function(data)
			{
				var ret = $.evalJSON(data);
				if(ret.success == true)
				{
					alert("提交成功~");
					$("#BtStuBack").click();
						
					for(var i = 0; i < alreadyPassedMem.length; i++)
					{
						alreadyPassedMem[i].html("已审核");
						alreadyPassedMem[i].removeAttr("onclick");
						alreadyPassedMem[i].removeAttr("href");
					}
				}
				else
				{
if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
					{
						alert(ret.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href = '../../../';
						return false;
					}
					alert('服务器端返回错误信息=>"'+ret.message+'"');
					return false;
				}
			}
		);
	}
	
	$("#CKA").attr("checked", false);
	$("#CKA").click();
	$("#CKA").attr("checked", false);

	passScholar = [];
}

//查看学生得奖情况
function checkAMem()
{
	$("#classes").hide();
	$("#users-contain").show();
	loadScholarship();
}

//专项奖学金的下拉框
function changeSelectZX()
{
	$('#selectZX').change(function(){
		$("#countZX").html($('#selectZX').val());
	});
}	

//加载指定奖学金测评班级的总体得奖情况
function loadDetail()
{
	var displayScholarNumber = {};
	displayScholarNumber["t_classid"] = currentMem.find("#cl_name").attr("name");
	var date = new Date();
	displayScholarNumber["year"] = date.getFullYear();
	//alert($.toJSON(displayScholarNumber));
	$.post(
		
		 "../../../testdrive/index.php?r=Scholarshiptype/DisplayScholarNumber",
		{displayScholarNumber:$.toJSON(displayScholarNumber)}, 
		function (data)
		{
			var ret = $.evalJSON(data);
			if(ret.success == false)
			{
if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
					{
						alert(ret.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href = '../../../';
						return false;
					}
				alert('服务器端返回错误信息=>"'+ret.message+'"');
				return false;
			}

			if(ret.results.length != 0)
			{
				$("#countGJ").html(ret.results["countryScholarNum"]);
				$("#countLZ").html(ret.results["lizhiScholarNum"]);
				$("#countJD").html(ret.results["ScholarA"]);
				$("#countYD").html(ret.results["ScholarB"]);
				$("#countBD").html(ret.results["ScholarC"]);

				$('#selectZX option:gt(0)').remove();
				var selector=$('#selectZX'); 
				var sum = 0;
				for (var i = 0; i < ret.results.special.length; i++)
				{
					sum += parseInt(ret.results.special[i]["num"]);
					selector.append('<option value="'+ret.results.special[i]["num"]+'">'+ret.results.special[i]["schname"]+'</option>'); 
				}
				$('#selectZX option').eq(0).attr("value", sum);
				$("#countZX").html(sum);
			}
			else
			{
				alert('暂无奖学金名额汇总信息~');
				return false;
			}
		}
	);
}

//将学生成绩加载到表中
function loadScholarship()
{
	parent.parent.document.getElementById("frame_content").height=420;	

	//加载指定奖学金测评班级的总体得奖情况
	loadDetail();
	
	//加载学生列表
	var displayScholarByClass = {};
	displayScholarByClass["t_classid"] = currentMem.find("#cl_name").attr("name");
	var date = new Date();
	displayScholarByClass["year"] = date.getFullYear();

	//alert($.toJSON(displayScholarByClass));
	$.get(
		
		"../../../testdrive/index.php?r=Scholarshiptype/DisplayScholarByClass",
		{displayScholarByClass:$.toJSON(displayScholarByClass)},
		function(data)
		{
			//alert(data);
			var ret = $.evalJSON(data);
			if(ret.success == false)
			{
if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
					{
						alert(ret.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href = '../../../';
						return false;
					}
				alert('服务器端返回错误信息=>"'+ret.message+'"');
			}

			if(ret.results.length != 0)
			{
				$("#tb tbody tr:gt(0)").remove();
				//alert(ret.results.length);
				for(var i = 0; i < ret.results.length; i++)
				{
					var row = $("#tb tbody tr:first").clone();	
					//alert(ret.results[i]["sname"]);
					//ret.results[i]["cname"] = "计算机一班";
					row.find("td").get(0).innerHTML = ret.results[i]["sid"];
					row.find("td").get(1).innerHTML = ret.results[i]["sname"];
					row.find("td").get(2).innerHTML = ret.results[i]["cname"];
					row.find("td").get(3).innerHTML = parseFloat(ret.results[i]["F1"]);
					row.find("td").get(4).innerHTML = parseFloat(ret.results[i]["F2"]);
					row.find("td").get(5).innerHTML = parseFloat(ret.results[i]["F3"]);
					row.find("td").get(6).innerHTML = parseFloat(ret.results[i]["total"]);
					row.find("td").get(7).innerHTML = ret.results[i]["scholar"];
					row.show();
					row.appendTo("#tb tbody");
				}
				setRecordPage1(ret.results.length);
				currentPage1 = 1;
				$("#tb tbody tr:gt(20)").each(function() {
					$(this).hide();
				});

				$("#showPage1").html(currentPage1+"/"+pages1);
				adjustHeight();

				$('html,body', window.parent.parent.document).animate({
					scrollTop: $("#mark").offset().top+135}, 
					400
				);
			}
			else
			{
				alert("测评班级学生列表为空，暂无记录~");
				return false;
			}
		}
	);
}

//设置下拉框中的年级
function setSearchGrade()
{
	  var years;
	  var today;
	  today = new Date();
	  Years = today.getFullYear();
	  var intYears;
	  intYears = parseInt(Years);
	  var selector=$('#selectGrade'); 
	  for(var i=intYears-4; i< intYears +4;i++)
	  {
			selector.append('<option value="'+i+'">'+i+'</option>');  
	  }     
	  selector.val(intYears);

	  //加载学院列表
	  loadMajor();
}

//载入一个学院的全部专业
function loadMajor()
{
	$.get(  
		
		 "../../../testdrive/index.php?r=Scholarshiptype/DisplayMajorByGrade",
		function(data)
		{
			var obj = $.evalJSON(data);
			if(obj.success == false)
			{
if(obj.results.hasOwnProperty("timeout") && obj.results.timeout)
					{
						alert(obj.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href = '../../../';
						return false;
					}
				alert("服务器端返回错误信息=>\""+obj.message+'"');
				return false;
			}
			if(obj.results.length != 0)
			{
				var selector=$('#selectMajor'); 
				for (var i = 0; i < obj.results.length; i++)
				{
					selector.append('<option value="'+obj.results[i]["majorid"]+'">'+obj.results[i]["majorname"]+'</option>'); 
				}

				//开始加载测评班级列表
				loadTClass();
			}
			else
			{
				alert("专业列表为空，请学院管理员先为该学院添加专业信息~");
				$("#error").show(); $("#classes").hide();
				return false;
			}
		}							
	);	
}

//显示班级信息
function loadTClass()
{
	var displayClassByGradeByMajor = {};
	displayClassByGradeByMajor["grade"] = $('#selectGrade').val();
	displayClassByGradeByMajor["majorid"] = $('#selectMajor').val();
	var date = new Date();
	displayClassByGradeByMajor["year"] = date.getFullYear();
	//alert($.toJSON(displayClassByGradeByMajor));
	$.get(
		 "../../../testdrive/index.php?r=Scholarshiptype/DisplayClassByGradeByMajor",
		{displayClassByGradeByMajor:$.toJSON(displayClassByGradeByMajor)},
		function(data)
		{
			var obj = $.evalJSON(data);
			if(obj.success == false)
			{
if(obj.results.hasOwnProperty("timeout") && obj.results.timeout)
					{
						alert(obj.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href = '../../../';
						return false;
					}
				alert("服务器端返回错误信息=>\""+obj.message+'"');
				return false;
			}
			if(obj.results.length != 0)
			{
				$("#stu-table tbody tr:gt(0)").remove();

				setRecordPage(obj.results.length);
				for (var i = 0; i < obj.results.length; i++)
				{
					var tr = $("#stu-table tr").eq(1).clone();
					tr.find("td").get(1).innerHTML = obj.results[i]['t_classname'];
					tr.find("td").eq(1).attr("name", obj.results[i]['t_classid']);
					tr.find("td").get(2).innerHTML = obj.results[i]['teachername'];

					if(parseInt(obj.results[i]['isPassed']) > 0)
					{
						tr.find("td").eq(3).find("a").removeAttr("href");
						tr.find("td").eq(3).find("a").removeAttr("onclick");
						tr.find("td").eq(3).find("a").html("已审核");
					}
					tr.show();
					tr.appendTo("#stu-table tbody");
				}
				currentPage = 1;
				$("#stu-table tbody tr:gt(10)").each(function() {
					$(this).hide();
				});

				$("#showPage").html(currentPage+"/"+pages);
				adjustHeight();
			}
			else
			{
				alert("测评班级列表为空~");
				return false;
			}
		}
	);
}

function changeMajor()
{
	$("#selectMajor").change(function(){
		$("#stu-table tbody tr:gt(0)").remove();
		loadTClass();
	});		
}

//改变学年
function changeGrade()
{
	$("#selectGrade").change(function(){
		$("#stu-table tbody tr:gt(0)").remove();
		loadTClass();
	});		
}


//studentPage的分页控制
function studentPage()
{
	$("#btLast").click(function() 
	{
		if(currentPage != pages)
		{
			//alert("即将跳转到最后一页");
			currentPage = pages;
			ShowPage(currentPage*10-9, totalRecord);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btNext").click(function() 
	{
		if(currentPage+1<=pages)
		{
			//alert("即将跳转到下一页");
			currentPage += 1;
			ShowPage(currentPage*10-9, currentPage*10);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btFormer").click(function() 
	{
		if(currentPage-1>0)
		{
			//alert("即将跳转到上一页");
			currentPage -= 1;
			ShowPage(currentPage*10-9, currentPage*10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
	$("#btFirst").click(function() 
	{
		if(currentPage != 1)
		{
			//alert("即将跳转到首页");
			currentPage = 1;
			ShowPage(1, 10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
	$("#btLast").click(function() 
	{
		if(currentPage != pages)
		{
			//alert("即将跳转到最后一页");
			currentPage = pages;
			ShowPage(currentPage*10-9, totalRecord);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
}

//奖学金审核分页控制
function scholarshipPage()
{
	$("#btLast1").click(function() 
	{
		if(currentPage1 != pages1)
		{
			//alert("即将跳转到最后一页");
			currentPage1 = pages1;
			ShowPage1(currentPage1*20-19, totalRecord1);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btNext1").click(function() 
	{
		if(currentPage1+1<=pages1)
		{
			//alert("即将跳转到下一页");
			currentPage1 += 1;
			ShowPage1(currentPage1*20-19, currentPage1*20);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btFormer1").click(function() 
	{
		if(currentPage1-1>0)
		{
			//alert("即将跳转到上一页");
			currentPage1 -= 1;
			ShowPage1(currentPage1*20-19, currentPage1*20);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
	$("#btFirst1").click(function() 
	{
		if(currentPage1 != 1)
		{
			//alert("即将跳转到首页");
			currentPage1 = 1;
			ShowPage1(1, 20);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
}

//设置页数和总记录数
function setRecordPage(number)
{
	totalRecord = number;
	if(number <= 0)
	{
		totalRecord = 0;
		pages = 0;
	}
	else
	{//每页有8条记录
		pages = parseInt(number/10);
		if(number%10 != 0)
			pages += 1;
	}
	//页数>1 需翻页按钮
	if(pages > 1)
	{
		$("#btLast").show(); $("#btNext").show(); $("#btFormer").show(); $("#btFirst").show(); 
	}
}

//传入需要被显示的数据的位置
//仅显示_min 和 _max之间的数据
function ShowPage(_min, _max)
{
	$("#stu-table tbody tr").each(function() {
		$(this).show();
	});
	var variable = "#stu-table tbody tr:lt("+_min+")";
	$(variable).each(function() {
		$(this).hide();
	});
	variable = "#stu-table tbody tr:gt("+_max+")";
	$(variable).each(function() {
		$(this).hide();
	});
	$("#showPage").html(currentPage+"/"+pages);
	adjustHeight();
}
//传入需要被显示的数据的位置
//仅显示_min 和 _max之间的数据
function ShowPage1(_min, _max)
{
	$("#tb tbody tr").each(function() {
		$(this).show();
	});
	
	var variable = "#tb tbody tr:lt("+_min+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	variable = "#tb tbody tr:gt("+_max+")";
	$(variable).each(function() {
		$(this).hide();
	});
	$("#showPage1").html(currentPage1+"/"+pages1);
	adjustHeight();
}
//设置页数和总记录数
function setRecordPage1(number)
{
	totalRecord1 = number;
	if(number <= 0)
	{
		totalRecord1 = 0;
		pages1 = 1;
	}
	else
	{//每页有8条记录
		pages1 = parseInt(number/20);
		if(number%20 != 0)
			pages1 += 1;
	}
	//页数>1 需翻页按钮
	if(pages1 > 1)
	{
		$("#btLast1").show(); $("#btNext1").show(); $("#btFormer1").show(); $("#btFirst1").show(); 
	}
	else
	{
		$("#btLast1").hide(); $("#btNext1").hide(); $("#btFormer1").hide(); $("#btFirst1").hide(); 
	}
}

//调整高度
function adjustHeight()
{
	var height = document.documentElement.scrollHeight>document.body.scrollHeight
						? document.body.scrollHeight:document.documentElement.scrollHeight; 
	//alert("before "+height);
	if(height > 420)
		parent.parent.document.getElementById("frame_content").height=height + 30;
	else
		parent.parent.document.getElementById("frame_content").height=420;	
}
