var totalRecord = 0;
var pages = 0;
var currentPage = 1;
$(function() {
	parent.parent.document.getElementById("frame_content").height=420;   
	//隐藏翻页按钮
	$("#btLast").hide(); $("#btNext").hide(); $("#btFormer").hide(); $("#btFirst").hide(); 
	//隐藏第一行
	$("#tb tr").eq(1).hide();
	//获取查询年级
	setSearchGrade();
	
	//改变年级
	changeYear();
	//改变专业
	changeMajor();
	//改变班级
	changeClass();
	//载入学生信息
	
	$("#btLast").click(function() 
	{
		if(currentPage != pages)
		{
			//alert("即将跳转到最后一页");
			currentPage = pages;
			ShowPage(currentPage*10-9, totalRecord);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btNext").click(function() 
	{
		if(currentPage+1<=pages)
		{
			//alert("即将跳转到下一页");
			currentPage += 1;
			ShowPage(currentPage*10-9, currentPage*10);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btFormer").click(function() 
	{
		if(currentPage-1>0)
		{
			//alert("即将跳转到上一页");
			currentPage -= 1;
			ShowPage(currentPage*10-9, currentPage*10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
	$("#btFirst").click(function() 
	{
		if(currentPage != 1)
		{
			//alert("即将跳转到首页");
			currentPage = 1;
			ShowPage(1, 10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
		   
});
//设置下拉框中的年级
function setSearchGrade()
{
	  var years;
	  var today;
	  today = new Date();
	  Years = today.getFullYear();
	  var intYears;
	  intYears = parseInt(Years);
	  var selector=$('#selectGrade'); 
	  for(var i=intYears-4; i< intYears +4;i++)
	  {
		  if(i == intYears)
			selector.append('<option selected="selected" value="intYears">'+i+'</option>');  	 
		  else
			selector.append('<option value="intYears">'+i+'</option>');  
	  }     
	  
	  //载入一个学院的全部专业
	loadMajor();

}
//载入一个学院的全部专业
function loadMajor()
{
	var displayMajorToList = {};
	displayMajorToList['grade'] = $('#selectGrade').find('option:selected').text();
	$.get(  
		
		"../../../testdrive/index.php?r=WhuClass/DisplayMajorToList",
		{
			displayMajorToList : $.toJSON(displayMajorToList)
		},
		function(data)
		{
			var obj = $.evalJSON(data);
			if(obj.success == false)
			{
if(obj.results.hasOwnProperty("timeout") && obj.results.timeout)
					{
						alert(obj.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href = '../../../';
						return false;
					}
				alert("服务器端返回错误信息=>\""+obj.message+'"');
				return false;
			}
			var selector=$('#selectMajor'); 
			if (obj.results.length == 0)
			{alert("对不起，没有相应的专业信息，请找管理员添加~~");}
			else
			{
				for (var i = 0; i < obj.results.length; i++)
				{
					/*alert(obj.results[i]["majorid"]);*/
					selector.append('<option value="'+obj.results[i]["majorid"]+'">'+obj.results[i]["majorname"]+'</option>'); 
				}
				showTable();
				//alert($('#selectMajor').find('option:selected').text());
			}
				//载入班级列表
				
		}							
	);	
}
function showTable()
{	
	var displayClassByYearByMajor = {};
	displayClassByYearByMajor['grade'] = $('#selectGrade').find('option:selected').text();
	displayClassByYearByMajor['majorid'] = $('#selectMajor').find('option:selected').val();
	//alert( $.toJSON(displayClassByYearByMajor));
	$.get(
		 "../../../testdrive/index.php?r=Student/DisplayClassByGrade",
		{
			displayClassByYearByMajor : $.toJSON(displayClassByYearByMajor)
		},
		function(data)
		{
			var obj = $.evalJSON(data);
			if(obj.success == false)
			{
if(obj.results.hasOwnProperty("timeout") && obj.results.timeout)
					{
						alert(obj.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href = '../../../';
						return false;
					}
				alert("数据库中无数据~"+obj.message);
				return false;
			}
			var selector=$('#selectClass'); 
			if(obj.results.length == 0)
			{alert("对不起，没有相应的班级信息，请重新选定年级及专业~~");}
			else
			{
				for (var i = 0; i < obj.results.length; i++)
				{
					selector.append('<option value="'+obj.results[i]["classid"]+'">'+obj.results[i]["classname"]+'</option>');
				}
				loadStudentInfo();
			}
			/////////////
			//$("#tb tbody tr:gt(0)").each(function(){ $(this).remove(); });
			/*if(document.getElementById("selectClass").length == 0)
			{alert("没有对应专业的班级~~");}
			else*/
			
			
		}
	);
}
function loadStudentInfo()
{	
	var displayStudentInfoByClass = {};
	displayStudentInfoByClass['grade'] = $('#selectGrade').find('option:selected').text();
	displayStudentInfoByClass['majorid'] = $('#selectMajor').find('option:selected').val();
	displayStudentInfoByClass['classid'] = $('#selectClass').find('option:selected').val();
	//alert( $.toJSON(displayStudentInfoByClass));
	$.get(
		 "../../../testdrive/index.php?r=Student/DisplayStudentInfoByClass",
		{
			displayStudentInfoByClass : $.toJSON(displayStudentInfoByClass)
		},
		function(data)
		{
			//alert(data);
			var obj = $.evalJSON(data);
			if(obj.success == false)
			{
if(obj.results.hasOwnProperty("timeout") && obj.results.timeout)
					{
						alert(obj.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href = '../../../';
						return false;
					}
				alert("数据库中无数据~"+obj.message);
				return false;
			}
			/////////////////
			$("#tb tbody tr:gt(0)").each(function(){ $(this).remove(); });
			
			setRecordPage(obj.results.length);
			if (obj.results.length == 0)
			{alert("对不起，该班级还没有学生基本信息，请找管理员添加~~");}
			else
			{
					for (var i = 0; i < obj.results.length; i++)
					{
						var tr = $("#tb tr").eq(1).clone();
						
						tr.find("td").get(0).innerHTML = obj.results[i]['studentid'];
						tr.find("td").get(1).innerHTML = obj.results[i]['studentname'];
						//tr.find("td").get(2).innerHTML = obj.results[i]['sex'];
						//tr.find("td").get(3).innerHTML = obj.results[i]['borndate'];
						tr.find("td").get(4).innerHTML = obj.results[i]['people'];
						tr.find("td").get(5).innerHTML = obj.results[i]['nativeplace'];
						tr.find("td").get(6).innerHTML = obj.results[i]['politicaloutlook'];
						tr.find("td").get(7).innerHTML = obj.results[i]['poorstudent'];
						tr.find("td").get(8).innerHTML = obj.results[i]['posting'];
						tr.find("td").get(9).innerHTML = obj.results[i]['studentsort'];
						
						tr.show();
						tr.appendTo("#tb tbody");
					}
					currentPage = 1;
					$("#tb tbody tr:gt(10)").each(function() {
						$(this).hide();
					});
			}
			var height = 
				document.documentElement.scrollHeight>document.body.scrollHeight?document.body.scrollHeight:document.documentElement.scrollHeight; 
			if(height > 495)
				parent.parent.document.getElementById("frame_content").height=height;
			else
				parent.parent.document.getElementById("frame_content").height=495;
		}
	);
}
function changeYear()
{
	$("#selectGrade").change(function(){
		$("#tb tbody tr:gt(0)").each(function(){ $(this).remove(); });
		loadStudentInfo();
	});	
	
}
function changeMajor()
{
	$("#selectMajor").change(function(){
		//var selected =   $("#selectGrade").find("option:selected").text();
		document.getElementById("selectClass").length=0;
		showTable();
		/*$("#tb tbody tr:gt(0)").each(function(){ $(this).remove(); });
		loadStudentInfo();*/
		//changeMajor();
	});		
}
function changeClass()
{
	$("#selectClass").change(function(){
		$("#tb tbody tr:gt(0)").each(function(){ $(this).remove(); });
		loadStudentInfo();
	});	
	
}
//传入需要被显示的数据的位置
//仅显示_min 和 _max之间的数据
function ShowPage(_min, _max)
{
	$("#tb tbody tr").each(function() {
		$(this).show();
	});
	
	var variable = "#tb tbody tr:lt("+_min+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	variable = "#tb tbody tr:gt("+_max+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	adjustHeight();
}
function adjustHeight()
{
	var height = document.documentElement.scrollHeight>document.body.scrollHeight
						? document.body.scrollHeight:document.documentElement.scrollHeight; 
	//alert("before "+height);
	if(height + 50 > 420)
		parent.parent.document.getElementById("frame_content").height=height + 50;
	else
		parent.parent.document.getElementById("frame_content").height=420;	
}
//设置页数和总记录数
function setRecordPage(number)
{
	totalRecord = number;
	if(number <= 0)
	{
		totalRecord = 0;
		pages = 1;
	}
	else
	{//每页有10条记录
		pages = parseInt(number/10);
		if(number%10 != 0)
			pages += 1;
	}
	//页数>1 需翻页按钮
	if(pages > 1)
	{
		$("#btLast").show(); $("#btNext").show(); $("#btFormer").show(); $("#btFirst").show(); 
	}
	else
	{
		$("#btLast").hide(); $("#btNext").hide(); $("#btFormer").hide(); $("#btFirst").hide(); 
	}
}
