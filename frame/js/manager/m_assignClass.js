/*
	可以按照年级、专业、班级来进行测评班级的划分
	初始化时，是一个年级的测评班级
*/

//设置班级列表的分页
//这个是创建测评班级的分页显示
var totalRecord = 0;
var pages = 0;
var currentPage = 1;

//这个是编辑测评班级的测评班级列表显示
var totalRecord1 = 0;
var pages1 = 0;
var currentPage1 = 1;

//这个是编辑测评班级的班级列表显示
var totalRecord2 = 0;
var pages2 = 0;
var currentPage2 = 1;

var v_editATClass, v_delATClass, v_delAClass;

$(function() {
	parent.parent.document.getElementById("frame_content").height=420;	
	//隐藏错误提示div
	$("#error").hide();
	
	$( "#tabs" ).tabs();
	
	//获取查询年级
	setSearchGrade();
	
	//载入一个学院的全部专业
	loadMajor();
	
	//tbToChoose:新增测评班级表格
	//tbToChoose1:编辑测评班级表格
	//tbChoosed1:班级表格
	//隐藏新增测评班级表格第一行
	$("#tbToChoose tr").eq(1).hide();
	//隐藏测评班级信息的第一行
	$("#tbToChoose1 tr").eq(1).hide();
	$("#tbChoosed1 tr").eq(1).hide();
	//$("#tbChoosed1 tr").eq(1).find("#selectTeacher1").hide();
	//$("#tbChoosed1 tr").eq(1).find("#tClass").hide();
	
	//隐藏翻页按钮
	$("#btLast").hide(); $("#btNext").hide(); $("#btFormer").hide(); $("#btFirst").hide(); 
	//隐藏测评班级列表按钮
	$("#btLast1").hide(); $("#btNext1").hide(); $("#btFormer1").hide(); $("#btFirst1").hide(); 
	$("#btLast2").hide(); $("#btNext2").hide(); $("#btFormer2").hide(); $("#btFirst2").hide(); 
	
	//左边的待选择表格的分页控制
	Table();

	//改变年级
	changeGrade();
	
	//改变专业
	changeMajor();
	
	//选择所有行
	$("#CKA").click(function() {		
		$("#tbToChoose tbody > tr").each(function() {
			if(!$(this).is(":hidden")) 	
				$(this).find("#CK").get(0).checked = $("#CKA").get(0).checked;
		});
	});
	
	//创建一个测评班级
	btCreate();
	
	//新增班级到已存在测评班级中
	btAdd();
	
	$('#tab1').click(function(){
		adjustHeight();	
	});
	$('#tab2').click(function(){
		adjustHeight();	
	});
});

function editTClassFunc()
{
	//TO DO
	var realAdd = []; //记录真正要添加的行
	//获取选中的行号
	$("#tbChoosed1 tbody tr:gt(0)").each(function() {
		//班级名和班级id加进去
		if($(this).find("td").eq(0).html() != "")
			realAdd.push($(this).find("td").eq(0).attr("name"));
	});
	
	if(realAdd.length <= 0)
	{
		if(confirm("您编辑测评班级后，该测评班级所对应的班级列表为空，保存后该测评班级将会被删除，是否继续？"))
		{
			delATClass();
			return true;
		}
		else return false;
	}

	//开始往后台传送
	var saveT_class = {};
	saveT_class["t_classid"] = v_editATClass.find("td").eq(2).attr("name");  //表示编辑一个测评班级
	saveT_class["t_classname"] = v_editATClass.find("td").eq(2).html();
	saveT_class["teacherid"] = v_editATClass.find("td #selectTeacher1").val();
	saveT_class["classArray"] = realAdd;

	//alert($.toJSON(saveT_class));
	$.ajax({
		url:"../../../testdrive/index.php?r=T_class/SaveT_Class",
		data:{saveT_class: $.toJSON(saveT_class)},
		async:false, 
		success:function (data)
		{
			var ret = $.evalJSON(data);
			if(ret.success == true && ret.results.length != 0)
			{
				//如果下拉框中存在那种临时建立的测评班级，则将其删除
				alert("编辑测评班级成功~");
			}
			else
			{
if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
					{
						alert(ret.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href = '../../../';
						return false;
					}
				alert('服务器端返回错误信息=>"'+ret.message+'"');
			}
		}
	});
}

//各种编辑功能
//var v_editATClass, v_delATClass, v_delAClass;
function editATClass()
{
	if(v_editATClass.find("#edit").html() == "编辑")
	{
		v_editATClass.parent().find("tr").each(function(){
			$(this).find("#edit").html("编辑");
		});
		getClassByTclass();	
		v_editATClass.find("#edit").html("保存");
	}
	else if(v_editATClass.find("#edit").html() == "保存")
	{
		//编辑测评班级
		editTClassFunc();
		v_editATClass.find("#edit").html("编辑");
	}
}

function delATClass()
{
	v_editATClass.parent().find("tr").each(function(){
		$(this).find("#edit").html("编辑");
	});
	
	if(confirm("您确认要删除该测评班级？删除后可以通过“创建测评班级”菜单重新创建。"))
	{
		var deleteTClass = {};
		var date = new Date();
		deleteTClass["t_classid"] = v_editATClass.find("td").eq(2).attr("name");
		deleteTClass["year"] = date.getFullYear();
		//alert($.toJSON(deleteTClass));
		$.get(
			 "../../../testdrive/index.php?r=T_class/DeleteTClass",
			{deleteTClass: $.toJSON(deleteTClass)},
			function(data){
				var ret = $.evalJSON(data);
				if(ret.success == true)
				{
					alert("删除测评班级\""+v_editATClass.find("td").eq(2).html()+'"成功~');
					v_editATClass.remove();
					$("#tbChoosed1 tr:gt(1)").remove();
					
					var tr = $("#tbToChoose1 tr").eq(1).clone();
					tr.show();
					tr.find("#selectTeacher1").hide();
					tr.appendTo("#tbToChoose1");
					
					//删除完了后 重新修正
					var rows = 0;
					$("#tbToChoose1 tbody tr:gt(0)").each(function() {
						if($(this).find("td").get(0).innerHTML == "&nbsp;")
						{
							rows++;
						}
					});
					if(pages1 >1 && rows >= 10)
					{
						$("#tbToChoose1 tbody tr:gt("+(totalRecord1-10)+")").remove();
						setRecordPage1(totalRecord1-10);
					}
					rePageToChoose1Table(totalRecord1, currentPage1);
					adjustHeight();
				}
				else
				{
if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
					{
						alert(ret.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href = '../../../';
						return false;
					}
					alert('服务器端返回错误信息"'+ret.message+'"');
				}
			}
		);
	}
}

function delAClass()
{
	if(confirm("您确认要删除该班级？点击相应测评班级后的“保存”按钮后才会生效。"))
	{
		v_delAClass.remove();

		//删除完了重新修正
		setRecordPage2(totalRecord2-1);
		rePageChoosedTable(totalRecord2, currentPage2);
	}
}


//充值选择单选按钮
function resetSelection()
{
	$("#CKA").attr("checked", false);
	$("#CKA").click();
	$("#CKA").attr("checked", false);
}

//创建测评班级的校验
function validator()
{
	var tclassname = $.trim($("#tname").val());
	if(tclassname == "")
	{
		alert("测评班级的名称不能为空！");
		return false;
	}
	
	if($.trim($("#selectTeacher").val()) == "")
	{
		alert("请您为测评班级指定辅导员！");
		return false;
	}
	
	return true;
}

//创建一个测评班级
function btCreate()
{
	$("#btCreate").click(function() {					  
		//TO DO
		var realAdd = []; //记录真正要添加的行
		var removeRow = [];
		//获取选中的行号
		$("#tbToChoose tbody tr").each(function() {
			if(!$(this).is(":hidden") && $(this).find("#CK").get(0).checked == true) 
			{
				if($(this).find("td").get(1).innerHTML != "&nbsp;")
				{
					//班级名和班级id加进去
					realAdd.push($(this).find("td").eq(3).attr("name"));
					removeRow.push($(this));
				}
			}
		});
		
		if(realAdd.length <= 0)
		{
			alert("您还没有选择要添加的班级~");
			resetSelection();
			return false;
		}
		
		//验证是否输入了测评班级名称和选择了教师
		if(!validator())
		{
			return false;
		}
		
		//开始往后台传送
		var saveT_class = {};
		saveT_class["t_classid"] = "-9";  //表示创建一个测评班级
		saveT_class["t_classname"] = $.trim($("#tname").val());
		saveT_class["teacherid"] = $("#selectTeacher").val();
		saveT_class["classArray"] = realAdd;

		//alert($.toJSON(saveT_class));
		$.ajax({
			url:"../../../testdrive/index.php?r=T_class/SaveT_Class",
			data:{saveT_class: $.toJSON(saveT_class)},
			async:false, 
			success:function (data)
			{
				var ret = $.evalJSON(data);
				if(ret.success == true && ret.results.length != 0)
				{
					//如果下拉框中存在那种临时建立的测评班级，则将其删除
					$("#selectTestclass").append('<option value="'+ret.results["t_classid"]+'">'+$.trim($("#tname").val())+'</option>'); 
					$("#selectTestclass").val(ret.results.t_classid);
					$("#tname").val("");
					$("#selectTeacher").val("");
					alert("创建测评班级成功~");
					
					//删除掉那一行
					for(var i = 0; i < removeRow.length; i++)
					{
						removeRow[i].remove();
						var row = $("#tbToChoose tr").eq(1).clone();
						row.show();
						row.appendTo("#tbToChoose");
					}
					//删除完了后 重新修正
					var rows = 0;
					$("#tbToChoose tbody tr:gt(0)").each(function() {
						if($(this).find("td").get(1).innerHTML == "&nbsp;")
						{
							rows++;
						}
					});
					if(pages >1 && rows >= 10)
					{
						$("#tbToChoose tbody tr:gt("+(totalRecord-10)+")").remove();
						setRecordPage(totalRecord-10);
					}
					rePageToChooseTable(totalRecord, currentPage);
					resetSelection();
					adjustHeight();
				}
				else
				{
if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
					{
						alert(ret.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href = '../../../';
						return false;
					}
					alert('服务器端返回错误信息=>"'+ret.message+'"');
				}
			}
		});
	});  
}
	
//新增班级到已存在测评班级中
function btAdd()
{
	$("#btAdd").click(function() {		
		//TO DO
		var realAdd = []; //记录真正要添加的行
		var removeRow = [];
		//获取选中的行号
		$("#tbToChoose tbody tr").each(function() {
			if(!$(this).is(":hidden") && $(this).find("#CK").get(0).checked == true) 
			{
				if($(this).find("td").get(1).innerHTML != "&nbsp;")
				{
					//班级名和班级id加进去
					realAdd.push($(this).find("td").eq(3).attr("name"));
					removeRow.push($(this));
				}
			}
		});
		
		if(realAdd.length <= 0)
		{
			alert("您还没有选择要添加的班级~");
			resetSelection();
			return false;
		}
		
		//验证是否选择了测评班级
		if($("#selectTestclass").val() == "-1")
		{
			alert("您还没有选择将要添加到的测评班级！");
			return false;
		}
		
		//开始往后台传送
		var addExistT_Class = {};
		addExistT_Class["t_classid"] = $("#selectTestclass").val();  //表示创建一个测评班级
		addExistT_Class["classArray"] = realAdd;

		//alert($.toJSON(saveT_class));
		$.ajax({
			url:"../../../testdrive/index.php?r=T_class/addExistT_Class",
			data:{addExistT_Class: $.toJSON(addExistT_Class)},
			async:false, 
			success:function (data)
			{
				var ret = $.evalJSON(data);
				if(ret.success == true)
				{
					$("#tname").val("");
					$("#selectTeacher").val("");
					alert("添加到测评班级成功~");
					
					//删除掉那一行
					for(var i = 0; i < removeRow.length; i++)
					{
						removeRow[i].remove();
						var row = $("#tbToChoose tr").eq(1).clone();
						row.show();
						row.appendTo("#tbToChoose");
					}
					//删除完了后 重新修正
					var rows = 0;
					$("#tbToChoose tbody tr:gt(0)").each(function() {
						if($(this).find("td").get(1).innerHTML == "&nbsp;")
						{
							rows++;
						}
					});
					if(pages >1 && rows >= 10)
					{
						$("#tbToChoose tbody tr:gt("+(totalRecord-10)+")").remove();
						setRecordPage(totalRecord-10);
					}
					rePageToChooseTable(totalRecord, currentPage);
					resetSelection();
					adjustHeight();
				}
				else
				{
if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
					{
						alert(ret.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href = '../../../';
						return false;
					}
					alert('服务器端返回错误信息=>"'+ret.message+'"');
				}
			}
		});
	});  
}
	
//初始化可选班级表格
function initToChoosedTable()
{
	for(var i = 0; i < 10; i++)
	{
		var row = $("#tbToChoose tr").eq(1).clone();
		row.show();
		row.appendTo("#tbToChoose");
	}
	totalRecord += 10;
}

//初始化测评班级列表
function initToChoosed1Table()
{
	for(var i = 0; i < 10; i++)
	{
		var row = $("#tbToChoose1 tr").eq(1).clone();
		row.show();
		row.appendTo("#tbToChoose1");
	}
	totalRecord1 += 10;
}

//初始化班级列表
function initChoosedTable()
{
	for(var i = 0; i < 10; i++)
	{
		var row = $("#tbChoosed1 tr").eq(1).clone();
		row.show();
		row.appendTo("#tbChoosed1");
	}
}

//设置下拉框中的年级
function setSearchGrade()
{
	var years;
	var today;
	today = new Date();
	Years = today.getFullYear();
	var intYears;
	intYears = parseInt(Years);
	var selector=$('#selectGrade'); 
	for(var i=intYears-4; i< intYears +4;i++)
	{
		selector.append('<option value="'+i+'">'+i+'</option>'); 
		$('#selectGrade1').append('<option value="'+i+'">'+i+'</option>'); 
	}
	selector.val(intYears);
	$('#selectGrade1').val(intYears);
	
	//默认返回当前年份的班级列表
	addClasses();
}

//载入一个学院的全部专业
function loadMajor()
{
	$.get(  
		"../../../testdrive/index.php?r=T_class/DisplayMajorToList",
		function(data)
		{
			var obj = $.evalJSON(data);
			if(obj.success == false)
			{
if(obj.results.hasOwnProperty("timeout") && obj.results.timeout)
					{
						alert(obj.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href = '../../../';
						return false;
					}
				alert("服务器端返回错误信息=>\""+obj.message+'"');
				return false;
			}
			
			if(obj.results.length != 0)
			{
				var selector=$('#selectMajor'); 
				for (var i = 0; i < obj.results.length; i++)
				{
					selector.append('<option value="'+obj.results[i]["majorid"]+'">'+
																	 obj.results[i]["majorname"]+'</option>'); 
					$('#selectMajor1').append('<option value="'+obj.results[i]["majorid"]+'">'+
																	 obj.results[i]["majorname"]+'</option>'); 
				}
			}
			else
			{
				alert("对不起，由于专业列表返回为空，无法执行本操作~请您先添加学院专业信息~");
				$("#error").show();  $("#info").hide();
			}
		}							
	);	
}

//显示班级信息
function addClasses()
{
	var showClass = {};
	showClass["grade"] = $("#selectGrade").val();
	showClass["majorid"] = $("#selectMajor").val();
	
	//alert($.toJSON(showClass));
	$.get(
	
		"../../../testdrive/index.php?r=T_class/ShowClass",
		{showClass:$.toJSON(showClass)},
		function(data)
		{
			var obj = $.evalJSON(data);
			if(obj.success == false)
			{
if(obj.results.hasOwnProperty("timeout") && obj.results.timeout)
					{
						alert(obj.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href = '../../../';
						return false;
					}
				alert("服务器端返回错误信息=>\""+obj.message+'"');
				return false;
			}
			
			//if(obj.results.length != 0)
			{
				$("#tbToChoose tbody tr:gt(0)").remove();
				setRecordPage(obj.results.length);
				for (var i = 0; i < obj.results.length; i++)
				{
					var tr = $("#tbToChoose tr").eq(1).clone();
					tr.find("td").get(1).innerHTML = obj.results[i]['grade'];
					tr.find("td").get(2).innerHTML = obj.results[i]['majorname'];
					tr.find("td").get(3).innerHTML = obj.results[i]['classname'];
					tr.find("td").eq(3).attr("name", obj.results[i]['classid']); 
					tr.show();
					tr.appendTo("#tbToChoose tbody");
				}
				for(var i = obj.results.length; i < pages*10; i++)
				{
					var row = $("#tbToChoose tr").eq(1).clone();
					row.show();
					row.appendTo("#tbToChoose");
				}
				
				setRecordPage(pages*10);
				currentPage = 1;
				$("#tbToChoose tbody tr:gt(10)").each(function() {
					$(this).hide();
				});
				
				$("#showPage").html(currentPage+"/"+pages);
				
				adjustHeight();
				
				//加载完班级列表后，加载测评班级的下拉框
				showTestClass();
			}
			/*else
			{
				alert('班级列表返回为空，请您为学院添加班级~');
				$("#error").show();  $("#info").hide();
			}*/
		}
	);
}

//年级下拉框改变事件
function changeGrade()
{
	$("#selectGrade").change(function(){
		addClasses();
	});	
	
	$("#selectGrade1").change(function(){
		$("#tbChoosed1 tr:gt(1)").remove();
		addTClasses();
	});	
}

//专业下拉框改变事件
function changeMajor()
{
	$("#selectMajor").change(function(){
		addClasses();
	});	
	
	$("#selectMajor1").change(function(){
		$("#tbChoosed1 tr:gt(1)").remove();
		addTClasses();
	});		
}

//填充测评班级列表
function addTClasses()
{
	var displayT_class = {};
	displayT_class["grade"] = $("#selectGrade1").val();
	displayT_class["majorid"] = $("#selectMajor1").val();
	//alert($.toJSON(displayT_class));
	$.get(
		 "../../../testdrive/index.php?r=T_class/DisplayTClass",
		{displayT_class:$.toJSON(displayT_class)},
		function(data)
		{
			var obj = $.evalJSON(data);
			if(obj.success == false)
			{
if(obj.results.hasOwnProperty("timeout") && obj.results.timeout)
					{
						alert(obj.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href = '../../../';
						return false;
					}
				alert("服务器端返回错误信息=>\""+obj.message+'"');
				return false;
			}
			
			$("#tbToChoose1 tbody tr:gt(0)").remove();
			setRecordPage1(obj.results.length);
			for (var i = 0; i < obj.results.length; i++)
			{
				var tr = $("#tbToChoose1 tr").eq(1).clone();
				tr.find("td").get(0).innerHTML = obj.results[i]['grade'];
				tr.find("td #selectTeacher1").val(obj.results[i]['teacherid']);
				tr.find("td").get(2).innerHTML = obj.results[i]['t_classname'];
				tr.find("td").eq(2).attr("name", obj.results[i]['t_classid']); 
				tr.show();
				tr.find("td").eq(3).find("a").show();
				tr.appendTo("#tbToChoose1 tbody");
			}
			for(var i = obj.results.length; i < pages1*10; i++)
			{
				var row = $("#tbToChoose1 tr").eq(1).clone();
				row.show();
				row.find("td #selectTeacher1").hide();
				row.appendTo("#tbToChoose1");
			}
			
			setRecordPage1(pages1*10);
			currentPage1 = 1;
			$("#tbToChoose1 tbody tr:gt(10)").each(function() {
				$(this).hide();
			});
			
			$("#showPage1").html(currentPage1+"/"+pages1);
			
			adjustHeight();
		}
	);	
}

//加载测评班级列表的下拉框
function showTestClass()
{
	var displayT_class = {};
	displayT_class["grade"] = $("#selectGrade").val();
	displayT_class["majorid"] = $("#selectMajor").val();
	//alert($.toJSON(displayT_class));
	$.get(
		 "../../../testdrive/index.php?r=T_class/DisplayTClass",
		{displayT_class:$.toJSON(displayT_class)},
		function(data)
		{
			//alert(data);
			
			var obj = $.evalJSON(data);
			if(obj.success == false)
			{
if(obj.results.hasOwnProperty("timeout") && obj.results.timeout)
					{
						alert(obj.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href = '../../../';
						return false;
					}
				alert("服务器端返回错误信息=>\""+obj.message+'"');
				return false;
			}
			$("#selectTestclass option:gt(0)").remove();
			if(obj.results.length != 0)
			{
				var selector=$('#selectTestclass'); 
				for (var i = 0; i < obj.results.length; i++)
				{
					selector.append('<option value="'+obj.results[i]["t_classid"]+'">'+obj.results[i]["t_classname"]+'</option>'); 
				}
			}
			
			//加载辅导员列表
			addTeachers();
		}
	);	
}

function addTeachers()
{
	$.get(
		"../../../testdrive/index.php?r=T_class/DisplayTeacherInfo",
		function(data)
		{
			var obj = $.evalJSON(data);
			if(obj.success == false)
			{
if(obj.results.hasOwnProperty("timeout") && obj.results.timeout)
					{
						alert(obj.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href = '../../../';
						return false;
					}
				alert("服务器端返回错误信息=>\""+obj.message+'"');
				return false;
			}
			
			if(obj.results.length != 0)
			{
				$('#selectTeacher option:gt(0)').remove();
				$('#selectTeacher1 option').remove();
				
				var selector=$('#selectTeacher'); 
				for (var i = 0; i < obj.results.length; i++)
				{
					selector.append('<option value="'+obj.results[i]["teacherid"]+'">'+obj.results[i]["teachername"]+'</option>'); 
					$('#selectTeacher1').append('<option value="'+obj.results[i]["teacherid"]+'">'+obj.results[i]["teachername"]+'</option>'); 
				}
			}
			else
			{
				alert('学院辅导员列表为空，请您为学院添加辅导员信息后再使用该功能~');
				$("#error").show();  $("#info").hide();
			}
			
			//添加测评班级列表
			addTClasses();
			//初始化右边的已添加表格
			$("#tbChoosed1 tbody tr:gt(0)").remove();
			initChoosedTable();
      
      		$('html,body', window.parent.parent.document).animate({
				scrollTop: $("#toChoose").offset().top+135}, 
				400
			);
		}
	);	
}

//根据测评班级id显示班级列表
function getClassByTclass()
{
		var showClassByTclass = {};
		showClassByTclass["t_classid"] =v_editATClass.find("td").eq(2).attr("name");
		var date = new Date();
		showClassByTclass["year"] = date.getFullYear();
		
		$.get(
	 		"../../../testdrive/index.php?r=T_class/DisplayTClassAll",
			{showClassByTclass:$.toJSON(showClassByTclass)},
			function(data)
			{
				var obj = $.evalJSON(data);
				if(obj.success == false)
				{
if(obj.results.hasOwnProperty("timeout") && obj.results.timeout)
					{
						alert(obj.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href = '../../../';
						return false;
					}
					alert("服务器端返回错误信息=>\""+obj.message+'"');
					return false;
				}
				
				if(obj.results.length != 0 && obj.results.classarray.length != 0)
				{
					$("#tbChoosed1 tbody tr:gt(0)").remove();
					var classes = obj.results.classarray;
					
					setRecordPage2(classes.length);
					//加载班级列表
					for(var i = 0; i <  classes.length; i++)
					{
						var tr = $("#tbChoosed1 tr").eq(1).clone();
					
						//显示测评班级名称
						tr.find("td").eq(0).html(classes[i].classname);
						tr.find("td").eq(0).attr("name", classes[i].classid);
						tr.show();
						tr.find("td #delAClass").show();
						tr.appendTo("#tbChoosed1 tbody");
					}

					currentPage2 = 1;
					$("#tbChoosed1 tbody tr:gt(10)").each(function() {
						$(this).hide();
					});
					
					$("#showPage2").html(currentPage2+"/"+pages2);

					adjustHeight();
				}
				else   //测评班级对应的班级列表为空
				{
					$("#tbChoosed1 tbody tr:gt(0)").remove();
					initChoosedTable();
				}
			}
		);	
}

//重新分页
function rePageToChooseTable(_totalRecord, _currentPage)
{
	var tempPage = _currentPage;
	setRecordPage(_totalRecord);
	
	if (tempPage < pages)  
	{
		currentPage = tempPage;
		ShowPage(tempPage*10-9, tempPage*10);
	}
	else
	{
		currentPage = pages;
		ShowPage(pages*10-9, _totalRecord);
	}
}

function rePageToChoose1Table(_totalRecord, _currentPage)
{
	var tempPage = _currentPage;
	setRecordPage1(_totalRecord);
	
	if (tempPage < pages1)  
	{
		currentPage1 = tempPage;
		ShowPage1(tempPage*10-9, tempPage*10);
	}
	else
	{
		currentPage1 = pages1;
		ShowPage1(pages1*10-9, _totalRecord);
	}
}

function rePageChoosedTable(_totalRecord, _currentPage)
{
	var tempPage = _currentPage;
	setRecordPage2(_totalRecord);
	
	if (tempPage < pages2)  
	{
		currentPage2 = tempPage;
		ShowPage2(tempPage*10-9, tempPage*10);
	}
	else
	{
		currentPage2 = pages2;
		ShowPage2(pages2*10-9, _totalRecord);
	}
}


//左边的翻页
function Table()
{
	$("#btLast").click(function() 
	{
		if(currentPage != pages)
		{
			//alert("即将跳转到最后一页");
			currentPage = pages;
			ShowPage(currentPage*10-9, totalRecord);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btNext").click(function() 
	{
		if(currentPage+1<=pages)
		{
			//alert("即将跳转到下一页");
			currentPage += 1;
			ShowPage(currentPage*10-9, currentPage*10);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btFormer").click(function() 
	{
		if(currentPage-1>0)
		{
			//alert("即将跳转到上一页");
			currentPage -= 1;
			ShowPage(currentPage*10-9, currentPage*10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
	$("#btFirst").click(function() 
	{
		if(currentPage != 1)
		{
			//alert("即将跳转到首页");
			currentPage = 1;
			ShowPage(1, 10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
	$("#btLast1").click(function() 
	{
		if(currentPage1 != pages1)
		{
			//alert("即将跳转到最后一页");
			currentPage1 = pages1;
			ShowPage1(currentPage1*10-9, totalRecord1);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btNext1").click(function() 
	{
		if(currentPage1+1<=pages1)
		{
			//alert("即将跳转到下一页");
			currentPage1 += 1;
			ShowPage1(currentPage1*10-9, currentPage1*10);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btFormer1").click(function() 
	{
		if(currentPage1-1>0)
		{
			//alert("即将跳转到上一页");
			currentPage1 -= 1;
			ShowPage1(currentPage1*10-9, currentPage1*10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
	$("#btFirst1").click(function() 
	{
		if(currentPage1 != 1)
		{
			//alert("即将跳转到首页");
			currentPage1 = 1;
			ShowPage1(1, 10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
	$("#btLast2").click(function() 
	{
		if(currentPage2 != pages2)
		{
			//alert("即将跳转到最后一页");
			currentPage2 = pages2;
			ShowPage2(currentPage2*10-9, totalRecord2);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btNext2").click(function() 
	{
		if(currentPage2+1<=pages2)
		{
			//alert("即将跳转到下一页");
			currentPage2 += 1;
			ShowPage2(currentPage2*10-9, currentPage2*10);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btFormer2").click(function() 
	{
		if(currentPage2-1>0)
		{
			//alert("即将跳转到上一页");

			currentPage2 -= 1;
			ShowPage2(currentPage2*10-9, currentPage2*10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
	$("#btFirst2").click(function() 
	{
		if(currentPage2 != 1)
		{
			//alert("即将跳转到首页");
			currentPage2 = 1;
			ShowPage2(1, 10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
}

//传入需要被显示的数据的位置
//仅显示_min 和 _max之间的数据
function ShowPage(_min, _max)
{
	$("#tbToChoose tbody tr").each(function() {
		$(this).show();
	});
	
	var variable = "#tbToChoose tbody tr:lt("+_min+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	variable = "#tbToChoose tbody tr:gt("+_max+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	$("#showPage").html(currentPage+"/"+pages);
	
	adjustHeight();
}
function ShowPage1(_min, _max)
{
	$("#tbToChoose1 tbody tr").each(function() {
		$(this).show();
	});
	
	var variable = "#tbToChoose1 tbody tr:lt("+_min+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	variable = "#tbToChoose1 tbody tr:gt("+_max+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	$("#showPage1").html(currentPage1+"/"+pages1);
	
	adjustHeight();
}
function ShowPage2(_min, _max)
{
	$("#tbChoosed1 tbody tr").each(function() {
		$(this).show();
	});
	
	var variable = "#tbChoosed1 tbody tr:lt("+_min+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	variable = "#tbChoosed1 tbody tr:gt("+_max+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	$("#showPage2").html(currentPage2+"/"+pages2);
	
	adjustHeight();
}

//设置页数和总记录数
function setRecordPage(number)
{
	totalRecord = number;
	if(number <= 0)
	{
		totalRecord = 0;
		pages = 1;
	}
	else
	{//每页有10条记录
		pages = parseInt(number/10);
		if(number%10 != 0)
			pages += 1;
	}
	
	//页数>1 需翻页按钮
	if(pages > 1)
	{
		$("#btLast").show(); $("#btNext").show(); $("#btFormer").show(); $("#btFirst").show(); 
	}
	else
	{
		$("#btLast").hide(); $("#btNext").hide(); $("#btFormer").hide(); $("#btFirst").hide(); 
	}
}

function setRecordPage1(number)
{
	totalRecord1 = number;
	if(number <= 0)
	{
		totalRecord1 = 0;
		pages1 = 1;
	}
	else
	{//每页有10条记录
		pages1 = parseInt(number/10);
		if(number%10 != 0)
			pages1 += 1;
	}
	
	//页数>1 需翻页按钮
	if(pages1 > 1)
	{
		$("#btLast1").show(); $("#btNext1").show(); $("#btFormer1").show(); $("#btFirst1").show(); 
	}
	else
	{
		$("#btLast1").hide(); $("#btNext1").hide(); $("#btFormer1").hide(); $("#btFirst1").hide(); 
	}
}

function setRecordPage2(number)
{
	totalRecord2 = number;
	if(number <= 0)
	{
		totalRecord2 = 0;
		pages2 = 1;
	}
	else
	{//每页有10条记录
		pages2 = parseInt(number/10);
		if(number%10 != 0)
			pages2 += 1;
	}
	
	//页数>1 需翻页按钮
	if(pages2 > 1)
	{
		$("#btLast2").show(); $("#btNext2").show(); $("#btFormer2").show(); $("#btFirst2").show(); 
	}
	else
	{
		$("#btLast2").hide(); $("#btNext2").hide(); $("#btFormer2").hide(); $("#btFirst2").hide(); 
	}
}

//调整高度
function adjustHeight()
{
	var height = document.documentElement.scrollHeight>document.body.scrollHeight
						? document.body.scrollHeight:document.documentElement.scrollHeight; 
						
	if(height > 420)
		parent.parent.document.getElementById("frame_content").height=height + 40;
	else
		parent.parent.document.getElementById("frame_content").height=420;	
}
