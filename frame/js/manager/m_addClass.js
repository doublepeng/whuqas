var allFields;
// JavaScript Document
$(document).ready(function() {
	parent.parent.document.getElementById("frame_content").height=420;      

	//隐藏模板tr
	$("#tb tr").eq(1).hide();
	var i = 0;
	
	//首先填充年份下拉框
	loadYear();

	//添加记录按钮
	$("#BtAdd").click(function() {
　　 //复制一行
		var tr = $("#tb tr").eq(1).clone();
		tr.find("td").get(0).innerHTML = ++i;
		tr.show();
		tr.appendTo("#tb tbody");
		
		$("#tb tbody tr:last :input:last").keypress(function(event){
			//tab enter
			if(event.keyCode==13 || event.keyCode==9){
				$("#BtAdd").click();
			}
		});
		
		adjustHeight();
	});

	//删除所选记录按钮
	$("#BtDel").click(function() {
		var selected = false;
		$("#tb tr:gt(1)").each(function() {
			if ($(this).find("#CK").get(0).checked == true) {
				selected = true;
				return false;
			}
		 });
		if(!selected)
		{
			alert("请您选择要删除的班级信息~");
			return false;
		}
		
		$("#tb tr:gt(1)").each(function() {
			if ($(this).find("#CK").get(0).checked == true) {
				$(this).remove();
			}
		});
		
		adjustHeight();
			
		//父窗口iframe的宽度自适应
		i = 0;
		$("#tb tr:gt(1)").each(function() {
			$(this).find("td").get(0).innerHTML = ++i;
		});
		$("#CKA").attr("checked", false);
	});
	
	//选择所有行
	$("#CKA").click(function() {
		$("#tb tr:gt(1)").each(function() {
			$(this).find("#CK").get(0).checked = $("#CKA").get(0).checked;
		});
	});
	
	//保存记录
	$("#BtSave").click(function() {
		if(validator())
		{
			var SaveArray = [];
			$("#tb tr:gt(1)").each(function() {
				if ($(this).find("#className").attr("name")=="1")								
				{
					var SaveRow = {};
					SaveRow['grade'] = $(this).find("#selectYear").val();
					SaveRow['major'] = $(this).find("#selectMajor").find("option:selected").text();
					SaveRow['classname'] = $(this).find("#className").val();
					SaveArray.push(SaveRow);
				}
				//$(this).find("#className").attr("name","0");
			});
			
			//alert($.toJSON(SaveArray));
			$.get(
			   "../../../testdrive/index.php?r=WhuClass/SaveClass",
			   {saveClass: $.toJSON(SaveArray)},
			   function(data)
			   {
					var obj = $.evalJSON(data);
					if(obj.success == false)
					{
						if(obj.results.hasOwnProperty("timeout") && obj.results.timeout)
					{
						alert(obj.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href = '../../../';
						return false;
					}
						alert("保存记录失败！"+obj.message);
						return false;
					}
					else
					{
						alert("保存班级信息成功~~");
						$("#tb tr:gt(1)").each(function() {
							if ($(this).find("#className").attr("name") == "1")	
							{
								$(this).find("#className").attr("name", "0");
							}
						});
						//return true;
					}
				}
			);
		}
	});
})

//加载专业列表
function loadMajor()
{
	$.get(
		"../../../testdrive/index.php?r=WhuClass/DisplayMajorToList",
		function(data)
		{
			var obj = $.evalJSON(data);
			if(obj.success == false)
			{
				if(obj.results.hasOwnProperty("timeout") && obj.results.timeout)
					{
						alert(obj.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href = '../../../';
						return false;
					}
				alert("数据库中无专业数据信息,请找院系管理员添加"+obj.message);
				return false;
			}
			for (var i = 0; i < obj.results.length; i++)
			{
				$('#selectMajor').append('<option value="'+obj.results[i]["majorid"]+'">'+obj.results[i]["majorname"]+'</option>');  
			}

			$("#BtAdd").click();
		}
	);	
}

//年份下拉框
function loadYear()
{
	var years;
	var today = new Date();
	Years = today.getFullYear();
	var intYears;
	intYears = parseInt(Years);

	var selector=$('#selectYear'); 
	/*for(var i=intYears-4; i< intYears +4;i++)
	{
		selector.append('<option value="'+i+'">'+i+'级</option>');  
	} */
	for(var i=intYears-4; i< intYears +4;i++)
	{
		if(i == intYears)
			selector.append('<option selected="selected" value="'+i+'">'+i+'级</option>');  	 
		else
			selector.append('<option value="'+i+'">'+i+'级</option>');  
	} 
	//selector.val(intYears);

	//接着加载班级列表
	loadMajor();	
}

//检查已添加条目的正确性
function updateTips( t ) {
	$( ".validateTips")
		.text( t )
		.addClass( "ui-state-highlight" );
	setTimeout(function() {
		$( ".validateTips").removeClass( "ui-state-highlight", 1500 );
		$( ".validateTips" ).text("");
	}, 1500 );
}

function checkLength( o, n, min, max ) {
	if ( o.val().length > max || o.val().length < min ) {
		o.addClass( "ui-state-error" );
		updateTips( '提示: " '+n+' "' + " 的长度应该在 " +
			min + " 和 " + max + "之间." );
		return false;
	} else {
		return true;
	}
}

function checkRegexp( o, regexp, n ) {
	if ( !( regexp.test( o.val() ) ) ) {
		o.addClass( "ui-state-error" );
		updateTips( n );
		return false;
	} else {
		return true;
	}
}

function validator()
{
	if($("#tb tr").length <= 2)
	{
		updateTips( "您还没有添加新纪录，无需保存" );
		return false;
	}
		
	var totalRows = new Array();
	var success = true;
	$("#tb tr:gt(1)").each(function() 
	{
		var className = $(this).find("#className");
		allFields = $( [] ).add( className );        
		var bValid = true;
		allFields.removeClass( "ui-state-error" );
		
		bValid = bValid && checkLength( className, "班级名称", 2, 25);
		//bValid = bValid && checkRegexp( className, /^[\u4e00-\u9fa5\da-zA-Z\-\_]+$/, "\"班级名称\"不能包含特殊字符，请输入中文、英文单词或者数字" );
		
		if ( ! bValid ) {
			success = false;
			return false;
		}
	});
	return success;
}

function adjustHeight()
{
	var height = document.body.clientHeight;
	//alert(height);
	if(height > 420)
		parent.parent.document.getElementById("frame_content").height=height + 40;
	else
		parent.parent.document.getElementById("frame_content").height=420;	
}
