// JavaScript Document
var allFields;	
$(document).ready(function() {
	parent.parent.document.getElementById("frame_content").height=420;	   

	//隐藏模板tr
	$("#tb tr").eq(1).hide();
	var i = 0;
	//添加记录按钮
	$("#BtAdd").click(function() {
　　 //复制一行
		var tr = $("#tb tr").eq(1).clone();
		tr.find("td").get(0).innerHTML = ++i;
		tr.show();
		tr.appendTo("#tb tbody");
		$("#tb tbody tr:last :input:last").keypress(function(event){
			//tab enter
			if(event.keyCode==13 || event.keyCode==9){
				$("#BtAdd").click();
			}
		});
		adjustHeight();
	});
	
  $("#BtAdd").click();
	
	//删除所选记录按钮
	$("#BtDel").click(function() {
		var selected = false;
		$("#tb tr:gt(1)").each(function() {
			if ($(this).find("#CK").get(0).checked == true) {
				selected = true;
				return false;
			}
		 });
		if(!selected)
		{
			alert("请您选择要删除的专项奖学金信息！");
			return false;
		}
		$("#tb tr:gt(1)").each(function() {
			if ($(this).find("#CK").get(0).checked == true) {
				$(this).remove();
			}
		});
		
		adjustHeight();
		//父窗口iframe的宽度自适应
			
		i = 0;
		$("#tb tr:gt(1)").each(function() {
			$(this).find("td").get(0).innerHTML = ++i;
		});
		$("#CKA").attr("checked", false);
	});
	
	//选择所有行
	$("#CKA").click(function() {
		$("#tb tr:gt(1)").each(function() {
			$(this).find("#CK").get(0).checked = $("#CKA").get(0).checked;
		});
	});

	//保存记录
	$("#BtSave").click(function() {
		if(validator())
		{
			var saveScholarType = [];
			$("#tb tr:gt(1)").each(function() {
				if ($(this).find("#selectSpecialScho").attr("name")=="1")	
				{	
					var SaveRow = {};		
					SaveRow['scholarname'] = $(this).find("#selectSpecialScho").val();
					SaveRow['money'] = $(this).find("#money").val();
					SaveRow['remark'] = $(this).find("#remark").val();
					SaveRow['scholartype'] = "专项奖学金";
					saveScholarType.push(SaveRow);
				}
				//$(this).find("#selectSpecialScho").attr("name","0");
			});
			//alert($.toJSON(saveScholarType));
      if(!confirm("您确认提交录入的专项奖学金信息？"))
      {
        return false;
      }

			$.post(
        "../../../testdrive/index.php?r=Scholarshiptype/SaveScholarType",
			  {saveScholarType: $.toJSON(saveScholarType)},
			  function(data)
			  {
          var obj = $.evalJSON(data);
					if(obj.success == false)
					{
						if(obj.results.hasOwnProperty("timeout") && obj.results.timeout)
					{
						alert(obj.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href = '../../../';
						return false;
					}
						alert("保存专项奖学金信息失败！"+obj.message);
						return false;
					}
					else
					{
						alert("保存专项奖学金信息成功！");
            $("#tb tr:gt(1)").each(function() {
				      if ($(this).find("#selectSpecialScho").attr("name")=="1")	
				      {	
                $(this).find("#selectSpecialScho").attr("name","0");
				      }
			      });
					}	
				} 
			);
		}
	});
})

//检查已添加条目的正确性
function updateTips( t ) {
	$( ".validateTips")
		.text( t )
		.addClass( "ui-state-highlight" );
	setTimeout(function() {
		$( ".validateTips").removeClass( "ui-state-highlight", 1500 );
		$( ".validateTips" ).text("");
	}, 1500 );
}

function checkLength( o, n, min, max ) {
	if ( o.val().length > max || o.val().length < min ) {
		o.addClass( "ui-state-error" );
		updateTips( '提示: " '+n+' "' + " 的长度应该在 " +
			min + " 和 " + max + "之间." );
		return false;
	} else {
		return true;
	}
}

function checkRegexp( o, regexp, n ) {
	if ( !( regexp.test( o.val() ) ) ) {
		o.addClass( "ui-state-error" );
		updateTips( n );
		return false;
	} else {
		return true;
	}
}

function checkValue( o, n, min, max ) {
	if ( parseFloat(o.val()) > max || parseFloat(o.val()) < min) {
		o.addClass( "ui-state-error" );
		updateTips( '" '+n+' "' + " 的值 " +
			min + " 和 " + max + "之间." );
		return false;
	} else {
		return true;
	}
}

function validator()
{
	if($("#tb tr").length <= 2)
	{
		updateTips( "您还没有添加新纪录，无需保存" );
		return false;
	}

	var success = true;
	$("#tb tr:gt(1)").each(function() 
	{
		var selectSpecialScho = $(this).find("#selectSpecialScho"),
		  money = $(this).find("#money"),
		  remark = $(this).find("#remark"),
		  allFields = $( [] ).add( selectSpecialScho ).add(money).add(remark); 

		var bValid = true;
		allFields.removeClass( "ui-state-error" );
		bValid = bValid && checkLength( selectSpecialScho, "专项奖学金名称", 2, 30);
		bValid = bValid && checkLength( money, "专项奖学金金额", 2, 7);
    bValid = bValid && checkRegexp( money, /^\d+(.\d+$|\d*$)/, "\"专项奖学金金额\"应该输入合法数字" );
    bValid = bValid && checkValue( money, "专项奖学金金额", 1, 1000000);
    bValid = bValid && checkLength( remark, "备注", 2, 100);
		//bValid = bValid && checkRegexp( selectSpecialScho, /^[\u4e00-\u9fa5\da-zA-Z\-\_]+$/, "\"专项奖学金\"不能包含特殊字符，请输入中文、英文单词或者数字" );
		//bValid = bValid && checkRegexp( remark, /^[\u4e00-\u9fa5\da-zA-Z\-\_]+$/, "\"备注\"不能包含特殊字符，请输入中文、英文单词或者数字" );
		if ( ! bValid ) {
			success = false;
			return false;
		}
	});
	return success;
}

function adjustHeight()
{
	var height = document.body.clientHeight;
	//alert(height);
	if(height > 420)
		parent.parent.document.getElementById("frame_content").height=height + 40;
	else
		parent.parent.document.getElementById("frame_content").height=420;	
}






