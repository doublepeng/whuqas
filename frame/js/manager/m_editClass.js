// JavaScript Document
var totalRecord = 0;
var pages = 0;
var currentPage = 1;

var currentMem;   //用来记录当前编辑的是哪一行
var update = {};   //用来记录用户需要更新的班级
var delete1 = {};   //用来记录删除的班级

$(function() {
	parent.parent.document.getElementById("frame_content").height=420;   
	//隐藏翻页按钮
	$("#btLast").hide(); $("#btNext").hide(); $("#btFormer").hide(); $("#btFirst").hide(); 
	//隐藏第一行
	$("#tb tr").eq(1).hide();
	
	//获取查询年级
	setSearchGrade();
	
	//改变专业
	changeMajor();
	
	//改变年级
	changeGrade();
	
	//删除所选记录按钮
	$("#BtDel").click(function() {
		var selected = false;
		$("#tb tr:gt(1)").each(function() {
			if ($(this).find("#CK").get(0).checked == true) {
				selected = true;
				return false;
			}
		 });
		if(!selected)
		{
			alert("请您选择要删除的班级~");
			return false;
		}
		$("#tb tr:gt(1)").each(function() {
			if ($(this).find("#CK").get(0).checked == true) {
				delete1[$(this).find("#className").attr("name")] = {
					'classid': $(this).find("#className").attr("name")
				};
				$(this).remove();
				totalRecord--;
			}
		});
		
		adjustHeight();
		
		//父窗口iframe的宽度自适应
		$("#CKA").attr("checked", false);
		//重新分页
		rePage(totalRecord, currentPage);
	});
	
	//选择所有行
	$("#CKA").click(function() {
		$("#tb tbody tr").each(function() {
			if(!$(this).is(":hidden")) 	
				$(this).find("#CK").get(0).checked = $("#CKA").get(0).checked;
		});
	});
	
	$("#btLast").click(function() 
	{
		if(currentPage != pages)
		{
			//alert("即将跳转到最后一页");
			currentPage = pages;
			ShowPage(currentPage*10-9, totalRecord);
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btNext").click(function() 
	{
		if(currentPage+1<=pages)
		{
			//alert("即将跳转到下一页");
			currentPage += 1;
			ShowPage(currentPage*10-9, currentPage*10);
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btFormer").click(function() 
	{
		if(currentPage-1>0)
		{
			//alert("即将跳转到上一页");
			currentPage -= 1;
			ShowPage(currentPage*10-9, currentPage*10);
		}
		else
		{
			alert("已是首页!");
		}
	});
	$("#btFirst").click(function() 
	{
		if(currentPage != 1)
		{
			//alert("即将跳转到首页");
			currentPage = 1;
			ShowPage(1, 10);
		}
		else
		{
			alert("已是首页!");
		}
	});
	//保存记录
	$("#BtSave").click(function() {
		if(validator())
		{
			//update
			var saveEditClass = {};
			saveEditClass["grade"] = $('#selectGrade').find('option:selected').text();
			saveEditClass["majorname"] = $('#selectMajor').find('option:selected').text();
			var saveeditclassbymajor = {};
			saveeditclassbymajor["update"] = [];
			saveeditclassbymajor["delete1"] = [];
			$.each(update, function(key, value){
				saveeditclassbymajor["update"].push({
					'classid': key,
					'classname': value["classname"]
				});
			});
			$.each(delete1, function(key, value){
				saveeditclassbymajor["delete1"].push({
					'classid': key
				});
			});
			saveEditClass["saveeditclassbymajor"] = saveeditclassbymajor;
			//alert($.toJSON(saveEditClass));
			if(saveeditclassbymajor["update"].length == 0 && saveeditclassbymajor["delete1"].length == 0)
			{
				alert("您尚未进行编辑操作，请您编辑后再提交保存！");
				return false;
			}
			$.post(
			    "../../../testdrive/index.php?r=WhuClass/SaveEditClass",
			   {
				   saveEditClass: $.toJSON(saveEditClass)
			   },
			   function(data)
			   {
					var obj = $.evalJSON(data);
					if(obj.success == false)
					{
if(obj.results.hasOwnProperty("timeout") && obj.results.timeout)
					{
						alert(obj.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href = '../../../';
						return false;
					}
						alert("保存记录失败！"+obj.message);
						return false;
					}
					else
					{
						alert("保存编辑后的班级信息成功！");
						update = {};
						delete1 = {};
						//return true;
					}
				}
			);
		}
	});
	
	
});

//设置下拉框中的年级
function setSearchGrade()
{
	  var years;
	  var today;
	  today = new Date();
	  Years = today.getFullYear();
	  var intYears;
	  intYears = parseInt(Years);
	  var selector=$('#selectGrade'); 
	  for(var i=intYears-4; i< intYears +4;i++)
	  {
		  if(i == intYears)
			selector.append('<option selected="selected" value="intYears">'+i+'</option>');  	 
		  else
			selector.append('<option value="intYears">'+i+'</option>');  
	  }   
	  //载入一个学院的全部专业
	loadMajor();
}

//载入一个学院的全部专业
function loadMajor()
{
	var displayMajorToList = {};
	displayMajorToList['grade'] = $('#selectGrade').find('option:selected').text();
	
	//alert( $.toJSON(displayMajorToList));
	$.get(  
		 "../../../testdrive/index.php?r=WhuClass/DisplayMajorToList",
		{
			displayMajorToList : $.toJSON(displayMajorToList)
		},
		function(data)
		{
			var obj = $.evalJSON(data);
			if(obj.success == false)
			{
if(obj.results.hasOwnProperty("timeout") && obj.results.timeout)
					{
						alert(obj.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href = '../../../';
						return false;
					}
				alert("服务器端返回错误信息=>\""+obj.message+'"');
				return false;
			}
			var selector=$('#selectMajor'); 
			if (obj.results.length == 0)
			{
        alert("您尚未为该年级添加专业信息，请选择“新增专业信息”进行添加。");
      }
			else
			{
				for (var i = 0; i < obj.results.length; i++)
				{
					/*alert(obj.results[i]["majorid"]);*/
					selector.append('<option>'+obj.results[i]["majorname"]+'</option>'); 
				}
				//alert($('#selectMajor').find('option:selected').text());
				showTable();
			}
		}
		//载入班级列表
		
	);	
}
//编辑班级
function editARow()
{
	if(currentMem.find("#edit").html() == "编辑")
	{
		currentMem.find("#className").removeAttr("disabled");
		currentMem.find("#edit").html("保存");
	}
	else if(currentMem.find("#edit").html() == "保存")
	{
		if(validateARow(currentMem))
		{
			if(currentMem.find("#className").attr("name") != "")
				update[currentMem.find("#className").attr("name")] = {
					'classname': currentMem.find("#className").val()
				};
			currentMem.find("#className").attr("disabled", "disabled");
			currentMem.find("#edit").html("编辑");
		}
	}
}

//重新分页
function rePage(totalRecord, currentPage)
{
	var tempPage = currentPage;
	setRecordPage(totalRecord);
	if (tempPage < pages)  
	{
		ShowPage(tempPage*10-9, tempPage*10);
		currentPage = tempPage;
	}
	else
	{
		ShowPage(pages*10-9, totalRecord);
		currentPage = pages;
	}
}

//传入需要被显示的数据的位置
//仅显示_min 和 _max之间的数据
function ShowPage(_min, _max)
{
	$("#tb tbody tr").each(function() {
		$(this).show();
	});
	
	var variable = "#tb tbody tr:lt("+_min+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	variable = "#tb tbody tr:gt("+_max+")";
	$(variable).each(function() {
		$(this).hide();
	});
	$("#showPage").html(currentPage+"/"+pages);
	adjustHeight();
}

//设置页数和总记录数
function setRecordPage(number)
{
	totalRecord = number;
	if(number <= 0)
	{
		totalRecord = 0;
		pages = 1;
	}
	else
	{//每页有10条记录
		pages = parseInt(number/10);
		if(number%10 != 0)
			pages += 1;
	}
	//页数>1 需翻页按钮
	if(pages > 1)
	{
		$("#btLast").show(); $("#btNext").show(); $("#btFormer").show(); $("#btFirst").show(); 
	}
	else
	{
		$("#btLast").hide(); $("#btNext").hide(); $("#btFormer").hide(); $("#btFirst").hide(); 
	}
}

function showTable()
{	
	var displayClassByYearByMajor = {};
	displayClassByYearByMajor['grade'] = $('#selectGrade').find('option:selected').text();
	displayClassByYearByMajor['major'] = $('#selectMajor').find('option:selected').text();
	//alert( $.toJSON(displayClassByYearByMajor));
	$.get(
		  "../../../testdrive/index.php?r=WhuClass/DisplayClassByYearByMajor",
		{
			displayClassByYearByMajor : $.toJSON(displayClassByYearByMajor)
		},
		function(data)
		{
			//alert(data);
			var obj = $.evalJSON(data);
			if(obj.success == false)
			{
if(obj.results.hasOwnProperty("timeout") && obj.results.timeout)
					{
						alert(obj.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href = '../../../';
						return false;
					}
				alert("数据库中无数据~"+obj.message);
				return false;
			}
			setRecordPage(obj.results.length);
			if(obj.results.length == 0)
			{
        updateTips("对不起，没有相应的班级信息，请选择“添加班级信息”进行添加操作。");
      }
			else
			{
				for (var i = 0; i < obj.results.length; i++)
				{
					var tr = $("#tb tr").eq(1).clone();
					tr.find("td #className").val(obj.results[i]['classname']);
					tr.find("td #className").attr("name",obj.results[i]['classid']);
					tr.show();
					tr.appendTo("#tb tbody");
				}
				currentPage = 1;
				$("#tb tbody tr:gt(10)").each(function() {
					$(this).hide();
				});
				$("#showPage").html(currentPage+"/"+pages);
			}
			adjustHeight();
		}
	);
}
function changeMajor()
{
	$("#selectMajor").change(function(){
		var selected = $("#selectMajor").find("option:selected").text();
		$("#tb tbody tr:gt(0)").each(function(){ $(this).remove(); });
		showTable();
	});		
}
//改变学年
function changeGrade()
{
	$("#selectGrade").change(function(){
		var selected = $("#selectGrade").find("option:selected").text();
		$("#tb tbody tr:gt(0)").each(function(){ $(this).remove(); });
		showTable();
	});		
}

//检查已添加条目的正确性
function updateTips( t ) {
	$( ".validateTips")
		.text( t )
		.addClass( "ui-state-highlight" );
	setTimeout(function() {
		$( ".validateTips").removeClass( "ui-state-highlight", 1500 );
		$( ".validateTips" ).text("");
	}, 3000 );
}

function checkLength( o, n, min, max ) {
	if ( o.val().length > max || o.val().length < min ) {
		o.addClass( "ui-state-error" );
		updateTips( '提示: " '+n+' "' + " 的长度应该在 " +
			min + " 和 " + max + "之间." );
		return false;
	} else {
		return true;
	}
}

function checkRegexp( o, regexp, n ) {
	if ( !( regexp.test( o.val() ) ) ) {
		o.addClass( "ui-state-error" );
		updateTips( n );
		return false;
	} else {
		return true;
	}
}

//验证一行
function validateARow(row)
{
	var className = row.find("#className"),
		  allFields = $( [] ).add( className ); 
  var bValid = true;
	allFields.removeClass( "ui-state-error" );
	bValid = bValid && checkLength( className, "班级名称", 2, 25);
	if ( ! bValid ) {
		return false;
	}

	return true;
}

function validator()
{
	var success = true;
	$("#tb tr:gt(1)").each(function() 
	{
		success = validateARow($(this));
		if(success == false)
		{
			return false;
		}
	});
	return success;
}

function adjustHeight()
{
	var height = document.body.clientHeight;
	//alert(height);
	if(height > 420)
		parent.parent.document.getElementById("frame_content").height=height + 40;
	else
		parent.parent.document.getElementById("frame_content").height=420;	
}




