﻿// JavaScript Document
$(function() {
  parent.parent.document.getElementById("frame_content").height=420;	
	
  //载入年份
	loadYear1();
		   
});

function loadYear1()
{
	var years;
	var today;
	today = new Date();
	Years = today.getFullYear();
	var intYears;
	intYears = parseInt(Years);
	var selector=$('#selectYear1'); 
	for(var i=intYears-4; i< intYears +4;i++)
	{
		selector.append('<option value='+i+'>'+i+'年</option>');  
	}	 
	selector.val(intYears);
}

//下载汇总表
function editARow1()
{
	$.get(
		"../../../testdrive/index.php?r=site/QASEnd",
		function(data)
		{
			var ret = $.evalJSON(data);
			if(ret.success == true)
			{
				var downloadtable1 = {};
				downloadtable1["year"] = $('#selectYear1').find('option:selected').val();
				var href = "../../../testdrive/protected/controllers/Excel/Tests/XueyuanDownload.php?downloadtable=";
				href += $.toJSON(downloadtable1);
				//alert(href);
				$("#edit1").attr("href", href);
				window.open(href,"_parent");
			}
			else
			{
				if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
					{
						alert(ret.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href = '../../../';
						return false;
					}
				alert(ret.message);
			}
			$("#edit1").attr("href", 'javascript:void(0)');
		}
	);
}

