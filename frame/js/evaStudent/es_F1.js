// JavaScript Document
// JavaScript Document
var totalRecord = 0;
var pages = 0;
var currentPage = 1;
$(function() {
	parent.parent.document.getElementById("frame_content").height=530;	
	
	//隐藏翻页按钮
	$("#btLast").hide(); $("#btNext").hide(); $("#btFormer").hide(); $("#btFirst").hide(); 
	//隐藏第一行
	$("#tb tr").eq(2).hide();
	
	showTable();

	$("#btLast").click(function() 
	{
		if(currentPage != pages)
		{
			//alert("即将跳转到最后一页");
			currentPage = pages;
			ShowPage(currentPage*10-9, totalRecord);
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btNext").click(function() 
	{
		if(currentPage+1<=pages)
		{
			//alert("即将跳转到下一页");
			currentPage += 1;
			ShowPage(currentPage*10-9, currentPage*10);
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btFormer").click(function() 
	{
		if(currentPage-1>0)
		{
			//alert("即将跳转到上一页");
			currentPage -= 1;
			ShowPage(currentPage*10-9, currentPage*10);
		}
		else
		{
			alert("已是首页!");
		}
	});
	$("#btFirst").click(function() 
	{
		if(currentPage != 1)
		{
			//alert("即将跳转到首页");
			currentPage = 1;
			ShowPage(1, 10);
		}
		else
		{
			alert("已是首页!");
		}
	});

	$("#btConfirm").click(function() 
	{
		if(validator())
		{
			var insertBasicScore = {};
			insertBasicScore.sid = "";
			insertBasicScore.type = "team";
			insertBasicScore.results = [];
			$("#tb tbody tr:gt(0)").each(function() {
				var dict = {};
				dict['sid'] = $(this).find("td").get(0).innerHTML;
				dict['A1'] = $(this).find("#A1").val();
				dict['A2'] = $(this).find("#A2").val();
				dict['A3'] = $(this).find("#A3").val();
				dict['A4'] = $(this).find("#A4").val();
				dict['A5'] = $(this).find("#A5").val();
				insertBasicScore.results.push(dict);
			});
			
			//alert($.toJSON(insertBasicScore));
			
			if(confirm('您将提交基本素质测评成绩，审核通过前您可以通过选择‘基本素质测评(F1)’菜单进行修改，确定继续？'))
			{
				$("#layer").height(parent.parent.document.getElementById("frame_content").height);
				$("#layer").mask("正在提交，请稍等...");
				$.post(
					"../../../testdrive/index.php?r=temp/saveBscores",
					{"insertBasicScore": $.toJSON(insertBasicScore)},
					function(data)
					{
						var ret = $.evalJSON(data);
						if(ret.success == true)
						{
							alert("提交成功!");
							$("#btConfirm span").html("已提交");
							$("#btConfirm").attr("disabled", "disabled");
						}
						else
						{
							//alert('服务器端返回错误信息=>"'+ret.message+'"');
							if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
							{
								alert(ret.message+'  页面即将跳转到首页～');
								window.parent.parent.location.href='../../../';
								return false;
							}
							alert(ret.message);
						}
						$("#layer").unmask();
					}
				);
			}
		}
	});
});


//传入需要被显示的数据的位置
//仅显示_min 和 _max之间的数据
function ShowPage(_min, _max)
{
	$("#tb tbody tr").each(function() {
		$(this).show();
	});
	
	var variable = "#tb tbody tr:lt("+_min+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	variable = "#tb tbody tr:gt("+_max+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	$("#showPage").html(currentPage+" 共"+pages+"页");
	
	adjustHeight();
}

//设置页数和总记录数
function setRecordPage(number)
{
	totalRecord = number;
	if(number <= 0)
	{
		totalRecord = 0;
		pages = 0;
	}
	else
	{//每页有10条记录
		pages = parseInt(number/10);
		if(number%10 != 0)
			pages += 1;
	}
	//页数>1 需翻页按钮
	if(pages > 1)
	{
		$("#btLast").show(); $("#btNext").show(); $("#btFormer").show(); $("#btFirst").show(); 
	}
}


//判断审核是否通过
/*function judgeIsPassed(isPassed)
{
	if(parseInt(isPassed) == 2)
	{
		$("#btConfirm").hide();
		$("#tb tr:gt(2)").each(function() {
			$(this).find("#A1").attr("disabled", "disabled");
			$(this).find("#A2").attr("disabled", "disabled");
			$(this).find("#A3").attr("disabled", "disabled");
			$(this).find("#A4").attr("disabled", "disabled");
			$(this).find("#A5").attr("disabled", "disabled");
		});
		
		$( ".validateTips")
		.text( "您的基本素质评分已经由辅导员审核通过，不能修改~" )
		.addClass( "ui-state-highlight" );
		setTimeout(function() {
			$( ".validateTips").removeClass( "ui-state-highlight", 1500 );
			$( ".validateTips" ).text("");
		}, 3000 );
	}
}*/

function judgeIsPassed(isPassed, selector)
{
	if(parseInt(isPassed) == 2)
	{
		selector.find("#A1").attr("readonly", "readonly");
		selector.find("#A2").attr("readonly", "readonly");
		selector.find("#A3").attr("readonly", "readonly");
		selector.find("#A4").attr("readonly", "readonly");
		selector.find("#A5").attr("readonly", "readonly");
		return 1;
	}
	return 0;
}

function showTable()
{
	var date = new Date();
	var vYear = date.getFullYear();
	
	var searchBasicMember = {};
	searchBasicMember["sid"] = "";
	searchBasicMember["year"] = vYear;
	$.post(
		"../../../testdrive/index.php?r=temp/searchBasicMember",
		{"searchBasicMember": $.toJSON(searchBasicMember)},
		function(data)
		{
			var obj = $.evalJSON(data);
			if(obj.success == false)
			{
				if(obj.results.hasOwnProperty("timeout") && obj.results.timeout)
				{
					alert(obj.message+'  页面即将跳转到首页～');
					window.parent.parent.location.href='../../../';
					return false;
				}

				alert("您暂时不能进行基本素质测评,服务器返回错误信息=>\""+obj.message+'"');
				return false;
			}
			
			if(obj.results.length != 0)
			{
				var results = obj.results.score;
				setRecordPage(results.length);
				var hasPassedNum = 0;  //用来记录有多少个学生已经通过审核
				for (var i = 0; i < results.length; i++)
				{
					var tr = $("#tb tr").eq(2).clone();
					tr.find("td").get(0).innerHTML = results[i]['sid'];
					tr.find("td").get(1).innerHTML = results[i]['sname'];
					tr.find("#A1").val(parseFloat(results[i]['A1']));
					tr.find("#A2").val(parseFloat(results[i]['A2']));
					tr.find("#A3").val(parseFloat(results[i]['A3']));
					tr.find("#A4").val(parseFloat(results[i]['A4']));
					tr.find("#A5").val(parseFloat(results[i]['A5']));
					if(results[i]['t_A1'] != "")
					{
						tr.find("#t_A1").html(parseFloat(results[i]['t_A1']));
					}
					if(results[i]['t_A2'] != "")
					{
						tr.find("#t_A2").html(parseFloat(results[i]['t_A2']));
					}
					if(results[i]['t_A3'] != "")
					{
						tr.find("#t_A3").html(parseFloat(results[i]['t_A3']));
					}
					if(results[i]['t_A4'] != "")
					{
						tr.find("#t_A4").html(parseFloat(results[i]['t_A4']));
					}
					if(results[i]['t_A5'] != "")
					{
						tr.find("#t_A5").html(parseFloat(results[i]['t_A5']));
					}
				
					hasPassedNum += judgeIsPassed(results[i]["isPassed"], tr);
					
					tr.show();
					tr.appendTo("#tb tbody");
				}
				
				//如果该学生评定的所有学生的f1成绩被辅导员审核通过
				if(hasPassedNum == results.length)
				{
					$("#btConfirm").hide();

					$( ".validateTips")
					.text( "您的基本素质评分已经由辅导员审核通过，不能修改~" )
					.addClass( "ui-state-highlight" );
					setTimeout(function() {
						$( ".validateTips").removeClass( "ui-state-highlight", 1500 );
						$( ".validateTips" ).text("");
					}, 3000 );
				}
				
				currentPage = 1;
				$("#tb tbody tr:gt(10)").each(function() {
					$(this).hide();
				});
			
				$("#showPage").html(currentPage+" 共"+pages+"页");
				adjustHeight();
				
				$('html,body', window.parent.parent.document).animate({
					scrollTop: $("#tb").offset().top+135}, 
					400
				);
			}
			else
			{
				alert("您暂时不能进行基本素质测评~");
				return false;
			}
		}
	);
}

//检查已添加条目的正确性
function updateTips( t ) {
	$( ".validateTips")
		.text( t )
		.addClass( "ui-state-highlight" );
	setTimeout(function() {
		$( ".validateTips").removeClass( "ui-state-highlight", 1500 );
		$( ".validateTips" ).text("");
	}, 1500 );
}

function checkLength( o, n, min, max ) {
	if ( o.val().length > max || o.val().length < min ) {
		o.addClass( "ui-state-error" );
		updateTips( '提示: " '+n+' "' + " 的长度应该在 " +
			min + " 和 " + max + "之间." );
		return false;
	} else {
		return true;
	}
}

function checkValue( o, n, min, max ) {
	if ( parseFloat(o.val()) > max || parseFloat(o.val()) < min) {
		o.addClass( "ui-state-error" );
		updateTips( '" '+n+' "' + " 的值 " +
			min + " 和 " + max + "之间." );
		return false;
	} else {
		return true;
	}
}

function checkRegexp( o, regexp, n ) {
	if ( !( regexp.test( o.val() ) ) ) {
		o.addClass( "ui-state-error" );
		updateTips( n );
		return false;
	} else {
		return true;
	}
}

function validator()
{
	if($("#tb tbody tr").length <= 1)
	{
		updateTips( "您没有需要测评的学生列表，无需保存" );
		return false;
	}
		
	var totalRows = new Array();
	var success = true;
	$("#tb tbody tr:gt(0)").each(function() 
	{
		var A1 = $(this).find("#A1"),
		A2 = $(this).find("#A2"),
		A3 = $(this).find("#A3"),
		A4 = $(this).find("#A4"),
		A5 = $(this).find("#A5"),
		allFields = $( [] ).add( A1 ).add( A2 ).add( A3 ).add( A4 ).add( A5 );
	
		var bValid = true;
		allFields.removeClass( "ui-state-error" );
		bValid = bValid && checkLength( A1, "思想政治表现", 1, 10);
		bValid = bValid && checkRegexp( A1, /^\d+(.\d+$|\d*$)/, "\"思想政治表现\"应该输入合法数字" );
		bValid = bValid && checkValue( A1, "思想政治表现", 0, 20);
		
		bValid = bValid && checkLength( A2, "个人品德修养", 1, 10);
		bValid = bValid && checkRegexp( A2, /^\d+(.\d+$|\d*$)/, "\"个人品德修养\"应该输入合法数字" );
		bValid = bValid && checkValue( A2, "个人品德修养", 0, 20);
		
		
		bValid = bValid && checkLength( A3, "学习态度状况", 1, 10);
		bValid = bValid && checkRegexp( A3, /^\d+(.\d+$|\d*$)/, "\"学习态度状况\"应该输入合法数字" );
		bValid = bValid && checkValue( A3, "学习态度状况", 0, 20);
		
		bValid = bValid && checkLength( A4, "组织纪律观念", 1, 10);
		bValid = bValid && checkRegexp( A4, /^\d+(.\d+$|\d*$)/, "\"组织纪律观念\"应该输入合法数字" );
		bValid = bValid && checkValue( A4, "组织纪律观念", 0, 20);
		
		bValid = bValid && checkLength( A5, "身心健康素质", 1, 10);
		bValid = bValid && checkRegexp( A5, /^\d+(.\d+$|\d*$)/, "\"身心健康素质\"应该输入合法数字" );
		bValid = bValid && checkValue( A5, "身心健康素质", 0, 20);
		
		if ( ! bValid ) {
			success = false;
			return false;
		}
	});
	return success;
}

function adjustHeight()
{
	var height = document.body.clientHeight;
	if(height + 40 > 530)
		parent.parent.document.getElementById("frame_content").height=height + 40;
	else
		parent.parent.document.getElementById("frame_content").height=530;	
}
