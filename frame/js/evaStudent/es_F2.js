// JavaScript Document

//首先加载学生列表
loadStuInfo();

/*var selectionArray;   //用来记录学院中评定选项的名称和最大得分值
var sumScore = 0;//用来记录一个学生的自评总成绩
var mydata = {};*/

//设置学生列表的分页
var totalRecord = 0;
var pages = 0;
var currentPage = 1;

//设置学生B1分页
var totalRecord1 = 0;
var pages1 = 0;
var currentPage1 = 1;

//设置学生B2分页
var totalRecord2 = 0;
var pages2 = 0;
var currentPage2 = 1;

var currentMem;    //记录当前选择的是哪个同学, 记录一行的id
var currentLink;  //记录选择的是哪个a标签，用来隐藏行，在生成自评分可变表格时用到
var alreadyPassedMem = [];  //用来记录哪些学生已经通过了审核，记录标签的id

$(document).ready(function() {
	//调整屏幕的高度
	parent.parent.document.getElementById("frame_content").height=420;	
	
	//隐藏错误信息提示
	$("#error").hide();
	
	//设置学生列表的分页显示
	//隐藏翻页按钮
	$("#btLast").hide(); $("#btNext").hide(); $("#btFormer").hide(); $("#btFirst").hide(); 
	//隐藏第一行
	$("#stu-table tr").eq(1).hide();
	
	//B1和B2首先隐藏
	$("#Choosed").hide(); $("#toChoose").hide(); $("#btBack").hide(); $("#btConfirm").hide(); 
	//隐藏翻页按钮
	$("#btLast1").hide(); $("#btNext1").hide(); $("#btFormer1").hide(); $("#btFirst1").hide(); 
	//隐藏第一行
	$("#tbToChoose tr").eq(1).hide();
	//隐藏翻页按钮
	$("#btLast2").hide(); $("#btNext2").hide(); $("#btFormer2").hide(); $("#btFirst2").hide(); 
	//隐藏第一行
	$("#tbChoosed tr").eq(1).hide();
	
	//加载得分选项
	//loadSelection();
	
	//setTimeout("showTable()", 1500);

	//返回到学生列表
	$("#btBack").click(function(){
		$("#tbChoosed tbody tr:gt(0)").each(function(){ $(this).remove(); });
		$("#tbToChoose tbody tr:gt(0)").each(function(){ $(this).remove(); });
		$("#Choosed").hide(); $("#toChoose").hide(); $("#btBack").hide(); $("#btConfirm").hide();
		$("#students").show();
		adjustHeight();
	});
	
	//学生列表的分页控制
	stuPage();
	
	//B1课程的分页控制
	B1Page();
	
	//B2课程的分页控制
	B2Page();
	
	//选择所有行
	$("#CKA").click(function() {
		
		$("#stu-table tbody tr:gt(0)").each(function() {
			if(!$(this).is(":hidden")) 	
				$(this).find("#CK").get(0).checked = $("#CKA").get(0).checked;
		});
	});
	
	//保存记录
	/*$("#BtSave").click(function() {
		
		if(validator())
		{
			mydata = {};
			$("#tb tr:gt(1)").each(function() {			
				var name = $(this).find("#selectType").find("option:selected").text();
				var content = $(this).find("#TContent").val();
				var score = $(this).find("#TScore").val();

				if(mydata.hasOwnProperty(name) == false)
				{
					mydata[name] = [];
				}
				mydata[name].push({content:content, score:score});
			});
			
			$("#users-contain").hide();
			$("#users-contain1").show();
			showSelfEva();
		}
	});*/
	
	/*$("#BtSelfBack").click(function(){
		$("#users-contain").show();
		$("#users-contain1").hide();
		$("#students").hide();
		adjustHeight();
		mydata = {};
	});
	
	$("#BtStuBack").click(function(){
		$("#tb tr:gt(1)").each(function(){$(this).remove();});
		$("#users-contain").hide();
		$("#users-contain1").hide();
		$("#students").show();
		adjustHeight();
	});*/
	
	//审核通过一个学生的成绩
	passAStu();
	
	//批量审核学生的成绩
	passMultiStu();
})

//批量审核学生的成绩
function passMultiStu()
{
	$("#btMutiCheck").click(function (){
		var selected = false;
		$("#stu-table tr:gt(1)").each(function() {
			if ($(this).find("#CK").get(0).checked == true && $(this).find("#link").html() != "已审核") {
				selected = true;
				return false;
			}
		});
		if(!selected)
		{
			alert("请您选择审核的学生~");
			$("#CKA").attr("checked", false);
			$("#CKA").click();
			$("#CKA").attr("checked", false);
			return false;
		}
		
		var passMultiSelfEva = [];
		$("#stu-table tr:gt(1)").each(function() {
			if ($(this).find("#CK").get(0).checked == true && $(this).find("#link").html() != "已审核") {
				passMultiSelfEva.push({"sid": $(this).find("#stuID").html()});
				
				alreadyPassedMem.push($(this).find("#link"));
			}
		});
		
		passSelfEva(passMultiSelfEva);
		
		$("#CKA").attr("checked", false);
		$("#CKA").click();
		$("#CKA").attr("checked", false);
	});
}

//审核通过一个学生的成绩
function passAStu()
{
	$("#btConfirm").click(function(){
		var passASelfEva = [];
		passASelfEva.push({'sid':currentMem.find("#stuID").html()});
		passSelfEva(passASelfEva);
		
		alreadyPassedMem.push(currentMem.find("#link"));
	});
}

function passSelfEva(passF2)
{
	//alert($.toJSON(passF2));
	if(confirm('您将确认审核通过？'))
	{
		$.get(
			"../../testdrive/index.php?r=temp/teamPassF2",
			{passF2: $.toJSON(passF2)},
			function(data)
			{
				var ret = $.evalJSON(data);
				
				if(ret.success == true)
				{
					//TO DO
					alert("审核通过~");
					$("#btBack").click();
					
					for(var i = 0; i < alreadyPassedMem.length; i++)
					{
						alreadyPassedMem[i].html("已审核");
						alreadyPassedMem[i].removeAttr("onclick");
						alreadyPassedMem[i].removeAttr("href");
					}
				}
				else
				{
					//alert('服务器端返回错误信息=>"' +ret.message+'"');
					if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
					{
						alert(ret.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href='../../../';
						return false;
					}
					alert(ret.message);
				}
			}
		);

	}
}

//传入需要被显示的数据的位置
//仅显示_min 和 _max之间的数据
function ShowPage1(_min, _max)
{
	$("#tbToChoose tbody tr").each(function() {
		$(this).show();
	});
	
	var variable = "#tbToChoose tbody tr:lt("+_min+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	variable = "#tbToChoose tbody tr:gt("+_max+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	$("#showPage1").html(currentPage1+" 共"+pages1+"页");
	
	adjustHeight();
}

//设置页数和总记录数
function setRecordPage1(number)
{
	totalRecord1 = number;
	if(number <= 0)
	{
		totalRecord1 = 0;
		pages1 = 1;
	}
	else
	{//每页有10条记录
		pages1 = parseInt(number/10);
		if(number%10 != 0)
			pages1 += 1;
	}
	//页数>1 需翻页按钮
	//alert(pages1);
	if(pages1 > 1)
	{
		$("#btLast1").show(); $("#btNext1").show(); $("#btFormer1").show(); $("#btFirst1").show(); 
	}
	else
	{
		$("#btLast1").hide(); $("#btNext1").hide(); $("#btFormer1").hide(); $("#btFirst1").hide(); 
	}
}

//传入需要被显示的数据的位置
//仅显示_min 和 _max之间的数据
function ShowPage2(_min, _max)
{
	$("#tbChoosed tbody tr").each(function() {
		$(this).show();
	});
	
	var variable = "#tbChoosed tbody tr:lt("+_min+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	variable = "#tbChoosed tbody tr:gt("+_max+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	$("#showPage2").html(currentPage2+" 共"+pages2+"页");
	
	adjustHeight();
}

//设置页数和总记录数
function setRecordPage2(number)
{
	totalRecord2 = number;
	if(number <= 0)
	{
		totalRecord2 = 0;
		pages2 = 1;
	}
	else
	{//每页有10条记录
		pages2 = parseInt(number/10);
		if(number%10 != 0)
			pages2 += 1;
	}
	//页数>1 需翻页按钮
	if(pages2 > 1)
	{
		$("#btLast2").show(); $("#btNext2").show(); $("#btFormer2").show(); $("#btFirst2").show(); 
	}
	else
	{
		$("#btLast2").hide(); $("#btNext2").hide(); $("#btFormer2").hide(); $("#btFirst2").hide(); 
	}
}

//从服务器加载一个学生的F2成绩
function loadAStuF2()
{
	parent.parent.document.getElementById("frame_content").height=420;	
	
	var date = new Date();
	var vYear = date.getFullYear();
	
	var showF2 = {};
	showF2["sid"] = currentMem.find("#stuID").html();
	showF2["year"] = vYear;
	
	$.get(
		"../../../testdrive/index.php?r=temp/displayCscores",
		{"showF2": $.toJSON(showF2)},
		function(data)
		{
			var ret = $.evalJSON(data);

			if(ret.success = true)
			{
				if(ret.results.length != 0 && ret.results.B1.length != 0 && ret.results.B2.length != 0)
				{
					var score = ret.results.score;  //记录学生的F2成绩
					var B1 = ret.results.B1;
					var B2 = ret.results.B2;

					$("#toChoose h1").html('课程成绩总分：'+score+'分');
					//B1分页
					setRecordPage1(B1.length);
					for(var i = 0; i < B1.length; i++)
					{
						var tr = $("#tbToChoose tr").eq(1).clone();//复制一行
						tr.find("td").get(0).innerHTML = B1[i].cid;
						tr.find("td").get(1).innerHTML = B1[i].cname;
						tr.find("td").get(2).innerHTML = B1[i].ctype;
						tr.find("td").get(3).innerHTML = B1[i].cpoint;
						tr.find("td").get(4).innerHTML = B1[i].cscore;
						
						tr.show();
						tr.appendTo("#tbToChoose");
					}

					currentPage1 = 1;
					$("#tbToChoose tbody tr:gt(10)").each(function() {
						$(this).hide();
					});
		
					$("#showPage1").html(currentPage1+" 共"+pages1+"页");
					//adjustHeight();
					
					//B2分页
					setRecordPage2(B2.length);
					for(var i = 0; i < B2.length; i++)
					{
						var tr = $("#tbChoosed tr").eq(1).clone();//复制一行
						tr.find("td").get(0).innerHTML = B2[i].cid;
						tr.find("td").get(1).innerHTML = B2[i].cname;
						tr.find("td").get(2).innerHTML = B2[i].ctype;
						tr.find("td").get(3).innerHTML = B2[i].cpoint;
						tr.find("td").get(4).innerHTML = B2[i].cscore;
						
						tr.show();
						tr.appendTo("#tbChoosed");
					}

					currentPage2 = 1;
					$("#tbChoosed tbody tr:gt(10)").each(function() {
						$(this).hide();
					});
		
					$("#showPage2").html(currentPage2+" 共"+pages2+"页");
					adjustHeight();
					
					$('html,body', window.parent.parent.document).animate({
						scrollTop: $("#toChoose").offset().top+135}, 
						600
					);
				}
				else
				{
					alert("该学生的课程成绩尚未自评，或者该学生成绩未导入到系统~");
					$("#btBack").click();
				}
			}
			else
			{
				//alert('服务器端返回错误信息=>"'+ret.message+'"');
				if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
					{
						alert(ret.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href='../../../';
						return false;
					}
				alert(ret.message);
				$("#btBack").click();
			}
		}
	);
}

//审核一个学生的成绩
function checkAMem()
{
	$("#students").hide();
	$("#toChoose").show();
	$("#Choosed").show();
	$("#btBack").show(); $("#btConfirm").show(); 
	//从服务器加载一个学生的F2成绩
	loadAStuF2();
}

//传入需要被显示的数据的位置
//仅显示_min 和 _max之间的数据
function ShowPage(_min, _max)
{
	$("#stu-table tbody tr").each(function() {
		$(this).show();
	});
	
	var variable = "#stu-table tbody tr:lt("+_min+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	variable = "#stu-table tbody tr:gt("+_max+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	$("#showPageStu").html(currentPage+" 共"+pages+"页");
	adjustHeight();
}

//设置页数和总记录数
function setRecordPage(number)
{
	totalRecord = number;
	if(number <= 0)
	{
		totalRecord = 0;
		pages = 0;
	}
	else
	{//每页有10条记录
		pages = parseInt(number/10);
		if(number%10 != 0)
			pages += 1;
	}
	//页数>1 需翻页按钮
	if(pages > 1)
	{
		$("#btLast").show(); $("#btNext").show(); $("#btFormer").show(); $("#btFirst").show(); 
	}
}

//从服务器加载学生列表
function loadStuInfo()
{
	var getStuByMem = {};
	getStuByMem["type"] = 'F2';
	getStuByMem["sid"] = "";
	
	$.get(
		"../../../testdrive/index.php?r=temp/getStuByMem",
		{getStuByMem: $.toJSON(getStuByMem)},
		function(data)
		{
			var obj = $.evalJSON(data);
			
			if(obj.success == true)
			{
				if(obj.results.length != 0)
				{
					setRecordPage(obj.results.length);
					for (var i = 0; i < obj.results.length; i++)
					{
						var tr = $("#stu-table tr").eq(1).clone();
						tr.find("td").get(1).innerHTML = obj.results[i]['sid'];
						tr.find("td").get(2).innerHTML = obj.results[i]['sname'];
						if(parseInt(obj.results[i]['isPassed']) != 0)
						{
							tr.find("#link").html("已审核");
							tr.find("#link").removeAttr("onclick");
							tr.find("#link").removeAttr("href");
						}
						tr.show();
						tr.appendTo("#stu-table tbody");
					}
					
					currentPage = 1;
					$("#stu-table tbody tr:gt(10)").each(function() {
						$(this).hide();
					});
					
					$("#showPageStu").html(currentPage+" 共"+pages+"页");
					
					adjustHeight();
				}
				else
				{
					$("#students").hide();
					$("#error").show();
				}
			}
			else
			{
				//alert("服务器端返回错误信息=>\""+obj.message+'"');
				
				alert(ret.message);
				$("#students").hide();
				$("#error").show();
				return false;
			}
		}
	);
}

//学生列表的分页控制
function stuPage()
{
	$("#btLast").click(function() 
	{
		if(currentPage != pages)
		{
			//alert("即将跳转到最后一页");
			currentPage = pages;
			ShowPage(currentPage*10-9, totalRecord);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btNext").click(function() 
	{
		if(currentPage+1<=pages)
		{
			//alert("即将跳转到下一页");
			currentPage += 1;
			ShowPage(currentPage*10-9, currentPage*10);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btFormer").click(function() 
	{
		if(currentPage-1>0)
		{
			//alert("即将跳转到上一页");
			currentPage -= 1;
			ShowPage(currentPage*10-9, currentPage*10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
	$("#btFirst").click(function() 
	{
		if(currentPage != 1)
		{
			//alert("即将跳转到首页");
			currentPage = 1;
			ShowPage(1, 10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
}

//B1的分页控制
function B1Page()
{
	$("#btLast1").click(function() 
	{
		if(currentPage1 != pages1)
		{
			//alert("即将跳转到最后一页");
			currentPage1 = pages1;
			ShowPage1(currentPage1*10-9, totalRecord1);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btNext1").click(function() 
	{
		if(currentPage1+1<=pages1)
		{
			//alert("即将跳转到下一页");
			currentPage1 += 1;
			ShowPage1(currentPage1*10-9, currentPage1*10);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btFormer1").click(function() 
	{
		if(currentPage1-1>0)
		{
			//alert("即将跳转到上一页");
			currentPage1 -= 1;
			ShowPage1(currentPage1*10-9, currentPage1*10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
	$("#btFirst1").click(function() 
	{
		if(currentPage1 != 1)
		{
			//alert("即将跳转到首页");
			currentPage1 = 1;
			ShowPage1(1, 10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
}

//B2的分页控制
function B2Page()
{
	$("#btLast2").click(function() 
	{
		if(currentPage2 != pages2)
		{
			//alert("即将跳转到最后一页");
			currentPage2 = pages2;
			ShowPage2(currentPage2*10-9, totalRecord2);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btNext2").click(function() 
	{
		if(currentPage2+1<=pages2)
		{
			//alert("即将跳转到下一页");
			currentPage2 += 1;
			ShowPage2(currentPage2*10-9, currentPage2*10);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btFormer2").click(function() 
	{
		if(currentPage2-1>0)
		{
			//alert("即将跳转到上一页");
			currentPage2 -= 1;
			ShowPage2(currentPage2*10-9, currentPage2*10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
	$("#btFirst2").click(function() 
	{
		if(currentPage2 != 1)
		{
			//alert("即将跳转到首页");
			currentPage2 = 1;
			ShowPage2(1, 10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
}

function adjustHeight()
{
	var height = document.body.clientHeight;
	if(height + 50 > 420)
		parent.parent.document.getElementById("frame_content").height=height + 50;
	else
		parent.parent.document.getElementById("frame_content").height=420;	
}
