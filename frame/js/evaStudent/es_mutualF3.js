// JavaScript Document
//从服务器加载需要审核的学生列表
loadStuInfo();

var selectionArray;   //用来记录学院中评定选项的名称和最大得分值
var sumScore = 0;//用来记录一个学生的自评总成绩
var mydata = {};

//设置学生列表的分页
var totalRecord = 0;
var pages = 0;
var currentPage = 1;

//设置学生自评列表
var totalRecord1 = 0;
var pages1 = 0;
var currentPage1 = 1;

var currentMem;    //记录当前选择的是哪个同学, 记录一行的id

var currentLink;  //记录选择的是哪个a标签，用来隐藏行，在生成自评分可变表格时用到

var alreadyPassedMem = [];  //用来记录哪些学生已经通过了审核，记录标签的id

$(document).ready(function() {
	//调整屏幕的高度
	parent.parent.document.getElementById("frame_content").height=420;	
	
	//暂时关闭审核通过
	$("#btMutiCheck").hide();
	//设置学生列表的分页显示
	//隐藏翻页按钮
	$("#btLast").hide(); $("#btNext").hide(); $("#btFormer").hide(); $("#btFirst").hide(); 
	//隐藏第一行
	$("#stu-table tr").eq(1).hide();
	
	//隐藏错误信息提示
	$("#error").hide();
	
	//设置自评列表的分页显示
	$("#users-contain").hide();
	//隐藏翻页按钮
	$("#btLast1").hide(); $("#btNext1").hide(); $("#btFormer1").hide(); $("#btFirst1").hide(); 
	//隐藏第一行
	$("#tb tr").eq(2).hide();
	
	$("#btLast").click(function() 
	{
		if(currentPage != pages)
		{
			//alert("即将跳转到最后一页");
			currentPage = pages;
			ShowPage(currentPage*20-19, totalRecord);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btNext").click(function() 
	{
		if(currentPage+1<=pages)
		{
			//alert("即将跳转到下一页");
			currentPage += 1;
			ShowPage(currentPage*20-19, currentPage*20);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btFormer").click(function() 
	{
		if(currentPage-1>0)
		{
			//alert("即将跳转到上一页");
			currentPage -= 1;
			ShowPage(currentPage*20-19, currentPage*20);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
	$("#btFirst").click(function() 
	{
		if(currentPage != 1)
		{
			//alert("即将跳转到首页");
			currentPage = 1;
			ShowPage(1, 20);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
	
	$("#btLast1").click(function() 
	{
		if(currentPage1 != pages1)
		{
			//alert("即将跳转到最后一页");
			currentPage1 = pages1;
			ShowPage1(currentPage1*10-9, totalRecord1);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btNext1").click(function() 
	{
		if(currentPage1+1<=pages1)
		{
			//alert("即将跳转到下一页");
			currentPage1 += 1;
			ShowPage1(currentPage1*10-9, currentPage1*10);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btFormer1").click(function() 
	{
		if(currentPage1-1>0)
		{
			//alert("即将跳转到上一页");
			currentPage1 -= 1;
			ShowPage1(currentPage1*10-9, currentPage1*10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
	$("#btFirst1").click(function() 
	{
		if(currentPage1 != 1)
		{
			//alert("即将跳转到首页");
			currentPage1 = 1;
			ShowPage1(1, 10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
	
	$("#BtStuBack").click(function(){
		$("#tb tr:gt(2)").each(function(){$(this).remove();});
		$("#users-contain").hide();
		$("#users-contain1").hide();
		$("#students").show();
		adjustHeight();
	});
	
	//批量审核学生的成绩
	passMultiStu();
})

//批量审核学生的成绩
/*
只能是审核多个，
如果type=team，当调用这个接口的时候，后台需要将这些学生的自评成绩直接赋值给测评小组的打分；
如果type=teacher，则后台将测评小组的打分赋值给辅导员的打分。
*/
function passMultiStu()
{
	$("#btMutiCheck").click(function (){
		if(confirm('您将确认"创新与实践评分"审核通过？'))
		{
			$.get(
				"./interface/results.php",
				function(data)
				{
					var ret = $.evalJSON(data);
					if(ret.success == true)
					{
						alert("提交成功~");
						$("#btMutiCheck span").html("已审核");
						$("#btMutiCheck").attr("disabled", "disabled");
					}
					else
					{
						//alert('服务器端返回错误信息=>"' +ret.message+'"');
						if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
					{
						alert(ret.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href='../../../';
						return false;
					}
						alert(ret.message);
					}
				}
			);
	
		}
	});
}

//点击下拉图表时，则显示隐藏的表格，学生自评查看时用到
/*function showDetail()
{
	if(currentLink.attr("class") == "visited")
	{
		currentLink.removeClass("visited");
		currentLink.addClass("initA");
		
		var parent = currentLink.parent().parent();
		parent.nextUntil(".ui-widget-header").hide();
		
		adjustHeight();
	}
	else if(currentLink.attr("class") == "initA")
	{
		$("#tb-self tbody #content").hide();
		$("#tb-self tbody .ui-widget-header th a").removeClass("visited");
		$("#tb-self tbody .ui-widget-header th a").addClass("initA");
		
		currentLink.removeClass("initA");
		currentLink.addClass("visited");
		
		var parent = currentLink.parent().parent();
		parent.nextUntil(".ui-widget-header").show();
		
		adjustHeight();
	}
}*/

//传入需要被显示的数据的位置
//仅显示_min 和 _max之间的数据
function ShowPage1(_min, _max)
{
	$("#tb tbody tr").each(function() {
		$(this).show();
	});
	
	var variable = "#tb tbody tr:lt("+_min+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	variable = "#tb tbody tr:gt("+_max+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	$("#showPageSelf").html(currentPage1+" 共"+pages1+"页");
	
	adjustHeight();
}

//设置页数和总记录数
function setRecordPage1(number)
{
	totalRecord1 = number;
	if(number <= 0)
	{
		totalRecord1 = 0;
		pages1 = 1;
	}
	else
	{//每页有10条记录
		pages1 = parseInt(number/10);
		if(number%10 != 0)
			pages1 += 1;
	}
	//页数>1 需翻页按钮
	//alert(pages1);
	if(pages1 > 1)
	{
		$("#btLast1").show(); $("#btNext1").show(); $("#btFormer1").show(); $("#btFirst1").show(); 
	}
	else
	{
		$("#btLast1").hide(); $("#btNext1").hide(); $("#btFormer1").hide(); $("#btFirst1").hide(); 
	}
}

//加载得分选项
function loadSelection()
{	
	$.ajax({
		url: '../../../testdrive/index.php?r=zing/getEvaCon',
		async: false, 
		success: function (data)
		{
			selectionArray = $.evalJSON(data);
			
			if(selectionArray.success == true)
			{
				selectionArray = selectionArray.results;
				if(selectionArray.length != 0)
				{
					var SavedArray = {};
					var selector=$("#selectType");  
					for(var i = 0; i < selectionArray.length; i++)
					{
						SavedArray[selectionArray[i].evaluatename] = Array(selectionArray[i].evaluateid, selectionArray[i].maxscore);
						selector.append('<option value="'+selectionArray[i].evaluateid+'">'+selectionArray[i].evaluatename+'</option>');  
					}
					
					selectionArray = SavedArray;
				}
				else
				{
					alert('服务器端返回测评选择项失败~');
				}
			}
			else
			{
				//alert('服务器端返回错误信息=>"'+data.message+'"');
				if(selectionArray.results.hasOwnProperty("timeout") && selectionArray.results.timeout)
					{
						alert(selectionArray.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href='../../../';
						return false;
					}
				alert(selectionArray.message);
			}
		}
	});
}


//将学生的自评成绩加载到表中
function loadSelfEva()
{
	parent.parent.document.getElementById("frame_content").height=420;	
	
	var date = new Date();
	var vYear = date.getFullYear();
	
	var showF3 = {};
	showF3["sid"] = currentMem.find("#stuID").html();
	showF3["year"] = vYear;
	
	$.get(
		"../../../testdrive/index.php?r=temp/displaySelfScore",
		{"showF3": $.toJSON(showF3)},
		function(data)
		{
			var ret = $.evalJSON(data);

			if(ret.success = false)
			{
				//alert('服务器端返回错误信息=>"'+ret.message+'"');
				if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
					{
						alert(ret.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href='../../../';
						return false;
					}
				alert(ret.message);
				return false;
			}
			
			var score = ret.results.score;
			var count = 0;
			var tempRecords = 0;
			var items = ret.results.contents;
			for(var i = 0; i < items.length; i++)
			{
				var choose = items[i].choose;
				for(var j = 0; j < items[i].contents.length; j++)
				{
					var tr = $("#tb tr").eq(2).clone();//复制一行
					tr.find("#selectType").val(choose); 
					tr.find("#TContent").html(items[i].contents[j].content);
					
					if(items[i].contents[j].score === "")
						tr.find("#TScore").html("未自评");
					else
						tr.find("#TScore").html(parseFloat(items[i].contents[j].score));
					
					if(items[i].contents[j].t_evascore === "")
						tr.find("#t_evascore").html("未审核");
					else
						tr.find("#t_evascore").html(parseFloat(items[i].contents[j].t_evascore));
					
					if(items[i].contents[j].te_evascore === "")
						tr.find("#te_evascore").html("未审核");
					else
						tr.find("#te_evascore").html(parseFloat(items[i].contents[j].te_evascore));
					
					tr.show();
					tr.appendTo("#tb");
					tempRecords++;
				}
			}
			
			setRecordPage1(tempRecords);

			currentPage1 = 1;
			$("#tb tbody tr:gt(10)").each(function() {
				$(this).hide();
			});

			$("#showPageSelf").html(currentPage1+" 共"+pages1+"页");
			
			adjustHeight();
			$('html,body', window.parent.parent.document).animate({
				scrollTop: $("#mark").offset().top+135}, 
				600
			);
		}
	);
}


function checkAMem()
{
	$("#students").hide();
	$("#users-contain").show();
	loadSelfEva();
}

//传入需要被显示的数据的位置
//仅显示_min 和 _max之间的数据
function ShowPage(_min, _max)
{
	$("#stu-table tbody tr").each(function() {
		$(this).show();
	});
	
	var variable = "#stu-table tbody tr:lt("+_min+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	variable = "#stu-table tbody tr:gt("+_max+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	$("#showPageStu").html(currentPage+" 共"+pages+"页");
	adjustHeight();
}

//设置页数和总记录数
function setRecordPage(number)
{
	totalRecord = number;
	if(number <= 0)
	{
		totalRecord = 0;
		pages = 0;
	}
	else
	{//每页有10条记录
		pages = parseInt(number/20);
		if(number%20 != 0)
			pages += 1;
	}
	//页数>1 需翻页按钮
	if(pages > 1)
	{
		$("#btLast").show(); $("#btNext").show(); $("#btFormer").show(); $("#btFirst").show(); 
	}
}

//从服务器加载学生列表
function loadStuInfo()
{
	var date = new Date();
	var vYear = date.getFullYear();
	
	var showAllF3 = {};
	showAllF3["year"] = vYear;
	
	$.get(
		"../../../testdrive/index.php?r=temp/displayAllSelfScore",
		{showAllF3:$.toJSON(showAllF3)},
		function(data)
		{
			var obj = $.evalJSON(data);
			
			if(obj.success == true)
			{
				if(obj.results.length != 0)
				{
					loadSelection();
					
					setRecordPage(obj.results.length);
					for (var i = 0; i < obj.results.length; i++)
					{
						var tr = $("#stu-table tr").eq(1).clone();
						tr.find("td").get(0).innerHTML = obj.results[i]['sid'];
						tr.find("td").get(1).innerHTML = obj.results[i]['sname'];
						tr.find("td").get(2).innerHTML = obj.results[i]['cname'];
						tr.find("td").get(3).innerHTML = parseFloat(obj.results[i]['score']);
						if(parseInt(obj.results[i]['changed']) == 1)
						{
							tr.find("#link").html("查看(有改动)");
							tr.find("#link").css("color", "red");
						}
						tr.show();
						tr.appendTo("#stu-table tbody");
					}
					
					currentPage = 1;
					$("#stu-table tbody tr:gt(20)").each(function() {
						$(this).hide();
					});
					
					$("#showPageStu").html(currentPage+" 共"+pages+"页");
					
					adjustHeight();
					
					$('html,body', window.parent.parent.document).animate({
						scrollTop: $("#stu-table").offset().top+135}, 
						600
					);
				}
				else
				{
					$("#students").hide();
					$("#error").show();
				}
			}
			else
			{
				//alert("服务器端返回错误信息=>\""+obj.message+'"');
				if(obj.results.hasOwnProperty("timeout") && obj.results.timeout)
					{
						alert(obj.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href='../../../';
						return false;
					}
				alert(obj.message);
				$("#students").hide();
				$("#error").show();
				return false;
			}
		}
	);
}

function adjustHeight()
{
	var height = document.body.clientHeight;
	if(height + 50 > 420)
		parent.parent.document.getElementById("frame_content").height=height + 50;
	else
		parent.parent.document.getElementById("frame_content").height=420;	
}
