// JavaScript Document
//加载得分选项
//loadSelection();
//从服务器加载需要审核的学生列表
loadStuInfo();

var selectionArray;   //用来记录学院中评定选项的名称和最大得分值
var sumScore = 0;//用来记录一个学生的自评总成绩
var mydata = {};

//设置学生列表的分页
var totalRecord = 0;
var pages = 0;
var currentPage = 1;

//设置学生自评列表
var totalRecord1 = 0;
var pages1 = 0;
var currentPage1 = 1;

var currentMem;    //记录当前选择的是哪个同学, 记录一行的id

var currentLink;  //记录选择的是哪个a标签，用来隐藏行，在生成自评分可变表格时用到

var alreadyPassedMem = [];  //用来记录哪些学生已经通过了审核，记录标签的id

$(document).ready(function() {
	//调整屏幕的高度
	parent.parent.document.getElementById("frame_content").height=420;	
	
	//设置学生列表的分页显示
	//隐藏翻页按钮
	$("#btLast").hide(); $("#btNext").hide(); $("#btFormer").hide(); $("#btFirst").hide(); 
	//隐藏第一行
	$("#stu-table tr").eq(1).hide();
	
	//隐藏错误信息提示
	$("#error").hide();
	
	//设置自评列表的分页显示
	$("#users-contain").hide();
	//隐藏翻页按钮
	$("#btLast1").hide(); $("#btNext1").hide(); $("#btFormer1").hide(); $("#btFirst1").hide(); 
	//隐藏第一行
	$("#tb tr").eq(2).hide();
	
	//设置自评总评列表的显示
	$("#users-contain1").hide();
	$("#tb-self tbody #head").eq(0).hide();$("#tb-self tbody #content").eq(0).hide();

	//返回到学生列表
	$("#btBack").click(function(){
		$("#tb tbody tr:gt(0)").each(function(){ $(this).remove(); });
		$("#users-contain").hide();
		$("#students").show();

		adjustHeight();
		
	});
	
	$("#btLast").click(function() 
	{
		if(currentPage != pages)
		{
			//alert("即将跳转到最后一页");
			currentPage = pages;
			ShowPage(currentPage*10-9, totalRecord);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btNext").click(function() 
	{
		if(currentPage+1<=pages)
		{
			//alert("即将跳转到下一页");
			currentPage += 1;
			ShowPage(currentPage*10-9, currentPage*10);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btFormer").click(function() 
	{
		if(currentPage-1>0)
		{
			//alert("即将跳转到上一页");
			currentPage -= 1;
			ShowPage(currentPage*10-9, currentPage*10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
	$("#btFirst").click(function() 
	{
		if(currentPage != 1)
		{
			//alert("即将跳转到首页");
			currentPage = 1;
			ShowPage(1, 10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
	
	//选择所有行
	$("#CKA1").click(function() {
		
		$("#stu-table tbody tr").each(function() {
			if(!$(this).is(":hidden")) 	
				$(this).find("#CK1").get(0).checked = $("#CKA1").get(0).checked;
		});
	});
	
	$("#btLast1").click(function() 
	{
		if(currentPage1 != pages1)
		{
			//alert("即将跳转到最后一页");
			currentPage1 = pages1;
			ShowPage1(currentPage1*10-9, totalRecord1);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btNext1").click(function() 
	{
		if(currentPage1+1<=pages1)
		{
			//alert("即将跳转到下一页");
			currentPage1 += 1;
			ShowPage1(currentPage1*10-9, currentPage1*10);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btFormer1").click(function() 
	{
		if(currentPage1-1>0)
		{
			//alert("即将跳转到上一页");
			currentPage1 -= 1;
			ShowPage1(currentPage1*10-9, currentPage1*10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
	$("#btFirst1").click(function() 
	{
		if(currentPage1 != 1)
		{
			//alert("即将跳转到首页");
			currentPage1 = 1;
			ShowPage1(1, 10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
	
	//保存记录
	$("#BtSave").click(function() {
		
		if(validator())
		{
			mydata = {};
			$("#tb tr:gt(2)").each(function() {			
				var name = $(this).find("#selectType").find("option:selected").text();
				var content = $(this).find("#TContent").val();
				var score = $(this).find("#t_evascore").val();
				var selfscoreid = $(this).find("#TContent").attr("name");

				if(mydata.hasOwnProperty(name) == false)
				{
					mydata[name] = [];
				}
				mydata[name].push({content:content, score:score, selfscoreid:selfscoreid});
			});
			
			$("#users-contain").hide();
			$("#users-contain1").show();
			showSelfEva();
		}
	});
	
	$("#BtSelfBack").click(function(){
		$("#users-contain").show();
		$("#users-contain1").hide();
		$("#students").hide();
		adjustHeight();
		mydata = {};
	});
	
	$("#BtStuBack").click(function(){
		$("#tb tr:gt(2)").each(function(){$(this).remove();});
		$("#t_Add").val(""); $("#selfAdd").val(""); $("#checkAdd").val("");
		$("#t_Del").val(""); $("#selfDel").val(""); $("#checkDel").val("");
		$("#users-contain").hide();
		$("#users-contain1").hide();
		$("#students").show();
		adjustHeight();
	});
	
	//审核通过一个学生的成绩
	passAStu();
	
	//批量审核学生的成绩
	passMultiStu();
})

//批量审核学生的成绩
/*
只能是审核多个，
如果type=team，当调用这个接口的时候，后台需要将这些学生的自评成绩直接赋值给测评小组的打分；
如果type=teacher，则后台将测评小组的打分赋值给辅导员的打分。
*/
function passMultiStu()
{
	$("#btMutiCheck").click(function (){
		var selected = false;
		$("#stu-table tr:gt(1)").each(function() {
			if ($(this).find("#CK1").get(0).checked == true) {
				selected = true;
				return false;
			}
		});
		if(!selected)
		{
			alert("请您选择审核的学生~");
			return false;
		}
		
		var passF3 = {};
		passF3["type"] = "team";
		passF3["members"] = [];
		$("#stu-table tr:gt(1)").each(function() {
			if ($(this).find("#CK1").get(0).checked == true) {
				passF3["members"].push({"sid": $(this).find("#stuID").html()});
				
				alreadyPassedMem.push($(this).find("#link"));
			}
		});
		
		passSelfEva(passF3);
		
		$("#CKA1").attr("checked", false);
		$("#CKA1").click();
		$("#CKA1").attr("checked", false);
	});
}

//审核通过一个学生的成绩,此时需要将该学生的审核分数传给后台，后台需要去修改，并且置为已测评小组审核标志
function passAStu()
{
	$("#BtSelfConfirm").click(function(){
		var saveF3 = {};
		var date = new Date();
		var vYear = date.getFullYear();
	
		saveF3["sid"] = currentMem.find("#stuID").html();
		saveF3["year"] = vYear;
		saveF3["type"] = "team";
		saveF3["score"] = parseFloat($("#tb-self th:last span").html());
		saveF3["contents"] = [];

		for(var key in mydata)
		{
			//{'selfscoreid:'1001’, ‘t_score’:10},
			for (var i = 0; i < mydata[key].length; i++)
			{
				var _item = {};
				_item["selfscoreid"] = mydata[key][i].selfscoreid;
				_item["t_score"] = mydata[key][i].score;
				saveF3["contents"].push(_item);
			}
		}
		
		//add
		if($.trim($("#t_Add").val()) != "" && $.trim($("#checkAdd").val()) != "")
		{
			var _item = {};
			_item["t_score"] = parseFloat($.trim($("#checkAdd").val()));
			_item["selfscoreid"] = $("#selfAdd").attr("name");
			//如果学生没有自评，但是测评小组主动加分，那么此时相当于重新插入一条记录
			//2012-02-18 modified by minzhenyu
			_item["content"] = $.trim($("#t_Add").replaceSpecialHtmlChar());
			//_item["choose"] = $("#t_Add").attr("name");
			//_item["contents"] = {"content": $.trim($("#t_Add").val()), "score": parseFloat($.trim($("#checkAdd").val()))};
			saveF3["contents"].push(_item);
		}
		//sub
		if($.trim($("#t_Del").val()) != "" && $.trim($("#checkDel").val()) != "")
		{
			var _item = {};
			_item["t_score"] = parseFloat($.trim($("#checkDel").val()));
			_item["selfscoreid"] = $("#selfDel").attr("name");
			_item["content"] =  $.trim($("#t_Del").replaceSpecialHtmlChar());
			//_item["choose"] = $("#t_Del").attr("name");
			//_item["contents"] = {"content": $.trim($("#t_Del").val()), "score": parseFloat($.trim($("#checkDel").val()))};
			saveF3["contents"].push(_item);
		}
		
		//alert($.toJSON(saveF3));		
		
		if(confirm('您将确认审核通过？'))
		{
			$.post(
				"../../../testdrive/index.php?r=temp/saveSelfscore",
				{saveF3: $.toJSON(saveF3)},
				function(data)
				{
					var ret = $.evalJSON(data);
					if(ret.success == true)
					{
						alert("提交成功~");
						$("#BtStuBack").click();
						
						for(var i = 0; i < alreadyPassedMem.length; i++)
						{
							alreadyPassedMem[i].html("已审核");
							alreadyPassedMem[i].removeAttr("onclick");
							alreadyPassedMem[i].removeAttr("href");
						}
					}
					else
					{
						//alert('服务器端返回错误信息=>"' +ret.message+'"');
						if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
						{
							alert(ret.message+'  页面即将跳转到首页～');
							window.parent.parent.location.href='../../../';
							return false;
						}
						alert(ret.message);
					}
				}
			);
	
		}
	
		alreadyPassedMem.push(currentMem.find("#link"));
	});
}

function passSelfEva(passSelfEva)
{
	if(confirm('您将确认审核通过？'))
	{
		$.get(
			"./interface/results.php",
			{passF3: passSelfEva},
			function(data)
			{
				var ret = $.evalJSON(data);
				
				if(ret.success == true)
				{
					alert("提交成功~");
					$("#BtStuBack").click();
					
					for(var i = 0; i < alreadyPassedMem.length; i++)
					{
						alreadyPassedMem[i].html("已审核");
						alreadyPassedMem[i].removeAttr("onclick");
						alreadyPassedMem[i].removeAttr("href");
					}
				}
				else
				{
					//alert('服务器端返回错误信息=>"' +ret.message+'"');
					if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
						{
							alert(ret.message+'  页面即将跳转到首页～');
							window.parent.parent.location.href='../../../';
							return false;
						}
					alert(ret.message);
				}
			}
		);

	}
}

//点击下拉图表时，则显示隐藏的表格，学生自评查看时用到
function showDetail()
{
	if(currentLink.attr("class") == "visited")
	{
		currentLink.removeClass("visited");
		currentLink.addClass("initA");
		
		var parent = currentLink.parent().parent();
		parent.nextUntil(".ui-widget-header").hide();
		
		adjustHeight();
	}
	else if(currentLink.attr("class") == "initA")
	{
		$("#tb-self tbody #content").hide();
		$("#tb-self tbody .ui-widget-header th a").removeClass("visited");
		$("#tb-self tbody .ui-widget-header th a").addClass("initA");
		
		currentLink.removeClass("initA");
		currentLink.addClass("visited");
		
		var parent = currentLink.parent().parent();
		parent.nextUntil(".ui-widget-header").show();
		
		adjustHeight();
	}
}

//显示学生自我测评，被审核后的结果
function showSelfEva()
{
	$("#tb-self tbody tr").remove();
	var scripts = "";
	var totalScore = 0;
	for(var key in mydata)
	{
		var score = 0;
		for(var i = 0; i < mydata[key].length; i++)
		{
			score += parseFloat(mydata[key][i].score);
		}
		
		//验证分数
		if(selectionArray[key] && score > selectionArray[key][1])
		{
			score = parseFloat(selectionArray[key][1]);
		}
		
		totalScore+=score;
		scripts = "<tr class='ui-widget-header'><th><a href='javascript:void(0)' onclick='currentLink = $(this); showDetail()' class='initA'></a>&nbsp;"+key+"  :  "+formatFloat(score,4)+" 分</th></tr>";
		scripts += '<tr id="content"><td style="padding:0"><table id="tb-self" class="ui-widget ui-widget-content" style="margin:0"><thead><tr><th style="width:170px">选项</th><th style="width:315px">成果</th><th style="width:60px">审核分</th></tr></thead><tbody>';
		
		for(var i = 0; i < mydata[key].length; i++)
		{
			scripts += '<tr><td>'+key+'</td><td>'+
				mydata[key][i].content+'</td><td>'+formatFloat(mydata[key][i].score,4)+'</td></tr>';
		}
		
		scripts += '</tbody></table></td></tr>';
		
		$("#tb-self tbody:first").append(scripts);
	}
	scripts = '<tr class="ui-widget-header"><th style="text-align: center">创新实践审核分：<span style="color:black">'+formatFloat(totalScore,4)+'</span> 分</th></tr>';
	$("#tb-self tbody:first").append(scripts);
	$("#tb-self tbody #content:gt(0)").hide();
	$("#tb-self tbody .ui-widget-header:first th a").removeClass("initA");
	$("#tb-self tbody .ui-widget-header:first th a").addClass("visited");
	
	$("#td_add").find("td").get(1).innerHTML = '<h1 style="color:red; font-size:12px">无</h1>';
	$("#td_add").find("td").get(2).innerHTML = '<h1 style="color:red; font-size:12px">无</h1>';
	$("#td_del").find("td").get(1).innerHTML = '<h1 style="color:red; font-size:12px">无</h1>';
	$("#td_del").find("td").get(2).innerHTML = '<h1 style="color:red; font-size:12px">无</h1>';
	
	//add
	if($.trim($("#t_Add").val()).length != 0 && $.trim($("#checkAdd").val()).length != 0)
	{
		$("#td_add").find("td").get(1).innerHTML = '<h1 style="color:red; font-size:12px">'+
			$.trim($("#t_Add").replaceSpecialHtmlChar())+'</h1>';
		$("#td_add").find("td").get(2).innerHTML = '<h1 style="color:red; font-size:12px">'+
			$.trim($("#checkAdd").val())+'</h1>';
	}
	//sub
	if($.trim($("#t_Del").val()).length != 0 && $.trim($("#checkDel").val()).length != 0)
	{
		$("#td_del").find("td").get(1).innerHTML = '<h1 style="color:red; font-size:12px">'+
			$.trim($("#t_Del").replaceSpecialHtmlChar())+'</h1>';
		$("#td_del").find("td").get(2).innerHTML = '<h1 style="color:red; font-size:12px">'+
			$.trim($("#checkDel").val())+'</h1>';
	}
	
	adjustHeight();
}

//重新分页
function rePage(_totalRecord, _currentPage)
{
	var tempPage = _currentPage;
	setRecordPage1(_totalRecord);
	
	if (tempPage < pages1)  
	{
		currentPage1 = tempPage;
		ShowPage1(tempPage*10-9, tempPage*10);
	}
	else
	{
		currentPage1 = pages1;
		ShowPage1(pages1*10-9, totalRecord);
	}
}

//传入需要被显示的数据的位置
//仅显示_min 和 _max之间的数据
function ShowPage1(_min, _max)
{
	$("#tb tbody tr").each(function() {
		$(this).show();
	});
	
	var variable = "#tb tbody tr:lt("+_min+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	variable = "#tb tbody tr:gt("+_max+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	$("#showPageSelf").html(currentPage1+" 共"+pages1+"页");
	
	adjustHeight();
}

//设置页数和总记录数
function setRecordPage1(number)
{
	totalRecord1 = number;
	if(number <= 0)
	{
		totalRecord1 = 0;
		pages1 = 1;
	}
	else
	{//每页有10条记录
		pages1 = parseInt(number/10);
		if(number%10 != 0)
			pages1 += 1;
	}
	//页数>1 需翻页按钮
	//alert(pages1);
	if(pages1 > 1)
	{
		$("#btLast1").show(); $("#btNext1").show(); $("#btFormer1").show(); $("#btFirst1").show(); 
	}
	else
	{
		$("#btLast1").hide(); $("#btNext1").hide(); $("#btFormer1").hide(); $("#btFirst1").hide(); 
	}
}

//加载得分选项
function loadSelection()
{	
	$.ajax({
		url: '../../../testdrive/index.php?r=zing/getEvaCon',
		async: false, 
		success: function (data)
		{
			selectionArray = $.evalJSON(data);
			
			if(selectionArray.success == true)
			{
				selectionArray = selectionArray.results;
				if(selectionArray.length != 0)
				{
					var SavedArray = {};
					var selector=$("#selectType");  
					for(var i = 0; i < selectionArray.length; i++)
					{
						SavedArray[selectionArray[i].evaluatename] = Array(selectionArray[i].evaluateid, selectionArray[i].maxscore);
						selector.append('<option value="'+selectionArray[i].evaluateid+'">'+selectionArray[i].evaluatename+'</option>');  
					}
					
					selectionArray = SavedArray;
				}
				else
				{
					alert('服务器端返回测评选择项失败~');
				}
			}
			else
			{
				if(selectionArray.results.hasOwnProperty("timeout") && selectionArray.results.timeout)
						{
							alert(selectionArray.message+'  页面即将跳转到首页～');
							window.parent.parent.location.href='../../../';
							return false;
						}
				//alert('服务器端返回错误信息=>"'+data.message+'"');
				alert(selectionArray.message);
			}
		}
	});
}

//判断是否审核通过
function judgePassed(isPassed)
{
	//只有为2的时候才认为已经被辅导员审核通过
	if(parseInt(isPassed) == 2)
	{
		$("#tb tbody tr:gt(0)").each(function () {
			$(this).find("#selectType").attr("disabled", "disabled");
			$(this).find("#TContent").attr("disabled", "disabled");
			$(this).find("#t_evascore").attr("disabled", "disabled");
		});
		
		$("#BtSave").hide();
		
		$( ".validateTips")
		.text( "您的创新与实践能力评分已经由辅导员审核通过，不能修改~" )
		.addClass( "ui-state-highlight" );
		setTimeout(function() {
			$( ".validateTips").removeClass( "ui-state-highlight", 1500 );
			$( ".validateTips" ).text("");
		}, 3000 );
	}
}

//加载F1表格
function loadF1(_add, _sub, isF1Passed)
{
	if(_add.length != 0)
	{
		$("#t_Add").setHtmlChar(_add.content); 
		if(_add.t_evascore === "")  $("#checkAdd").val("");
		else $("#checkAdd").val(parseFloat(_add.t_evascore)); 
		if(_add.score ==="") $("#selfAdd").val("");
		else $("#selfAdd").val(parseFloat(_add.score));
		$("#t_Add").attr("name", _add.choose);    //将id加到标签的name属性中
		$("#selfAdd").attr("name", _add.selfscoreid);
		if(parseInt(isF1Passed) == 2)  //如果审核通过
		{
			$("#t_Add").attr("disabled", "disabled"); 
			$("#checkAdd").attr("disabled", "disabled");
		}
	}
	if(_sub.length != 0)
	{
		$("#t_Del").setHtmlChar(_sub.content); 
		if(_sub.t_evascore === "") $("#checkDel").val("");
		else $("#checkDel").val(parseFloat(_sub.t_evascore)); 
		if(_sub.score === "") $("#selfDel").val("");
		else $("#selfDel").val(parseFloat(_sub.score));
		$("#t_Del").attr("name",  _sub.choose);
		$("#selfDel").attr("name",  _sub.selfscoreid);   //将流水号加到标签的name属性中
		if(parseInt(isF1Passed) == 2)  //如果审核通过
		{
			$("#t_Del").attr("disabled", "disabled"); 
			$("#checkDel").attr("disabled", "disabled");
		}
	}
}

//保留两位小数
function formatFloat(src, pos)
{
    return Math.round(src*Math.pow(10, pos))/Math.pow(10, pos);
}

//将学生的自评成绩加载到表中
function loadSelfEva()
{
	parent.parent.document.getElementById("frame_content").height=420;	
	
	var date = new Date();
	var vYear = date.getFullYear();
	
	var showF3 = {};
	showF3["sid"] = currentMem.find("#stuID").html();
	showF3["year"] = vYear;
	//alert($.toJSON(reviseSelfScore));
	
	$.get(
		"../../../testdrive/index.php?r=temp/displaySelfScore",
		{"showF3": $.toJSON(showF3)},
		function(data)
		{
			var ret = $.evalJSON(data);

			if(ret.success = false)
			{
				//alert('服务器端返回错误信息=>"'+ret.message+'"');
				if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
					{
						alert(ret.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href='../../../';
						return false;
					}
				alert(ret.message);
				return false;
			}
			
			var score = ret.results.score;
			var count = 0;
			var tempRecords = 0;
			var items = ret.results.contents;
			for(var i = 0; i < items.length; i++)
			{
				var choose = items[i].choose;
				for(var j = 0; j < items[i].contents.length; j++)
				{
					var tr = $("#tb tr").eq(2).clone();//复制一行
					tr.find("#selectType").val(choose);
					tr.find("#TContent").val(items[i].contents[j].content);
					//加流水号
					tr.find("#TContent").attr("name", items[i].contents[j].selfscoreid);
					
					if(items[i].contents[j].score === "")
						tr.find("#TScore").html("未自评");
					else
						tr.find("#TScore").html(parseFloat(items[i].contents[j].score));
					
					if(items[i].contents[j].t_evascore === "")
						 tr.find("#t_evascore").val("");
					else tr.find("#t_evascore").val(parseFloat(items[i].contents[j].t_evascore));
					
					if(items[i].contents[j].te_evascore === "")
						tr.find("#te_evascore").html("未审核");
					else
						tr.find("#te_evascore").html(parseFloat(items[i].contents[j].te_evascore));
					
					tr.show();
					tr.appendTo("#tb");
					tempRecords++;
				}
			}
			
			judgePassed(ret.results.isPassed);
			
			setRecordPage1(tempRecords);

			currentPage1 = 1;
			$("#tb tbody tr:gt(10)").each(function() {
				$(this).hide();
			});

			$("#showPageSelf").html(currentPage1+" 共"+pages1+"页");

			loadF1(ret.results.add, ret.results['sub'], ret.results.isF1Passed);
			
			adjustHeight();
			$('html,body', window.parent.parent.document).animate({
				scrollTop: $("#mark").offset().top+135}, 
				600
			);
		}
	);
}

function checkAMem()
{
	$("#BtSave").show();
	$("#students").hide();
	$("#users-contain").show();
	loadSelfEva();
}

//传入需要被显示的数据的位置
//仅显示_min 和 _max之间的数据
function ShowPage(_min, _max)
{
	$("#stu-table tbody tr").each(function() {
		$(this).show();
	});
	
	var variable = "#stu-table tbody tr:lt("+_min+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	variable = "#stu-table tbody tr:gt("+_max+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	$("#showPageStu").html(currentPage+" 共"+pages+"页");
	adjustHeight();
}

//设置页数和总记录数
function setRecordPage(number)
{
	totalRecord = number;
	if(number <= 0)
	{
		totalRecord = 0;
		pages = 0;
	}
	else
	{//每页有10条记录
		pages = parseInt(number/10);
		if(number%10 != 0)
			pages += 1;
	}
	//页数>1 需翻页按钮
	if(pages > 1)
	{
		$("#btLast").show(); $("#btNext").show(); $("#btFormer").show(); $("#btFirst").show(); 
	}
}

//从服务器加载学生列表
function loadStuInfo()
{
	var getStuByMem = {};
	getStuByMem["sid"] = "";
	getStuByMem["type"] = "F3";
	
	$.get(
		"../../../testdrive/index.php?r=temp/getStuByMem",
		{getStuByMem:$.toJSON(getStuByMem)},
		function(data)
		{
			var obj = $.evalJSON(data);
			
			if(obj.success == true)
			{
				if(obj.results.length != 0)
				{
					loadSelection();
					
					setRecordPage(obj.results.length);
					for (var i = 0; i < obj.results.length; i++)
					{
						var tr = $("#stu-table tr").eq(1).clone();
						tr.find("td").get(1).innerHTML = obj.results[i]['sid'];
						tr.find("td").get(2).innerHTML = obj.results[i]['sname'];
						if(parseInt(obj.results[i]['isPassed']) == 2)
						{
							tr.find("#link").html("已审核");
							tr.find("#link").removeAttr("onclick");
							tr.find("#link").removeAttr("href");
						}
						tr.show();
						tr.appendTo("#stu-table tbody");
					}
					
					currentPage = 1;
					$("#stu-table tbody tr:gt(10)").each(function() {
						$(this).hide();
					});
					
					$("#showPageStu").html(currentPage+" 共"+pages+"页");
					
					adjustHeight();
				}
				else
				{
					$("#students").hide();
					$("#error").show();
				}
			}
			else
			{
				//alert("服务器端返回错误信息=>\""+obj.message+'"');
				if(obj.results.hasOwnProperty("timeout") && obj.results.timeout)
					{
						alert(obj.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href='../../../';
						return false;
					}

				alert(obj.message);
				$("#students").hide();
				$("#error").show();
				return false;
			}
			
		}
	);
}

function adjustHeight()
{
	var height = document.body.clientHeight;
	//alert("body="+document.body.clientHeight);
	//alert("documentElement="+document.documentElement.clientHeight);
	//var height = document.documentElement.scrollHeight>document.body.scrollHeight
	//					? document.body.scrollHeight:document.documentElement.scrollHeight; 
	if(height + 50 > 420)
		parent.parent.document.getElementById("frame_content").height=height + 50;
	else
		parent.parent.document.getElementById("frame_content").height=420;	
}

//检查已添加条目的正确性
function updateTips( t ) {
	$( ".validateTips")
		.text( t )
		.addClass( "ui-state-highlight" );
	setTimeout(function() {
		$( ".validateTips").removeClass( "ui-state-highlight", 1500 );
		$( ".validateTips" ).text("");
	}, 1500 );
}

function checkLength( o, n, min, max ) {
	if ( o.val().length > max || o.val().length < min ) {
		o.addClass( "ui-state-error" );
		updateTips( '提示: " '+n+' "' + " 的长度应该在 " +
			min + " 和 " + max + "之间." );
		return false;
	} else {
		return true;
	}
}

function checkValue( o, n, min, max ) {
	if ( parseFloat(o.val()) > max || parseFloat(o.val()) < min) {
		o.addClass( "ui-state-error" );
		updateTips( '" '+n+' "' + " 的值 " +
			min + " 和 " + max + "之间." );
		return false;
	} else {
		return true;
	}
}

function checkRegexp( o, regexp, n ) {
	if ( !( regexp.test( o.val() ) ) ) {
		o.addClass( "ui-state-error" );
		updateTips( n );
		return false;
	} else {
		return true;
	}
}

function validator()
{
	var success = true;
	$("#tb tr:gt(2)").each(function() 
	{
		var selectType = $(this).find("#selectType"),
		TContent = $(this).find("#TContent"),
		TScore = $(this).find("#t_evascore"),
		allFields = $( [] ).add( selectType ).add( TContent ).add( TScore );

		var bValid = true;
		allFields.removeClass( "ui-state-error" );
		bValid = bValid && checkLength( TContent, "成果", 2, 50 );
		bValid = bValid && checkLength( TScore, "得分", 1, 8);
		//bValid = bValid && checkRegexp( TContent, /^[\u4e00-\u9fa5\da-zA-Z\-\_]+$/, "\"成果名称\"不能包含特殊字符，请输入中文、英文单词或者数字" );
		bValid = bValid && checkRegexp( TScore, /^\d+(.\d+$|\d*$)/, "\"得分\"应该输入合法数字" );
		bValid = bValid && checkValue( TScore, "得分", 0, 20);
		
		if ( ! bValid ) {
			success = false;
			return false;
		}
	});
	
	if(success == false)
		return false;
	
	if($("#t_Add").val() === "" && $("#checkAdd").val() === "")
	{
		//do nothing
	}
	else
	{
		//验证F1
		var selfAdd = $("#checkAdd"),
		t_Add = $("#t_Add"),
		allFields = $( [] ).add(t_Add).add( selfAdd );
	
		var bValid = true;
		allFields.removeClass( "ui-state-error" );
		bValid = bValid && checkLength( t_Add, "奖励加分", 2, 100);
		bValid = bValid && checkRegexp( selfAdd, /^\d+(.\d+$|\d*$)/, "\"核查分\"应该输入合法数字" );
		bValid = bValid && checkValue( selfAdd, "核查分", 0, 20);
		
		if ( ! bValid ) {
			success = false;
			return false;
		}
	}
	
	if(success == false)
	{
		return false;
	}
	
	if($("#t_Del").val() === "" && $("#checkDel").val() === "")
	{
		//do nothing 
	}
	else
	{
		var t_Del = $("#t_Del"),
		selfDel = $("#checkDel"),
		allFields = $( [] ).add( t_Del ).add( selfDel );
	
		var bValid = true;
		allFields.removeClass( "ui-state-error" );
		bValid = bValid && checkLength( t_Del, "惩处扣分", 2, 100);
		bValid = bValid && checkRegexp( selfDel, /^\d+(.\d+$|\d*$)/, "\"核查扣分\"应该输入合法数字" );
		bValid = bValid && checkValue( selfDel, "核查扣分", 0, 20);
		
		if ( ! bValid ) {
			success = false;
			return false;
		}
	}
	
	return success;
}
