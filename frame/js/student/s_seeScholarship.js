// JavaScript Document


$(document).ready(function() {
	//调整屏幕的高度
	parent.parent.document.getElementById("frame_content").height=420;	

	$("#error").hide();

	//获取查询年份
	setSearchYear();
	
	//初始化时，需要加载一个默认的表格
	drawGPAtable($("#selectYear").find("option:selected").text());
	
	//当下拉框中得年份改变时
	changeYear();

})

function adjustHeight()
{
	var height = document.documentElement.scrollHeight>document.body.scrollHeight
						? document.body.scrollHeight:document.documentElement.scrollHeight; 
	if(height+40 > 420)
		parent.parent.document.getElementById("frame_content").height=height + 40;
	else
		parent.parent.document.getElementById("frame_content").height=420;	
}

//设置下拉框中得年份
function setSearchYear()
{
	var date = new Date();
	var vYear = parseInt(date.getFullYear());
	
	//begin to add to select
	for(var year = vYear-4; year < vYear+4; year++)
	{
		if(year == vYear)
			$("#selectYear").append('<option selected="selected" value="'+year+'">'+year+'</option>');
		else
			$("#selectYear").append('<option value="'+year+'">'+year+'</option>');
	}
}

//下拉框改变时，需要重新查询构造成绩表格
function changeYear()
{
	$("#selectYear").change(function(){
		var selected = $("#selectYear").find("option:selected").text();
		drawGPAtable(selected);
	});
}

function drawGPAtable(selected)
{
	var searchScholarship = {};
	searchScholarship["sid"] = "";
	searchScholarship["year"] = selected;
	searchScholarship = $.toJSON(searchScholarship);
	
	$.get(
		"../../../testdrive/index.php?r=zing/searchScholar",
		{"searchScholarship": searchScholarship},
		function (data)
		{
			var ret = $.evalJSON(data);
			
			if(ret.success == true)
			{
				if(ret.results.length != 0)
				{
					$("#error").hide();
					$("#showInfo").show();
					$("#sid").html(ret.results.sid);
					$("#sname").html(ret.results.sname);
					$("#F1").html(parseFloat(ret.results.basicscore)+'分');
					$("#F2").html(parseFloat(ret.results.gpascore+'分'));
					$("#F3").html(parseFloat(ret.results.selfscore+'分'));
					$("#total").html(parseFloat(ret.results.total+'分'));
					//2012-02-18 modified by minzhenyu
					/*var ranks = ret.results.rank.split("/");
					if(ranks.length == 2)
					{
						$("#rank").html(ranks[0]+'名（共'+ranks[1]+'人参评）');
					}
					else
					{
						$("#rank").html('未知');
					}*/
					if($.trim(ret.results.scholarship) != "")
						$("#scholarship").html(ret.results.scholarship);
					else
						$("#scholarship").html("无");
				}
				else
				{
					$("#error").show();
					$("#showInfo").hide();
				}
			}
			else
			{
				if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
						{
							alert(ret.message+'  页面即将跳转到首页～');
							window.parent.parent.location.href='../../../';
							return false;
						}
				alert("服务器端返回错误信息=>\""+ret.message+'"');
			}
		}
	);
}
