// JavaScript Document

//从服务器请求学生的F2成绩，并初始化表格
loadF2();

//待选择学生列表分页
var totalRecord = 0;
var pages = 0;
var currentPage = 1;
//已添加学生列表分页
var totalRecord1 = 0;
var pages1 = 0;
var currentPage1 = 1;

//记录课程id号和对应的课程类型
var savedCourse = {};
//记录已经选择的B1和B2中包含的选修以及辅修之和
var Vice_B1 = 0;
var Vice_B2 = 0;

$(function() {
	//parent.parent.document.getElementById("frame_content").height=420;	
	//隐藏翻页按钮
	$("#btLast").hide(); $("#btNext").hide(); $("#btFormer").hide(); $("#btFirst").hide(); 
	//隐藏翻页按钮
	$("#btLast1").hide(); $("#btNext1").hide(); $("#btFormer1").hide(); $("#btFirst1").hide(); 
	//隐藏模板tr
	$("#tbToChoose tr").eq(1).hide();
	//隐藏模板tr
	$("#tbChoosed tr").eq(1).hide();

	btConfirm();
	
	//选择所有行
	$("#CKA").click(function() {
		$("#tbToChoose tbody tr").each(function() {
			if(!$(this).is(":hidden") && $(this).find("#CK").attr("disabled") != "disabled") 	
				$(this).find("#CK").get(0).checked = $("#CKA").get(0).checked;
		});
	});
	
	//选择所有行
	$("#CKA1").click(function() {
		$("#tbChoosed tbody tr").each(function() {
			if(!$(this).is(":hidden") && $(this).find("#CK1").attr("disabled") != "disabled") 	
				$(this).find("#CK1").get(0).checked = $("#CKA1").get(0).checked;
		});
	});
	
	//用来B1/B2的表格显示
	toChooseTable();
	choosedTable();

	//用来添加课程到B1/B2
	addB1OrB2();

	//用来从B1/B2删除课程
	deleteB1OrB2();
	
	adjustHeight();
});


//将select框中的选项添加到指定的表格中
function addSelected(table)
{
	$("#others option:selected").each(function(){
		var option = $(this);
		//先判断被添加的选项是否符合要求
		var success = false;
		if(table=="tbChoosed" && savedCourse[option.val()][0].indexOf("必修") == -1 && Vice_B2+1 <= 8)  //add b2
		{
			success = true;
			Vice_B2++;
		}

		if(table=="tbToChoose" && Vice_B1+1 <= 4)  //add b1
		{
			if(savedCourse[option.val()][0].indexOf("必修") != -1 || savedCourse[option.val()][0].indexOf("专业选修") != -1)
			{
				success = true;
				Vice_B1++;
			}
		}

		//只有符合要求才进行增加操作
		if(success)
		{
			if($("#"+table+" tr:last").find("td").get(1).innerHTML != "&nbsp;")  //需要分页
			{
				//增加一个空页
				for(var i = 0; i < 10; i++)
				{
					var row = $("#"+table+" tr").eq(1).clone();
					row.show();
					row.appendTo("#"+table);
				}
				if(table=="tbChoosed")
				{
					totalRecord1 += 10;
					setRecordPage1(totalRecord1);	
					rePage1(totalRecord1, pages1);
				}
				else if(table=="tbToChoose")
				{
					totalRecord += 10;
					setRecordPage(totalRecord);	
					rePage(totalRecord, pages);
				}
			}
			$("#"+table+" tr:gt(1)").each(function()
			{
				if($(this).find("td").get(1).innerHTML == "&nbsp;")
				{
					
					if(success)
					{
						$(this).find("td").get(1).innerHTML = option.val();
						$(this).find("td").get(2).innerHTML = option.text();
					}
					return false;
				}
			});

			$(this).remove();
			
			//计算B1、B2成绩
			calB1B2();
		}
		else
		{
			if (table == "tbToChoose")
			{
				alert("您不能将公共选修或者辅修课程加入到B1，或者您B1课程中专业选修科目超过4门，请重新选择~");
				return false;
			}
			else if(table == "tbChoosed")
			{
				alert("您不能将必修课程加入到B2，或者您B2课程中科目超过8门，请重新选择~");
				return false;
			}
			return false;
		}
	});
}

//删除选中的记录
function deleteSelected(table)
{
	var rows = 0; //记录空的行数
	var success = false;

	$("#"+table+" tbody tr:gt(0)").each(function() {
		var checkbox = "";
		if(table == "tbToChoose")
			checkbox = "CK";
		else if(table == "tbChoosed")
			checkbox = "CK1";
		if(!$(this).is(":hidden") && $(this).find("#"+checkbox).get(0).checked == true) 
		{
			var cid = $(this).find("td").get(1).innerHTML;
			if(cid != "&nbsp;")
			{
				var ok = false;
				if(table == "tbToChoose" && savedCourse[cid][0].indexOf("必修") == -1)  //不是必修课，则可以删除
				{
					ok = true;
					Vice_B1--;
				}
				else if(table == "tbChoosed")
				{
					ok = true;
					Vice_B2--;
				}
				if(ok)
				{
					$(this).remove();
					$("#others").append('<option value="'+
						$(this).find("td").get(1).innerHTML+'">'+$(this).find("td").get(2).innerHTML+'</option>');

					var row = $("#"+table+" tr").eq(1).clone();
					row.show();
					row.appendTo("#"+table);
					success = true;
				}
			}
		}
	});
		
	if(!success)
	{
		alert("您还没有选择要删除的课程~");
		resetSelection();
		return false;
	}
	
	$("#"+table+" tbody tr:gt(0)").each(function() {
		if($(this).find("td").get(1).innerHTML == "&nbsp;")
		{
			rows++;
		}
	});

	if(table == "tbToChoose")
	{
		if(pages >1 && rows >= 10)
		{
			$("#tbToChoose tbody tr:gt("+(totalRecord-10)+")").remove();
			setRecordPage(totalRecord-10);
		}

		rePage(totalRecord, currentPage);
	}
	else if(table == "tbChoosed")
	{
		if(pages1 >1 && rows >= 10)
		{
			$("#tbChoosed tbody tr:gt("+(totalRecord1-10)+")").remove();
			setRecordPage1(totalRecord1-10);
		}

		rePage1(totalRecord1, currentPage1);
	}
	resetSelection();
	
	//计算B1、B2成绩
	calB1B2();
}

function deleteB1OrB2()
{
	$("#btDelB1").click(function(){
		deleteSelected("tbToChoose");
    adjustHeight();
	});

	$("#btDelB2").click(function(){
		deleteSelected("tbChoosed");
    adjustHeight();
	});
}

//用来添加课程到B1/B2
function addB1OrB2()
{
	$("#addB1").click(function(){
		addSelected("tbToChoose");
    adjustHeight();
	});

	$("#addB2").click(function(){
		addSelected("tbChoosed");
    adjustHeight();
	});
}

//绘制B1表示
function drawB1(B1)
{
	//根据返回的结果数目计算分页，分页不足一页的，总是保证每页10条，不足的用空行补充
	setRecordPage(B1.length);
			
	for(var i = 0; i < B1.length; i++)
	{
		savedCourse[B1[i].cid] =Array(B1[i].ctype, parseFloat(B1[i].cpoint), parseFloat(B1[i].cscore));
		var row = $("#tbToChoose tr").eq(1).clone();
		row.show();
		row.appendTo("#tbToChoose");
		
		row.find("td").get(1).innerHTML = B1[i].cid;
		if(B1[i].ctype.indexOf("必修") != -1)
		{
			row.find("#CK").attr("disabled", "disabled");
		}
		else
		{
			Vice_B1++;
		}
		row.find("td").get(2).innerHTML = B1[i].cname+" "+B1[i].ctype+" "+B1[i].cpoint+"学分 "+B1[i].cscore+"分";
	}
	for(var i = B1.length; i < pages*10; i++)
	{
		var row = $("#tbToChoose tr").eq(1).clone();
		row.show();
		row.appendTo("#tbToChoose");
	}
	setRecordPage(pages*10);
	currentPage = 1;
	$("#tbToChoose tbody tr:gt(10)").each(function() {
		$(this).hide();
	});
					
	$("#showPage").html(currentPage+"/"+pages+" 页");
					
	adjustHeight();
}

//绘制B2表格
function drawB2(B2)
{
	//根据返回的结果数目计算分页，分页不足一页的，总是保证每页10条，不足的用空行补充
	setRecordPage1(B2.length);
		
	for(var i = 0; i < B2.length; i++)
	{
		savedCourse[B2[i].cid] =Array(B2[i].ctype, parseFloat(B2[i].cpoint), parseFloat(B2[i].cscore));
		var row = $("#tbChoosed tr").eq(1).clone();
		row.show();
		row.appendTo("#tbChoosed");
		
		row.find("td").get(1).innerHTML = B2[i].cid;
		row.find("td").get(2).innerHTML = B2[i].cname+" "+B2[i].ctype+" "+B2[i].cpoint+"学分 "+B2[i].cscore+"分";
		
		Vice_B2++;
	}
	for(var i = B2.length; i < pages1*10; i++)
	{
		var row = $("#tbChoosed tr").eq(1).clone();
		row.show();
		row.appendTo("#tbChoosed");
	}
	setRecordPage1(pages1*10);
	currentPage1 = 1;
	$("#tbChoosed tbody tr:gt(10)").each(function() {
		$(this).hide();
	});
					
	$("#showPage1").html(currentPage1+"/"+pages1+" 页");
					
	adjustHeight();
}

//绘制others表格
function drawOthers(others)
{			
	for(var i = 0; i < others.length; i++)
	{
		savedCourse[others[i].cid] = Array(others[i].ctype, parseFloat(others[i].cpoint), parseFloat(others[i].cscore));
		var score = others[i].cname+" "+others[i].ctype+" "+others[i].cpoint+"学分 "+others[i].cscore+"分";
		$("#others").append('<option value="'+others[i].cid+'">'+score+'</option>');
	}
}

//ajax请求F2成绩
function loadF2()
{
	var date = new Date();
	var vYear = date.getFullYear();

	var getF2 = {};
	getF2["sid"] = "";
	getF2["year"] = vYear;
	$.get(
		"../../../testdrive/index.php?r=zing/getF2",
		{"getF2": $.toJSON(getF2)},
		function(data)
		{
			var ret = $.evalJSON(data);
			if(ret.success == true)
			{
				if(!($.isEmptyObject(ret.results)))
				{
					var B1 = ret.results.B1;
					drawB1(B1);
					var B2 = ret.results.B2;
					drawB2(B2);
					var others = ret.results.others;
					drawOthers(others);
					
					//计算B1、B2成绩
					calB1B2();
					
					adjustHeight();
					//alert($.toJSON(savedCourse));

					$('html,body', window.parent.parent.document).animate({
						scrollTop: $("#mark").offset().top+135}, 
						600
					);
				}
				else
				{
					alert("服务器端返回信息不完整，请重新选择~");
				}
			}
			else
			{
				if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
				{
					alert(ret.message+'  页面即将跳转到首页～');
					window.parent.parent.location.href='../../../';
					return false;
				}

				alert('服务器端返回错误信息=>"'+ret.message+'"');
			}
		}
	);
}

//已添加的分页按钮控制
function choosedTable()
{
	$("#btLast1").click(function() 
	{
		if(currentPage1 != pages1)
		{
			//alert("即将跳转到最后一页");
			
			currentPage1 = pages1;
			ShowPage1(currentPage1*10-9, totalRecord1);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btNext1").click(function() 
	{
		if(currentPage1+1<=pages1)
		{
			//alert("即将跳转到下一页");
			currentPage1 += 1;
			ShowPage1(currentPage1*10-9, currentPage1*10);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btFormer1").click(function() 
	{
		if(currentPage1-1>0)
		{
			//alert("即将跳转到上一页");
			currentPage1 -= 1;
			ShowPage1(currentPage1*10-9, currentPage1*10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
	$("#btFirst1").click(function() 
	{
		if(currentPage1 != 1)
		{
			//alert("即将跳转到首页");
			currentPage1 = 1;
			ShowPage1(1, 10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
}

//重新分页
function rePage1(_totalRecord, _currentPage)
{
	var tempPage = _currentPage;
	setRecordPage1(_totalRecord);
	
	if (tempPage < pages1)  
	{
		currentPage1 = tempPage;
		ShowPage1(tempPage*10-9, tempPage*10);
	}
	else
	{
		currentPage1 = pages1;
		ShowPage1(pages1*10-9, _totalRecord);
	}
}

//重新分页
function rePage(_totalRecord, _currentPage)
{
	var tempPage = _currentPage;
	setRecordPage(_totalRecord);
	
	if (tempPage < pages)  
	{
		currentPage = tempPage;
		ShowPage(tempPage*10-9, tempPage*10);
	}
	else
	{
		currentPage = pages;
		ShowPage(pages*10-9, _totalRecord);
	}
}
//传入需要被显示的数据的位置
//仅显示_min 和 _max之间的数据
function ShowPage1(_min, _max)
{
	$("#tbChoosed tbody tr").each(function() {
		$(this).show();
	});
	
	var variable = "#tbChoosed tbody tr:lt("+_min+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	variable = "#tbChoosed tbody tr:gt("+_max+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	$("#showPage1").html(currentPage1+"/"+pages1+" 页");
	adjustHeight();
}

//设置页数和总记录数
function setRecordPage1(number)
{
	totalRecord1 = number;
	if(number <= 0)
	{
		totalRecord1 = 0;
		pages1 = 1;
	}
	else
	{//每页有10条记录
		pages1 = parseInt(number/10);
		if(number%10 != 0)
			pages1 += 1;
	}
	//页数>1 需翻页按钮
	//alert(pages1);
	if(pages1 > 1)
	{
		$("#btLast1").show(); $("#btNext1").show(); $("#btFormer1").show(); $("#btFirst1").show(); 
	}
	else
	{
		$("#btLast1").hide(); $("#btNext1").hide(); $("#btFormer1").hide(); $("#btFirst1").hide(); 
	}
}


//左边的翻页
function toChooseTable()
{
	$("#btLast").click(function() 
	{
		if(currentPage != pages)
		{
			//alert("即将跳转到最后一页");
			currentPage = pages;
			ShowPage(currentPage*10-9, totalRecord);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btNext").click(function() 
	{
		if(currentPage+1<=pages)
		{
			//alert("即将跳转到下一页");
			currentPage += 1;
			ShowPage(currentPage*10-9, currentPage*10);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btFormer").click(function() 
	{
		if(currentPage-1>0)
		{
			//alert("即将跳转到上一页");
			currentPage -= 1;
			ShowPage(currentPage*10-9, currentPage*10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
	$("#btFirst").click(function() 
	{
		if(currentPage != 1)
		{
			//alert("即将跳转到首页");
			currentPage = 1;
			ShowPage(1, 10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
}

//传入需要被显示的数据的位置
//仅显示_min 和 _max之间的数据
function ShowPage(_min, _max)
{
	$("#tbToChoose tbody tr").each(function() {
		$(this).show();
	});
	
	var variable = "#tbToChoose tbody tr:lt("+_min+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	variable = "#tbToChoose tbody tr:gt("+_max+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	$("#showPage").html(currentPage+"/"+pages+" 页");
	
	adjustHeight();
}

//设置页数和总记录数
function setRecordPage(number)
{
	totalRecord = number;
	if(number <= 0)
	{
		totalRecord = 0;
		pages = 1;
	}
	else
	{//每页有10条记录
		pages = parseInt(number/10);
		if(number%10 != 0)
			pages += 1;
	}
	
	//页数>1 需翻页按钮
	if(pages > 1)
	{
		$("#btLast").show(); $("#btNext").show(); $("#btFormer").show(); $("#btFirst").show(); 
	}
	else
	{
		$("#btLast").hide(); $("#btNext").hide(); $("#btFormer").hide(); $("#btFirst").hide(); 
	}
}

//保留两位小数
function formatFloat(src, pos)
{
    return Math.round(src*Math.pow(10, pos))/Math.pow(10, pos);
}

//实时计算B1和B2得分，以及总分
//modified by minzhenyu 2012-02-16
function calB1B2()
{
	var saveF2 = {};
	saveF2["B2"] = [];
	$("#tbChoosed tbody tr:gt(0)").each(function() {
		if($(this).find("td").get(1).innerHTML != "&nbsp;")
			saveF2["B2"].push($(this).find("td").get(1).innerHTML);
	});

	saveF2["B1"] = [];
	$("#tbToChoose tbody tr:gt(0)").each(function() {
		if($(this).find("td").get(1).innerHTML != "&nbsp;")
			saveF2["B1"].push($(this).find("td").get(1).innerHTML);
	});

	//计算课程成绩
	var sum = 0;
	var point = 0;
	saveF2["score"] = 0;
	for(var i = 0; i < saveF2["B1"].length; i++)
	{
		sum += savedCourse[saveF2["B1"][i]][2]*savedCourse[saveF2["B1"][i]][1];
		point += savedCourse[saveF2["B1"][i]][1];
	}
	if(saveF2["B1"].length)
	{
		saveF2["score"] += formatFloat(sum*1.0/point, 3);
	}
	else saveF2["score"] = 0;
	//设置前端显示
        $("#spanB1").get(0).innerHTML = '（'+saveF2["score"] +'分）';
	
	sum = 0;
	for(var i = 0; i < saveF2["B2"].length; i++)
	{
		sum += savedCourse[saveF2["B2"][i]][2]*savedCourse[saveF2["B2"][i]][1];
		//point += savedCourse[saveF2["B2"][i]][1];
	}
	//设置前端显示
	$("#spanB2").get(0).innerHTML =  '（'+formatFloat(sum*0.002, 3)+'分）';
	saveF2["score"] += formatFloat(sum*0.002, 3);
	
	saveF2["score"] = formatFloat(saveF2["score"], 3);
	//设置前端显示
	$("#totalScore").get(0).innerHTML = '课程成绩（'+saveF2["score"]+'分）';
}

//saveF2
function btConfirm()
{
	$("#btConfirm").click(function() {
		//Vice_B1  Vice_B2这两个条件是软性要求
		var success = false;
		$("#tbToChoose tbody tr:gt(0)").each(function() {
			if($(this).find("td").get(1).innerHTML != "&nbsp;")
			{
				success = true;
				return false;
			}
		});
		if(!success)
		{
			alert("您还没有选择B1课程，无需提交~");
			resetSelection();
			return false;
		}

		if(Vice_B2 < 0)
		{
			alert("您还没有选择B2课程，无需提交~");
			resetSelection();
			return false;
		}

		if(confirm('您将提交自我测评课程成绩(即奖学金评定中F2选项)，确定继续？'))
		{
			var saveF2 = {};
			saveF2["B2"] = [];
			$("#tbChoosed tbody tr:gt(0)").each(function() {
				if($(this).find("td").get(1).innerHTML != "&nbsp;")
					saveF2["B2"].push($(this).find("td").get(1).innerHTML);
			});

			saveF2["B1"] = [];
			$("#tbToChoose tbody tr:gt(0)").each(function() {
				if($(this).find("td").get(1).innerHTML != "&nbsp;")
					saveF2["B1"].push($(this).find("td").get(1).innerHTML);
			});
			
			//计算课程成绩
			var sum = 0;
			var point = 0;
			saveF2["score"] = 0;
			for(var i = 0; i < saveF2["B1"].length; i++)
			{
				sum += savedCourse[saveF2["B1"][i]][2]*savedCourse[saveF2["B1"][i]][1];
				point += savedCourse[saveF2["B1"][i]][1];
			}
			saveF2["score"] += formatFloat(sum*1.0/point, 3);
			sum = 0;
			for(var i = 0; i < saveF2["B2"].length; i++)
			{
				sum += savedCourse[saveF2["B2"][i]][2]*savedCourse[saveF2["B2"][i]][1];
				//point += savedCourse[saveF2["B2"][i]][1];
			}
			saveF2["score"] += formatFloat(sum*0.002, 3);
			saveF2["score"] = formatFloat(saveF2["score"], 3);

			//alert($.toJSON(saveF2));
			$("#layer").height(parent.parent.document.getElementById("frame_content").height);
			$("#layer").mask("正在提交，请稍等...");
			$.post(
				"../../../testdrive/index.php?r=zing/saveF2",
				{"saveF2": $.toJSON(saveF2)},
				function(data)
				{
					var ret = $.evalJSON(data);
					if(ret.success == true)
					{
						alert("提交成功~");
						//$("#btConfirm").attr({"disabled":"disabled"});
					}
					else
					{
						if(ret.results.hasOwnProperty('timeout') && ret.results.timeout)
						{
							alert(ret.message+'  页面即将跳转到首页～');
							window.parent.parent.location.href='../../../';
							return false;
						}

						alert('服务器端返回错误信息=>"'+ret.message+'"');
					}
					$("#layer").unmask();
					resetSelection();
				}
			);
		}
	});
}

//充值选择单选按钮
function resetSelection()
{
	$("#CKA1").attr("checked", false);
	$("#CKA1").click();
	$("#CKA1").attr("checked", false);
	$("#CKA").attr("checked", false);
	$("#CKA").click();
	$("#CKA").attr("checked", false);
}

//2012-02-17 modified by minzhenyu
function adjustHeight()
{
	var height = document.documentElement.scrollHeight<document.body.scrollHeight
						? document.body.scrollHeight:document.documentElement.scrollHeight; 
	if(height > 420)
		parent.parent.document.getElementById("frame_content").height=height;
	else
		parent.parent.document.getElementById("frame_content").height=420;	
}
