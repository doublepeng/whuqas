// JavaScript Document

$(document).ready(function() {
	//调整屏幕的高度
	parent.parent.document.getElementById("frame_content").height=420;	
	//隐藏翻页按钮
	$("#btLast").hide(); $("#btNext").hide(); $("#btFormer").hide(); $("#btFirst").hide(); 
	//隐藏模板tr
	$("#tb tr").eq(2).hide();

	//获取查询年份
	setSearchYear();
	
	//初始化时，需要加载一个默认的表格
	drawGPAtable($("#selectYear").find("option:selected").text());
	
	//当下拉框中得年份改变时
	changeYear();

})

function adjustHeight()
{
	var height = document.documentElement.scrollHeight>document.body.scrollHeight
						? document.body.scrollHeight:document.documentElement.scrollHeight; 
	if(height > 420)
		parent.parent.document.getElementById("frame_content").height=height + 28;
	else
		parent.parent.document.getElementById("frame_content").height=420;	
}

//设置下拉框中得年份
function setSearchYear()
{
	var date = new Date();
	var vYear = parseInt(date.getFullYear());
	
	//begin to add to select
	for(var year = vYear-4; year < vYear+4; year++)
	{
		if(year == vYear)
			$("#selectYear").append('<option selected="selected" value="'+year+'">'+year+'</option>');
		else
			$("#selectYear").append('<option value="'+year+'">'+year+'</option>');
	}
}

//下拉框改变时，需要重新查询构造成绩表格
function changeYear()
{
	$("#selectYear").change(function(){
		var selected = $("#selectYear").find("option:selected").text();
		drawGPAtable(selected);
	});
}

//保留两位小数
function formatFloat(src, pos)
{
    return Math.round(src*Math.pow(10, pos))/Math.pow(10, pos);
}

function drawGPAtable(selected)
{
	parent.parent.document.getElementById("frame_content").height=420;	

	var searchBasic = {};
	searchBasic["sid"] = "";
	searchBasic["year"] = selected;
	searchBasic = $.toJSON(searchBasic);
	//alert(searchGPA);
	
	$.get(
		"../../../testdrive/index.php?r=zing/getBasicScore",
		{"searchBasic": searchBasic},
		function (data)
		{
			var ret = $.evalJSON(data);
			
			if(ret.success == true)
			{
				if(ret.results.length != 0 && ret.results.contents.length != 0)
				{
					$("#tb tr:gt(2)").remove();
					//var scripts = "";
					
					var contents = ret.results.contents;
					var totalScore = 0, totalCount = 0, _score = 0;
					for(var i = 0; i < contents.length; i++)
					{
						var tr = $("#tb tr").eq(2).clone();//复制一行
						tr.show();
						tr.appendTo("#tb");
						tr.find("td").get(0).innerHTML = parseFloat(contents[i].A1);
						tr.find("td").get(1).innerHTML = parseFloat(contents[i].t_A1);
						tr.find("td").get(2).innerHTML = parseFloat(contents[i].A2);
						tr.find("td").get(3).innerHTML = parseFloat(contents[i].t_A2);
						tr.find("td").get(4).innerHTML = parseFloat(contents[i].A3);
						tr.find("td").get(5).innerHTML = parseFloat(contents[i].t_A3);
						tr.find("td").get(6).innerHTML = parseFloat(contents[i].A4);
						tr.find("td").get(7).innerHTML = parseFloat(contents[i].t_A4);
						tr.find("td").get(8).innerHTML = parseFloat(contents[i].A5);
						tr.find("td").get(9).innerHTML = parseFloat(contents[i].t_A5);
						if(parseInt(contents[i].check) == 0 || parseInt(contents[i].check) == 1)   //辅导员未审核通过
						{
							tr.find("td").get(11).innerHTML = '<h1 style="font-size:12px; margin:0;font-weight:normal">未审核</h1>';

							tr.find("td").get(1).innerHTML = '<h1 style="font-size:12px; margin:0;font-weight:normal">未审核</h1>';
							tr.find("td").get(3).innerHTML = '<h1 style="font-size:12px; margin:0;font-weight:normal">未审核</h1>';
							tr.find("td").get(5).innerHTML = '<h1 style="font-size:12px; margin:0;font-weight:normal">未审核</h1>';
							tr.find("td").get(7).innerHTML = '<h1 style="font-size:12px; margin:0;font-weight:normal">未审核</h1>';
							tr.find("td").get(9).innerHTML = '<h1 style="font-size:12px; margin:0;font-weight:normal">未审核</h1>';
							tr.find("td").get(10).innerHTML = '<h1 style="font-size:12px; margin:0;font-weight:normal">暂无</h1>';
						}
						else //审核通过
						{
							tr.find("td").get(11).innerHTML = '<h1 style="font-size:12px; margin:0;font-weight:normal">通过</h1>';
							var sum = parseFloat(contents[i].t_A1) + parseFloat(contents[i].t_A2) +parseFloat(contents[i].t_A3)
								+ parseFloat(contents[i].t_A4) +parseFloat(contents[i].t_A5);
							tr.find("td").get(10).innerHTML = sum;
							totalScore += sum;
							totalCount ++;
						}
					}
					if(totalCount != 0)
						_score = formatFloat(totalScore/totalCount,3);
					$("#tb tbody").append('<tr><td colspan="12"><h1 style="color:red; font-size:12px">基本素质测评分(F1)：'+_score+'（仅计算审核状态为"通过"的平均分）</h1></td></tr>');
					adjustHeight();
				}
				else
				{
					$("#tb tr:gt(2)").remove();

					alert("您没有该学年的基本素质测评成绩，或者服务器端返回数据出错~");

					return false;
				}
				
				var add_score = 0;  //用来记录额外加分
				var sub_score = 0;   //用来记录额外减分
				$("#tb tbody").append('<tr class="ui-widget-header"><td colspan="2">评项</td><td colspan="2">测评内容</td><td colspan="2">自评分</td><td colspan="2">测评小组审核分</td><td colspan="2">辅导员审核分</td><td>终评分</td><td>审核<br />通过</td></tr>');
				$("#tb tbody").append('<tr id="td_add"><td colspan="2">奖励加分</td><td colspan="2">无</td><td colspan="2">无</td><td colspan="2">无</td><td colspan="2">无</td><td>0</td><td>未审核</td></tr>');
				$("#tb tbody").append('<tr id="td_sub"><td colspan="2">惩处扣分</td><td colspan="2">无</td><td colspan="2">无</td><td colspan="2">无</td><td colspan="2">无</td><td>0</td><td>未审核</td></tr>');
				
				if(ret.results["add"].length != 0)
				{
					//$("#td_add").find("td").get(0).innerHTML = ret.results["add"]["choose"];
					$("#td_add").find("td").get(1).innerHTML = ret.results["add"]["content"];
					$("#td_add").find("td").get(2).innerHTML = parseFloat(ret.results["add"]["score"]);

					if(parseInt(ret.results["add"]["check"]) == 0)   //都没审核
					{
						$("#td_add").find("td").get(3).innerHTML = '未审核';
						$("#td_add").find("td").get(4).innerHTML = '未审核';
						$("#td_add").find("td").get(5).innerHTML = '未审核';
						$("#td_add").find("td").get(6).innerHTML = '未审核';
					}
					else if(parseInt(ret.results.add["check"]) == 1)   //测评小组审核通过
					{
						$("#td_add").find("td").get(3).innerHTML = parseFloat(ret.results.add["t_evascore"]);
						$("#td_add").find("td").get(4).innerHTML = '未审核';
						$("#td_add").find("td").get(5).innerHTML = '未审核';
						$("#td_add").find("td").get(6).innerHTML = '未审核';
					}
					else if(parseInt(ret.results.add["check"]) == 2)   //辅导员审核通过
					{
						$("#td_add").find("td").get(3).innerHTML = parseFloat(ret.results.add["t_evascore"]);
						$("#td_add").find("td").get(4).innerHTML = parseFloat(ret.results.add["te_evascore"]);
						$("#td_add").find("td").get(5).innerHTML = parseFloat(ret.results.add["te_evascore"]);
						$("#td_add").find("td").get(6).innerHTML = '通过';
						add_score = parseFloat(ret.results.add["te_evascore"]);
					}
				}

				if(ret.results["sub"].length != 0)
				{
					//$("#td_sub").find("td").get(0).innerHTML = ret.results["sub"]["choose"];
					$("#td_sub").find("td").get(1).innerHTML = ret.results["sub"]["content"];
					$("#td_sub").find("td").get(2).innerHTML = parseFloat(ret.results["sub"]["score"]);

					if(parseInt(ret.results["sub"]["check"]) == 0)   //都没审核
					{
						$("#td_sub").find("td").get(3).innerHTML = '未审核';
						$("#td_sub").find("td").get(4).innerHTML = '未审核';
						$("#td_sub").find("td").get(5).innerHTML = '未审核';
						$("#td_sub").find("td").get(6).innerHTML = '未审核';
					}
					else if(parseInt(ret.results.sub["check"]) == 1)   //测评小组审核通过
					{
						$("#td_sub").find("td").get(3).innerHTML = parseFloat(ret.results.sub["t_evascore"]);
						$("#td_sub").find("td").get(4).innerHTML = '未审核';
						$("#td_sub").find("td").get(5).innerHTML = '未审核';
						$("#td_sub").find("td").get(6).innerHTML = '未审核';
					}
					else if(parseInt(ret.results.sub["check"]) == 2)   //辅导员审核通过
					{
						$("#td_sub").find("td").get(3).innerHTML = parseFloat(ret.results.sub["t_evascore"]);
						$("#td_sub").find("td").get(4).innerHTML = parseFloat(ret.results.sub["te_evascore"]);
						$("#td_sub").find("td").get(5).innerHTML = parseFloat(ret.results.sub["te_evascore"]);
						$("#td_sub").find("td").get(6).innerHTML = '通过';
						sub_score = parseFloat(ret.results.sub["te_evascore"]);
					}
				}
				$("#tb tbody").append('<tr><td colspan="12"><h1 style="color:red; font-size:12px">总得分：'+(_score-sub_score+add_score)+'（F1+奖励加分-惩处扣分）</h1></td></tr>');
				
				adjustHeight();

				$('html,body', window.parent.parent.document).animate({
					scrollTop: $("#tb").offset().top+135}, 
					400
				);
			}
			else
			{
				if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
						{
							alert(ret.message+'  页面即将跳转到首页～');
							window.parent.parent.location.href='../../../';
							return false;
						}
				alert("服务器端返回错误信息=>\""+ret.message+'"');
			}
		}
	);
}
