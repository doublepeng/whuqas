// JavaScript Document
//设置自评列表分页
var totalRecord = 0;
var pages = 0;
var currentPage = 1;
var index = 0;   //提供给表格用的序号

loadYearScore();

var allFields;	
var selectionArray;
var currentMem;   //用来记录当前编辑的是哪一行

//key：cid
//value: 存储一行的各项值
//{“cid”:”1”, “cname”:”高等数学”, “ctype”:”公共必修”, “cpoint”:5, “coursescore”:98}
var update = {};   //用来记录用户需要更新哪几门成绩
 
$(document).ready(function() {
	//调整屏幕的高度
	parent.parent.document.getElementById("frame_content").height=420;							   
	//隐藏翻页按钮
	$("#btLast").hide(); $("#btNext").hide(); $("#btFormer").hide(); $("#btFirst").hide(); 
	
	//隐藏模板tr
	$("#tb tr").eq(1).hide();
	$("#tb th").eq(2).hide(); $("#tb tbody #cid").hide();
		
	//添加记录按钮
	$("#BtAdd").click(function() {
		var tr = $("#tb tr").eq(1).clone();//复制一行
		tr.find("td").get(0).innerHTML = ++index;
		tr.show();
		tr.appendTo("#tb");
		
		totalRecord++;
		
		$("#tb tr:last :input:last").keypress(function(event){
			//tab enter
			if(event.keyCode==13 || event.keyCode==9){
				$("#BtAdd").click();
			}
		});
				
		var height = 
		document.documentElement.scrollHeight<document.body.scrollHeight?document.body.scrollHeight:document.documentElement.scrollHeight; 
		//alert(height);
		if(height > 420)
			parent.parent.document.getElementById("frame_content").height=height+34;
		else
			parent.parent.document.getElementById("frame_content").height=420;
		
		setRecordPage(totalRecord);	
		rePage(totalRecord, pages);
	});
	
	//删除所选记录按钮
	$("#BtDel").click(function() {
		var selected = false;
		$("#tb tr:gt(1)").each(function() {
			if ($(this).find("#CK").get(0).checked == true) {
				selected = true;
				return false;
			}
		});
		if(!selected)
		{
			alert("请您选择要删除的得分记录~");
			return false;
		}
		
		var height = 
		document.documentElement.scrollHeight<document.body.scrollHeight?document.body.scrollHeight:document.documentElement.scrollHeight; 
		
		var cantDel = "";
		$("#tb tr:gt(1)").each(function() {
			if ($(this).find("#CK").get(0).checked == true) {
				if($(this).find("#cid").html() == "") {
					$(this).remove();
					totalRecord--;
					height -= 34;
				}
				else
				{
					cantDel += '"'+$(this).find("#cname").val() + '", ';
				}
			}
		});
		if(cantDel.length != 0)
		{
			alert('课程'+cantDel+'是系统成绩，无法删除~');
		}
		
		if(height > 420)
			parent.parent.document.getElementById("frame_content").height=height;
		else
			parent.parent.document.getElementById("frame_content").height=420;	
			
		//父窗口iframe的宽度自适应	
		index = 0;
		$("#tb tr:gt(1)").each(function() {
			$(this).find("td").get(0).innerHTML = ++index;
		});
		$("#CKA").attr("checked", false);
		$("#CKA").click();
		$("#CKA").attr("checked", false);
		rePage(totalRecord, currentPage);
	});
	
	//选择所有行
	$("#CKA").click(function() {
		$("#tb tbody tr").each(function() {
			if(!$(this).is(":hidden")) 	
				$(this).find("#CK").get(0).checked = $("#CKA").get(0).checked;
		});
	});

	//保存记录
	$("#BtSave").click(function() {
		
		if(validator())
		{
			var saveScore = {};
			saveScore["update"] = [];
			saveScore["insert"] = [];
			$("#tb tr:gt(1)").each(function() {
				if($(this).find("#cid").html() == "")
				{
					saveScore["insert"].push({
						'cname': $(this).find("#cname").val(),
						'ctype': $(this).find("#ctype option:selected").val(),
						'cpoint': $(this).find("#cpoint").val(),
						'cscore': $(this).find("#cscore").val()
					});
				}
			});
			
			$.each(update, function(key, value){
				saveScore["update"].push({
					'cid': key,
					'cname': value["cname"],
					'ctype': value["ctype"],
					'cpoint': value["cpoint"],
					'cscore': value["cscore"]
				});
			});
			
			$.post(
				"../../../testdrive/index.php?r=zing/saveReCscore",
				{"saveScore": $.toJSON(saveScore)},
				function(data){
					var ret = $.evalJSON(data);
					if(ret.success == true)
					{
						alert("保存课程成绩成功~");
					}
					else
					{
						if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
						{
							alert(ret.message+'  页面即将跳转到首页～');
							window.parent.parent.location.href='../../../';
							return false;
						}

						alert('服务器端返回错误信息=>"'+ret.message+'"');
					}
					
					update = {};
				}
			);
			//alert($.toJSON(saveScore));
		}
	});
	
	$("#btLast").click(function() 
	{
		if(currentPage != pages)
		{
			//alert("即将跳转到最后一页");
			currentPage = pages;
			ShowPage(currentPage*10-9, totalRecord);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btNext").click(function() 
	{
		if(currentPage+1<=pages)
		{
			//alert("即将跳转到下一页");
			currentPage += 1;
			ShowPage(currentPage*10-9, currentPage*10);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btFormer").click(function() 
	{
		if(currentPage-1>0)
		{
			//alert("即将跳转到上一页");
			currentPage -= 1;
			ShowPage(currentPage*10-9, currentPage*10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
	$("#btFirst").click(function() 
	{
		if(currentPage != 1)
		{
			//alert("即将跳转到首页");
			currentPage = 1;
			ShowPage(1, 10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
})

//重新分页
function rePage(totalRecord1, currentPage1)
{
	var tempPage = currentPage1;
	setRecordPage(totalRecord1);
	
	if (tempPage < pages)  
	{
		currentPage = tempPage;
		ShowPage(tempPage*10-9, tempPage*10);
	}
	else
	{
		currentPage = pages;
		ShowPage(pages*10-9, totalRecord);
	}
}

//传入需要被显示的数据的位置
//仅显示_min 和 _max之间的数据
function ShowPage(_min, _max)
{
	$("#tb tbody tr").each(function() {
		$(this).show();
	});
	
	var variable = "#tb tbody tr:lt("+_min+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	variable = "#tb tbody tr:gt("+_max+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	$("#showPage").html(currentPage+" 共"+pages+"页");
	
	adjustHeight();
}

//设置页数和总记录数
function setRecordPage(number)
{
	totalRecord = number;
	if(number <= 0)
	{
		totalRecord = 0;
		pages = 1;
	}
	else
	{//每页有10条记录
		pages = parseInt(number/10);
		if(number%10 != 0)
			pages += 1;
	}
	
	//页数>1 需翻页按钮
	if(pages > 1)
	{
		$("#btLast").show(); $("#btNext").show(); $("#btFormer").show(); $("#btFirst").show(); 
	}
	else
	{
		$("#btLast").hide(); $("#btNext").hide(); $("#btFormer").hide(); $("#btFirst").hide(); 
	}
}

//编辑一门课程成绩
function editARow()
{
	if(currentMem.find("#edit").html() == "编辑")
	{
		//currentMem.find("#cname").removeAttr("disabled");
		//currentMem.find("#ctype").removeAttr("disabled");
		//currentMem.find("#cpoint").removeAttr("disabled");
		currentMem.find("#cscore").removeAttr("disabled");
		currentMem.find("#edit").html("保存");
	}
	else if(currentMem.find("#edit").html() == "保存")
	{
		if(validateARow(currentMem))
		{
			if(currentMem.find("#cid").html() != "")
				update[currentMem.find("#cid").html()] = {
					//'cname': currentMem.find("#cname").val(),
					//'ctype': currentMem.find("#ctype option:selected").val(),
					//'cpoint': currentMem.find("#cpoint").val(),
					'cscore': currentMem.find("#cscore").val()
				};

			currentMem.find("#cname").attr("disabled", "disabled");
			currentMem.find("#ctype").attr("disabled", "disabled");
			currentMem.find("#cpoint").attr("disabled", "disabled");
			currentMem.find("#cscore").attr("disabled", "disabled");
			currentMem.find("#edit").html("编辑");
		}
	}
}


//判断审核是否通过
function judgeIsPassed(isPassed)
{
	if(isPassed == 1)
	{
		$("#BtAdd").hide(); $("#BtSave").hide(); $("#BtDel").hide();
		$("#tb tr:gt(1)").each(function() {
			$(this).find("#cname").attr("disabled", "disabled");
			$(this).find("#ctype").attr("disabled", "disabled");
			$(this).find("#cpoint").attr("disabled", "disabled");
			$(this).find("#cscore").attr("disabled", "disabled");

			$(this).find("#edit").removeAttr("href");
			$(this).find("#edit").removeAttr("onclick");
		});
		
		$( ".validateTips")
		.text( "您本年度课程成绩自评分已经由辅导员审核通过，不能修改~" )
		.addClass( "ui-state-highlight" );
		setTimeout(function() {
			$( ".validateTips").removeClass( "ui-state-highlight", 1500 );
			$( ".validateTips" ).text("");
		}, 3000 );
	}
}

//加载学生本年度的课程成绩
function loadYearScore()
{
	parent.parent.document.getElementById("frame_content").height=420;	

	var date = new Date();
	var vYear = date.getFullYear();
	
	var yearScore = {};
	yearScore["sid"] = "";
	yearScore["year"] = vYear;

	$.get(
		"../../../testdrive/index.php?r=zing/displayReCscore",
		{"yearScore": $.toJSON(yearScore)},
		function(data)
		{
			var ret = $.evalJSON(data);
			
			if(ret.success == true)
			{
				if(!($.isEmptyObject(ret.results)) && parseInt(ret.results.hasPrivilege) != 0)
				{
					var score = ret.results.content;
					var tempRecords = 0;
					for(var i = 0; i < score.length; i++)
					{
						var tr = $("#tb tr").eq(1).clone();//复制一行
						tr.show();
						tr.appendTo("#tb");
						$("#tb tr:last").find("#index").html(++index);
						$("#tb tr:last").find("#cid").html(score[i].cid);
						$("#tb tr:last").find("#cname").val(score[i].cname);
						$("#tb tr:last").find("#ctype").val(score[i].ctype);
						$("#tb tr:last").find("#cpoint").val(score[i].cpoint);
						$("#tb tr:last").find("#cscore").val(score[i].cscore);
						$("#tb tr:last").find("#cname").attr("disabled", "disabled");
						$("#tb tr:last").find("#ctype").attr("disabled", "disabled");
						$("#tb tr:last").find("#cpoint").attr("disabled", "disabled");
						$("#tb tr:last").find("#cscore").attr("disabled", "disabled");	
						tempRecords++;
					}
					
					
					//判断是否审核通过
					judgeIsPassed(ret.results.isPassed);
					
					setRecordPage(tempRecords);

					currentPage = 1;
					$("#tb tbody tr:gt(10)").each(function() {
						$(this).hide();
					});

					$("#showPage").html(currentPage+" 共"+pages+"页");
					adjustHeight();
				}
				else
				{
					alert("您没有权限进行该操作~请可以向辅导员提出申请~");
					window.location.href = "../s_standard.html";
				}
			}
			else
			{
				if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
				{
					alert(ret.message+'  页面即将跳转到首页～');
					window.parent.parent.location.href='../../../';
					return false;
				}

				alert('服务器端返回错误信息=>"'+ret.message+'"');
			}
		}
	);
}

//检查已添加条目的正确性

function updateTips( t ) {
	$( ".validateTips")
		.text( t )
		.addClass( "ui-state-highlight" );
	setTimeout(function() {
		$( ".validateTips").removeClass( "ui-state-highlight", 1500 );
		$( ".validateTips" ).text("");
	}, 1500 );
}

function checkLength( o, n, min, max ) {
	if ( o.val().length > max || o.val().length < min ) {
		o.addClass( "ui-state-error" );
		updateTips( '提示: " '+n+' "' + " 的长度应该在 " +
			min + " 和 " + max + "之间." );
		return false;
	} else {
		return true;
	}
}

function checkValue( o, n, min, max ) {
	if ( parseFloat(o.val()) > max || parseFloat(o.val()) < min) {
		o.addClass( "ui-state-error" );
		updateTips( '" '+n+' "' + " 的值 " +
			min + " 和 " + max + "之间." );
		return false;
	} else {
		return true;
	}
}

function checkRegexp( o, regexp, n ) {
	if ( !( regexp.test( o.val() ) ) ) {
		o.addClass( "ui-state-error" );
		updateTips( n );
		return false;
	} else {
		return true;
	}
}

function validator()
{
	if($("#tb tr").length <= 2)
	{
		updateTips( "您还没有添加新纪录，无需保存" );
		return false;
	}

	var success = true;
	$("#tb tr:gt(1)").each(function() 
	{
		success = validateARow($(this));
		if(success == false)
		{
			return false;
		}
	});
	return success;
}

//验证一行
function validateARow(row)
{
	var cname = row.find("#cname"),
		cpoint = row.find("#cpoint"),
		cscore = row.find("#cscore"),
		allFields = $( [] ).add( cname ).add( cpoint ).add( cscore );
	
	var v_cname = cname.val();
	var v_cscore = cscore.val();
	var v_cpoint = cpoint.val();

	var bValid = true;
	allFields.removeClass( "ui-state-error" );
	bValid = bValid && checkLength( cname, "课程名称", 2, 50 );
	bValid = bValid && checkLength( cpoint, "学分", 1, 5);
	bValid = bValid && checkLength( cscore, "总评分", 1, 8);
	/*bValid = bValid && checkRegexp( TContent, /^[\u4e00-\u9fa5\da-zA-Z\-\_]+$/, "\"成果名称\"不能包含特殊字符，请输入中文、英文单词或者数字" );*/
	bValid = bValid && checkRegexp( cpoint, /^\d+(.\d+$|\d*$)/, "\"学分\"应该输入合法数字" );
	bValid = bValid && checkRegexp( cscore, /^\d+(.\d+$|\d*$)/, "\"总评分\"应该输入合法数字" );
	bValid = bValid && checkValue( cpoint, "学分", 1, 10);
	bValid = bValid && checkValue( cscore, "总评分", 0, 100);
		
	if ( ! bValid ) {
		return false;
	}

	return true;
}

function adjustHeight()
{
	var height = document.body.clientHeight;
	if(height + 40 > 420)
		parent.parent.document.getElementById("frame_content").height=height + 70;
	else
		parent.parent.document.getElementById("frame_content").height=420;	
}
