// JavaScript Document
//从服务器加载学生自评成绩
loadSelfEva();

//设置自评列表分页
var totalRecord = 0;
var pages = 0;
var currentPage = 1;

var allFields;	
var selectionArray;

var index = 0;   //提供给表格用的序号

var _delete = [];//用来表示学生删除记录的id

$(document).ready(function() {
	//调整屏幕的高度
	parent.parent.document.getElementById("frame_content").height=420;							   
	//隐藏翻页按钮
	$("#btLast").hide(); $("#btNext").hide(); $("#btFormer").hide(); $("#btFirst").hide(); 
	//隐藏模板tr
	$("#tb tr").eq(1).hide();

	//添加记录按钮
	$("#BtAdd").click(function() {
		var tr = $("#tb tr").eq(1).clone();//复制一行
		tr.find("td").get(0).innerHTML = ++index;
		tr.show();
		tr.appendTo("#tb");
		
		totalRecord++;
		
		$("#tb tr:last :input:last").keypress(function(event){
			//tab enter
			if(event.keyCode==13 || event.keyCode==9){
				$("#BtAdd").click();
			}
		});
				
		var height = 
		document.documentElement.scrollHeight<document.body.scrollHeight?document.body.scrollHeight:document.documentElement.scrollHeight; 
		//alert(height);
		if(height > 420)
			parent.parent.document.getElementById("frame_content").height=height+34;
		else
			parent.parent.document.getElementById("frame_content").height=420;
		
		setRecordPage(totalRecord);	
		rePage(totalRecord, pages);
	});
	
	//删除所选记录按钮
	$("#BtDel").click(function() {
		var selected = false;
		$("#tb tr:gt(1)").each(function() {
			if ($(this).find("#CK").get(0).checked == true) {
				selected = true;
				return false;
			}
		 });
		if(!selected)
		{
			alert("请您选择要删除的得分记录~");
			return false;
		}
		
		var height = 
		document.documentElement.scrollHeight<document.body.scrollHeight?document.body.scrollHeight:document.documentElement.scrollHeight; 
		
		$("#tb tr:gt(1)").each(function() {
			if ($(this).find("#CK").get(0).checked == true) {
				if($(this).find("#TContent").attr("name") != "")
				{
					_delete.push($(this).find("#TContent").attr("name"));
				}
				$(this).remove();
				totalRecord--;
				height -= 34;
			}
		});
		
		if(height > 420)
			parent.parent.document.getElementById("frame_content").height=height;
		else
			parent.parent.document.getElementById("frame_content").height=420;	
			
		//父窗口iframe的宽度自适应	
		index = 0;
		$("#tb tr:gt(1)").each(function() {
			$(this).find("td").get(0).innerHTML = ++index;
		});
		$("#CKA").attr("checked", false);
		
		rePage(totalRecord, currentPage);
	});
	
	//选择所有行
	$("#CKA").click(function() {
		$("#tb tbody tr").each(function() {
			if(!$(this).is(":hidden")) 	
				$(this).find("#CK").get(0).checked = $("#CKA").get(0).checked;
		});
	});
	
	//保存记录
	$("#BtSave").click(function() {
		
		if(validator())
		{
			var SaveArray = new Array();
			var AddSubF1 = new Array();
			
			if(($("#tb tr").length == 3 && $("#tb tr:last").find("#TContent").val() == "" && $("#tb tr:last").find("#TScore").val() == ""))
			{
				//do nothing
			}
			else
			{
				$("#tb tr:gt(1)").each(function() {
					var SaveRow = new Array($(this).find("#selectType").find("option:selected").text(),
															$(this).find("#TContent").replaceSpecialHtmlChar(),
															$(this).find("#TScore").val(), $(this).find("#TContent").attr("name"));
					SaveArray.push(SaveRow);
				});
			}

			if($("#t_Add").val() == "" && $("#selfAdd").val() == "")
			{
				//do nothing
				//AddSubF1.push(Array(0,$("#t_Add").attr("name"),'','', ''));
				if($("#selfAdd").attr("name") != "") _delete.push($("#selfAdd").attr("name"));
			}
			else
			{
				//验证F1
				//数组第一个是0，表示是加分
				AddSubF1.push(Array(0,$("#t_Add").attr("name"),$("#t_Add").replaceSpecialHtmlChar(),$("#selfAdd").val(),$("#selfAdd").attr('name')));
			}
			
			if($("#t_Del").val() == "" && $("#selfDel").val() == "")
			{
				//do nothing 
				//AddSubF1.push(Array(1,$("#t_Del").attr("name"),'','',''));
				if($("#selfDel").attr("name") != "") _delete.push($("#selfDel").attr("name"));
			}
			else
			{
				//验证F1
				//数组第一个是1，表示是减分
				AddSubF1.push(Array(1,$("#t_Del").attr("name"),$("#t_Del").replaceSpecialHtmlChar(),$("#selfDel").val(),$("#selfDel").attr('name')));
			}
		
			$.cookie('savedSelfEva', SaveArray, { expires: 7, path: '/' });	
			$.cookie('cAddSubF1', AddSubF1, { expires: 7, path: '/' });	
			$.cookie('cDelete', _delete, { expires: 7, path: '/' });	
			location.href = "s_checkSelfEva.html";
		}
	});
	
	$("#btLast").click(function() 
	{
		if(currentPage != pages)
		{
			//alert("即将跳转到最后一页");
			currentPage = pages;
			ShowPage(currentPage*10-9, totalRecord);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btNext").click(function() 
	{
		if(currentPage+1<=pages)
		{
			//alert("即将跳转到下一页");
			currentPage += 1;
			ShowPage(currentPage*10-9, currentPage*10);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btFormer").click(function() 
	{
		if(currentPage-1>0)
		{
			//alert("即将跳转到上一页");
			currentPage -= 1;
			ShowPage(currentPage*10-9, currentPage*10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
	$("#btFirst").click(function() 
	{
		if(currentPage != 1)
		{
			//alert("即将跳转到首页");
			currentPage = 1;
			ShowPage(1, 10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
})

//重新分页
function rePage(totalRecord1, currentPage1)
{
	var tempPage = currentPage1;
	setRecordPage(totalRecord1);
	
	if (tempPage < pages)  
	{
		currentPage = tempPage;
		ShowPage(tempPage*10-9, tempPage*10);
	}
	else
	{
		currentPage = pages;
		ShowPage(pages*10-9, totalRecord);
	}
}

//传入需要被显示的数据的位置
//仅显示_min 和 _max之间的数据
function ShowPage(_min, _max)
{
	$("#tb tbody tr").each(function() {
		$(this).show();
	});
	
	var variable = "#tb tbody tr:lt("+_min+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	variable = "#tb tbody tr:gt("+_max+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	$("#showPage").html(currentPage+" 共"+pages+"页");
	
	adjustHeight();
}

//设置页数和总记录数
function setRecordPage(number)
{
	totalRecord = number;
	if(number <= 0)
	{
		totalRecord = 0;
		pages = 1;
	}
	else
	{//每页有10条记录
		pages = parseInt(number/10);
		if(number%10 != 0)
			pages += 1;
	}
	
	//页数>1 需翻页按钮
	if(pages > 1)
	{
		$("#btLast").show(); $("#btNext").show(); $("#btFormer").show(); $("#btFirst").show(); 
	}
	else
	{
		$("#btLast").hide(); $("#btNext").hide(); $("#btFormer").hide(); $("#btFirst").hide(); 
	}
}


//加载得分选项
function loadSelection(selectionArray)
{	
	var SavedArray = new Array();
	var selector=$("#selectType");  
	for(var i = 0; i < selectionArray.length; i++)
	{
		var row = new Array(selectionArray[i].evaluateid, selectionArray[i].evaluatename, selectionArray[i].maxscore);
		SavedArray.push(row);
		selector.append('<option value="'+selectionArray[i].evaluateid+'">'+selectionArray[i].evaluatename+'</option>');  
	}
	$.cookie('selectionArray', SavedArray, { expires: 7, path: '/' });	
}

//填充F3表格，如果为空，则初始化一行
function loadF3(score, isPassed)
{
	var tempRecords = 0;
	for(var i = 0; i < score.length; i++)
	{
		/*var choose = score[i].choose;
		for(var j = 0; j < score[i].contents.length; j++)
		{
			var tr = $("#tb tr").eq(1).clone();//复制一行
			tr.find("td").get(0).innerHTML = ++index;
			tr.show();
			tr.appendTo("#tb");

			//alert("#selectType option[text='"+choose+"']");
			$("#tb tr:last").find("#selectType option[value='"+choose+"']").attr("selected", "selected"); 
			$("#tb tr:last").find("#TContent").val(score[i].contents[j].content);
			$("#tb tr:last").find("#TScore").val(score[i].contents[j].score);
			
			tempRecords++;
		}*/

		var tr = $("#tb tr").eq(1).clone();//复制一行
		tr.find("td").get(0).innerHTML = ++index;
		tr.show();
		tr.appendTo("#tb");

		$("#tb tr:last").find("#selectType option[value='"+score[i].choose+"']").attr("selected", "selected"); 
		//2012-03-28 minzhenyu
		$("#tb tr:last").find("#TContent").setHtmlChar(score[i].content);
		//$("#tb tr:last").find("#TContent").val(score[i].content);
		$("#tb tr:last").find("#TContent").attr("name", score[i].selfid);
		$("#tb tr:last").find("#TScore").val(score[i].score);
		
		tempRecords++;
	}
	
	
	//判断是否审核通过
	judgeIsPassed(isPassed);
	
	setRecordPage(tempRecords);

	currentPage = 1;
	$("#tb tbody tr:gt(10)").each(function() {
		$(this).hide();
	});

	$("#showPage").html(currentPage+" 共"+pages+"页");
	adjustHeight();

}

//判断审核是否通过
function judgeIsPassed(isPassed)
{
	if(parseInt(isPassed) != 0)
	{
		$("#BtAdd").hide(); $("#BtDel").hide(); $("#BtSave").hide();
		$("#tb tr:gt(1)").each(function() {
			$(this).find("#selectType").attr("disabled", "disabled");
			$(this).find("#TContent").attr("disabled", "disabled");
			$(this).find("#TScore").attr("disabled", "disabled");
		});
		
		$( ".validateTips")
		.text( "您的创新与实践能力评分已经由测评小组或者辅导员审核通过，不能修改~" )
		.addClass( "ui-state-highlight" );
		setTimeout(function() {
			$( ".validateTips").removeClass( "ui-state-highlight", 1500 );
			$( ".validateTips" ).text("");
		}, 3000 );
	}
}

//加载F1表格
function loadF1(_add, _sub, isF1Passed)
{
	//$("#t_Add").val(_add.content); $("#selfAdd").val(_add.score);
	//$("#t_Del").val(_sub.content); $("#selfDel").val(_sub.score);
	//2012-03-28 minzhenyu
	$("#t_Add").setHtmlChar(_add.content); $("#selfAdd").val(_add.score);
	$("#t_Del").setHtmlChar(_sub.content); $("#selfDel").val(_sub.score);
	
	$("#t_Add").attr("name", _add.choose); $("#t_Del").attr("name",  _sub.choose);
	$("#selfAdd").attr("name", _add.selfid); $("#selfDel").attr("name",  _sub.selfid);
	if(isF1Passed == 1)  //如果审核通过
	{
		$("#t_Add").attr("disabled", "disabled"); $("#selfAdd").attr("disabled", "disabled");
		$("#t_Del").attr("disabled", "disabled"); $("#selfDel").attr("disabled", "disabled");
	}
}

function loadSelfEva()
{
	parent.parent.document.getElementById("frame_content").height=420;	
	
	var date = new Date();
	var vYear = date.getFullYear();
	
	var reviseSelfScore = {};
	reviseSelfScore["sid"] = "";
	reviseSelfScore["year"] = vYear;

	$.get(
		"../../../testdrive/index.php?r=zing/displaySelfScore",
		{"reviseSelfScore": $.toJSON(reviseSelfScore)},
		function(data)
		{
			var ret = $.evalJSON(data);
			
			if(ret.success = true)
			{
				if(ret.results.length != 0)
				{
					//加载得分列表
					loadSelection(ret.results.chooses);
					
					//加载F3
					if(ret.results.score.length != 0)
					{
						loadF3(ret.results.score, ret.results.isPassed);
					}
					else
					{
						//如果为空则初始化一条空记录
						$("#BtAdd").click();
						judgeIsPassed(ret.results.isPassed);
					}
					
					loadF1(ret.results.add, ret.results['sub'], ret.results.isF1Passed);

					//alert($("#users-contain").offset().top);
					$('html,body', window.parent.parent.document).animate({
						scrollTop: $("#users-contain").offset().top+135}, 
						600
					);
				}
				else
				{
					alert("服务器端返回数据出错~");
				}
			}
			else
			{
				if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
				{
					alert(ret.message+'  页面即将跳转到首页～');
					window.parent.parent.location.href='../../../';
					return false;
				}

				alert('服务器端返回错误信息=>"'+ret.message+'"');
			}
		}
	);
}

//检查已添加条目的正确性

function updateTips( t ) {
	$( ".validateTips")
		.text( t )
		.addClass( "ui-state-highlight" );
	setTimeout(function() {
		$( ".validateTips").removeClass( "ui-state-highlight", 1500 );
		$( ".validateTips" ).text("");
	}, 1500 );
}

function checkLength( o, n, min, max ) {
	if ( $.trim(o.val()).length > max || $.trim(o.val()).length < min ) {
		o.addClass( "ui-state-error" );
		updateTips( '提示: " '+n+' "' + " 的长度应该在 " +
			min + " 和 " + max + "之间." );
		return false;
	} else {
		return true;
	}
}

function checkValue( o, n, min, max ) {
	if ( parseFloat(o.val()) > max || parseFloat(o.val()) < min) {
		o.addClass( "ui-state-error" );
		updateTips( '" '+n+' "' + " 的值 " +
			min + " 和 " + max + "之间." );
		return false;
	} else {
		return true;
	}
}

function checkRegexp( o, regexp, n ) {
	if ( !( regexp.test( o.val() ) ) ) {
		o.addClass( "ui-state-error" );
		updateTips( n );
		return false;
	} else {
		return true;
	}
}

function validator()
{
	var check = 0;
	var success = true;
	
	if($("#tb tr").length <= 2)
	{
		check += 1;
	}
	
	if($("#tb tr").length == 3 && $("#tb tr:last").find("#TContent").val() == "" && $("#tb tr:last").find("#TScore").val() == "")
	{
		check += 2;
	}
	
	if(check == 1 || check == 2)
	{
		//do nothing
	}
	else
	{
		//验证F3
		$("#tb tr:gt(1)").each(function() 
		{
			var selectType = $(this).find("#selectType"),
			TContent = $(this).find("#TContent"),
			TScore = $(this).find("#TScore"),
			allFields = $( [] ).add( selectType ).add( TContent ).add( TScore );
		
			var bValid = true;
			allFields.removeClass( "ui-state-error" );
			bValid = bValid && checkLength( TContent, "成果", 2, 100 );
			bValid = bValid && checkLength( TScore, "得分", 1, 8);
			bValid = bValid && checkRegexp( TContent, /^[^,]*$/, "\"成果名称\"请不要出现英文字符逗号','" );
			/*bValid = bValid && checkRegexp( TContent, /^[\u4e00-\u9fa5\da-zA-Z\-\_]+$/, "\"成果名称\"不能包含特殊字符，请输入中文、英文单词或者数字" );*/
			bValid = bValid && checkRegexp( TScore, /^\d+(.\d+$|\d*$)/, "\"得分\"应该输入合法数字" );
			bValid = bValid && checkValue( TScore, "得分", 0, 20);
			
			if ( ! bValid ) {
				success = false;
				return false;
			}
		});
	}
	

	if($("#t_Add").val() == "" && $("#selfAdd").val() == "")
	{
		//do nothing
	}
	else
	{
		//验证F1
		var t_Add = $("#t_Add"),
		selfAdd = $("#selfAdd"),
		allFields = $( [] ).add( t_Add ).add( selfAdd );
	
		var bValid = true;
		allFields.removeClass( "ui-state-error" );
		bValid = bValid && checkLength( t_Add, "奖励加分", 2, 100);
		bValid = bValid && checkRegexp( t_Add, /^[^,]*$/, "\"奖励加分\"请不要出现英文字符逗号','" );
		bValid = bValid && checkRegexp( selfAdd, /^\d+(.\d+$|\d*$)/, "\"自评分\"应该输入合法数字" );
		bValid = bValid && checkValue( selfAdd, "自评分", 0, 20);
		
		if ( ! bValid ) {
			success = false;
			return false;
		}
	}
	
	if($("#t_Del").val() == "" && $("#selfDel").val() == "")
	{
		//do nothing 
	}
	else
	{
		//验证F1
		var t_Del = $("#t_Del"),
		selfDel = $("#selfDel"),
		allFields = $( [] ).add( t_Del ).add( selfDel );
	
		var bValid = true;
		allFields.removeClass( "ui-state-error" );
		bValid = bValid && checkLength( t_Del, "惩处扣分", 2, 100);
		bValid = bValid && checkRegexp( t_Del, /^[^,]*$/, "\"惩处扣分\"请不要出现英文字符逗号','" );
		bValid = bValid && checkRegexp( selfDel, /^\d+(.\d+$|\d*$)/, "\"自评扣分\"应该输入合法数字" );
		bValid = bValid && checkValue( selfDel, "自评扣分", 0, 20);
		
		if ( ! bValid ) {
			success = false;
			return false;
		}
	}
	
	return success;
}

function adjustHeight()
{
	var height = document.body.clientHeight;
	if(height + 60 > 420)
		parent.parent.document.getElementById("frame_content").height=height + 60;
	else
		parent.parent.document.getElementById("frame_content").height=420;	
}
