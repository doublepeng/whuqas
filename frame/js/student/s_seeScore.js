// JavaScript Document
$body = (window.opera) ? (document.compatMode == "CSS1Compat" ? $('html') : $('body')) : $('html,body');

var currentLink;  //记录点击了哪个toolbar，用来隐藏表格
$(document).ready(function() {
	//调整主窗口高度
	parent.parent.document.getElementById("frame_content").height=420;	
	
	//获取查询年份
	setSearchYear();
	
	//初始化时，需要加载一个默认的表格
	drawtable($("#selectYear").find("option:selected").text());
	
	//当下拉框中得年份改变时
	changeYear();
})

//点击下拉图表时，则显示隐藏的表格，学生自评查看时用到
function showDetail()
{
	$('html,body', window.parent.parent.document).animate({
		scrollTop: currentLink.parent().parent().parent().parent().offset().top+135}, 
		400
	);

	if(currentLink.attr("class") == "visited")
	{
		currentLink.removeClass("visited");
		currentLink.addClass("initA");
		
		var parent = currentLink.parent().parent().parent().parent();
		parent.find("tbody:first").hide();

		adjustHeight();
	}
	else if(currentLink.attr("class") == "initA")
	{
		/*$("#users-contain > table > tbody").hide();
		$("#users-contain > table > thead .ui-widget-header th a").removeClass("visited");
		$("#users-contain > table > thead .ui-widget-header th a").addClass("initA");*/
		
		currentLink.removeClass("initA");
		currentLink.addClass("visited");
		
		var parent = currentLink.parent().parent().parent().parent();
		parent.find("tbody:first").show();

		adjustHeight();
	}
}

function adjustHeight()
{
	var height = document.documentElement.scrollHeight>document.body.scrollHeight?document.body.scrollHeight:document.documentElement.scrollHeight; 
	if(height > 420)
		parent.parent.document.getElementById("frame_content").height=height + 35;
	else
		parent.parent.document.getElementById("frame_content").height=420;	
}

//设置下拉框中得年份
function setSearchYear()
{
	var date = new Date();
	var vYear = parseInt(date.getFullYear());
	
	//begin to add to select
	for(var year = vYear-4; year < vYear+4; year++)
	{
		if(year == vYear)
			$("#selectYear").append('<option selected="selected" value="'+year+'">'+year+'</option>');
		else
			$("#selectYear").append('<option value="'+year+'">'+year+'</option>');
	}
}

//下拉框改变时，需要重新查询构造成绩表格
function changeYear()
{
	$("#selectYear").change(function(){
		var selected = $("#selectYear").find("option:selected").text();
		drawtable(selected);
	});
}

function drawtable(selected)
{
	var searchFinal = {};
	searchFinal["sid"] = "";
	searchFinal["year"] = selected;
	searchFinal = $.toJSON(searchFinal);
	//alert(searchGPA);
	
	$.get(
		"../../../testdrive/index.php?r=zing/getFinal",
		{"searchFinal": searchFinal},
		function (data)
		{
			var ret = $.evalJSON(data);
			
			if(ret.success == true)
			{
				if(ret.results.length != 0)
				{
					var finalScore = ret.results.score;
					var gpascore = ret.results.gpascore;
					var basicscore = ret.results.basicscore;
					var selfscore = ret.results.selfscore;
					
					$("#tb-basic tr").remove();
					if(basicscore.length != 0)
					{
						var scripts = "";
						scripts += '<tr><th style="width:90px">思想政治表现</th><th style="width:90px">个人品德修养</th><th style="width:90px">学习态度状况</th><th style="width:90px">组织纪律观念</th><th style="width:90px">身心健康素质</th><th style="width:50px">总分</th><th style="width:50px">审核通过</th></tr>';
						$("#tb-basic thead").append(scripts);
						
						var contents = basicscore.contents;
						for(var i = 0; i < contents.length; i++)
						{
							var sum = 0;
							scripts = "";
							scripts += '<tr><td>'+parseFloat(contents[i].t_A1)+'</td><td>'+
								parseFloat(contents[i].t_A2)+'</td><td>'+parseFloat(contents[i].t_A3)+'</td><td>'+
								parseFloat(contents[i].t_A4)+'</td><td>'+parseFloat(contents[i].t_A5)+'</td><td style="color:red">'+
								(parseFloat(contents[i].t_A1)+parseFloat(contents[i].t_A2)+
								parseFloat(contents[i].t_A3)+parseFloat(contents[i].t_A4)+parseFloat(contents[i].t_A5));
		
							scripts += '</td><td style="color:red">是</td></tr>';
							$("#tb-basic tbody").append(scripts);
						}
						$("#tb-basic tbody").append('<tr><th colspan="2">评项</th><th colspan="3">测评内容</th><th>得分</th><th>审核通过</th></tr>');
						var add = basicscore.add, sub = basicscore.sub;  //得到F1加减分
						if(add.length != 0)
						{
							$("#tb-basic tbody").append('<tr><td colspan="2">奖励加分</td><td colspan="3">'+add.content+'</td><td style="color:red">'+parseFloat(add.score)+'</td><td style="color:red">是</td></tr>');
						}
						else
						{
							$("#tb-basic tbody").append('<tr><td colspan="2">奖励加分</td><td colspan="3">无</td><td style="color:red">0</td><td style="color:red">是</td></tr>');
						}
						if(sub.length != 0)
						{
							$("#tb-basic tbody").append('<tr><td colspan="2">惩处扣分</td><td colspan="3">'+sub.content+'</td><td style="color:red">'+parseFloat(sub.score)+'</td><td style="color:red">是</td></tr>');
						}
						else
						{
							$("#tb-basic tbody").append('<tr><td colspan="2">惩处扣分</td><td colspan="3">无</td><td style="color:red">0</td><td style="color:red">是</td></tr>');
						}
						$("#tb-basic tbody").append('<tr><td colspan="7"><h1 style="font-size:12px; color:red;margin:0">F1得分：'+parseFloat(basicscore.score)+'分 (基本素质测评成绩的平均分-惩处扣分+奖励加分)</h1></td></tr>');
					}
					else
					{
						$("#tb-basic tbody").append('<tr><td><h1 style="font-size:12px; color:red;margin:0">F1得分：0分</h1></td></tr>');
					}
					
		
					$("#tb-gpa tr").remove();
					if(gpascore.length != 0)
					{
						scripts = "";
						scripts += '<tr><th style="width:170px">课程名</th><th style="width:60px">课程类型</th><th style="width:60px">学分</th><th style="width:60px">得分</th></tr>';
						$("#tb-gpa thead").append(scripts);	
						
						var contents = gpascore.contents;
						for(var i = 0; i < contents.length; i++)
						{
							scripts = "";
							scripts += "<tr><td>"+contents[i].cname+"</td><td>"+
								contents[i].ctype+"</td><td>"+contents[i].cpoint+"</td><td>"+
								contents[i].cscore+"</td></tr>";
							$("#tb-gpa tbody").append(scripts);
						}

						$("#tb-gpa tbody").append('<tr><td colspan="4"><h1 style="font-size:12px; color:red;margin:0">F2得分：'+gpascore.score+'分</h1></td></tr>');
					}
					else
					{
						$("#tb-gpa tbody").append('<tr><td><h1 style="font-size:12px; color:red;margin:0">F2得分：0分</h1></td></tr>');
					}

		
					$("#tb-self tr").remove();
					if(selfscore.length != 0)
					{
						scripts = "";
						scripts += '<tr><th style="width:170px">选项</th><th style="width:315px">成果</th><th style="width:60px">得分</th><th style="width:60px">单项汇总</th></tr>';
						$("#tb-self thead").append(scripts);
						
						var contents = selfscore.contents;
						scripts = "";
						var TOTAL = 0;
						for(var i = 0; i < contents.length; i++)
						{
							var totalSum = 0;
							$("#tb-self tbody").append('<tr><td rowspan="'+
												  contents[i].contents.length+'">'+
												  contents[i].choose+'<br />(最大分值:'+parseFloat(contents[i].score)+')</td><td>'+
												  contents[i].contents[0].content+'</td><td>'+
												 parseFloat( contents[i].contents[0].te_evascore)+
												 '</td><td style="color:red" id= "sum" rowspan="'+ 
												 contents[i].contents.length+'"></td></tr>');
							totalSum += parseFloat(contents[i].contents[0].te_evascore);
							for(var j = 1; j < contents[i].contents.length; j++)
							{
								$("#tb-self tbody").append('<tr><td>'+
												  contents[i].contents[j].content+'</td><td>'+
												  parseFloat(contents[i].contents[j].te_evascore)+'</td></tr>');
								totalSum += parseFloat(contents[i].contents[j].te_evascore);
							}
							if(totalSum > parseFloat(contents[i].score))
							{
								totalSum = parseFloat(contents[i].score);
							}
							$("#tb-self #sum:last").html(totalSum);
							TOTAL += totalSum;
						}

						$("#tb-self tbody").append('<tr><td colspan="4"><h1 style="font-size:12px; color:red;margin:0">F3得分：'+TOTAL+'分</h1></td></tr>');
					}
					else
					{
						$("#tb-self tbody").append('<tr><td><h1 style="font-size:12px; color:red;margin:0">F3得分：0分</h1></td></tr>');
					}
					
					$("#tb-total span").html(finalScore);
					
					$("#tb-gpa1 > tbody").hide();
					$("#tb-self1 > tbody").hide();
					$("#tb thead .ui-widget-header:first th a").removeClass("initA");
					$("#tb thead .ui-widget-header:first th a").addClass("visited");
					adjustHeight();
				}
				else
				{
					$("#tb-basic tr").remove();
					$("#tb-gpa tr").remove();
					$("#tb-self tr").remove();
					$("#tb-gpa tbody").append('<tr><td><h1 style="font-size:12px; color:red;margin:0">F1得分：0分</h1></td></tr>');
					$("#tb-basic tbody").append('<tr><td><h1 style="font-size:12px; color:red;margin:0">F2得分：0分</h1></td></tr>');
					$("#tb-self tbody").append('<tr><td><h1 style="font-size:12px; color:red;margin:0">F3得分：0分</h1></td></tr>');
					$("#tb-total span").html("0");
				}
			}
			else
			{
				if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
						{
							alert(ret.message+'  页面即将跳转到首页～');
							window.parent.parent.location.href='../../../';
							return false;
						}
				alert("服务器端返回错误信息=>\""+ret.message+'"');
			}
		}
	);
	
}
