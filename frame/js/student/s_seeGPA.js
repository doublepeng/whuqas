// JavaScript Document
//设置自评列表分页
var totalRecord = 0;
var pages = 0;
var currentPage = 1;

$(document).ready(function() {
	//调整屏幕的高度
	parent.parent.document.getElementById("frame_content").height=420;	
	//隐藏翻页按钮
	$("#btLast").hide(); $("#btNext").hide(); $("#btFormer").hide(); $("#btFirst").hide(); 
	//隐藏模板tr
	$("#tb tr").eq(1).hide();

	//获取查询年份
	setSearchYear();
	
	//初始化时，需要加载一个默认的表格
	drawGPAtable($("#selectYear").find("option:selected").text());
	
	//当下拉框中得年份改变时
	changeYear();

	//用来分页的控制
	page();
})

//用来分页的控制
function page()
{
	$("#btLast").click(function() 
	{
		if(currentPage != pages)
		{
			//alert("即将跳转到最后一页");
			currentPage = pages;
			ShowPage(currentPage*10-9, totalRecord);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btNext").click(function() 
	{
		if(currentPage+1<=pages)
		{
			//alert("即将跳转到下一页");
			currentPage += 1;
			ShowPage(currentPage*10-9, currentPage*10);
			adjustHeight();
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btFormer").click(function() 
	{
		if(currentPage-1>0)
		{
			//alert("即将跳转到上一页");
			currentPage -= 1;
			ShowPage(currentPage*10-9, currentPage*10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
	$("#btFirst").click(function() 
	{
		if(currentPage != 1)
		{
			//alert("即将跳转到首页");
			currentPage = 1;
			ShowPage(1, 10);
			adjustHeight();
		}
		else
		{
			alert("已是首页!");
		}
	});
}

//传入需要被显示的数据的位置
//仅显示_min 和 _max之间的数据
function ShowPage(_min, _max)
{
	$("#tb tbody tr").each(function() {
		$(this).show();
	});
	
	var variable = "#tb tbody tr:lt("+_min+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	variable = "#tb tbody tr:gt("+_max+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	$("#showPage").html(currentPage+" 共"+pages+"页");
	
	adjustHeight();
}

//设置页数和总记录数
function setRecordPage(number)
{
	totalRecord = number;
	if(number <= 0)
	{
		totalRecord = 0;
		pages = 1;
	}
	else
	{//每页有10条记录
		pages = parseInt(number/10);
		if(number%10 != 0)
			pages += 1;
	}
	
	//页数>1 需翻页按钮
	if(pages > 1)
	{
		$("#btLast").show(); $("#btNext").show(); $("#btFormer").show(); $("#btFirst").show(); 
	}
	else
	{
		$("#btLast").hide(); $("#btNext").hide(); $("#btFormer").hide(); $("#btFirst").hide(); 
	}
}

function adjustHeight()
{
	var height = document.documentElement.scrollHeight>document.body.scrollHeight
						? document.body.scrollHeight:document.documentElement.scrollHeight; 
	if(height+40 > 420)
		parent.parent.document.getElementById("frame_content").height=height + 40;
	else
		parent.parent.document.getElementById("frame_content").height=420;	
}

//设置下拉框中得年份
function setSearchYear()
{
	var date = new Date();
	var vYear = parseInt(date.getFullYear());
	
	//begin to add to select
	for(var year = vYear-4; year < vYear+4; year++)
	{
		if(year == vYear)
			$("#selectYear").append('<option selected="selected" value="'+year+'">'+year+'</option>');
		else
			$("#selectYear").append('<option value="'+year+'">'+year+'</option>');
	}
}

//下拉框改变时，需要重新查询构造成绩表格
function changeYear()
{
	$("#selectYear").change(function(){
		var selected = $("#selectYear").find("option:selected").text();
		drawGPAtable(selected);
	});
}

function drawGPAtable(selected)
{
	var searchGPA = {};
	searchGPA["sid"] = "";
	searchGPA["year"] = selected;
	searchGPA = $.toJSON(searchGPA);
	//alert(searchGPA);
	
	$.get(
		"../../../testdrive/index.php?r=zing/getCoScores",
		{"searchGPA": searchGPA},
		function (data)
		{
			var ret = $.evalJSON(data);
			
			if(ret.success == true)
			{
				if(ret.results.length != 0)
				{
					$("#tb tr:gt(1)").remove();
					var tempRecords = 0;
					for(var i = 0; i < ret.results.length; i++)
					{
						var tr = $("#tb tr").eq(1).clone();//复制一行
						tr.show();
						tr.appendTo("#tb");
						tr.find("td").get(0).innerHTML = ret.results[i].cname;
						tr.find("td").get(1).innerHTML = ret.results[i].ctype;
						tr.find("td").get(2).innerHTML = ret.results[i].cpoint;
						tr.find("td").get(3).innerHTML = ret.results[i].cscore;
						tempRecords++;
					}
					
					setRecordPage(tempRecords);

					currentPage = 1;
					$("#tb tbody tr:gt(10)").each(function() {
						$(this).hide();
					});

					$("#showPage").html(currentPage+" 共"+pages+"页");
					adjustHeight();

					$('html,body', window.parent.parent.document).animate({
						scrollTop: $("#tb").offset().top+135}, 
						400
					);
				}
				else
				{
					$("#tb tr:gt(1)").remove();
					currentPage = 1;
					$("#showPage").html(currentPage+" 共1页");

					alert("您没有该学年的课程成绩，或者服务器端返回数据出错~");
					
				}
			}
			else
			{
				if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
						{
							alert(ret.message+'  页面即将跳转到首页～');
							window.parent.parent.location.href='../../../';
							return false;
						}
				alert("服务器端返回错误信息=>\""+ret.message+'"');
			}
		}
	);
}
