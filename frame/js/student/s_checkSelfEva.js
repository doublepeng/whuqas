var postData = {};
var selectionArray;   //用来记录学院中评定选项的名称和最大得分值

$(function(){
	//加载得分选项
	loadSelection();
	
	//根据cookie中存储的数据添加到表格中
	addItems();
	
	//当下拉框改变时，要设置frame窗口的自动适应长度
	
	//确认提交按钮
	$("#btConfirm").click(function()
	{
		//alert($.toJSON(postData));
		if(confirm('您将提交自评成绩，审核通过前您可以通过‘创新与实践能力评分(F3)’菜单进行修改，确定继续？'))
		{
			//$("#right_navigate", parent.document).mask("正在提交");
			//$("#layer").mask('正在提交');
			$("#layer").height(parent.parent.document.getElementById("frame_content").height);
			$("#layer").mask("正在提交，请稍等...");
			$.post(
				'../../../testdrive/index.php?r=zing/saveSelfscore',
				{'studentSelfEva':$.toJSON(postData)},
				function(data)
				{
					var obj = $.evalJSON(data);
					if(obj.success == true)
					{
						alert("提交成功!");
						//$.cookie('the_cookie', null);
						$("#btConfirm span").html("已提交");
						$("#btConfirm").attr({"disabled":"disabled"});
					}
					else
					{
						if(obj.results.hasOwnProperty("timeout") && obj.results.timeout)
						{
							alert(obj.message+'  页面即将跳转到首页～');
							window.parent.parent.location.href='../../../';
							return false;
						}

						alert(obj.message);
						//return false;
					}
					
					$("#layer").unmask();
				}
			);
		}
	});
	
});

//加载得分选项
function loadSelection()
{
	var recvs = $.cookie('selectionArray');
	$.cookie('selectionArray', null);
	
	recvs = recvs.split(',');
	
	selectionArray = {};
	for(var i = 0; i < recvs.length; )
	{
		var j = 0;
		
		//selectionArray.push({'evaluateid': recvs[i], 'evaluatename':recvs[i+1],'maxscore':recvs[i+2]});
		selectionArray[recvs[i+1]] = Array(recvs[i],recvs[i+2]);
		i = i+3;
	}
}

function reCheckScore(key, score)
{
	if(selectionArray[key] && score > selectionArray[key][1])
	{
		$("#users-contain").append('<p style="color:red; font-size:12px">由于您 "'+key+'" 选项中得分超过限制分数'+selectionArray[key][1]+'分，系统已帮你置为'+selectionArray[key][1]+'分~</p>');
		score = parseFloat(selectionArray[key][1]);
	}
	return score;
}

function addItems()
{	
	var recv = $.cookie('savedSelfEva');
	var AddSubF1 = $.cookie("cAddSubF1");
	var _delete = $.cookie('cDelete');
	$.cookie('savedSelfEva', null, { path: '/' });
	$.cookie('cAddSubF1', null, { path: '/' });
	$.cookie('cDelete', null, { path: '/' });
	
	$("#t_add").find("td").get(1).innerHTML = '<h1 style="color:red; font-size:12px">无</h1>';
	$("#t_add").find("td").get(2).innerHTML = '<h1 style="color:red; font-size:12px">无</h1>';
	$("#t_del").find("td").get(1).innerHTML = '<h1 style="color:red; font-size:12px">无</h1>';
	$("#t_del").find("td").get(2).innerHTML = '<h1 style="color:red; font-size:12px">无</h1>';

	var recv_F1 = new Array();
	//2012-02-20 modified by minzhenyu
	//if(AddSubF1 != "")
	if(AddSubF1 != "" && AddSubF1 != null)
	{
		var AddSubF1s =  AddSubF1.split(',');
		for(var i = 0; i < AddSubF1s.length; )
		{
			if(AddSubF1s[i] == 0)   //add
			{
				if(AddSubF1s[i+2] != '')
				{
				$("#t_add").find("td h1").get(0).innerHTML = AddSubF1s[i+2];
				$("#t_add").find("td h1").get(1).innerHTML = AddSubF1s[i+3];
				
				}
				recv_F1.push(Array(AddSubF1s[i+1], AddSubF1s[i+2], AddSubF1s[i+3], AddSubF1s[i+4]));
			}
			else if(AddSubF1s[i] == 1) //sub
			{
				if(AddSubF1s[i+2] != '')
				{
				$("#t_del").find("td h1").get(0).innerHTML = AddSubF1s[i+2];
				$("#t_del").find("td h1").get(1).innerHTML = AddSubF1s[i+3];
				
				}
				recv_F1.push(Array(AddSubF1s[i+1], AddSubF1s[i+2], AddSubF1s[i+3], AddSubF1s[i+4]));
			}
			i = i+5;
		}
	}
	
	var tbData = [];
	var totalScore = 0;
	if(recv != "" && recv != null)
	{
		var recvs = new Array();
		recvs = recv.split(',');
		for(var i = 0; i < recvs.length; )
		{
			var j = 0;
			for (j = 0; j < tbData.length; j++)
			{
				if (tbData[j].key == recvs[i])
				{
					tbData[j].value.push(Array(recvs[i+1], recvs[i+2], recvs[i+3]));
					break;
				}
			}
			if(j >= tbData.length)
			{
				tbData.push({key: recvs[i], value: Array(Array(recvs[i+1], recvs[i+2], recvs[i+3]))});
			}
			i = i+4;
		}
		
		var item = '';
		
		for (var i = 0; i < tbData.length; i++)
		{
			item = "";
			//添加选项
			item += ("<tr><td>"+tbData[i].key+"</td>");
			
			//添加成果表
			item += ('<td style="padding:0"><table style="margin:0">');
			for(var j = 0; j < tbData[i].value.length; j++)
			{
				item += ('<tr><td>'+tbData[i].value[j][0]+'</td></tr>');
			}
			item += ('</table></td>');
			
			//添加得分表
			item += ('<td style="padding:0"><table style="margin:0">');
			var score = 0;
			for(var j = 0; j < tbData[i].value.length; j++)
			{
				score += parseFloat(tbData[i].value[j][1]);
				item += ('<tr><td>'+tbData[i].value[j][1]+'</td></tr>');
			}
			
			score = reCheckScore(tbData[i].key, score);
			
			totalScore += score;
			item += ('</table></td>');
		
			//添加汇总表
			item += ('<td>'+score+'</td></tr>');
			
			//alert(item);
			$( "#users tbody:first" ).append(item);
		}
	}
	
	item = '<tr><td><h1 style="color:red; font-size:12px">总得分</h1></td><td></td><td></td>'
	item += '<td><h1 style="color:red; font-size:12px">'+totalScore+'</h1></td></tr>';
	$( "#users tbody:first" ).append(item);
	
	postData["update"] = [];
	postData["delete"] = [];
	postData["insert"] = [];
	
	//delete
	if(_delete != "" && _delete != null)
	{
		var _deletes = _delete.split(',');
		for(var i = 0; i < _deletes.length; i++)
		{
			postData["delete"].push(_deletes[i]);
		}
	}
	
	var dict = {};
	for (var i = 0; i < tbData.length; i++)
	{

		for(var j = 0; j < tbData[i].value.length; j++)
		{
			dict = {};
			dict.choose = selectionArray[tbData[i].key][0];
			dict.content = tbData[i].value[j][0];
			dict.score = tbData[i].value[j][1];
			if(tbData[i].value[j][2] != "")
			{
				dict.selfid = tbData[i].value[j][2];
				postData["update"].push(dict);
			}
			else
			{
				postData["insert"].push(dict);
			}
		}
	}

//将F1成绩也加到返回的数组里
	for(var i = 0; i < recv_F1.length; i++)
	{
		dict = {};
		dict.choose = recv_F1[i][0];
		dict.content = recv_F1[i][1];
		dict.score = recv_F1[i][2];
		if(recv_F1[i][3] != "")
		{
			dict.selfid = recv_F1[i][3];
			postData["update"].push(dict);
		}
		else
		{
			postData["insert"].push(dict);
		}
	}
	
	var height = 
	document.documentElement.scrollHeight>document.body.scrollHeight?document.body.scrollHeight:document.documentElement.scrollHeight; 
	//alert(document.documentElement.scrollHeight);
	//alert(document.body.scrollHeight);
	if(height > 420)
		parent.parent.document.getElementById("frame_content").height=height+50;
	else
		parent.parent.document.getElementById("frame_content").height=420;	
}
