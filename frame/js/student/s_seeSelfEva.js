// JavaScript Document
$(document).ready(function() {
	//调整屏幕的高度
	parent.parent.document.getElementById("frame_content").height=420;	
	
	//获取查询年份
	setSearchYear();
	
	//初始化时，需要加载一个默认的表格
	drawGPAtable($("#selectYear").find("option:selected").text());
	
	//当下拉框中得年份改变时
	changeYear();
})

function adjustHeight()
{
	var height = document.documentElement.scrollHeight>document.body.scrollHeight
						? document.body.scrollHeight:document.documentElement.scrollHeight; 
	if(height > 420)
		parent.parent.document.getElementById("frame_content").height=height + 35;
	else
		parent.parent.document.getElementById("frame_content").height=420;	
}

//设置下拉框中得年份
function setSearchYear()
{
	var date = new Date();
	var vYear = parseInt(date.getFullYear());
	
	//begin to add to select
	for(var year = vYear-4; year < vYear+4; year++)
	{
		if(year == vYear)
			$("#selectYear").append('<option selected="selected" value="'+year+'">'+year+'</option>');
		else
			$("#selectYear").append('<option value="'+year+'">'+year+'</option>');
	}
}

//下拉框改变时，需要重新查询构造成绩表格
function changeYear()
{
	$("#selectYear").change(function(){
		var selected = $("#selectYear").find("option:selected").text();
		drawGPAtable(selected);
	});
}

function tagSum(check, contents)
{
	var total = parseFloat(contents.score);
	var items = contents.contents;
	var sum = 0;
	if(parseInt(check) == 0)   //没有审核通过
	{
		$.each(items, function(key, value){
			sum += parseFloat(value["score"]);
		});
	}
	else if(parseInt(check) == 1)   //测评小组审核通过
	{
		$.each(items, function(key, value){
			sum += parseFloat(value["t_evascore"]);
		});
	}
	else if(parseInt(check) == 2)   //辅导员审核通过
	{
		$.each(items, function(key, value){
			sum += parseFloat(value["te_evascore"]);
		});
	}
	
	if(sum > total)
	{
		sum = total;
	}
	
	$("#tb tbody tr:last").find("#sum").get(0).innerHTML = sum;
	return sum;
}

//根据审核状态标记前端表格
function tagTable(tr, check)
{
	if(parseInt(check) == 0)   //没有审核通过
	{
		tr.find("#t").get(0).innerHTML = "未审核";
		tr.find("#te").get(0).innerHTML = "未审核";
	}
	else if(parseInt(check) == 1)   //测评小组审核通过
	{
		tr.find("#te").get(0).innerHTML = "未审核";
	}
	else if(parseInt(check) == 2)   //辅导员审核通过
	{
	}
}
function drawGPAtable(selected)
{
	var searchSelf = {};
	searchSelf["sid"] = "";
	searchSelf["year"] = selected;
	searchSelf = $.toJSON(searchSelf);
	//alert(searchGPA);
	
	$.get(
		"../../../testdrive/index.php?r=zing/getSelfScore",
		{"searchSelf": searchSelf},
		function (data)
		{
			var ret = $.evalJSON(data);
			
			if(ret.success == true)
			{
				$("#tb tbody tr").remove();
				
				if(ret.results.length!=0 && ret.results.contents.length != 0)
				{
					var items = ret.results.contents;
					/*{‘choose’:’科研论文’, ‘score’:13, ‘contents’:[{‘content’:’发表了论文’, ‘score’:10, “t_evascore”:10, “te_evascore”:11},{‘content’:’发表了论文’, ‘score’:10, “t_evascore”:10, “te_evascore”:11}]},*/
					var _item;  //用来遍历每个得分选项
					var totalSum = 0;  //用来记录总得分
					for(var i = 0; i < items.length; i++)
					{
						$("#tb tbody").append('<tr><td rowspan="'+
											  items[i].contents.length+'">'+
											  items[i].choose+'<br />(最大分值:'+parseFloat(items[i].score)+')</td><td>'+
											  items[i].contents[0].content+'</td><td>'+
											  parseFloat(items[i].contents[0].score)+'</td><td id="t">'+
											  parseFloat(items[i].contents[0].t_evascore)+ '</td><td id="te">'+
											  parseFloat(items[i].contents[0].te_evascore)+'</td><td id= "sum" rowspan="'+ 
											  items[i].contents.length+'"></td></tr>');
						tagTable($("#tb tbody tr:last"), ret.results.check);   //用来记录审核状态
						totalSum += tagSum(ret.results.check, items[i]);  //用来写入单项汇总
						for(var j = 1; j < items[i].contents.length; j++)
						{
							$("#tb tbody").append('<tr><td>'+
											  items[i].contents[j].content+'</td><td>'+
											  parseFloat(items[i].contents[j].score)+'</td><td id="t">'+
											  parseFloat(items[i].contents[j].t_evascore)+ '</td><td id="te">'+
											  parseFloat(items[i].contents[j].te_evascore)+'</td></tr>');
							tagTable($("#tb tbody tr:last"), ret.results.check);
						}
					}
					
					$("#tb tbody").append('<tr><td colspan="6"><h1 style="font-size:12px; color:red">F3总得分: '+totalSum+'分</h1></td></tr>');
				
					adjustHeight();

					$('html,body', window.parent.parent.document).animate({
						scrollTop: $("#tb").offset().top+135}, 
						400
					);
				}
				else
				{
					$("#tb tbody").append('<tr><td colspan="6"><h1 style="font-size:12px; color:red">F3总得分: 0分</h1></td></tr>');
				}
			}
			else
			{
				if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
						{
							alert(ret.message+'  页面即将跳转到首页～');
							window.parent.parent.location.href='../../../';
							return false;
						}
				alert("服务器端返回错误信息=>\""+ret.message+'"');
			}
		}
	);
}
