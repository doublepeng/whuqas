// JavaScript Document
loadDepartment();

$.validator.setDefaults({
	submitHandler: function() { 
		saveInfo(); 
	},
	highlight: function(input) {
		$(input).addClass("ui-state-highlight");
	},
	unhighlight: function(input) {
		$(input).removeClass("ui-state-highlight");
	}
});

$().ready(function() {
	//loadDepartment();
	$.datepicker.setDefaults( $.datepicker.regional[ "" ] );
	$("#birthday").datepicker($.datepicker.regional['zh-CN']);
	/*$( "#birthday" ).datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'yy-mm-dd'
	});*/
	$("#birthday").attr("readonly", "true").datepicker(); 
	$("#birthday").val('1990-01-01');

	$("#major").attr("disabled", "disabled");
	$("#class").attr("disabled", "disabled");
	$("#grade").attr("disabled", "disabled");
			
	jQuery.validator.addMethod("isIdentity", function(value, element){
		if($('#IDtype').val() == '1')
		{
			var identity = /^[0-9]*(X|x|[0-9])$/;
			return this.optional(element) || (identity.test(value));
		}
		else
			return true;
	}, "请填写正确的身份证号");
	
	// validate signup form on keyup and submit
	$("#signupForm").validate({
		rules: {
			province: {
				required: true,
				minlength: 2,
				maxlength: 30
			},
			nation: {
				required: true,
				minlength: 1,
				maxlength: 14
			},
			phone: {
				required: true,
				number: true,
				minlength: 8,
				maxlength: 20
			},
			IDnum: {
				required: true,
				isIdentity: true,
				minlength: 5,
				maxlength: 30
			},
			bankid: {
				required: true,
				number: true,
				minlength: 18,
				maxlength: 25
			},
			birthday: {required:true},
			major: {
				required: true,
				minlength: 3
			}
		}
	});
	
	//$("#signupForm input:not(:submit)").addClass("ui-widget-content");
	
	//捕捉学院下拉框的change事件
	departChange();
	
	//捕捉专业下拉框的change事件
	majorChange();

	//捕捉年级下拉框的change事件
	gradeChange();
	
	$(":submit").button();
});

//填充年级下拉框
function addGrade()
{
	var date = new Date();
	var vYear = parseInt(date.getFullYear());
	
	//begin to add to select
	for(var year = vYear-5; year < vYear+4; year++)
	{
		$("#grade").append('<option value="'+year+'">'+year+'级</option>');
	}
	$("#grade").val(0);
}

//验证学生是否选择了学院等信息
function validateClassID()
{
	if($('#sex').val() == 0)
	{
		alert("您还没有选择性别～");
		return false;
	}
	if($("#department option:selected").text() == "——")
	{
		alert("您还没有选择学院~");
		return false;
	}
	if($("#major option:selected").text() == "——")
	{
		alert("您还没有选择专业~");
		return false;
	}
	if($("#grade option:selected").text() == "——")
	{
		alert("您还没有选择年级~");
		return false;
	}
	if($("#class option:selected").text() == "——")
	{
		alert("您还没有选择班级~");
		return false;
	}
	return true;
}

//根据专业id和年级返回班级列表
function getClasses()
{
	//首先清除班级列表
	var classname = $("#class");
	classname.find("option:gt(0)").remove();

	var retClass = {};
	retClass["majorid"] = parseInt($("#major").find("option:selected").val());
	retClass["grade"] = parseInt($("#grade").val());
	$.get(
		"../../testdrive/index.php?r=zing/searchAllClass",
		{"retClass": $.toJSON(retClass)},
		function (data)
		{
			var ret = $.evalJSON(data);
			if(ret.success == true)
			{
				if(ret.results.length == 0)
				{
					alert("班级列表返回为空，请联系学工办管理员添加~");
					return false;
				}
						
				
				for(var i = 0; i < ret.results.length; i++)
				{
					classname.append('<option value="'+ret.results[i].cid+'">'+ret.results[i].name+'</option>');
				}
			}
			else
			{
				if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
				{
					alert(ret.message+'  页面即将跳转到首页～');
					window.location.href = '../../';
					return false;
				}

				//alert('服务器返回错误信息=>"'+ret.message+'"');
				alert(ret.message);
			}
		}
	);
}

//捕捉年级下拉框的change事件
function gradeChange()
{
	$("#grade").change(function (){
		if($("#grade").val() == '0')
		{
			$('#class')[0].selectedIndex = 0; 
			//$("#class option:first").attr("selected", "selected");
			$("#class").attr("disabled", "disabled");
		}
		else
		{
			if($("#class").attr("disabled") == "disabled")
			{
				$("#class").removeAttr("disabled");
			}
			
			getClasses();
		}
	});
}

//捕捉专业下拉框的change事件
function majorChange()
{
	$("#major").change(function (){
		if($("#major").find("option:selected").text() == "——")
		{
			$('#class')[0].selectedIndex = 0; 
			//$("#class option:first").attr("selected", "selected");
			$("#class").attr("disabled", "disabled");
			$('#grade')[0].selectedIndex = 0; 
			$("#grade").attr("disabled", "disabled");
		}
		else
		{
			if($("#grade").attr("disabled") == "disabled")
			{
				$("#grade").removeAttr("disabled");
			}
			if($("#grade").val() != '0')
				getClasses();
		}
	});
}
	
//捕捉学院下拉框的change事件
function departChange()
{
	$("#department").change(function (){
		if($("#department").find("option:selected").text() == "——")
		{
			$('#class')[0].selectedIndex = 0; 
			$('#major')[0].selectedIndex = 0; 
			$('#grade')[0].selectedIndex = 0; 
			//$("#class option:first").attr("selected", "selected");
			//$("#major option:first").attr("selected", "selected");
			$("#major").attr("disabled", "disabled");
			$("#class").attr("disabled", "disabled");
			$("#grade").attr("disabled", "disabled");
		}
		else
		{
			if($("#major").attr("disabled") == "disabled")
			{
				$("#major").removeAttr("disabled");
			}
			
			var major = $("#major");
			major.find("option:gt(0)").remove();
			$('#class option:gt(0)').remove();

			var retMajor = {};
			retMajor["departid"] = parseInt($("#department").find("option:selected").val());
			$.get(
				"../../testdrive/index.php?r=zing/searchAllMajor",
				{"retMajor": $.toJSON(retMajor)},
				function (data)
				{
					var ret = $.evalJSON(data);
					if(ret.success == true)
					{
						if(ret.results.length == 0)
						{
							alert("专业列表返回为空，请联系学工办管理员添加~");
							return false;
						}
				
						//var major = $("#major");
						//major.find("option:gt(0)").remove();
						for(var i = 0; i < ret.results.length; i++)
						{
							major.append('<option value="'+ret.results[i].mid+'">'+ret.results[i].name+'</option>');
						}
					}
					else
					{
						if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
						{
							alert(ret.message+'  页面即将跳转到首页～');
							window.location.href = '../../';
							return false;
						}

						alert('服务器返回错误信息=>"'+ret.message+'"');
					}
				}
			);
		}
	});
}

//填充学生类别下拉框
function getStuType()
{
	$.get(
		"../../testdrive/index.php?r=zing/getStuType",
		function(data)
		{
			var ret = $.evalJSON(data);
			//ret.success = false;
			//ret.results.timeout = true;
			if(ret.success == true)
			{
				if(ret.results.length == 0)
				{
					alert("学生类别列表返回为空，请联系学工部管理员添加~");
					return false;
				}
				
				for(var i = 0; i < ret.results.length; i++)
				{
					$("#stuType").append('<option value="'+ret.results[i].stuType+'">'+ret.results[i].content+'</option>');
				}
			}
			else
			{
				if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
				{
					alert(ret.message+'  页面即将跳转到首页～');
					window.location.href = '../../';
					return false;
				}

				alert('服务器返回错误信息=>"'+ret.message+'"');
				return false;
			}
		}
	);
}

//获取学院列表
function loadDepartment()
{
	alert("您初次登陆本系统，或者您的个人信息未完善，请您完善个人信息~");
	$.get(
		"../../testdrive/index.php?r=zing/searchAllDep",
		function (data)
		{
			var ret = $.evalJSON(data);
			//for test
			//ret.success = false;
			//ret.results.timeout = true;
			if(ret.success == true)
			{
				if(ret.results.length == 0)
				{
					alert("院系列表返回为空，请联系学工部管理员添加~");
					return false;
				}
				
				var sidInfo = $.cookie("csidInfo");
				if(sidInfo.length == 0 || sidInfo.split(',').length < 2)
				{
					alert("获取您的姓名和学号信息失败！请稍候重试~");
					return false;
				}
				//$.cookie('csidInfo', null, { path: '/' });
				$("#sname").val(sidInfo.split(',')[0]);
				$("#sid").val(sidInfo.split(',')[1]);
	
				var department = $("#department");
				for(var i = 0; i < ret.results.length; i++)
				{
					department.append('<option value="'+ret.results[i].did+'">'+ret.results[i].name+'</option>');
				}
				//填充年级下拉框
				addGrade();
				//填充学生类别下拉框
				getStuType();
			}
			else
			{
				if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
				{
					alert(ret.message+'  页面即将跳转到首页～');
					window.location.href = '../../';
					return false;
				}
				alert('服务器返回错误信息=>"'+ret.message+'"');
			}
		}
	);
}

function saveInfo()
{
	//验证学生是否选择了学院等信息
	if(!validateClassID())
		return false;

	var saveStuInfo = {};
	saveStuInfo['sname'] = $("#sname").replaceSpecialHtmlChar();
	saveStuInfo["province"] = $("#province").replaceSpecialHtmlChar();
	saveStuInfo["political"] = $("#political").find("option:selected").val();
	saveStuInfo["IDtype"] = $("#IDtype").find("option:selected").text();
	saveStuInfo["IDnum"] = $("#IDnum").replaceSpecialHtmlChar();
	saveStuInfo["nation"] = $("#nation").replaceSpecialHtmlChar();
	saveStuInfo["phone"] = $("#phone").replaceSpecialHtmlChar();
	saveStuInfo["classid"] = $("#class").find("option:selected").val();
	saveStuInfo["bankid"] = $("#bankid").replaceSpecialHtmlChar();
	saveStuInfo["cadre"] = $("#cadre").find("option:selected").text();
	saveStuInfo["ispoor"] = $("#ispoor").find("option:selected").text();
	saveStuInfo["stuType"] = $("#stuType").find("option:selected").val();
	saveStuInfo["grade"] = $("#grade").replaceSpecialHtmlChar();
	saveStuInfo["sex"] = $("#sex").find("option:selected").text();
	saveStuInfo["birthday"] = $.datepicker.formatDate('yy-mm-dd', $("#birthday").datepicker('getDate'));
	//alert(date);
	//saveStuInfo["birthday"] = $("#birthday").datepicker({ dateFormat: 'yy-mm-dd' });
	//alert($.toJSON(saveStuInfo));

	$.post(
		"../../testdrive/index.php?r=zing/saveStuinfo",
		{"saveStuInfo": $.toJSON(saveStuInfo)},
		function (data) 
		{
			var ret = $.evalJSON(data);
			if (ret.success == true)
			{
				alert("保存信息成功~");
				$.ajax({
					url : "../../testdrive/index.php?r=zing/addStuGrade",
					async : false,
					type : "POST",
					success : function (result){
						var para = $.evalJSON(result);
						if(para.success == false)
						{
							if(para.results.hasOwnProperty("timeout") && para.results.timeout)
							{
								alert(para.message+'  页面即将跳转到首页～');
								window.location.href = '../../';
								return false;
							}

							alert('服务器返回错误信息=>"'+para.message+'"');
							return false;
						}
						window.location.href = "main.html"; 
					}
				});
			}
			else
			{
				if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
				{
					alert(ret.message+'  页面即将跳转到首页～');
					window.location.href = '../../';
					return false;
				}

				alert('服务器端返回错误信息=>"'+ret.message+'"');
			}
		}
	);
	
}
