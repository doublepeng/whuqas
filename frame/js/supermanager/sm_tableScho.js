// JavaScript Document
$(function() {
  //调整屏幕的高度
	parent.parent.document.getElementById("frame_content").height=420;	
	//载入年份
	loadYear5();
	loadYear6();
});

function loadYear5()
{
	var years;
	var today;
	today = new Date();
	Years = today.getFullYear();
	var intYears;
	intYears = parseInt(Years);
	var selector=$('#selectYear5'); 
	for(var i=intYears-4; i< intYears +4;i++)
	{
		if(i == intYears)
			selector.append('<option value='+i+'>'+i+'年</option>');  	 
		else
			selector.append('<option value='+i+'>'+i+'年</option>');  
		$('#selectYear7').append('<option value='+i+'>'+i+'年</option>');
	}
	selector.val(intYears);
	$('#selectYear7').val(intYears);
}

//下载汇总表
function editARow5()
{
	$.get(
		"../../../testdrive/index.php?r=site/QASEnd",
		function(data)
		{
			var ret = $.evalJSON(data);
			if(ret.success == true)
			{
				var downloadtable5 = {};
				downloadtable5["year"] = $('#selectYear5').val();
				downloadtable5["tableid"] = 'eTable';
				var href = "../../../testdrive/protected/controllers/Excel/Tests/Finaltable.php?downloadtable5=";
				href += $.toJSON(downloadtable5);
				//alert(href);
				$("#edit5").attr("href", href);		
				window.open(href,"_parent");
			}
			else
			{
if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
					{
						alert(ret.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href = '../../../';
						return false;
					}
				alert(ret.message);
			}
			$("#edit5").attr("href", 'javascript:void(0)');
		}
	);
		
}

function loadYear6()
{
	var years;
	var today;
	today = new Date();
	Years = today.getFullYear();
	var intYears;
	intYears = parseInt(Years);
	var selector=$('#selectYear6'); 
	for(var i=intYears-4; i< intYears +4;i++)
	{
		if(i == intYears)
			selector.append('<option selected="selected" value='+i+'>'+i+'年</option>');  	 
		else
			selector.append('<option value='+i+'>'+i+'年</option>');  
	} 
}

//下载汇总表
function editARow6()
{
	$.get(
		"../../../testdrive/index.php?r=site/QASEnd",
		function(data)
		{
			var ret = $.evalJSON(data);
			if(ret.success == true)
			{
				var downloadtable6 = {};
				downloadtable6["year"] = $('#selectYear6').val();
				downloadtable6["tableid"] = 'fTable';

				var href = "../../../testdrive/protected/controllers/Excel/Tests/Collegestatical.php?downloadtable6=";
				href += $.toJSON(downloadtable6);
				//alert(href);
				$("#edit6").attr("href", href);
				window.open(href,"_parent");
			}
			else
			{
if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
					{
						alert(ret.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href = '../../../';
						return false;
					}
				alert(ret.message);
			}

			$("#edit6").attr("href", 'javascript:void(0)');
		}
	);

}

function editARow7()
{
	$.get(
		"../../../testdrive/index.php?r=site/QASEnd",
		function(data)
		{
			var ret = $.evalJSON(data);
			if(ret.success == true)
			{
				var downloadtable = {};
				downloadtable["year"] = $('#selectYear7').val();
				
				var href = "../../../testdrive/protected/controllers/Excel/Tests/WhuAllScho.php?downloadtable=";
				href += $.toJSON(downloadtable);
				//alert(href);
				$("#edit7").attr("href", href);
				window.open(href,"_parent");
			}
			else
			{
if(ret.results.hasOwnProperty("timeout") && ret.results.timeout)
					{
						alert(ret.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href = '../../../';
						return false;
					}
				alert(ret.message);
			}
			
			$("#edit7").attr("href", 'javascript:void(0)');
		}
	);
}

