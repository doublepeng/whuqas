// JavaScript Document
// JavaScript Document
var totalRecord = 0;
var pages = 0;
var currentPage = 1;
$(function() {
	//调整屏幕的高度
	parent.parent.document.getElementById("frame_content").height=420;	
	//隐藏翻页按钮
	$("#btLast").hide(); $("#btNext").hide(); $("#btFormer").hide(); $("#btFirst").hide(); 
	//隐藏第一行
	$("#tb tr").eq(1).hide();
	
	showTable();
	
	//删除所选记录按钮
	
	$("#BtDel").click(function() {
		var selected = false;
		$("#tb tr:gt(1)").each(function() {
			if ($(this).find("#CK").get(0).checked == true) {
				selected = true;
				return false;
			}
		 });
		if(!selected)
		{
			alert("请您选择要删除的测评系数~");
			return false;
		}
		
		$("#tb tr:gt(1)").each(function() {
			if ($(this).find("#CK").get(0).checked == true) {
				$(this).remove();
				totalRecord--;
			}
		});
		
		var height = 
		document.documentElement.scrollHeight>document.body.scrollHeight?document.body.scrollHeight:document.documentElement.scrollHeight; 
		//alert(document.documentElement.scrollHeight);
		if(height > 420)
			parent.parent.document.getElementById("frame_content").height=height;
		else
			parent.parent.document.getElementById("frame_content").height=420;	
			
		//父窗口iframe的宽度自适应

		$("#CKA").attr("checked", false);
		
		
		//重新分页
		rePage(totalRecord, currentPage);
		
	});
	
	//选择所有行
	$("#CKA").click(function() {		
		$("#tb tbody tr").each(function() {
			if(!$(this).is(":hidden")) 	
				$(this).find("#CK").get(0).checked = $("#CKA").get(0).checked;
		});

	});
	
	
	
	$("#btLast").click(function() 
	{
		if(currentPage != pages)
		{
			//alert("即将跳转到最后一页");
			currentPage = pages;
			ShowPage(currentPage*10-9, totalRecord);
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btNext").click(function() 
	{
		if(currentPage+1<=pages)
		{
			//alert("即将跳转到下一页");
			currentPage += 1;
			ShowPage(currentPage*10-9, currentPage*10);
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btFormer").click(function() 
	{
		if(currentPage-1>0)
		{
			//alert("即将跳转到上一页");
			currentPage -= 1;
			ShowPage(currentPage*10-9, currentPage*10);
		}
		else
		{
			alert("已是首页!");
		}
	});
	$("#btFirst").click(function() 
	{
		if(currentPage != 1)
		{
			//alert("即将跳转到首页");
			currentPage = 1;
			ShowPage(1, 10);
		}
		else
		{
			alert("已是首页!");
		}
	});
	
});


//重新分页
function rePage(totalRecord, currentPage)
{
	var tempPage = currentPage;
	setRecordPage(totalRecord);
	
	if (tempPage < pages)  
	{
		ShowPage(tempPage*10-9, tempPage*10);
		currentPage = tempPage;
	}
	else
	{
		ShowPage(pages*10-9, totalRecord);
		currentPage = pages;
	}
	
	
}

//传入需要被显示的数据的位置
//仅显示_min 和 _max之间的数据
function ShowPage(_min, _max)
{
	$("#tb tbody tr").each(function() {
		$(this).show();
	});
	
	var variable = "#tb tbody tr:lt("+_min+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	variable = "#tb tbody tr:gt("+_max+")";
	$(variable).each(function() {
		$(this).hide();
	});
	$("#showPage").html(currentPage+"/"+pages);
	var height = document.documentElement.scrollHeight>document.body.scrollHeight?document.body.scrollHeight:document.documentElement.scrollHeight; 
				
	if(height+30 > 420)
		parent.parent.document.getElementById("frame_content").height=height+30;
	else
		parent.parent.document.getElementById("frame_content").height=420;	
}

//设置页数和总记录数
function setRecordPage(number)


{
	totalRecord = number;
	if(number <= 0)
	{
		totalRecord = 0;
		pages = 1;
	}
	else
	{//每页有10条记录
		pages = parseInt(number/10);
		if(number%10 != 0)
			pages += 1;
	}
	//页数>1 需翻页按钮
	if(pages > 1)
	{
		$("#btLast").show(); $("#btNext").show(); $("#btFormer").show(); $("#btFirst").show(); 
	}
	else
	{
		$("#btLast").hide(); $("#btNext").hide(); $("#btFormer").hide(); $("#btFirst").hide(); 
	}
}

function showTable()
{
	$.get(
		 "../../../testdrive/index.php?r=Manager/CheckFactors",
		function(data)
		{
			//alert(data);
			var obj = $.evalJSON(data);
			if(obj.success == false)
			{
if(obj.results.hasOwnProperty("timeout") && obj.results.timeout)
					{
						alert(obj.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href = '../../../';
						return false;
					}
				alert("数据库中无数据~"+obj.message);
				return false;
			}

			setRecordPage(obj.results.length);
			
			for (var i = 0; i < obj.results.length; i++)
			{
				var tr = $("#tb tr").eq(1).clone();
				//tr.find("td").get(0).innerHTML = ++i;
				
				tr.find("td").get(0).innerHTML = obj.results[i]['departmentname'];
				tr.find("td").get(1).innerHTML = obj.results[i]['factor1'];
				tr.find("td").get(2).innerHTML = obj.results[i]['factor2'];
				tr.find("td").get(3).innerHTML = obj.results[i]['factor3'];
				tr.show();
				tr.appendTo("#tb tbody");
			}
			
			
			currentPage = 1;
			$("#tb tbody tr:gt(10)").each(function() {
				$(this).hide();
				//alert(height);
			});
			$("#showPage").html(currentPage+"/"+pages);
			var height = 
				document.documentElement.scrollHeight>document.body.scrollHeight?document.body.scrollHeight:document.documentElement.scrollHeight; 
			if(height+30 > 420)
				parent.parent.document.getElementById("frame_content").height=height+30;
			else
				parent.parent.document.getElementById("frame_content").height=420;	

		}
	);
}

//检查已添加条目的正确性
function updateTips( t ) {
	$( ".validateTips")
		.text( t )
		.addClass( "ui-state-highlight" );
	setTimeout(function() {
		$( ".validateTips").removeClass( "ui-state-highlight", 1500 );
		$( ".validateTips" ).text("");
	}, 1500 );
}

function checkLength( o, n, min, max ) {
	if ( o.val().length > max || o.val().length < min ) {
		o.addClass( "ui-state-error" );
		updateTips( '提示: " '+n+' "' + " 的长度应该在 " +
			min + " 和 " + max + "之间." );
		return false;
	} else {
		return true;
	}
}

function checkValue( o, n, min, max ) {
	if ( parseFloat(o.val()) > max || parseFloat(o.val()) < min) {
		o.addClass( "ui-state-error" );
		updateTips( '" '+n+' "' + " 的值 " +
			min + " 和 " + max + "之间." );
		return false;
	} else {
		return true;
	}
}

function checkRegexp( o, regexp, n ) {
	if ( !( regexp.test( o.val() ) ) ) {
		o.addClass( "ui-state-error" );
		updateTips( n );
		return false;
	} else {
		return true;
	}
}

function validator()
{
	if($("#tb tbody tr").length <= 1)
	{
		updateTips( "您还没有添加新纪录，无需保存" );
		return false;
	}
		
	var totalRows = new Array();
	var success = true;
	$("#tb tbody tr:gt(0)").each(function() 
	{
		var studentname = $(this).find("#studentname"),
		
		
		allFields = $( [] ).add( studentname );
	
		var bValid = true;
		allFields.removeClass( "ui-state-error" );
		bValid = bValid && checkLength( studentname, "学生姓名", 1, 5);
		bValid = bValid && checkRegexp( studentname, /^[0-9]+.?[0-9]*$/, "\"学生姓名\"应该输入合法数字" );
		bValid = bValid && checkValue( studentname, "学生姓名", 0, 20);
		
		
		
		
		
		if ( ! bValid ) {
			success = false;
			return false;
		}
	});
	return success;
}





