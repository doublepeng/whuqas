// JavaScript Document
// JavaScript Document
var totalRecord = 0;
var pages = 0;
var currentPage = 1;
var currentMem;   //用来记录当前编辑的是哪一行
var update = {};   //用来记录用户需要更新的学院
var delete1 = {};   //用来记录删除的学院
$(function() {
	parent.parent.document.getElementById("frame_content").height=420;   
	//隐藏翻页按钮
	$("#btLast").hide(); $("#btNext").hide(); $("#btFormer").hide(); $("#btFirst").hide(); 
	//隐藏第一行
	$("#tb tr").eq(1).hide();

	showTable();
	
	//删除所选记录按钮
	$("#BtDel").click(function() {
		var selected = false;
		$("#tb tr:gt(1)").each(
			function() {
				if ($(this).find("#CK").get(0).checked == true) {
					selected = true;
					return false;
			}
		 });
		if(!selected)
		{
			alert("请您选择要删除的学院信息~");
			return false;
		}
		$("#tb tr:gt(1)").each(function() {
			if ($(this).find("#CK").get(0).checked == true) 
			{
				delete1[$(this).find("#departmentname").attr("name")] = {
					'departmentid': $(this).find("#departmentname").attr("name")
				};
				$(this).remove();
				totalRecord--;
			}
		});
		
		adjustHeight();
		//父窗口iframe的宽度自适应

		$("#CKA").attr("checked", false);
		//重新分页
		rePage(totalRecord, currentPage);
	});
	
	//选择所有行
	$("#CKA").click(function() {		
		$("#tb tbody tr").each(function() {
			if(!$(this).is(":hidden")) 	
				$(this).find("#CK").get(0).checked = $("#CKA").get(0).checked;
		});

	});
	
	
	
	$("#btLast").click(function() 
	{
		if(currentPage != pages)
		{
			//alert("即将跳转到最后一页");
			currentPage = pages;
			ShowPage(currentPage*10-9, totalRecord);
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btNext").click(function() 
	{
		if(currentPage+1<=pages)
		{
			//alert("即将跳转到下一页");
			currentPage += 1;
			ShowPage(currentPage*10-9, currentPage*10);
		}
		else
		{
			alert("已是最后一页!");
		}
	});
	$("#btFormer").click(function() 
	{
		if(currentPage-1>0)
		{
			//alert("即将跳转到上一页");
			currentPage -= 1;
			ShowPage(currentPage*10-9, currentPage*10);
		}
		else
		{
			alert("已是首页!");
		}
	});
	$("#btFirst").click(function() 
	{
		if(currentPage != 1)
		{
			//alert("即将跳转到首页");
			currentPage = 1;
			ShowPage(1, 10);
		}
		else
		{
			alert("已是首页!");
		}
	});
	$("#edit").click(function(){
							  
	});
	
	//保存记录
	$("#BtSave").click(function() {
		if(validator())
		{
			//update
			var saveEditDepartment = {};
			saveEditDepartment["update"] = [];
			saveEditDepartment["delete1"] = [];
			$.each(update, function(key, value){
				saveEditDepartment["update"].push({
					'departmentid': key,
					'departmentname': value["departmentname"]
				});
			});
			$.each(delete1, function(key, value){
				saveEditDepartment["delete1"].push({
					'departmentid': key
					//'departmentname': value["departmentname"]
				});
			});
			if(saveEditDepartment["update"].length == 0 && saveEditDepartment["delete1"].length == 0)
			{
				alert("您尚未进行编辑操作，请您编辑后再提交保存！");
				return false;
			}
      //alert($.toJSON(saveEditDepartment));
      if(!confirm("您确认保存编辑后的学院信息？"))
      {
        return false;
      }
			$.post(
			   "../../../testdrive/index.php?r=Manager/SaveEditDepartment",
			   {saveEditDepartment: $.toJSON(saveEditDepartment)},
			   function(data)
			   {
					var obj = $.evalJSON(data);
					if(obj.success == false)
					{
if(obj.results.hasOwnProperty("timeout") && obj.results.timeout)
					{
						alert(obj.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href = '../../../';
						return false;
					}
						alert("保存记录失败！"+obj.message);
						return false;
					}
					else
					{
						alert("成功保存编辑后的学员信息记录~~");
						update = {};
						delete1 = {};
						//return true;
					}
				}
			);
		}
	});
});

//编辑学院
function editARow()
{
	if(currentMem.find("#edit").html() == "编辑")
	{
		currentMem.find("#departmentname").removeAttr("disabled");
		currentMem.find("#edit").html("保存");
	}
	else if(currentMem.find("#edit").html() == "保存")
	{
		if(validateARow(currentMem))
		{
			if(currentMem.find("#departmentname").attr("name") != "")
				update[currentMem.find("#departmentname").attr("name")] = {
					'departmentname': currentMem.find("#departmentname").val()
				};
			currentMem.find("#departmentname").attr("disabled", "disabled");
			currentMem.find("#edit").html("编辑");
		}
	}
}

//重新分页
function rePage(totalRecord, currentPage)
{
	var tempPage = currentPage;
	setRecordPage(totalRecord);
	
	if (tempPage < pages)  
	{
		ShowPage(tempPage*10-9, tempPage*10);
		currentPage = tempPage;
	}
	else
	{
		ShowPage(pages*10-9, totalRecord);
		currentPage = pages;
	}
}

//传入需要被显示的数据的位置
//仅显示_min 和 _max之间的数据
function ShowPage(_min, _max)
{
	$("#tb tbody tr").each(function() {
		$(this).show();
	});
	
	var variable = "#tb tbody tr:lt("+_min+")";
	$(variable).each(function() {
		$(this).hide();
	});
	
	variable = "#tb tbody tr:gt("+_max+")";
	$(variable).each(function() {
		$(this).hide();
	});
	$("#showPage").html(currentPage+"/"+pages);
	adjustHeight();
}

//设置页数和总记录数
function setRecordPage(number)
{
	totalRecord = number;
	if(number <= 0)
	{
		totalRecord = 0;
		pages = 1;
	}
	else
	{//每页有10条记录
		pages = parseInt(number/10);
		if(number%10 != 0)
			pages += 1;
	}
	//页数>1 需翻页按钮
	if(pages > 1)
	{
		$("#btLast").show(); $("#btNext").show(); $("#btFormer").show(); $("#btFirst").show(); 
	}
	else
	{
		$("#btLast").hide(); $("#btNext").hide(); $("#btFormer").hide(); $("#btFirst").hide(); 
	}
}

function showTable()
{
	$.get(
		"../../../testdrive/index.php?r=Manager/ShowDepartment",
		function(data)
		{
			//alert(data);
			var obj = $.evalJSON(data);
			if(obj.success == false)
			{
if(obj.results.hasOwnProperty("timeout") && obj.results.timeout)
					{
						alert(obj.message+'  页面即将跳转到首页～');
						window.parent.parent.location.href = '../../../';
						return false;
					}
				alert("返回学院信息失败！服务器端返回错误=>"+obj.message);
				return false;
			}

			setRecordPage(obj.results.length);
			for (var i = 0; i < obj.results.length; i++)
			{
				var tr = $("#tb tr").eq(1).clone();
				tr.find("td #departmentname").val(obj.results[i]['departmentname']);
				tr.find("td #departmentname").attr("name",obj.results[i]['departmentid']);
				tr.show();
				tr.appendTo("#tb tbody");
			}
			currentPage = 1;
			$("#tb tbody tr:gt(10)").each(function() {
				$(this).hide();
			});
			$("#showPage").html(currentPage+"/"+pages);
			adjustHeight();
		}
	);
}

//检查已添加条目的正确性
function updateTips( t ) {
	$( ".validateTips")
		.text( t )
		.addClass( "ui-state-highlight" );
	setTimeout(function() {
		$( ".validateTips").removeClass( "ui-state-highlight", 1500 );
		$( ".validateTips" ).text("");
	}, 1500 );
}

function checkLength( o, n, min, max ) {
	if ( o.val().length > max || o.val().length < min ) {
		o.addClass( "ui-state-error" );
		updateTips( '提示: " '+n+' "' + " 的长度应该在 " +
			min + " 和 " + max + "之间." );
		return false;
	} else {
		return true;
	}
}

function checkRegexp( o, regexp, n ) {
	if ( !( regexp.test( o.val() ) ) ) {
		o.addClass( "ui-state-error" );
		updateTips( n );
		return false;
	} else {
		return true;
	}
}

//验证一行
function validateARow(row)
{
	var departmentname = row.find("#departmentname"),
    allFields = $( [] ).add( departmentname );
	var bValid = true;
	allFields.removeClass( "ui-state-error" );
	bValid = bValid && checkLength( departmentname, "学院名称", 2, 35);
	//bValid = bValid && checkRegexp( departmentname, /^[\u4e00-\u9fa5\da-zA-Z\-\_]+$/, "\"学院名称\"不能包含特殊字符，请输入中文、英文单词或者数字" );
	if ( ! bValid ) {
		success = false;
		return false;
	}
	return true;
}

function validator()
{
	var success = true;
	$("#tb tr:gt(1)").each(function() 
	{
		success = validateARow($(this));
		if(success == false)
		{
			return false;
		}
	});
	return success;
}

function adjustHeight()
{
	var height = document.body.clientHeight;
	//alert(height);
	if(height > 420)
		parent.parent.document.getElementById("frame_content").height=height + 40;
	else
		parent.parent.document.getElementById("frame_content").height=420;	
}




