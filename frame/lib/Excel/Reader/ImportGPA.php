<?php

require_once 'Excel/reader.php';

class ImportGPA 
{
	private $_data;
	function ImportGPA(){

		// ExcelFile($filename, $encoding);
		$this->_data = new Spreadsheet_Excel_Reader();

		// Set output Encoding.
		$this->_data->setOutputEncoding('gb2312');
    }

	public function import($filename)
	{
		$this->_data->read($filename);

		error_reporting(E_ALL ^ E_NOTICE);

		$rows = $this->_data->sheets[0]['numRows'];
		$cols = $this->_data->sheets[0]['numCols'];

		if($rows > 1 && $cols > 1)
		{
			$GPA = $this->_getRecords();
			return array("return" => "true", "serializeGPA" => $GPA);
		}
		else
		{
			return array("return" => "false");
		}
	}
	public function _getCourses()//获得课程名，类型，学分
	{
	//首先解析课程和课程类型以及学分
		$courses = array();  //$courses[id] = array("课程名", "课程类型", "学分");
		for ($j = 1; $j <= $this->_data->sheets[0]['numCols']; $j++) {
			$course = explode(' ', $this->_data->sheets[0]['cells'][1][$j]);
			if(count($course) < 3)
				continue;
			else
			{	
				$courses[$j] = array(trim($course[0]), trim($course[1]), trim($course[2]));
			}
		}
		return $courses;
	}
	public function _getStudents()//获得学生的学号，姓名
	{
		$students = array();
		for ($i = 2; $i <= $this->_data->sheets[0]['numRows']; $i++) 
		{
			if(!eregi('^[0-9]+$',$this->_data->sheets[0]['cells'][$i][1]))
				break;
			$aStudent = array($this->_data->sheets[0]['cells'][$i][1], $this->_data->sheets[0]['cells'][$i][2]);
			array_push($students,$aStudent);
		} 
		return $students;
	}
	public function _getRecords()
	{
		//首先解析课程和课程类型以及学分
		/*$courses = array();  //$courses[id] = array("课程名", "课程类型", "学分");
		for ($j = 1; $j <= $this->_data->sheets[0]['numCols']; $j++) {
			$course = explode(' ', $this->_data->sheets[0]['cells'][1][$j]);
			if(count($course) < 3)
				continue;
			else
			{	
				$courses[$j] = array(trim($course[0]), trim($course[1]), trim($course[2]));
			}
		}*/
		$courses = $this->_getCourses();
		//开始解析学生成绩
		$studentVSscore = array();
		for ($i = 2; $i <= $this->_data->sheets[0]['numRows']; $i++) 
		{
			if(!eregi('^[0-9]+$',$this->_data->sheets[0]['cells'][$i][1]))
				break;

			$aStudent = array($this->_data->sheets[0]['cells'][$i][1], $this->_data->sheets[0]['cells'][$i][2]); 
			//存储一个学生的学号、姓名、所有课程成绩
			
			$aScore = array();   //存储一个学生的课程成绩
			for ($j = 3; $j <= $this->_data->sheets[0]['numCols']; $j++) 
			{
				$str = $this->_data->sheets[0]['cells'][$i][$j];   //获取成绩
				if($str != '')
				{
					$score = $courses[$j];   
					array_push($score, $str);   //取得成绩对应的课程相关信息
					array_push($aScore, $score);
				}
			}
			array_push($aStudent, serialize($aScore));
			array_push($studentVSscore, $aStudent);			
		}
		return $studentVSscore;
	}
}

?>
