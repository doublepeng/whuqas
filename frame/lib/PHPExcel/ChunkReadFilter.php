<?php 
ini_set("memory_limit","1024M");
set_time_limit(0);
error_reporting(E_ALL);
require_once 'Classes/PHPExcel/IOFactory.php';

class ChunkReadFilter implements PHPExcel_Reader_IReadFilter
{

    private $_startRow = 0;
    private $_endRow = 0;
    public function setRows($startRow, $chunkSize)
    {
        $this->_startRow    = $startRow;
        $this->_endRow      = $startRow + $chunkSize;
    }

    public function readCell($column, $row, $worksheetName = '')
    {
        //  Only read the heading row, and the rows that are configured in $this->_startRow and $this->_endRow
        if (($row == 1) || ($row >= $this->_startRow && $row < $this->_endRow)) {
            return true;
        }
        return false;
    }
}
?>
