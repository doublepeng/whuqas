<?php
# all text and code within this file are (c)opyright
# pangolin software limited 2000.
#
# you may edit this file to customise it for your web-site,
# however, the actual source code may not be altered in
# any way without the prior written permission of pangolin
# software limited.
# this file is part of the pangolin vote distribution.
# contact: pangolin@pango.co.uk or www.pango.co.uk for more info.
/*
* utility routines for mysql.
* modified from code from: http://www.webwizard.com/tutorials/mysql/
*/

class m_mysql {
    # make sure you fill in the values below for your web-site.
    # they are for user,password,host and database respectively.
    #
    var $user = "root";
    var $pass = "root";
    var $thedatabase ="whuqas2011";
    var $host = "localhost";

    var $db, $id, $result, $rows, $data, $a_rows;
    /*
     * its a minor security hole to have the username and password
     * appear here.  generally there isnt any way of getting around it
     * if youre using a commercial web hosting service.  there are other
     * ways if its your own web server.
     */
    function m_mysql () {
        initPara();
		$this->create();
    }

	function initPara()
	{
		$fp = fopen("config.inc", "r");
		if($fp == null)
		{
			die ("<p>unable to open configuration file.<p>");
		}

		$dict = array();
		while (!feof($fp)) {
		   $line = fgets($fp);
		   $lines = explode(':', $line);
		   $dict[trim($lines[0])] = trim($lines[1]);
		}

		$this->user = $dict["username"];
        $this->pass = $dict["password"];
		$this->thedatabase = $dict["dbname"];
		$this->host = $dict["dbhost"];

		fclose($fp);

	}

    function create () {
        $db=$this->thedatabase;
        $this->db = $db;
        $crash=0;
        $this->id = @mysql_pconnect($this->host, $this->user, $this->pass) or
        $crash=1;
        if ($crash==1) {
            mysql_errormsg("unable to connect to mysql server: $this->host - either your username, password or database values are incorrect in vutil.php3 or you have not started mysql on your web server!");
            die("<p>unable to continue.<p>");
        }
		
		mysql_query("set names 'utf8'");
        $this->selectdb($db);
    }

    function selectdb ($db) {
        $crash=0;
        @mysql_select_db($db, $this->id) or $crash=1;
        if ($crash==1)
        {
        mysql_errormsg ("unable to select database: $db : the value in vutil.php3 may be incorrect.");
        die ("<p>unable to continue.<p>");
        }
    }
    # use this function is the query will return multiple rows.  use the fetch
    # routine to loop through those rows.
    function query ($query) {
        $this->result = @mysql_query($query, $this->id) or
            mysql_errormsg ("unable to perform query: $query");
        $this->rows = @mysql_num_rows($this->result);
        $this->a_rows = @mysql_affected_rows($this->result);
    }
    # use this function if the query will only return a
    # single data element.
    function queryitem ($query) {
        $this->result = @mysql_query($query, $this->id) or
            mysql_errormsg ("unable to perform query: $query");
        $this->rows = @mysql_num_rows($this->result);
        $this->a_rows = @mysql_affected_rows($this->result);
        $this->data = @mysql_fetch_array($this->result) or mysql_errormsg ("unable to fetch.");
        return($this->data[0]);
    }
    # this function is useful if the query will only return a
    # single row.
    function queryrow ($query) {
        $this->result = @mysql_query($query, $this->id) or
            mysql_errormsg ("unable to perform query: $query");
        $this->rows = @mysql_num_rows($this->result);
        $this->a_rows = @mysql_affected_rows($this->result);
        $this->data = @mysql_fetch_array($this->result) or mysql_errormsg ("unable to fetch.");
        return($this->data);
    }
    function fetch ($row) {
        @mysql_data_seek($this->result, $row) or mysql_errormsg ("unable to seek data.");
        $this->data = @mysql_fetch_array($this->result) or mysql_errormsg ("unable to fetch.");
    }
    function insert ($query) {
        $this->result = @mysql_query($query, $this->id) or
            mysql_errormsg ("unable to perform insert: $query");
        $this->a_rows = @mysql_affected_rows($this->result);
    }
    function update ($query) {
        $this->result = @mysql_query($query, $this->id) or
            mysql_errormsg ("unable to perform update: $query");
        $this->a_rows = @mysql_affected_rows($this->result);
    }
    function delete ($query) {
        $this->result = @mysql_query($query, $this->id) or
            mysql_errormsg ("unable to perform delete: $query");
        $this->a_rows = @mysql_affected_rows($this->result);
    }

	//add by minzhenyu
	function close()
	{
		@mysql_close($this->id) or mysql_errormsg ("unable to close mysql");;
	}
}
/* ********************************************************************
* mysql_errormsg
*
* print out an mysql error message
*
*/
function mysql_errormsg ($msg) {
    # close out a bunch of html constructs which might prevent
    # the html page from displaying the error text.
    echo("</ul></dl></ol<\n");
    echo("</table></script>\n");
    # display the error message
    $text  = "<font color=\"#ff0000\"><p><b>error: $msg :";
    $text .= mysql_error();
    $text .= "</b></font>\n";
    $errormsg=$text;
    # get rid of unable to fetch error messages
    if (strpos($errormsg,"unable to fetch")==false)
    print "$errormsg\n";
}
?> 