﻿<?php 
	header('Content-Type:text/html;charset=utf-8');//防止中文信息有乱码
	require_once './../frame/lib/PHPExcel/Classes/PHPExcel.php';
	require_once './../frame/lib/PHPExcel/Classes/PHPExcel/IOFactory.php';
	require_once './../frame/lib/PHPExcel/ChunkReadFilter.php';

  //测试服务器是同时有其他人导入
	$file = "./test.txt";
  $fp = fopen($file , 'w');
  //flock($fp , LOCK_UN);    
  //开始检测
	$success = true;
  if(flock($fp , LOCK_EX | LOCK_NB)){ 
      //echo "hello";    
      //表示正在测试中 




 
	if (!empty($_FILES)) {
		$tempFile = $_FILES['Filedata']['tmp_name'];
		$targetPath = $_SERVER['DOCUMENT_ROOT'] . $_REQUEST['folder'] . '/';
		$targetFile =  str_replace('//','/',$targetPath) . $_FILES['Filedata']['name'];

		$targetType = "Excel5";

    //2012-01-15 minzhenyu
    //utf-8 => gb2312 is used for windows
    //It's useless for linux
		//$targetFile = iconv("utf-8", "gb2312", $targetFile);
		$success = move_uploaded_file($tempFile, $targetFile);
    if(!$success)
    {
      echo "服务器端移动文件出错，请重新尝试！";
      exit;
    }
		//echo str_replace($_SERVER['DOCUMENT_ROOT'],'',$targetFile);
    //echo "文件上传成功";
    //exit;
	}
	else
	{
		$success = false;
    echo "文件上传中出错，服务器没有找到文件！请稍候重试！";
    exit;
	}
  
  //$targetFile = "./../uploads/全校学生名单1.xls";
  //$targetType = "Excel5";
	//将学生信息导入到mysql数据库中
	if($success)
	{
		$hostname = 'localhost';
		$user = 'root';
		$pwd = 'root';
		$encode = "set names 'utf8'";
		$dbname = 'whuqas2011212';
		$password = md5(123456);
    //@表示禁止失败尝试导致的任何错误信息,用户将看到的是die()中指定的错误信息
    $connect = @mysql_connect($hostname,$user, $pwd) or die("服务器端数据库连接失败，请稍候重试上传文件！"); 
    mysql_query($encode);
    @mysql_select_db($dbname) or die("服务器端选择数据库".$dbname."失败！"); 
        
    $file_name = $targetFile;
    $file_type = $targetType;
    //对Excel文件进行处理
    $objReader = PHPExcel_IOFactory::createReader($file_type);
	  $chunkSize = 65536;
	  $chunkFilter = new ChunkReadFilter();
	  $objReader->setReadFilter($chunkFilter);
	  $arraystu = array();//保存学生全部信息

    for ($startRow = 1; $startRow <= 65536; $startRow += $chunkSize) {
	    $chunkFilter->setRows($startRow, $chunkSize);
      $objPHPExcel = $objReader->load($file_name);

	   	foreach ($objPHPExcel->getActiveSheet()->getRowIterator() as $row_key => $row) {
        $cellIterator = $row->getCellIterator();//抓取这一行信息
		    $cellIterator->setIterateOnlyExistingCells(false);
        
        $count = 0;
		    foreach ($cellIterator as $cell_key=>$cell) {
		      if ($cell_key < 3)
		      {
            $count ++;
		        //$cename = iconv("utf-8","gbk",$cell->getValue()."");
						$cename = $cell->getValue()."";
            $arraystu[$row_key][$cell_key] = $cename;//为了强迫写入的是字,强迫写入的时gbk
		       }
		    }
        if($count != 3)
        {
          echo "您导入的文件格式信息有误，检查后请重新导入！";
          exit;
        }
      }
    }
    
    //var_dump($arraystu);
    //2012-01-15 校验头部信息，检查模板中前三列是否为：xh,xm,nj
    if(!empty($arraystu))
    {
        //首先获得第一行信息，进行校验
        if($arraystu[1][0] == "xh" && $arraystu[1][1] == "xm" && $arraystu[1][2] == "nj")
        {
          //表头信息对了，那么进行开始导入的操作，
          //边导入，边校验，如果导入过程中有误，那么用数组进行记录
          //导入结束后一次性返回给前端
        }
        else
        {
          echo "您导入的文件中表头信息有误，应为（xh, xm, nj），请重新导入！";
          exit;
        }
    }
    else
    {
      echo "您导入的文件中没有学生信息，请重新导入！";
      exit;
    }
    
    //retError 用来记录有误的学生信息
    $retError = "";	  
    
		//导入全校学生的学号，密码，年级,密码三个信息
		$students= $arraystu;//获得多个学生的信息，包括第一行，xh,xm等信息
		//得到数组的长度
		$len = count($arraystu,0);
		//一次最多插入多少条
		$resize = 1000;
		for($i = 2; $i <= $len; $i = $i + $resize)//对每一条学生记录
		{
			$sql = "insert ignore into student(studentid,studentname,grade,password) values";
			$temp = $sql;
			//对于其中的第$i,和第$i+$resize条记录
			for($j = $i;$j < $i + $resize && $j <= $len;$j++)
			{
				//得到学号
				$studentid = $students[$j][0];
				//中文处理
				$studentname = $students[$j][1]; 
				$grade = $students[$j][2]; 
        
        //$studentname=iconv('utf-8','gbk',$studentname);
        //对每条学生记录进行校验，学号必须全部都是数字，姓名必须不含特殊字符，年级必须是数字
        if(preg_match("/^\d*$/", $studentid) && preg_match("/^\d*$/", $grade) && !empty($studentname))
        {
          //校验成功则可以进行插入
        }
        else
        {
          echo "您导入的文件中含有非法字符，非法字符出现在=>学号：".$studentid." 姓名：".$studentname." 年级：".$grade."！";
          echo "请您更新信息后，重新进行导入！";
          exit;
        }
				//$studentname  = iconv("gbk","utf-8",$studentname);//学生姓名

				//新建学生对象
				//需要加入异常处理，重复添加！！！！！！！！
				$sql = $sql."('$studentid','$studentname','$grade','$password'),";
			}
			//如果$sql 发生变化
			if($sql != $temp)
			{
				$sql= substr($sql, 0, -1);//将最后一个逗号去掉
				$query = @mysql_query($sql,$connect) or die("服务器端导入学生基本信息时失败，请稍候重新导入！");//执行操作
			}
		}

    echo "导入学生基本信息成功！";
	}
	else
	{
    echo "文件上传中出错，请稍候重试！";
    exit;
	}


//测试是否有其他人导入
    flock($fp , LOCK_UN);    
  } 
  else 
  {
    echo "服务器正忙，请稍候重试！";
  }

  fclose($fp);
?>
