﻿<?php 
	//添加课程表以及学生成绩按照整个学院导入
	header('Content-Type:text/html;charset=utf-8');//防止中文信息有乱码
	require_once './../frame/lib/PHPExcel/Classes/PHPExcel.php';
	require_once './../frame/lib/PHPExcel/Classes/PHPExcel/IOFactory.php';
	require_once './../frame/lib/PHPExcel/ChunkReadFilter.php';
  //测试服务器是同时有其他人导入成绩
	$file = "./test.txt";
  $fp = fopen($file , 'w');
  //flock($fp , LOCK_UN);    
//开始检测
  if(flock($fp , LOCK_EX | LOCK_NB)){    
   //表示正在测试中    

  
	$success = true;
	if (!empty($_FILES)) {
		$tempFile = $_FILES['Filedata']['tmp_name'];
		$targetPath = $_SERVER['DOCUMENT_ROOT'] . $_REQUEST['folder'] . '/';
		$targetFile =  str_replace('//','/',$targetPath) . $_FILES['Filedata']['name'];
		//将文件信息写入到fp中
		$targetType = "Excel5";

		//$targetFile = iconv("utf-8", "gb2312", $targetFile);
		$success = move_uploaded_file($tempFile, $targetFile);
    if(!$success)
    {
      echo "服务器端移动文件出错，请重新尝试！";
      exit;
    }
		//echo str_replace($_SERVER['DOCUMENT_ROOT'],'',$targetFile);
	}
	else
	{
		$success = false;
    echo "文件上传中出错，服务器没有找到文件！请稍候重试！";
    exit;
	}

	/*$success = true;
	$targetFile = './../uploads/for test/测试用09信息安全2班成绩.xls';
	$targetType = "Excel5";*/
	//将学生信息导入到mysql数据库中
	if($success)
	{
    //if(!isset($_SESSION)) session_start();
		//$year = $_SESSION['year'];
		//$year = 2012;
		$hostname = 'localhost';
		$user = 'root';
		$pwd = 'root';
		$encode = "set names 'utf8'";
		$dbname = 'whuqas2011212';
		//@表示禁止失败尝试导致的任何错误信息,用户将看到的是die()中指定的错误信息
    $connect = @mysql_connect($hostname,$user, $pwd) or die("服务器端数据库连接失败，请稍候重试上传文件！"); 
    mysql_query($encode);
    @mysql_select_db($dbname) or die("服务器端选择数据库".$dbname."失败！"); 

    //2012-01-15 minzhenyu
    $sqlyear = "select YEAR(starttime),starttime,endtime from checklist order by starttime desc limit 1";
		$queryyear = @mysql_query( $sqlyear) or die ("查询checklist表格，获取当前年份失败~");
		//if (!$queryyear)   
    //   throw new Exception(iconv("gb2312","utf-8","获取测评开始年份失败~"));				
		$rowyear = mysql_fetch_row($queryyear);		
		$year = $rowyear[0];//返回年份，参数

    $file_name = $targetFile;
    $file_type = $targetType;
    //对Excel文件进行处理
    $objReader = PHPExcel_IOFactory::createReader($file_type);
	  $chunkSize = 65536;
	  $chunkFilter = new ChunkReadFilter();
	  $objReader->setReadFilter($chunkFilter);
	  $arraystu = array();//保存学生全部信息
	  for ($startRow = 2; $startRow <= 65536; $startRow += $chunkSize) {
	    $chunkFilter->setRows($startRow, $chunkSize);
	    $objPHPExcel = $objReader->load($file_name);
	    foreach ($objPHPExcel->getActiveSheet()->getRowIterator() as $row_key => $row) {
	   	  $cellIterator = $row->getCellIterator();//抓取这一行信息
		    $cellIterator->setIterateOnlyExistingCells(false);
		    foreach ($cellIterator as $cell_key=>$cell) {
		      //$cename = iconv("utf-8","gbk",$cell->getValue()."");
				  $cename = $cell->getValue()."";
		      $arraystu[$row_key][$cell_key] = $cename;//为了强迫写入的是字,强迫写入的时gbk
		    }
	    }
    }

		//var_dump($arraystu);
    if(empty($arraystu))
    {
      echo "您导入的文件中没有学生成绩信息，请重新导入！";
      exit;
    }

	  //首先导入的是课程表头
	  //对数组的[1]进行处理
		$stucourses = $arraystu[1];
		//print_r($stucourses);
		//得到$courses的长度
	  $colen = count($stucourses,0);
    if($colen <= 2)
    {
      echo "您导入的文件格式不正确，请重新导入！";
      exit;
    }
  
    if($stucourses[0] != "学号" || $stucourses[1] != "姓名")
    {
      echo "您导入的文件格式不正确，请重新导入！";
      exit;
    }
  
		$courses = array();  //$courses[id] = array("课程名", "课程类型", "学分");
		$j = 0;
		//for($n = 2;$n <= $colen;$n++)
		//2012-01-14 minzhenyu
		for($n = 2;$n < $colen;$n++)
		{
			//处理$stucourses[$n]记录
			$course = explode(' ', $stucourses[$n]);
			if(count($course) != 3)
			{
				//echo $stucourses[$n];
        echo "您导入的文件格式不正确，课程成绩一栏应该填写为（课程名 课程类型 学分）。请重新导入！";
        exit;
      }
			else
			{	
        if(!preg_match("/^\d+(.\d+$|\d*$)/", trim($course[2])))
        {
          echo "您导入的文件格式不正确，课程成绩一栏中学分应该为数字。出错位置：（".trim($course[0])." ".trim($course[1])." ".trim($course[2])." "."）请重新导入！";
          exit;
        }
				$courses[$j] = array(trim($course[0]), trim($course[1]), trim($course[2]));
				$j++;
			}
		}

		//对待插入的课程成绩进行处理，$courses
		$waitsql = "insert into course(coursename,coursetype,coursepoint) values";
		$wtemp = $waitsql;
		if(count($courses) != 0)
		{
			foreach ($courses as $cour)//对于其中的每一条记录
			{
				$name = $cour[0];
				$type = $cour[1];
				$point = $cour[2];
				$sql = "select courseid from course where coursename = '$name' and 
				coursetype = '$type' and coursepoint = $point";
				$query= @mysql_query($sql) or die("查询课程表时出错！");//查询数据库是否存在改课程，如果没有
				$rows = mysql_fetch_array($query);
				if($rows['courseid'] == null)//如果对象不存在，说明数据库无课程
				{
					$waitsql = $waitsql."('$name','$type','$point'),";
				}
			}
			if($waitsql != $wtemp)
			{
				$waitsql= substr($waitsql, 0, -1);//将最后一个逗号去掉
				$query = @mysql_query($waitsql,$connect) or die("插入成绩表时失败！");//执行操作
			}
		}

    //接着插入学生成绩，这里需要校验
		//执行完毕，查询每个课程对应的课程courseid
		$idarray = array();
		if(count($courses) != 0)
		{
			$idlen = count($courses);
			for($id = 0;$id < $idlen;$id++)//对于其中的每一条记录
			{
				$name = $courses[$id][0];
				$type = $courses[$id][1];
				$point = $courses[$id][2];
				$idsql = "select courseid from course where coursename = '$name' and 
				coursetype = '$type' and coursepoint = $point";
				$query= @mysql_query($idsql) or die("从课程表中查询课程号失败！");//查询数据库是否存在改课程，如果没有
				$idrows = mysql_fetch_array($query);
				$idarray[$id] = $idrows['courseid'];	
			}
		}

		//导入学生的成绩信息
		$students= $arraystu;//获得多个学生的成绩信息，包括第一行，xh,xm等信息
		//得到数组的长度
		$len = count($arraystu,0);
		//fwrite($fp,$len);//写
		//一次最多插入多少条
		$resize = 200;
		for($i = 2; $i <= $len; $i = $i + $resize)//对每一条学生记录,这里 $i <= $len是因为php得到的数组从1开始
		{
			$sql = "insert ignore into score(studentid,courseid,year,coursescore) values";
			$temp = $sql;
			//对于其中的第$i,和第$i+$resize条记录
			for($j = $i;$j < $i + $resize && $j <= $len;$j++)
			{
				//得到学号
				$studentid = $students[$j][0];
        $studentname = $students[$j][1];
       // echo $studentname." dsfdsaf ".$studentid;
				//如果学号不为空
				if(!empty($studentid) && !empty($studentname) && preg_match("/^\d*$/", $studentid))		
        {
					//得到学生记录长度
					$stulen = count($students[$j]);
					//对该学生进行处理
					for($si = 2;$si<$stulen;$si++)
					{
						if(trim($students[$j][$si]) != "")//如果不为空，并且
						{
							$courseid = $idarray[$si-2];//顺序不变
							$score = trim($students[$j][$si]);
              if(!preg_match("/^\d+(.\d+$|\d*$)/", $score))
              {
                echo "导入成绩表时，成绩表中学生分数不合法，目前只支持百分制导入。出错位置=>（学号：".$studentid." 姓名：".$studentname."），请稍候重试！";
                exit;
              }
							$sql = $sql."('$studentid','$courseid','$year','$score'),";
						}
					}
				}
        else
        {
          echo "导入成绩表时，成绩表中学生学号或者姓名不合法。出错位置=>（学号：".$studentid." 姓名：".$studentname."），请稍候重试！";
          exit;
        }
			}
			//如果$sql 发生变化
			if($sql != $temp)
			{
				$sql= substr($sql, 0, -1);//将最后一个逗号去掉
				$file = fopen('whu-sql.txt', 'w');
				fwrite($file, $sql);
				fclose($file);
				$query = @mysql_query($sql,$connect) or die("插入学生记录失败，可能数据库中没有该学生基本信息，请由学院管理员录入后(学院信息管理菜单->新增学生信息菜单)，重新重试导入成绩！");//执行操作
			}
		}
		
    echo "导入学生成绩表成功！";
	}
	else
	{
    echo "文件上传中出错，请稍候重试！";
    exit;
	}

    flock($fp , LOCK_UN);    
  } 
  else 
  {
    echo "服务器正忙，请稍候重试！";
  }

  fclose($fp);
?>
